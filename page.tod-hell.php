<?php
session_start();
define("PATH_INFO", "");

include_once "include.functions.php";
include_once "include.maintenance-check.php";

$stage = (int) $_GET['stage'];

const STAGE_MINI = 112;
const STAGE_MAXI = 127;

const TOD_HELL_DIRECTORY = "/assets/images/tod-hell/2016-12/";

if ($stage < STAGE_MINI || $stage > STAGE_MAXI)
{
  header("location:/");exit;
}
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- common head -->
    <?php include "template.head.php"; ?>

    <!-- custom css -->
    <link rel="stylesheet" href="/assets/css/calculator.css?<?php echo filemtime('assets/css/calculator.css'); ?>">
    <link rel="stylesheet" href="/assets/css/fluff.css?<?php echo filemtime('assets/css/fluff.css'); ?>"> 

    <title>Tod Hell - Stage <?php echo $stage; ?></title>
  </head>
  <body>
    <div class="loader" style="display:none;"><img src="/assets/images/icon/black-caps.png" class="ncaps roll"></div>
    <header>
      <?php include "template.header-belt.php"; ?>
    </header>
    <div class="container-fluid" style="position:relative;overflow:hidden;">
      <?php include "template.notification.php"; ?>

      <h1 class="text-center">ToD Hell - Stage  <?php echo $stage; ?></h1>

      <?php include "template.tod-hell-stages.php"; ?>
      
      <div class="row">
        <div class="xs-hidden col-md-1"></div>
        <div class="col-xs-12 col-md-10">
          <div class="stage-pic">
            <img src="<?php echo TOD_HELL_DIRECTORY . "stage-" . $stage . ".png"; ?> " alt="" class="img-responsive">
          </div>
        </div>
        <div class="xs-hidden col-md-1"></div>
      </div>

      <?php include "template.tod-hell-stages.php"; ?>
    </div>
    <footer>
      <!-- common postload -->
      <?php include "template.postload.php"; ?>
    </footer>
  </body>
</html>