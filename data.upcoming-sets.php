<?php
$upcoming_sets = [
    [
        'start_date' => new DateTime('2018-05-13 14:00:00'),
        'end_date'   => new DateTime('2018-05-14 14:00:00'),
        'sets'       => ['15','251'],
    ],
    [
        'start_date' => new DateTime('2018-05-14 14:00:00'),
        'end_date'   => new DateTime('2018-05-15 14:00:00'),
        'sets'       => ['130','68'],
    ],
    [
        'start_date' => new DateTime('2018-05-15 14:00:00'),
        'end_date'   => new DateTime('2018-05-16 14:00:00'),
        'sets'       => ['248','99'],
    ],
    [
        'start_date' => new DateTime('2018-05-16 14:00:00'),
        'end_date'   => new DateTime('2018-05-17 14:00:00'),
        'sets'       => ['249','266'],
    ],
    [
        'start_date' => new DateTime('2018-05-17 14:00:00'),
        'end_date'   => new DateTime('2018-05-18 14:00:00'),
        'sets'       => ['152','275'],
    ],
    [
        'start_date' => new DateTime('2018-05-18 14:00:00'),
        'end_date'   => new DateTime('2018-05-19 14:00:00'),
        'sets'       => ['150','252'],
    ],
    [
        'start_date' => new DateTime('2018-05-19 14:00:00'),
        'end_date'   => new DateTime('2018-05-20 14:00:00'),
        'sets'       => ['16','159'],
    ],
    [
        'start_date' => new DateTime('2018-05-20 14:00:00'),
        'end_date'   => new DateTime('2018-05-21 14:00:00'),
        'sets'       => ['47','157'],
    ],
    [
        'start_date' => new DateTime('2018-05-21 14:00:00'),
        'end_date'   => new DateTime('2018-05-22 14:00:00'),
        'sets'       => ['56','65'],
    ],
    [
        'start_date' => new DateTime('2018-05-22 14:00:00'),
        'end_date'   => new DateTime('2018-05-23 14:00:00'),
        'sets'       => ['101','154'],
    ],
    [
        'start_date' => new DateTime('2018-05-23 14:00:00'),
        'end_date'   => new DateTime('2018-05-24 14:00:00'),
        'sets'       => ['151','113'],
    ],
    [
        'start_date' => new DateTime('2018-05-24 14:00:00'),
        'end_date'   => new DateTime('2018-05-25 14:00:00'),
        'sets'       => ['8','86'],
    ],
    [
        'start_date' => new DateTime('2018-05-25 14:00:00'),
        'end_date'   => new DateTime('2018-05-26 14:00:00'),
        'sets'       => ['50','83'],
    ],
    [
        'start_date' => new DateTime('2018-05-26 14:00:00'),
        'end_date'   => new DateTime('2018-05-27 14:00:00'),
        'sets'       => ['243','270'],
    ],
    [
        'start_date' => new DateTime('2018-05-27 14:00:00'),
        'end_date'   => new DateTime('2018-05-28 14:00:00'),
        'sets'       => ['36','160'],
    ],
    [
        'start_date' => new DateTime('2018-05-28 14:00:00'),
        'end_date'   => new DateTime('2018-05-29 14:00:00'),
        'sets'       => ['238','155'],
    ],
    [
        'start_date' => new DateTime('2018-05-29 14:00:00'),
        'end_date'   => new DateTime('2018-05-30 14:00:00'),
        'sets'       => ['153','271'],
    ],
    [
        'start_date' => new DateTime('2018-05-30 14:00:00'),
        'end_date'   => new DateTime('2018-05-31 14:00:00'),
        'sets'       => ['267','88'],
    ],
    [
        'start_date' => new DateTime('2018-05-31 14:00:00'),
        'end_date'   => new DateTime('2018-06-01 14:00:00'),
        'sets'       => ['268','250'],
    ],

    [
        'start_date' => new DateTime('2018-06-01 14:00:00'),
        'end_date'   => new DateTime('2018-06-02 14:00:00'),
        'sets'       => ['103','273'],
    ],
    [
        'start_date' => new DateTime('2018-06-02 14:00:00'),
        'end_date'   => new DateTime('2018-06-03 14:00:00'),
        'sets'       => ['128','247'],
    ],
    [
        'start_date' => new DateTime('2018-06-03 14:00:00'),
        'end_date'   => new DateTime('2018-06-04 14:00:00'),
        'sets'       => ['15','254'],
    ],
    [
        'start_date' => new DateTime('2018-06-04 14:00:00'),
        'end_date'   => new DateTime('2018-06-05 14:00:00'),
        'sets'       => ['5','253'],
    ],
    [
        'start_date' => new DateTime('2018-06-05 14:00:00'),
        'end_date'   => new DateTime('2018-06-06 14:00:00'),
        'sets'       => ['34','274'],
    ],
    [
        'start_date' => new DateTime('2018-06-06 14:00:00'),
        'end_date'   => new DateTime('2018-06-07 14:00:00'),
        'sets'       => ['31','158'],
    ],
    [
        'start_date' => new DateTime('2018-06-07 14:00:00'),
        'end_date'   => new DateTime('2018-06-08 14:00:00'),
        'sets'       => ['29','105'],
    ],
    [
        'start_date' => new DateTime('2018-06-08 14:00:00'),
        'end_date'   => new DateTime('2018-06-09 14:00:00'),
        'sets'       => ['27','71'],
    ],
    [
        'start_date' => new DateTime('2018-06-09 14:00:00'),
        'end_date'   => new DateTime('2018-06-10 14:00:00'),
        'sets'       => ['24','72'],
    ],
    [
        'start_date' => new DateTime('2018-06-10 14:00:00'),
        'end_date'   => new DateTime('2018-06-11 14:00:00'),
        'sets'       => ['239','107'],
    ],
    [
        'start_date' => new DateTime('2018-06-11 14:00:00'),
        'end_date'   => new DateTime('2018-06-12 14:00:00'),
        'sets'       => ['240','94'],
    ],
    [
        'start_date' => new DateTime('2018-06-12 14:00:00'),
        'end_date'   => new DateTime('2018-06-13 14:00:00'),
        'sets'       => ['241','156'],
    ],
    [
        'start_date' => new DateTime('2018-06-13 14:00:00'),
        'end_date'   => new DateTime('2018-06-14 14:00:00'),
        'sets'       => ['242','110'],
    ],
    [
        'start_date' => new DateTime('2018-06-14 14:00:00'),
        'end_date'   => new DateTime('2018-06-15 14:00:00'),
        'sets'       => ['269','161'],
    ],
    [
        'start_date' => new DateTime('2018-06-15 14:00:00'),
        'end_date'   => new DateTime('2018-06-16 14:00:00'),
        'sets'       => ['15','251'],
    ],
    [
        'start_date' => new DateTime('2018-06-16 14:00:00'),
        'end_date'   => new DateTime('2018-06-17 14:00:00'),
        'sets'       => ['130','68'],
    ],
    [
        'start_date' => new DateTime('2018-06-17 14:00:00'),
        'end_date'   => new DateTime('2018-06-18 14:00:00'),
        'sets'       => ['248','99'],
    ],
    [
        'start_date' => new DateTime('2018-06-18 14:00:00'),
        'end_date'   => new DateTime('2018-06-19 14:00:00'),
        'sets'       => ['249','266'],
    ],
    [
        'start_date' => new DateTime('2018-06-19 14:00:00'),
        'end_date'   => new DateTime('2018-06-20 14:00:00'),
        'sets'       => ['152','275'],
    ],
    [
        'start_date' => new DateTime('2018-06-20 14:00:00'),
        'end_date'   => new DateTime('2018-06-21 14:00:00'),
        'sets'       => ['150','252'],
    ],
];

// $upcoming_sets = [];


if ( !function_exists('filterActiveUpcomingSet') )
{
    function filterActiveUpcomingSet($arr)
    {
        return ( new DateTime() < $arr['end_date'] );
    }
}

$upcoming_sets = array_filter($upcoming_sets, 'filterActiveUpcomingSet');

// Clean JSON button

// Load edition mode
// = current json + mask of a new day
// 1 save button
// end edition

// use start date as ID ?
// all next-set images becomes selectable (radio like) + SS filter mandatory (without hero sets if possible)
// clicking on a set clone it into the selection
// clicking on save button update the JSON

// 