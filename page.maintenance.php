<?php
session_start();
define("PATH_INFO", "");

include_once "include.maintenance-check.php";

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- common head -->
    <?php include "template.head.php"; ?>

		<!-- custom js/css -->
		<style>
			html, body
			{
				height:100%;
			}

			body
			{
				background:url("/assets/images/bg/yekaterina-by-fizintine.jpg") no-repeat center center;
				-webkit-background-size: contain; /* pour anciens Chrome et Safari */
			  background-size: contain; /* version standardisée */
			}
			
			header
			{
				margin-bottom:15px;
				background-color:#000;
				color:#fff;
			}

		</style>

		<title>FWT Stats Calculator - Maintenance x.X</title>
  </head>
	<body>
		<header>
			<?php include "template.header-belt.php"; ?>
		</header>
		<div class="container-fluid text-center" >
			<?php if (MAINTENANCE) { ?>
			<div class="maintenance">
				<h1>Maintenance !</h1>
				<h3>The app is under maintenance and will be back soon&nbsp;!</h3>
				<h5>Meanwhile why not try your luck @ the coocoo machine ?</h5>
			</div>	
			<?php } else { ?>
			<div class="maintenance">
				<h1>Maintenance is over !</h1>
				<h3><a href="/">You can go back to calc ^^</a></h3>
			</div>
			<?php } ?>
		</div>
		<footer>
      <!-- common postload -->
      <?php include "template.postload.php"; ?>
    </footer>
	</body>
</html>