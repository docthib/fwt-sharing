<?php
session_start();
define("PATH_INFO", "");

include_once "include.maintenance-check.php";

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- common head -->
    <?php include "template.head.php"; ?>

    <!-- custom header meta -->
		<title>FWT Stats Calculator - Coocoo Machine</title>

		<!-- custom js/css -->
		<link rel="stylesheet" href="/assets/css/calculator.css">
		<link rel="stylesheet" href="/assets/css/fluff.css">

  </head>
	<body>
		<header>
			<?php include "template.header-belt.php"; ?>
		</header>
		<div class="cache">
			<img src="/assets/images/icon/black-coocoo-machine-opened.png" alt="">
			<img src="/assets/images/icon/black-caps.png" alt="">
			<img src="/assets/images/icon/noblesse-caps.png" alt="">
			<img src="/assets/images/icon/blue-caps.png" alt="">
			<img src="/assets/images/icon/red-caps.png" alt="">
			<img src="/assets/images/icon/pink-caps.png" alt="">
			<img src="/assets/images/icon/green-caps.png" alt="">
		</div>
		<div class="container">
			<?php // include "template.notification.php"; ?>

			<!-- <div class="wrap text-center">
				<img class="caps" src="assets/images/icon/black-caps.png" alt="">
				<img class="caps" src="assets/images/icon/black-caps.png" alt="">
				<img class="caps" src="assets/images/icon/black-caps.png" alt="">
				<img class="caps" src="assets/images/icon/black-caps.png" alt="">

				<img class="caps" src="assets/images/icon/noblesse-caps.png" alt="">
				<img class="caps" src="assets/images/icon/noblesse-caps.png" alt="">
				<img class="caps" src="assets/images/icon/noblesse-caps.png" alt="">
				<img class="caps" src="assets/images/icon/noblesse-caps.png" alt="">
			</div> -->
			<div class="machine">
				<img src="/assets/images/icon/black-coocoo-machine.png" class='coocoo' alt="">
				<!-- <div class="stats">
					Bullshit : <span class="bullshit">0</span><br />
					A : <span class="seta">0</span><br />
					S : <span class="sets">0</span><br />
					SS : <span class="setss">0</span><br />
					Noblesse : <span class="setnoblesse">0</span><br />
				</div>
				<button class="btn btn-primary">Magic !</button> -->
				<!-- <button class="btn btn-success">Trothon button</button> -->
				<div class="button"></div>
			</div>
			<div class="result">
				
			</div>
		</div>

		<script>
			$(document).ready(function()
			{
				
				var coocooOpened = "/assets/images/icon/black-coocoo-machine-opened.png";
				

				$(".machine button").removeAttr("disabled");

				$("body").on("click", ".machine .button", function() {
					var machine = $(".machine");
					$(".coocoo").attr("src", coocooOpened);

					// disable button
					$(this).hide();

					// remove old machine caps
					$(".result").html("");

					var pullDuration = 5000;
					for (i = 1 ; i <= 10 ; i++)
					{
						// var t /= pullDuration
						// c*t*t + b
						// var time = 1000 * (1 - (i * 10) / 100) + lastTime;
						var time = 100 * i;
						setTimeout("capsPull()", time);
						// lastTime = time;
					}

					time += pullDuration;
					setTimeout("endOfPull()", time);
				});
			});
			
			function endOfPull()
			{
				var coocooClosed = "/assets/images/icon/black-coocoo-machine.png";

				$(".coocoo").attr("src", coocooClosed);
				$(".machine .button").show();
				var allCaps = $(".machine .caps").attr("class", "caps").detach();
				allCaps.appendTo(".result");
			}

			function capsPull()
			{
				var globalChance = Math.floor((Math.random() * 100) + 1);
				var caps = "";

				if (globalChance > 10)
				{
					var rarity = new Array("green", "blue", "red", "pink");
					var rarityChance = Math.floor(Math.random() * 100);

					if (rarityChance <= 5) // 5%
					{
						rarityResult = 3;
					}
					else if (rarityChance <= 15) // 10%
					{
						rarityResult = 2;
					}
					else if (rarityChance <= 25) // 10%
					{
						rarityResult = 0;
					}
					else // 75%
					{
						rarityResult = 1;
					}

					caps   = "<img class='caps pull' src='/assets/images/icon/" + rarity[rarityResult] + "-caps.png' />";
					result = "bullshit";
				}
				else
				{
					var noblesseChance = Math.floor((Math.random() * 2) + 1);

					if (noblesseChance == 1) // 50%
					{
						caps   = "<img class='caps pull' src='/assets/images/icon/noblesse-caps.png' />";
						result = "setnoblesse";
					}
					else
					{
						caps = "<img class='caps pull' src='/assets/images/icon/black-caps.png' />";

						var gradeChance = Math.floor((Math.random() * 100) + 1);
						if (gradeChance <= 10) // 10%
						{
							result = "setss";
						}
						else if (gradeChance <= 40) // 30%
						{
							result = "sets";
						}
						else // 60%
						{
							result = "seta";
						}
					}
					
				}

				$(".machine").append(caps);
			}
		</script>
	</body>
</html>