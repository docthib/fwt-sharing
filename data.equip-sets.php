<?php
include_once "include.functions.php";

define("SETS_IMAGE_DIRECTORY", "/assets/images/sets/");
define("DEFAULT_SET_IMAGE", "default.png");
define("PULL_RATE_BROWN", 56.5);
define("PULL_RATE_B", PULL_RATE_BROWN + 27);
define("PULL_RATE_A", PULL_RATE_B + 10);
define("PULL_RATE_S", PULL_RATE_A + 4.5);
define("PULL_RATE_SS", PULL_RATE_S + 2);

// build val list here
function renderSets()
{
	global $sets;
	global $equip_slots;

	usort($sets, "sortByGradeThenTitle");

	$old_grade = 1;
	echo "<div class='sets-grade' data-grade='" . $old_grade . "'>";
	echo "<div class='grade-title'><span class='slot'>" . $equip_slots[ $old_grade ] . "</span></div>";
	echo "<div class='flex'>";
		foreach ($sets as $set) 
		{
			$set_data     = "";
			$effects_html = "";

			foreach ($set as $set_property => $set_property_value) 
			{
				if ($set_property == "effect2" || $set_property == "effect3")
				{
					foreach ($set[$set_property] as $effect) 
					{
						$effect_data = "";
						foreach ($effect as $effect_property => $effect_property_value) 
						{
							if ($effect_property == "stat")
							{
								$effect_property_value = tdf($effect_property_value);
							}

							$effect_data.= " data-" . $effect_property . "=\"" . $effect_property_value . "\"";
						}

						$effects_html .= "<span class='effect-stat " . $set_property . "'" . $effect_data . "></span>";
					}
				}
				elseif ($set_property == "effect2_label" || $set_property == "effect3_label")
				{
					$num_effet    = substr($set_property, 0, -6);
					$effects_html .= "<span class='effect " . $num_effet . "'>" . $set[$set_property] . "</span>";
				}
				else
				{
					$set_data .= " data-" . $set_property . "=\"" . $set_property_value . "\"";
				}
			}

			if ($set['grade'] != $old_grade)
			{
				$old_grade = $set['grade'];
				echo "</div>"; // end flex container
				echo "</div>"; // end grade
				echo "<div class='sets-grade' data-grade='" . $set['grade'] . "'>";
					echo "<div class='grade-title'><span class='slot'>" . $equip_slots[ $set['grade'] ] . "</span></div>";
					echo "<div class='flex'>";
			}
			
			// col-xs-12 col-sm-6 col-md-3 
			echo "<div class='set-info'" . $set_data . ">";
				echo "<span class='rank-" . $set['rank'] . "'></span>";
				echo "<span class='title'>" . $set['title'] . "</span>";
				echo "<span class='equips'>";
					echo "<img class='icon' data-stuff='" . $set['weapon'] . "' src='" . ICONS_DIRECTORY . $set['weapon'] . ".png' />";
					echo "<img class='icon' data-stuff='" . $set['armor'] . "' src='" . ICONS_DIRECTORY . $set['armor'] . ".png' />";
					echo "<img class='icon' data-stuff='" . $set['accessory'] . "' src='" . ICONS_DIRECTORY . $set['accessory'] . ".png' />";
				echo "</span>";

				echo $effects_html;
				echo "<span class='condition'>" . $set['condition_label'] . "</span>";
			echo "</div>";
		}
	echo "</div>"; // end flex container
	echo "</div>"; // end grade
}

function filterByValue ($array, $search, $value)
{
  if(is_array($array) && count($array)>0) 
  {
    foreach($array as $entry)
    {
      if ($entry[$search] == $value)
      {
        $newarray[] = $entry;
      }
    }
  }

	return $newarray;
} 

function sortBySetTitle($a,$b)
{
	return $a['title'] > $b['title'];
}


function sortByGradeThenTitle($a,$b)
{
	if ($a['grade'] == $b['grade'])
	{
	  return strcmp($a['title'], $b['title']);
	}

	return $a['grade'] - $b['grade'];
}

function searchForId($id, $array) 
{
   foreach ($array as $key => $val) 
   {
       if ($val['id'] == $id) 
       {
           return $key;
       }
   }
   return false;
}

$sets = 
[
	[
		"id"              => 1,
		"tags"            => "",
		"title"           => "Volley of Bullets",
		"grade"           => 1,
		"rank"            => "s",
		"weapon"          => "mace",
		"armor"           => "shield",
		"accessory"       => "ring",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2_label"   => "Increase Hit Rate by 600",
		"effect3_label"   => "Increase Critical damage by 6%",
		"condition_label" => "",
		"condition"       => "",
		"effect2"         => array(
				array("label" => "Increase Hit Rate by 600", "stat" => "Hit Rate", "value" => "600")
		),
		"effect3" => array(
				array("label" => "Increase Critical damage by 6%", "stat" => "Critical Damage%", "value" => "6")
		),
	],
	[
		"id"              => 2,
		"tags"            => "",
		"title"           => "Hot-blooded Fighter Boy",
		"grade"           => 1,
		"rank"            => "ss",
		"weapon"          => "bow",
		"armor"           => "boots",
		"accessory"       => "necklace",
		"weapon_image"    => "1-hot-blooded.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2_label"   => "Decrease all enemies' defense by 1200 within 1-tile area",
		"effect3_label"   => "Increase Critical Damage by 15% for 2 turns if there is one or more enemy within one tile of the wearer",
		"condition_label" => "",
		"condition"       => "",
		"effect2"         => array(
				array("label" => "Decrease all enemies' defense by 1200 within 1-tile area", "stat" => "Enemy Defense", "value" => "-1200", "condition" => "IG")
			),
		"effect3" => array(
				array("label" => "Increase Critical Damage by 15% for 2 turns if there is one or more enemy within one tile of the wearer", "stat" => "Critical Damage%", "value" => "15", "condition" => "IG")
			),
	],
	[
		"id"              => 3,
		"tags"            => "",
		"title"           => "I'm a Cat",
		"grade"           => 1,
		"rank"            => "a",
		"weapon"          => "mace",
		"armor"           => "boots",
		"accessory"       => "necklace",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2_label"   => "Increase Attack by 340",
		"effect3_label"   => "Increase Dodge by 600",
		"condition_label" => "Balance-type character only",
		"condition"       => "type=Balance",
		"effect2"         => array(
				array("label" => "Increase Attack by 340", "stat" => "Attack", "value" => "340")
			),
		"effect3" => array(
				array("label" => "Increase Dodge by 600", "stat" => "Dodge", "value" => "600")
			),
	],
	[
		"id"              => 4,
		"tags"            => "",
		"title"           => "Transform! Magic Girl!",
		"grade"           => 1,
		"rank"            => "s",
		"weapon"          => "bow",
		"armor"           => "armor",
		"accessory"       => "ring",
		"weapon_image"    => "1-magical.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2_label"   => "Increase HP by 2270",
		"effect3_label"   => "Increase Final Skill Power by 1460",
		"condition_label" => "Activated on 3rd turn after the beginning of combat",
		"condition"       => "IG",
		"effect2"         => array(
			array("label" => "Increase HP by 2270", "stat" => "HP", "value" => "2270", "condition" => "IG")
		),
		"effect3" => array(
			array("label" => "Increase Final Skill Power by 1460", "stat" => "FSP", "value" => "1460", "condition" => "IG")
		),
	],
	[
		"id"              => 5,
		"tags"            => "",
		"title"           => "Death Blow",
		"grade"           => 1,
		"rank"            => "ss",
		"weapon"          => "sword",
		"armor"           => "boots",
		"accessory"       => "brooch",
		"weapon_image"    => "1-db.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2_label"   => "Increase Dodge by 900",
		"effect3_label"   => "Increase Critical damage by 16%",
		"condition_label" => "Attack-type character only",
		"condition"       => "type=Attack",
		"effect2"         => array(
			array("label" => "Increase Dodge by 900", "stat" => "Dodge", "value" => "900")
		),
		"effect3" => array(
			array("label" => "Increase Critical damage by 16%", "stat" => "Critical Damage%", "value" => "16")
		),
	],
	[
		"id"              => 6,
		"tags"            => "",
		"title"           => "Here comes the gnome!",
		"grade"           => 1,
		"rank"            => "a",
		"weapon"          => "mace",
		"armor"           => "boots",
		"accessory"       => "necklace",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2_label"   => "Increase 250 Defense to all allies",
		"effect3_label"   => "Increases attack damage by 1.5% to all allies",
		"condition_label" => "",
		"condition"       => "",
		"effect2"         => array(
				array("label" => "Increase 250 Defense to all allies", "stat" => "Defense", "value" => "250", "aura")
			),
		"effect3" => array(
				array("label" => "Increases attack damage by 1.5% to all allies", "stat" => "Attack%", "value" => "1.5", "aura")
			),
	],
	[
		"id"              => 7,
		"tags"            => "",
		"title"           => "Super Ranger: Yellow",
		"grade"           => 1,
		"rank"            => "s",
		"weapon"          => "sword",
		"armor"           => "armor",
		"accessory"       => "necklace",
		"weapon_image"    => "1-yellow-ranger.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2_label"   => "Increase Dodge by 700",
		"effect3_label"   => "Increase Mastery by 1000",
		"condition_label" => "",
		"condition"       => "",
		"effect2"         => array(
			array("label" => "Increase Dodge by 700", "stat" => "Dodge", "value" => "700")
		),
		"effect3" => array(
			array("label" => "Increase Mastery by 1000", "stat" => "Mastery", "value" => "1000")
		),
	],
	[
		"id"              => 8,
		"tags"            => "",
		"title"           => "Slime King's Dignity",
		"grade"           => 1,
		"rank"            => "ss",
		"weapon"          => "mace",
		"armor"           => "shield",
		"accessory"       => "brooch",
		"weapon_image"    => "1-sk.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2_label"   => "Increase Defense by 1000",
		"effect3_label"   => "Decrease damage dealt by all Attack-type enemies by 25%",
		"condition_label" => "",
		"condition"       => "",
		"effect2"         => array(
			array("label" => "Increase Defense by 1000", "stat" => "Defense", "value" => "1000")
		),
		"effect3" => array(
			array("label" => "Decrease damage dealt by all Attack-type enemies by 25%", "stat" => "Damage Taken%", "value" => "25", "condition" => "IG")
		),
	],
	[
		"id"              => 9,
		"tags"            => "",
		"title"           => "Girdas Sandstorm",
		"grade"           => 1,
		"rank"            => "a",
		"weapon"          => "sword",
		"armor"           => "boots",
		"accessory"       => "ring",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2_label"   => "Increase Hit Rate by 600",
		"effect3_label"   => "Thornbush Accessible",
		"condition_label" => "",
		"condition"       => "",
		"effect2"         => array(
			array("label" => "Increase Hit Rate by 600", "stat" => "Hit Rate", "value" => "600")
		),
		"effect3" => array(
			array("label" => "Thornbush Accessible", "stat" => "Terrain Thornbush", "value" => "1")
		),
	],
	[
		"id"              => 10,
		"tags"            => "",
		"title"           => "Lista Blizzard",
		"grade"           => 1,
		"rank"            => "a",
		"weapon"          => "mace",
		"armor"           => "armor",
		"accessory"       => "necklace",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2_label"   => "Increase Hit Rate by 600",
		"effect3_label"   => "Glacier Accessible",
		"condition_label" => "",
		"condition"       => "",
		"effect2" => array(
			array("label" => "Increase Hit Rate by 600", "stat" => "Hit Rate", "value" => "600")
		),
		"effect3" => array(
			array("label" => "Glacier Accessible", "stat" => "Terrain Glacier", "value" => "1")
		),
	],
	[
		"id" => 11,
		"tags"            => "",
		"title"           => "Cold-Hearted Sniper",
		"grade"           => 1,
		"rank"            => "s",
		"weapon"          => "bow",
		"armor"           => "boots",
		"accessory"       => "ring",
		"weapon_image"    => "1-cold-sniper.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2_label"   => "Increase Attack by 440",
		"effect3_label"   => "Increase Attack by 4.5%",
		"condition_label" => "Attack-type character only",
		"condition"       => "type=Attack",
		"effect2"         => array(
				array("label" => "Increase Attack by 440", "stat" => "Attack", "value" => "440")
			),
		"effect3" => array(
				array("label" => "Increase Attack by 4.5%", "stat" => "Attack%", "value" => "4.5")
			),
	],
	[
		"id"              => 12,
		"tags"            => "",
		"title"           => "Battlefield Healer",
		"grade"           => 1,
		"rank"            => "a",
		"weapon"          => "mace",
		"armor"           => "armor",
		"accessory"       => "necklace",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"condition_label" => "Support-type character only",
		"condition"       => "type=Support",
		"effect2_label"   => "Increase Defense by 1000",
		"effect3_label"   => "Increase the Final Skill Power by 820",
		"effect2"         => array(
				array("label" => "Increase Defense by 1000", "stat" => "Defense", "value" => "1000")
			),
		"effect3" => array(
				array("label" => "Increase the Final Skill Power by 820", "stat" => "FSP", "value" => "820")
			),
	],
	[
		"id"              => 13,
		"tags"            => "",
		"title"           => "Ketarh Apprentice",
		"grade"           => 1,
		"rank"            => "a",
		"weapon"          => "mace",
		"armor"           => "armor",
		"accessory"       => "brooch",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2_label"   => "Increase MP by 24",
		"effect3_label"   => "Recover MP by 8 every turn",
		"condition_label" => "Area attack-type character only",
		"condition"       => "type=Area",
		"effect2"         => array(
				array("label" => "Increase MP by 24", "stat" => "MP", "value" => "24")
			),
		"effect3" => array(
				array("label" => "Recover MP by 8 every turn", "stat" => "MP Recovery", "value" => "8")
			),
	],
	[
		"id"              => 14,
		"tags"            => "",
		"title"           => "Witch's Tool",
		"grade"           => 1,
		"rank"            => "a",
		"weapon"          => "mace",
		"armor"           => "armor",
		"accessory"       => "brooch",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2_label"   => "Recover MP by 8 every turn",
		"effect3_label"   => "Increase the Final Skill Power by 950",
		"condition_label" => "Area attack-type character only",
		"condition"       => "type=Area",
		"effect2"         => array(
				array("label" => "Recover MP by 8 every turn", "stat" => "MP Recovery", "value" => "8")
			),
		"effect3" => array(
				array("label" => "Increase the Final Skill Power by 950", "stat" => "FSP", "value" => "950")
			),
	],
	[
		"id"              => 15,
		"tags"            => "",
		"title"           => "Rebel Army Supply",
		"grade"           => 1,
		"rank"            => "ss",
		"weapon"          => "sword",
		"armor"           => "shield",
		"accessory"       => "brooch",
		"weapon_image"    => "1-rebel.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2_label"   => "Increase HP by 6%",
		"effect3_label"   => "Increase Attack by 8%",
		"condition_label" => "",
		"condition"       => "",
		"effect2"         => array(
				array("label" => "Increase HP by 6%", "stat" => "HP%", "value" => "6")
			),
		"effect3" => array(
				array("label" => "Increase Attack by 8%", "stat" => "Attack%", "value" => "8")
			),
	],
	[
		"id"              => 16,
		"tags"            => "",
		"title"           => "Crusader's Path",
		"grade"           => 1,
		"rank"            => "ss",
		"weapon"          => "sword",
		"armor"           => "shield",
		"accessory"       => "necklace",
		"weapon_image"    => "1-crusa.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2_label"   => "Increase HP by 2904",
		"effect3_label"   => "Recover HP by 5% every turn and increase Defense by 600",
		"condition_label" => "",
		"condition"       => "",
		"effect2"         => array(
				array("label" => "Increase HP by 2904", "stat" => "HP", "value" => "2904")
			),
		"effect3" => array(
				array("label" => "Recover HP by 5% every turn", "stat" => "HP Recovery%", "value" => "5"),
				array("label" => "Increase Defense by 600", "stat" => "Defense", "value" => "600")
			),
	],
	[
		"id"              => 17,
		"tags"            => "",
		"title"           => "Born Assassin",
		"grade"           => 1,
		"rank"            => "s",
		"weapon"          => "sword",
		"armor"           => "armor",
		"accessory"       => "ring",
		"weapon_image"    => "1-assassin.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2_label"   => "Increase Attack by 634",
		"effect3_label"   => "Increase Crit Rate by 800",
		"condition_label" => "",
		"condition"       => "",
		"effect2"         => array(
				array("label" => "Increase Attack by 634", "stat" => "Attack", "value" => "634")
			),
		"effect3" => array(
				array("label" => "Increase Crit Rate by 800", "stat" => "Crit Rate", "value" => "800")
			),
	],
	[
		"id"              => 18,
		"tags"            => "",
		"title"           => "Savage Token",
		"grade"           => 1,
		"rank"            => "s",
		"weapon"          => "sword",
		"armor"           => "armor",
		"accessory"       => "brooch",
		"weapon_image"    => "1-savage.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2_label"   => "Increase HP by 1573",
		"effect3_label"   => "Increase Advantage strategy power by 6%",
		"condition_label" => "",
		"condition"       => "",
		"effect2"         => array(
				array("label" => "Increase HP by 1573", "stat" => "HP", "value" => "1573")
			),
		"effect3" => array(
				array("label" => "Increase Advantage strategy power by 6%", "stat" => "BonusAdvantage%", "value" => "6")
			),
	],
	[
		"id"              => 19,
		"tags"            => "",
		"title"           => "Skilled Martial Artist",
		"grade"           => 2,
		"rank"            => "s",
		"weapon"          => "mace",
		"armor"           => "boots",
		"accessory"       => "ring",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2_label"   => "Increase Defense by 1000",
		"effect3_label"   => "Increase Crit Rate by 1000",
		"condition_label" => "Activated if 1 or more enemies are within 1 cell around the wearer",
		"condition"       => "IG",
		"effect2"         => array(
				array("label" => "Increase Defense by 1000", "stat" => "Defense", "value" => "1000", "condition" => "IG")
			),
		"effect3" => array(
				array("label" => "Increase Crit Rate by 1000", "stat" => "Crit Rate", "value" => "1000", "condition" => "IG")
			),
	],
	[
		"id"              => 20,
		"tags"            => "",
		"title"           => "Intelligence War",
		"grade"           => 2,
		"rank"            => "ss",
		"weapon"          => "bow",
		"armor"           => "armor",
		"accessory"       => "brooch",
		"weapon_image"    => "2-intelligence.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
				array("label" => "Increase Direction Strategy Power by 9%", "stat" => "BonusDirection%", "value" => "9")
			),
		"effect2_label" => "Increase Direction Strategy Power by 9%",
		"effect3" => array(
				array("label" => "Increase Movement by 2", "stat" => "Movement", "value" => "2")
			),
		"effect3_label" => "Increase Movement by 2",
		"condition_label" => "Activated when user has 85% HP or more",
		"condition" => "IG",
	],
	[
		"id" => 21,
		"tags"            => "",
		"title" => "Nephthys Adventurer",
		"grade" => 2,
		"rank" => "a",
		"weapon" => "sword",
		"armor" => "boots",
		"accessory" => "ring",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
				array("label" => "Increase HP by 1460 for all allies", "stat" => "HP", "value" => "1460", "condition" => "IG", "aura")
			),
		"effect2_label" => "Increase HP by 1460 for all allies",
		"effect3" => array(
				array("label" => "Increase Attack by 350 for all allies", "stat" => "Attack", "value" => "350", "condition" => "IG", "aura")
			),
		"effect3_label" => "Increase Attack by 350 for all allies",
		"condition_label" => "Activaten only in the Nephthys Dungeon",
		"condition" => "IG",
	],
	[
		"id" => 22,
		"tags"            => "",
		"title" => "Shadow Death Band",
		"grade" => 2,
		"rank" => "a",
		"weapon" => "mace",
		"armor" => "armor",
		"accessory" => "brooch",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
				array("label" => "Increase MP by 23 for all allies", "stat" => "MP", "value" => "23", "condition" => "IG", "aura")
			),
		"effect2_label" => "Increase MP by 23 for all allies",
		"effect3" => array(
				array("label" => "Increase all allies Final Skill Power by 1300", "stat" => "FSP", "value" => "1300", "condition" => "IG", "aura")
			),
		"effect3_label" => "Increase all allies Final Skill Power by 1300",
		"condition_label" => "Activated only in Tower of Dawn",
		"condition" => "IG",
	],
	[
		"id"              => 23,
		"tags"            => "",
		"title"           => "Extra Curriculum",
		"grade"           => 2,
		"rank"            => "s",
		"weapon"          => "sword",
		"armor"           => "boots",
		"accessory"       => "ring",
		"weapon_image"    => "2-ec.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2_label"   => "Recovers 7% of HP every turn",
		"effect3_label"   => "Increase Attack by 12%",
		"condition_label" => "Activated on 3rd turn after the beginning of combat",
		"condition"       => "IG",
		"effect2"         => array(
			array("label" => "Recovers 7% of HP every turn", "stat" => "HP Recovery%", "value" => "7", "condition" => "IG")
		),
		"effect3" => array(
			array("label" => "Increase Attack by 12%", "stat" => "Attack%", "value" => "12", "condition" => "IG")
		),
	],
	[
		"id" => 24,
		"tags"            => "",
		"title" => "Taunting Busybody",
		"grade" => 2,
		"rank" => "ss",
		"weapon" => "sword",
		"armor" => "boots",
		"accessory" => "necklace",
		"weapon_image"    => "2-busybody.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
				array("label" => "Increase chance of getting attacked by 50%", "stat" => "Aggro%", "value" => "50"),
				array("label" => "Increase HP by 3510", "stat" => "HP", "value" => "3510")
			),
		"effect2_label" => "Increase chance of getting attacked by 50% and increase HP by 3510",
		"effect3" => array(
				array("label" => "Increase Defense by 800 (Max 2400)", "stat" => "Defense", "value" => "800"),
				array("label" => "Increase Defense by 400 per turn, upon attacking enemies (Max 1600)", "stat" => "Defense", "value" => "400", "max" => "1600", "condition" => "IG")
			),
		"effect3_label" => "Increase Defense by 800 and increase Defense by 400 per turn, upon attacking enemies (Max 1600)",
		"condition_label" => "",
		"condition" => "",
	],
	[
		"id" => 25,
		"tags"            => "",
		"title" => "Root Vine Walk",
		"grade" => 2,
		"rank" => "a",
		"weapon" => "mace",
		"armor" => "boots",
		"accessory" => "brooch",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
				array("label" => "Increases Attack by 6%", "stat" => "Attack%", "value" => "6")
			),
		"effect2_label" => "Increases Attack by 6%",
		"effect3" => array(
				array("label" => "Increases Movement by 2", "stat" => "Movement", "value" => "2")
			),
		"effect3_label" => "Increases Movement by 2",
		"condition_label" => "Activates when user Movement has decreased",
		"condition" => "IG",
	],
	[
		"id" => 26,
		"tags"            => "",
		"title" => "Unyielding Champion",
		"grade" => 2,
		"rank" => "s",
		"weapon" => "mace",
		"armor" => "armor",
		"accessory" => "necklace",
		"weapon_image"    => "2-unyielding.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
				array("label" => "Reflects 20% of all damage taken", "stat" => "Reflect%", "value" => "20")
			),
		"effect2_label" => "Reflects 20% of all damage taken",
		"effect3" => array(
				array("label" => "Increases attack damage by 10%", "stat" => "Attack%", "value" => "10")
			),
		"effect3_label" => "Increases attack damage by 10%",
		"condition_label" => "Activates when owner has 1 or more buffs from other active skills",
		"condition" => "IG",
	],
	[
		"id" => 27,
		"tags"            => "",
		"title" => "The Lab Demon",
		"grade" => 2,
		"rank" => "ss",
		"weapon" => "mace",
		"armor" => "shield",
		"accessory" => "brooch",
		"weapon_image"    => "2-lab.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
				array("label" => "Increase attack damage by 10%", "stat" => "Attack%", "value" => "10")
			),
		"effect2_label" => "Increase attack damage by 10%",
		"effect3" => array(
				array("label" => "Increase Defense by 800", "stat" => "Defense", "value" => "800"),
				array("label" => "At the beginning of every turn, creates 1 potion randomly within 2 tiles from the owner", "stat" => "Potion", "value" => "1")
			),
		"effect3_label" => "Increase Defense by 800. At the beginning of every turn, creates 1 potion randomly within 2 tiles from the owner",
		"condition_label" => "",
	],
	[
		"id" => 28,
		"tags"            => "",
		"title" => "Material Collector",
		"grade" => 2,
		"rank" => "a",
		"weapon" => "sword",
		"armor" => "boots",
		"accessory" => "ring",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
				array("label" => "Increase Attack by 1500", "stat" => "Attack", "value" => "1500")
			),
		"effect2_label" => "Increase Attack by 1500",
		"effect3" => array(
				array("label" => "Increase HP by 5620", "stat" => "HP", "value" => "5620")
			),
		"effect3_label" => "Increase HP by 5620",
		"condition_label" => "Activated only in the Nephthys Dungeon",
		"condition" => "IG",
	],
	[
		"id" => 29,
		"tags"            => "",
		"title" => "Cave Chaser",
		"grade" => 2,
		"rank" => "ss",
		"weapon" => "sword",
		"armor" => "boots",
		"accessory" => "ring",
		"weapon_image"    => "2-cave.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
				array("label" => "Increase Movement by 1", "stat" => "Movement", "value" => "1")
			),
		"effect2_label" => "Increase Movement by 1",
		"effect3" => array(
				array("label" => "Acquire Lava Terrain bonus", "stat" => "Terrain Lava", "value" => "1"),
				array("label" => "Increase Attack by 1460", "stat" => "Attack", "value" => "1460")
			),
		"effect3_label" => "Lava Accessible and increase Attack by 1460",
		"condition_label" => "",
	],
	[
		"id" => 30,
		"tags"            => "",
		"title" => "Pacifist",
		"grade" => 2,
		"rank" => "a",
		"weapon" => "mace",
		"armor" => "armor",
		"accessory" => "necklace",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
				array("label" => "Increase MP by 32", "stat" => "MP", "value" => "32")
			),
		"effect2_label" => "Increase MP by 32",
		"effect3" => array(
				array("label" => "Increase the Final Skill Power by 1550", "stat" => "FSP", "value" => "1550")
			),
		"effect3_label" => "Increase the Final Skill Power by 1550",
		"condition_label" => "Support-type character only",
		"condition" => "type=Support",
	],
	[
		"id" => 31,
		"tags"            => "",
		"title" => "Lumen Hymn",
		"grade" => 2,
		"rank" => "ss",
		"weapon" => "mace",
		"armor" => "armor",
		"accessory" => "necklace",
		"weapon_image"    => "2-lumen.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
				array("label" => "Increase MP by 32", "stat" => "MP", "value" => "32")
			),
		"effect2_label" => "Increase MP by 32",
		"effect3" => array(
			array("label" => "Increase Movement by 1", "stat" => "Movement", "value" => "1"), 
			array("label" => "Increase Attack by 1370", "stat" => "Attack", "value" => "1370")
		),
		"effect3_label" => "Increase Movement by 1 and Attack by 1370",
		"condition_label" => "",
	],
	[
		"id"              => 32,
		"tags"            => "",
		"title"           => "Legion Banner",
		"grade"           => 2,
		"rank"            => "s",
		"weapon"          => "sword",
		"armor"           => "shield",
		"accessory"       => "necklace",
		"weapon_image"    => "2-legion.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2_label"   => "Increase Aggro by 21%",
		"effect3_label"   => "Increase HP by 1870 for all allies",
		"condition_label" => "Defense-type character only",
		"condition"       => "type=Defense",
		"effect2"         => array(
			array("label" => "Increase Aggro by 21%", "stat" => "Aggro%", "value" => "21")
		),
		"effect3" => array(
			array("label" => "Increase HP by 1870 for all allies", "stat" => "HP", "value" => "1870", "aura")
		),
	],
	[
		"id" => 33,
		"tags"            => "",
		"title" => "Ancient Witch's Curse",
		"grade" => 2,
		"rank" => "a",
		"weapon" => "mace",
		"armor" => "armor",
		"accessory" => "brooch",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
				array("label" => "Increase MP by 23", "stat" => "MP", "value" => "23")
			),
		"effect2_label" => "Increase MP by 23",
		"effect3" => array(
				array("label" => "Increase the Final Skill Power by 1780", "stat" => "FSP", "value" => "1780")
			),
		"effect3_label" => "Increase the Final Skill Power by 1780",
		"condition_label" => "Area attack-type character only",
		"condition" => "type=Area",
	],
	[
		"id" => 34,
		"tags"            => "",
		"title" => "Frenzy Token",
		"grade" => 2,
		"rank" => "ss",
		"weapon" => "sword",
		"armor" => "shield",
		"accessory" => "brooch",
		"weapon_image"    => "2-frenzy.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
				array("label" => "Increase Attack by 10%", "stat" => "Attack%", "value" => "10")
			),
		"effect2_label" => "Increase Attack by 10%",
		"effect3" => array(
			array("label" => "Increase Movement by 1", "stat" => "Movement", "value" => "1"),
			array("label" => "Increase Dodge by 850", "stat" => "Dodge", "value" => "850")
		),
		"effect3_label" => "Increase Movement by 1 and Dodge by 850",
		"condition_label" => "",
	],
	[
		"id"              => 35,
		"tags"            => "",
		"title"           => "Super Ranger: Red",
		"grade"           => 2,
		"rank"            => "s",
		"weapon"          => "bow",
		"armor"           => "armor",
		"accessory"       => "brooch",
		"weapon_image"    => "2-red-ranger.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2_label"   => "Increase Attack by 200 every turn (up to 1200)",
		"effect3_label"   => "Increase the Final Skill Power by 550 every turn (up to 3300)",
		"condition_label" => "Activates the Set effects at the beginning of combat",
		"condition"       => "IG",
		"effect2"         => array(
			array("label" => "Increase Attack by 200 every turn (up to 1200)", "stat" => "Attack", "value" => "200", "max" => "1200", "condition" => "IG")
		),
		"effect3" => array(
			array("label" => "Increase the Final Skill Power by 550 every turn (up to 3300)", "stat" => "FSP", "value" => "550", "max" => "3300", "condition" => "IG")
		),
	],
	[
		"id" => 36,
		"tags"            => "",
		"title" => "Dasomier Battle Suit",
		"grade" => 2,
		"rank" => "ss",
		"weapon" => "bow",
		"armor" => "armor",
		"accessory" => "brooch",
		"weapon_image"    => "2-daso.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
				array("label" => "Increase Attack by 60%", "stat" => "Attack%", "value" => "60", "condition" => "IG")
			),
		"effect2_label" => "Increase Attack by 60%",
		"effect3" => array(
				array("label" => "Increase Hit Rate by 2000", "stat" => "Hit Rate", "value" => "2000", "condition" => "IG")
			),
		"effect3_label" => "Increase Hit Rate by 2000",
		"condition_label" => "Activated for 2 turns at the beginning of combat",
		"condition" => "IG",
	],
	[
		"id" => 37,
		"tags"            => "",
		"title" => "Serial Killer",
		"grade" => 2,
		"rank" => "a",
		"weapon" => "sword",
		"armor" => "boots",
		"accessory" => "brooch",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
				array("label" => "Increase Attack by 601", "stat" => "Attack", "value" => "601")
			),
		"effect2_label" => "Increase Attack by 601",
		"effect3" => array(
				array("label" => "Increase Crit Rate by 800", "stat" => "Crit Rate", "value" => "800")
			),
		"effect3_label" => "Increase Crit Rate by 800",
		"condition_label" => "Scissors-type character only",
		"condition" => "element=Scissors",
	],
	[
		"id" => 38,
		"tags"            => "heroonly",
		"title" => "Heir of Flesh and Blood",
		"grade" => 3,
		"rank" => "s",
		"weapon" => "sword",
		"armor" => "armor",
		"accessory" => "ring",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
				array("label" => "Increase all allies Defense by 450 in the dungeon", "stat" => "Defense", "value" => "450", "aura")
			),
		"effect2_label" => "Increase all allies Defense by 450 in the dungeon",
		"effect3" => array(
				array("label" => "Increase all allies HP by 1500 in the dungeon", "stat" => "HP", "value" => "1500", "aura")
			),
		"effect3_label" => "Increase all allies HP by 1500 in the dungeon",
		"condition_label" => "Exclusive to Raskreia (Noblesse Hero)",
		"condition" => "name=Raskreia",
	],
	[
		"id" => 39,
		"tags"            => "heroonly",
		"title" => "Loyard's Star",
		"grade" => 3,
		"rank" => "s",
		"weapon" => "sword",
		"armor" => "armor",
		"accessory" => "necklace",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
				array("label" => "Increase Attack by 1564", "stat" => "Attack", "value" => "1564")
			),
		"effect2_label" => "Increase Attack by 1564",
		"effect3" => array(
				array("label" => "Increase Dodge by 900", "stat" => "Dodge", "value" => "900")
			),
		"effect3_label" => "Increase Dodge by 900",
		"condition_label" => "Exclusive to Seira (Noblesse Hero)",
		"condition" => "name=Seira",
	],
	[
		"id" => 40,
		"tags"            => "heroonly",
		"title" => "Vivid Claw Mark",
		"grade" => 3,
		"rank" => "s",
		"weapon" => "mace",
		"armor" => "shield",
		"accessory" => "necklace",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
				array("label" => "Increase Crit Rate by 900", "stat" => "Crit Rate", "value" => "900")
			),
		"effect2_label" => "Increase Crit Rate by 900",
		"effect3" => array(
				array("label" => "Recovers HP by 5% every turn", "stat" => "HP Recovery%", "value" => "5")
			),
		"effect3_label" => "Recovers HP by 5% every turn",
		"condition_label" => "Exclusive to Muzaka (Noblesse Hero)",
		"condition" => "name=Muzaka",
	],
	[
		"id" => 41,
		"tags"            => "heroonly",
		"title" => "Mad Scientist",
		"grade" => 3,
		"rank" => "s",
		"weapon" => "sword",
		"armor" => "boots",
		"accessory" => "necklace",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
				array("label" => "Increase Hit Rate by 900", "stat" => "Hit Rate", "value" => "900")
			),
		"effect2_label" => "Increase Hit Rate by 900",
		"effect3" => array(
				array("label" => "Increase Attack by 700 every turn (up to 2100)", "stat" => "Attack", "value" => "700", "max" => "2100", "condition" => "IG")
			),
		"effect3_label" => "Increase Attack by 700 every turn (up to 2100)",
		"condition_label" => "Exclusive to Frankenstein (Noblesse Hero)",
		"condition" => "name=Frankenstein",
	],
	[
		"id" => 42,
		"tags"            => "",
		"title" => "God's Judgement",
		"grade" => 3,
		"rank" => "s",
		"weapon" => "mace",
		"armor" => "boots",
		"accessory" => "brooch",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
				array("label" => "Increase Defense by 1500 for 1 turn when a skill is used on allies", "stat" => "Defense", "value" => "1500")
			),
		"effect2_label" => "Increase Defense by 1500 for 1 turn when a skill is used on allies",
		"effect3" => array(
				array("label" => "Increase HP by 6100", "stat" => "HP", "value" => "6100")
			),
		"effect3_label" => "Increase HP by 6100",
		"condition_label" => "Support-type character only",
		"condition" => "type=Support",
	],
	[
		"id" => 43,
		"tags"            => "",
		"title" => "Arrogant Pureblood",
		"grade" => 3,
		"rank" => "ss",
		"weapon" => "sword",
		"armor" => "armor",
		"accessory" => "ring",
		"weapon_image"    => "3-apb.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
				array("label" => "Increase Crit Rate by 1100", "stat" => "Crit Rate", "value" => "1100", "condition" => "IG")
			),
		"effect2_label" => "Increase Crit Rate by 1100",
		"effect3" => array(
				array("label" => "Revive with 30% of HP if struck with a deathly attack", "stat" => "Revive", "value" => "30")
			),
		"effect3_label" => "Revive with 30% of HP if struck with a deathly attack",
		"condition_label" => "Activated for 2 turns at the beginning of combat",
		"condition" => "IG",
	],
	[
		"id" => 44,
		"tags"            => "",
		"title" => "Sweet and Scary Chocolate Warrior",
		"grade" => 3,
		"rank" => "s",
		"weapon" => "mace",
		"armor" => "shield",
		"accessory" => "brooch",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
				array("label" => "Increase Crit Rate by 760", "stat" => "Crit Rate", "value" => "760")
			),
		"effect2_label" => "Increase Crit Rate by 760",
		"effect3" => array(
				array("label" => "Increase Attack by 1154", "stat" => "Attack", "value" => "1154")
			),
		"effect3_label" => "Increase Attack by 1154",
		"condition_label" => "",
	],
	[
		"id" => 45,
		"tags"            => "",
		"title" => "Mad Guitarist",
		"grade" => 3,
		"rank" => "a",
		"weapon" => "bow",
		"armor" => "armor",
		"accessory" => "ring",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
				array("label" => "Increase Crit Rate by 250 (Max 1000)", "stat" => "Crit Rate", "value" => "250", "max" => "1000", "condition" => "IG")
			),
		"effect2_label" => "Increase Crit Rate by 250 (Max 1000)",
		"effect3" => array(
				array("label" => "Increase Attack by 400 (Max 1600)", "stat" => "Attack", "value" => "400", "max" => "1600", "condition" => "IG")
			),
		"effect3_label" => "Increase Attack by 400 (Max 1600)",
		"condition_label" => "Activated each time the character is attacked",
		"condition" => "IG",
	],
	[
		"id" => 46,
		"tags"            => "",
		"title" => "Unbeatable Warlord",
		"grade" => 3,
		"rank" => "s",
		"weapon" => "mace",
		"armor" => "boots",
		"accessory" => "necklace",
		"weapon_image"    => "3-warlord.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
				array("label" => "Increase Dodge by 1100", "stat" => "Dodge", "value" => "1100", "condition" => "IG")
			),
		"effect2_label" => "Increase Dodge by 1100",
		"effect3" => array(
				array("label" => "Increase Attack by 3129", "stat" => "Attack", "value" => "3129", "condition" => "IG")
			),
		"effect3_label" => "Increase Attack by 3129",
		"condition_label" => "Activated when there are 2 or more enemies within one tile of the wearer",
		"condition" => "IG",
	],
	[
		"id" => 47,
		"tags"            => "",
		"title" => "Strategy Support System",
		"grade" => 3,
		"rank" => "ss",
		"weapon" => "bow",
		"armor" => "shield",
		"accessory" => "ring",
		"weapon_image"    => "3-strategy.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
				array("label" => "Increase all allies Attack by 1080", "stat" => "Attack", "value" => "1080", "aura")
			),
		"effect2_label" => "Increase all allies Attack by 1080",
		"effect3" => array(
				array("label" => "Increase Defense by 1000", "stat" => "Defense", "value" => "1000"),
				array("label" => "Recovers 12% of HP when using a skill on allies", "stat" => "Instant HP Recovery%", "value" => "12")
			),
		"effect3_label" => "Increase Defense by 1000 and recovers 12% of HP when using a skill on allies",
		"condition_label" => "",
		"condition" => "",
	],
	[
		"id" => 48,
		"tags"            => "",
		"title" => "Shadow Corps",
		"grade" => 3,
		"rank" => "a",
		"weapon" => "sword",
		"armor" => "armor",
		"accessory" => "ring",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
				array("label" => "Increases Crit Rate by 200 to all allies", "stat" => "Crit Rate", "value" => "200", "aura")
			),
		"effect2_label" => "Increases Crit Rate by 200 to all allies",
		"effect3" => array(
				array("label" => "Boosts Critical Damage by 2% to all allies", "stat" => "Critical Damage%", "value" => "2", "aura")
			),
		"effect3_label" => "Boosts Critical Damage by 2% to all allies",
		"condition_label" => "",
	],
	[
		"id" => 49,
		"tags"            => "",
		"title" => "Outer Space!",
		"grade" => 3,
		"rank" => "s",
		"weapon" => "mace",
		"armor" => "armor",
		"accessory" => "necklace",
		"weapon_image"    => "3-outer-space.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
				array("label" => "Increases Counter Damage by 50%", "stat" => "Counter Damage", "value" => "5000", "condition" => "IG")
			),
		"effect2_label" => "Increases Counter Damage by 50%",
		"effect3" => array(
				array("label" => "Increases Counter Rate by 8000", "stat" => "Counter Rate", "value" => "8000", "condition" => "IG")
			),
		"effect3_label" => "Increases Counter Rate by 8000",
		"condition_label" => "Activates when the owner's HP falls below 75%",
		"condition" => "IG",
	],
	[
		"id" => 50,
		"tags"            => "",
		"title" => "High Human Guardian",
		"grade" => 3,
		"rank" => "ss",
		"weapon" => "mace",
		"armor" => "shield",
		"accessory" => "brooch",
		"weapon_image"    => "3-hhg.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Restores 6% HP every turn", "stat" => "HP Recovery%", "value" => "6"),
			array("label" => "Increase HP by 6040", "stat" => "HP", "value" => "6040")
		),
		"effect2_label" => "Restores 6% HP every turn and increase HP by 6040",
		"effect3" => array(
			array("label" => "The owner distributes 50% of all the attack damage toward allies within 2 tile"	, "stat" => "DistributeDamage%", "value" => "50", "condition" => "IG"), 
			array("label" => "Increase all nearby allies' Defense by 1000 for one turn"	, "stat" => "Defense", "value" => "1000", "condition" => "IG", "aura")			
		),
		"effect3_label" => "The owner distributes 50% of all the attack damage toward allies within 2 tile, and increase all nearby allies' Defense by 1000 for one turn",
		"condition_label" => "Defense-type character only",
		"condition" => "type=Defense",
	],
	[
		"id" => 51,
		"tags"            => "",
		"title" => "Nephthys Conqueror",
		"grade" => 3,
		"rank" => "a",
		"weapon" => "mace",
		"armor" => "boots",
		"accessory" => "ring",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
				array("label" => "Increase Attack by 3000", "stat" => "Attack", "value" => "3000", "condition" => "IG")
			),
		"effect2_label" => "Increase Attack by 3000",
		"effect3" => array(
				array("label" => "Increase Crit Rate by 2500", "stat" => "Crit Rate", "value" => "2500", "condition" => "IG")
			),
		"effect3_label" => "Increase Crit Rate by 2500",
		"condition_label" => "Activated only in the Nephthys Dungeon",
		"condition" => "IG",
	],
	[
		"id" => 52,
		"tags"            => "",
		"title" => "Wild Chaser",
		"grade" => 3,
		"rank" => "a",
		"weapon" => "bow",
		"armor" => "shield",
		"accessory" => "ring",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
				array("label" => "Water Accessible", "stat" => "Terrain Water", "value" => "1")
			),
		"effect2_label" => "Water Accessible",
		"effect3" => array(
				array("label" => "Swamp Accessible", "stat" => "Terrain Swamp", "value" => "1")
			),
		"effect3_label" => "Swamp Accessible",
		"condition_label" => "",
	],
	[
		"id" => 53,
		"tags"            => "",
		"title" => "Super Ranger: Pink",
		"grade" => 3,
		"rank" => "s",
		"weapon" => "bow",
		"armor" => "armor",
		"accessory" => "brooch",
		"weapon_image"    => "3-pink-ranger.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
				array("label" => "Increase Hit Rate by 220 every turn (up to 1100)", "stat" => "Hit Rate", "value" => "220", "max" => "1100", "condition" => "IG")
			),
		"effect2_label" => "Increase Hit Rate by 220 every turn (up to 1100)",
		"effect3" => array(
				array("label" => "Increase Attack by 782 every turn (up to 3910)", "stat" => "Attack", "value" => "782", "max" => "3910", "condition" => "IG")
			),
		"effect3_label" => "Increase Attack by 782 every turn (up to 3910)",
		"condition_label" => "Activates the Set effects at the beginning of combat",
		"condition" => "IG",
	],
	[
		"id" => 54,
		"tags"            => "",
		"title" => "Royal Family Assassin",
		"grade" => 3,
		"rank" => "a",
		"weapon" => "sword",
		"armor" => "boots",
		"accessory" => "necklace",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
				array("label" => "Increase Crit Rate by 700", "stat" => "Crit Rate", "value" => "700")
			),
		"effect2_label" => "Increase Crit Rate by 700",
		"effect3" => array(
				array("label" => "Increase Attack by 1043", "stat" => "Attack", "value" => "1043")
			),
		"effect3_label" => "Increase Attack by 1043",
		"condition_label" => "Attack-type character only",
		"condition" => "type=Attack",
	],
	[
		"id" => 55,
		"tags"            => "",
		"title" => "Vulcan Essence",
		"grade" => 3,
		"rank" => "a",
		"weapon" => "mace",
		"armor" => "armor",
		"accessory" => "ring",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
				array("label" => "Increase MP by 28", "stat" => "MP", "value" => "28")
			),
		"effect2_label" => "Increase MP by 28",
		"effect3" => array(
				array("label" => "Increase the Final Skill Power by 2610", "stat" => "FSP", "value" => "2610")
			),
		"effect3_label" => "Increase the Final Skill Power by 2610",
		"condition_label" => "Area attack-type character only",
		"condition" => "type=Area",
	],
	[
		"id" => 56,
		"tags"            => "",
		"title" => "Beast Rain Project Prototype",
		"grade" => 3,
		"rank" => "ss",
		"weapon" => "bow",
		"armor" => "armor",
		"accessory" => "brooch",
		"weapon_image"    => "3-br.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
				array("label" => "Increase Attack by 60%", "stat" => "Attack%", "value" => "60", "condition" => "IG")
			),
		"effect2_label" => "Increase Attack by 60%",
		"effect3" => array(
				array("label" => "Increase Hit Rate by 1500", "stat" => "Hit Rate", "value" => "1500", "condition" => "IG")
			),
		"effect3_label" => "Increase Hit Rate by 1500",
		"condition_label" => "Activated for 3 turns at the beginning of combat",
		"condition" => "IG",
	],
	[
		"id" => 57,
		"tags"            => "",
		"title" => "Tiger Claw Tribe Guardian",
		"grade" => 3,
		"rank" => "a",
		"weapon" => "mace",
		"armor" => "boots",
		"accessory" => "necklace",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
				array("label" => "Increase Defense by 1500", "stat" => "Defense", "value" => "1500", "condition" => "IG")
			),
		"effect2_label" => "Increase Defense by 1500",
		"effect3" => array(
				array("label" => "Increase Dodge by 1500", "stat" => "Dodge", "value" => "1500", "condition" => "IG")
			),
		"effect3_label" => "Increase Dodge by 1500",
		"condition_label" => "Activated when HP is more than 50%",
		"condition" => "IG"
	],
	[
		"id" => 58,
		"tags"            => "",
		"title" => "Rebel Army Commander",
		"grade" => 3,
		"rank" => "ss",
		"weapon" => "sword",
		"armor" => "boots",
		"accessory" => "brooch",
		"weapon_image"    => "3-rebel.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
				array("label" => "Increase all allies HP by 4600", "stat" => "HP", "value" => "4600", "condition" => "IG", "aura")
			),
		"effect2_label" => "Increase all allies HP by 4600",
		"effect3" => array(
				array("label" => "Increase all allies Attack by 1185", "stat" => "Attack", "value" => "1185", "condition" => "IG", "aura")
			),
		"effect3_label" => "Increase all allies Attack by 1185",
		"condition_label" => "Activated when 2 or more allies are alive",
		"condition" => "IG"
	],
	[
		"id" => 59,
		"tags"            => "",
		"title" => "Wilderness Desperado",
		"grade" => 3,
		"rank" => "s",
		"weapon" => "bow",
		"armor" => "boots",
		"accessory" => "necklace",
		"weapon_image"    => "3-desperado.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
				array("label" => "Increase Attack by 50%", "stat" => "Attack%", "value" => "50", "condition" => "IG")
			),
		"effect2_label" => "Increase Attack by 50%",
		"effect3" => array(
				array("label" => "Increase Dodge by 2500", "stat" => "Dodge", "value" => "2500", "condition" => "IG")
			),
		"effect3_label" => "Increase Dodge by 2500",
		"condition_label" => "Activated if only you are left standing",
		"condition" => "IG"
	],
	[
		"id" => 60,
		"tags"            => "",
		"title" => "Black Crescent",
		"grade" => 3,
		"rank" => "s",
		"weapon" => "sword",
		"armor" => "boots",
		"accessory" => "ring",
		"weapon_image"    => "3-crescent.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
				array("label" => "Increase Attack by 1044", "stat" => "Attack", "value" => "1044")
			),
		"effect2_label" => "Increase Attack by 1044",
		"effect3" => array(
				array("label" => "Increase Advantage strategy power by 7%", "stat" => "BonusAdvantage%", "value" => "7")
			),
		"effect3_label" => "Increase Advantage strategy power by 7%",
		"condition_label" => "Attack-type character only",
		"condition" => "type=Attack"
	],
	[
		"id" => 61,
		"tags"            => "",
		"title" => "Austere Noble",
		"grade" => 4,
		"rank" => "s",
		"weapon" => "bow",
		"armor" => "shield",
		"accessory" => "brooch",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Water terrain accessible", "stat" => "Terrain Water", "value" => "1")
		),
		"effect2_label" => "Water terrain accessible",
		"effect3" => array(
			array("label" => "Increase Attack by 2400", "stat" => "Attack", "value" => "2400")
		),
		"effect3_label" => "Increase Attack by 2400",
		"condition_label" => ""
	],
	[
		"id" => 62,
		"tags"            => "",
		"title" => "Commander of the Central Knights",
		"grade" => 4,
		"rank" => "ss",
		"weapon" => "sword",
		"armor" => "shield",
		"accessory" => "necklace",
		"weapon_image"    => "4-commander.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Increase Defense by 1100 for all allies", "stat" => "Defense", "value" => "1100", "aura")
		),
		"effect2_label" => "Increase Defense by 1100 for all allies",
		"effect3" => array(
			array("label" => "Increase Attack by 2500 for all allies in 2 cells around the wearer", "stat" => "Attack", "value" => "2500", "aura")
		),
		"effect3_label" => "Increase Attack by 2500 for all allies in 2 cells around the wearer",
		"condition_label" => ""
	],
	[
		"id" => 63,
		"tags"            => "",
		"title" => "Star Song",
		"grade" => 4,
		"rank" => "a",
		"weapon" => "bow",
		"armor" => "boots",
		"accessory" => "brooch",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Recovers 9 MP every turn", "stat" => "MP Recovery", "value" => "9")
		),
		"effect2_label" => "Recovers 9 MP every turn",
		"effect3" => array(
			array("label" => "Increase Defense by 1500 for 1 turn each time a skill is used on allies", "stat" => "Defense", "value" => "1500")
		),
		"effect3_label" => "Increase Defense by 1500 for 1 turn each time a skill is used on allies",
		"condition_label" => "Support-type character only",
		"condition" => "type=Support"
	],
	[
		"id" => 64,
		"tags"            => "",
		"title" => "Dragon Slayer",
		"grade" => 4,
		"rank" => "s",
		"weapon" => "sword",
		"armor" => "shield",
		"accessory" => "ring",
		"weapon_image"    => "4-dragon.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Acquire Lava Terrain bonus", "stat" => "Terrain Lava", "value" => "1")
		),
		"effect2_label" => "Acquire Lava Terrain bonus",
		"effect3" => array(
			array("label" => "Increase Attack by 2870", "stat" => "Attack", "value" => "2870")
		),
		"effect3_label" => "Increase Attack by 2870",
		"condition_label" => "Scissors-type character only",
		"condition" => "element=Scissors"
	],
	[
		"id" => 65,
		"tags"            => "",
		"title" => "Defender of the Earth",
		"grade" => 4,
		"rank" => "ss",
		"weapon" => "mace",
		"armor" => "shield",
		"accessory" => "brooch",
		"weapon_image"    => "4-dote.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Increase HP by 18000", "stat" => "HP", "value" => "18000", "condition" => "IG")
		),
		"effect2_label" => "Increase HP by 18000",
		"effect3" => array(
			array("label" => "Increase Defense by 2050", "stat" => "Defense", "value" => "2050", "condition" => "IG")
		),
		"effect3_label" => "Increase Defense by 2050",
		"condition_label" => "Activated if there are 2 or more allies alive",
		"condition" => "IG"
	],
	[
		"id" => 66,
		"tags"            => "",
		"title" => "Ninja Attack",
		"grade" => 4,
		"rank" => "a",
		"weapon" => "bow",
		"armor" => "boots",
		"accessory" => "ring",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Increases Dodge by 1000", "stat" => "Dodge", "value" => "1000", "condition" => "IG")
		),
		"effect2_label" => "Increases Dodge by 1000",
		"effect3" => array(
			array("label" => "Increases Movement by 1", "stat" => "Movement", "value" => "1", "condition" => "IG")
		),
		"effect3_label" => "Increases Movement by 1",
		"condition_label" => "Activated when wearer's defense is below 40%",
		"condition" => "IG"
	],
	[
		"id" => 67,
		"tags"            => "",
		"title" => "Sunset Sheriff",
		"grade" => 4,
		"rank" => "s",
		"weapon" => "bow",
		"armor" => "boots",
		"accessory" => "brooch",
		"weapon_image"    => "4-sheriff.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Increases Attack damage by 3%. The value can be up to 12%", "stat" => "Attack%", "value" => "3", "max" => "12", "condition" => "IG")
		),
		"effect2_label" => "Increases Attack damage by 3%. The value can be up to 12%",
		"effect3" => array(
			array("label" => "Increases Critical Damage by 3%. The value can be up tp 12%", "stat" => "Critical Damage%", "value" => "3", "max" => "12", "condition" => "IG")
		),
		"effect3_label" => "Increases Critical Damage by 3%. The value can be up tp 12%",
		"condition_label" => "Activates on owner's skill use on an enemy",
		"condition" => "IG"
	],
	[
		"id" => 68,
		"tags"            => "",
		"title" => "Tower of Dawn Guardian",
		"grade" => 4,
		"rank" => "ss",
		"weapon" => "mace",
		"armor" => "armor",
		"accessory" => "brooch",
		"weapon_image"    => "4-tod.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Increases Hit Rate by 1200", "stat" => "Hit Rate", "value" => "1200")
		),
		"effect2_label" => "Increases Hit Rate by 1200",
		"effect3" => array(
			array("label" => "Increases Critical Damage by 24%", "stat" => "Critical Damage%", "value" => "24")
		),
		"effect3_label" => "Increases Critical Damage by 24%",
		"condition_label" => "",
		"condition" => "IG"
	],
	[
		"id" => 69,
		"tags"            => "",
		"title" => "Dimension Conqueror",
		"grade" => 4,
		"rank" => "a",
		"weapon" => "sword",
		"armor" => "boots",
		"accessory" => "ring",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Increase HP by 7137", "stat" => "HP", "value" => "7137", "condition" => "IG")
		),
		"effect2_label" => "Increase HP by 7137",
		"effect3" => array(
			array("label" => "Increase HP by 4520 for all allies", "stat" => "HP", "value" => "4520", "condition" => "IG", "aura")
		),
		"effect3_label" => "Increase HP by 4520 for all allies",
		"condition_label" => "Activated only in Dimensional Breakthrough",
		"condition" => "IG"
	],
	[
		"id" => 71,
		"tags"            => "",
		"title" => "Pathfinder",
		"grade" => 4,
		"rank" => "ss",
		"weapon" => "bow",
		"armor" => "boots",
		"accessory" => "ring",
		"weapon_image"    => "4-pf.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Increase Movement by 1", "stat" => "Movement", "value" => "1")
		),
		"effect2_label" => "Increase Movement by 1",
		"effect3" => array(
			array("label" => "Increase HP by 19040", "stat" => "HP", "value" => "19040")
		),
		"effect3_label" => "Increase HP by 19040",
		"condition_label" => ""
	],
	[
		"id" => 72,
		"tags"            => "",
		"title" => "Ghost Step",
		"grade" => 4,
		"rank" => "ss",
		"weapon" => "sword",
		"armor" => "armor",
		"accessory" => "brooch",
		"weapon_image"    => "4-gs.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Increase Movement by 4", "stat" => "Movement", "value" => "4", "condition" => "IG")
		),
		"effect2_label" => "Increase Movement by 4",
		"effect3" => array(
			array("label" => "Increase Dodge by 8000", "stat" => "Dodge", "value" => "8000", "condition" => "IG")
		),
		"effect3_label" => "Increase Dodge by 8000",
		"condition_label" => "Activated for 2 turns at the beginning of combat",
		"condition" => "IG"
	],
	[
		"id" => 73,
		"tags"            => "",
		"title" => "Ancestor's Voice",
		"grade" => 4,
		"rank" => "s",
		"weapon" => "mace",
		"armor" => "armor",
		"accessory" => "necklace",
		"weapon_image"    => "4-ancestor.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Increase Attack by 2428", "stat" => "Attack", "value" => "2428", "condition" => "IG")
		),
		"effect2_label" => "Increase Attack by 2428",
		"effect3" => array(
			array("label" => "Increase the Final Skill Power by 5010", "stat" => "FSP", "value" => "5010", "condition" => "IG")
		),
		"effect3_label" => "Increase the Final Skill Power by 5010",
		"condition_label" => "Activated when mana falls below 65%",
		"condition" => "IG"
	],
	[
		"id" => 74,
		"tags"            => "",
		"title" => "Ketarh's Secret",
		"grade" => 4,
		"rank" => "a",
		"weapon" => "mace",
		"armor" => "armor",
		"accessory" => "brooch",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Recover MP by 8 every turn", "stat" => "MP Recovery", "value" => "8")
		),
		"effect2_label" => "Recover MP by 8 every turn",
		"effect3" => array(
			array("label" => "Increase the Final Skill Power by 3820", "stat" => "FSP", "value" => "3820")
		),
		"effect3_label" => "Increase the Final Skill Power by 3820",
		"condition_label" => "Area attack-type character only",
		"condition" => "type=Area"
	],
	[
		"id" => 75,
		"tags"            => "",
		"title" => "New Elian Kingdom Cavalry",
		"grade" => 4,
		"rank" => "a",
		"weapon" => "mace",
		"armor" => "armor",
		"accessory" => "ring",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Increase Attack by 1324", "stat" => "Attack", "value" => "1324")
		),
		"effect2_label" => "Increase Attack by 1324",
		"effect3" => array(
			array("label" => "Increase Hit Rate by 800", "stat" => "Hit Rate", "value" => "800")
		),
		"effect3_label" => "Increase Hit Rate by 800",
		"condition_label" => ""
	],
	[
		"id" => 76,
		"tags"            => "",
		"title" => "Crusader's Light",
		"grade" => 4,
		"rank" => "s",
		"weapon" => "sword",
		"armor" => "armor",
		"accessory" => "brooch",
		"weapon_image"    => "4-crusader.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Increase Aggro by 31%", "stat" => "Aggro%", "value" => "31")
		),
		"effect2_label" => "Increase Aggro by 31%",
		"effect3" => array(
			array("label" => "Increase HP by 3010 for all allies", "stat" => "HP", "value" => "3010", "aura")
		),
		"effect3_label" => "Increase HP by 3010 for all allies",
		"condition_label" => "Defense-type character only",
		"condition" => "type=Defense"
	],
	[
		"id" => 77,
		"tags"            => "",
		"title" => "Super Ranger: Black",
		"grade" => 4,
		"rank" => "a",
		"weapon" => "sword",
		"armor" => "armor",
		"accessory" => "brooch",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Increase Defense by 1200", "stat" => "Defense", "value" => "1200")
		),
		"effect2_label" => "Increase Defense by 1200",
		"effect3" => array(
			array("label" => "Increase the Final Skill Power by 3580", "stat" => "FSP", "value" => "3580")
		),
		"effect3_label" => "Increase the Final Skill Power by 3580",
		"condition_label" => "Rock-type character only",
		"condition" => "element=Rock"
	],
	[
		"id" => 78,
		"tags"            => "",
		"title" => "Persona's Trick",
		"grade" => 4,
		"rank" => "a",
		"weapon" => "sword",
		"armor" => "boots",
		"accessory" => "brooch",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Increase Attack by 7%", "stat" => "Attack%", "value" => "7")
		),
		"effect2_label" => "Increase Attack by 7%",
		"effect3" => array(
			array("label" => "Increase Crit Rate by 900", "stat" => "Crit Rate", "value" => "900")
		),
		"effect3_label" => "Increase Crit Rate by 900",
		"condition_label" => "Attack-type character only",
		"condition" => "type=Attack"
	],
	[
		"id" => 79,
		"tags"            => "",
		"title" => "Short in Height, Tall in Heart",
		"grade" => 5,
		"rank" => "s",
		"weapon" => "mace",
		"armor" => "armor",
		"accessory" => "brooch",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Increase Crit Rate by 1100", "stat" => "Crit Rate", "value" => "1100")
		),
		"effect2_label" => "Increase Crit Rate by 1100",
		"effect3" => array(
			array("label" => "Increase Attack by 3210", "stat" => "Attack", "value" => "3210")
		),
		"effect3_label" => "Increase Attack by 3210",
		"condition_label" => "Paper-type character only",
		"condition" => "element=Paper"
	],
	[
		"id" => 80,
		"tags"            => "",
		"title" => "Difference between You and Me",
		"grade" => 5,
		"rank" => "ss",
		"weapon" => "bow",
		"armor" => "shield",
		"accessory" => "ring",
		"weapon_image"    => "5-difference.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Increases Property Strategy power by 10%", "stat" => "BonusAdvantage", "value" => "10")
		),
		"effect2_label" => "Increases Property Strategy power by 10%",
		"effect3" => array(
			array("label" => "Decreases Critical Damage from enemy attacks by 20%", "stat" => "Critical Damage Taken%", "value" => "20")
		),
		"effect3_label" => "Decreases Critical Damage from enemy attacks by 20%",
		"condition_label" => "Activated when the wearer's HP is at least 75%",
		"condition" => "IG"
	],
	[
		"id" => 81,
		"tags"            => "",
		"title" => "Friday the 13th",
		"grade" => 5,
		"rank" => "a",
		"weapon" => "sword",
		"armor" => "boots",
		"accessory" => "necklace",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Increase Critical Damage by 5% (Max 30%)", "stat" => "Critical Damage%", "value" => "5", "max" => "30", "condition" => "IG")
		),
		"effect2_label" => "Increase Critical Damage by 5% (Max 30%)",
		"effect3" => array(
			array("label" => "Recovers HP by 5%", "stat" => "Instant HP Recovery%", "value" => "5", "condition" => "IG")
		),
		"effect3_label" => "Recovers HP by 5%",
		"condition_label" => "Activates on enemy kill",
		"condition" => "IG"
	],
	[
		"id" => 82,
		"tags"            => "",
		"title" => "Shadow Ruler",
		"grade" => 5,
		"rank" => "s",
		"weapon" => "mace",
		"armor" => "shield",
		"accessory" => "brooch",
		"weapon_image"    => "5-shadow.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Increase Defense by 2000 for all allies", "stat" => "Defense", "value" => "2000", "condition" => "IG", "aura")
		),
		"effect2_label" => "Increase Defense by 2000 for all allies",
		"effect3" => array(
			array("label" => "Increase Attack by 2870 for all allies", "stat" => "Attack", "value" => "2870", "condition" => "IG", "aura")
		),
		"effect3_label" => "Increase Attack by 2870 for all allies",
		"condition_label" => "Activated only in Tower of Dawn",
		"condition" => "IG"
	],
	[
		"id" => 83,
		"tags"            => "",
		"title" => "Klein's Secret Notebook",
		"grade" => 5,
		"rank" => "ss",
		"weapon" => "mace",
		"armor" => "shield",
		"accessory" => "necklace",
		"weapon_image"    => "5-klein.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Increase Attack by 5992", "stat" => "Attack", "value" => "5992")
		),
		"effect2_label" => "Increase Attack by 5992",
		"effect3" => array(
			array("label" => "Skill MP cost -20%", "stat" => "MP Cost%", "value" => "-20")
		),
		"effect3_label" => "Skill MP cost -20%",
		"condition_label" => "Area attack-type character only",
		"condition" => "type=Area"
	],
	[
		"id" => 84,
		"tags"            => "",
		"title" => "Mantra of Awakening",
		"grade" => 5,
		"rank" => "a",
		"weapon" => "sword",
		"armor" => "armor",
		"accessory" => "ring",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Increase MP by 5 (up to 50)", "stat" => "MP", "value" => "5", "max" => "50", "condition" => "IG")
		),
		"effect2_label" => "Increase MP by 5 (up to 50)",
		"effect3" => array(
			array("label" => "Increase Attack by 900 (up to 9000)", "stat" => "Attack", "value" => "900", "max" => "9000", "condition" => "IG")
		),
		"effect3_label" => "Increase Attack by 900 (up to 9000)",
		"condition_label" => "Activates on enemy kill",
		"condition" => "IG"
	],
	[
		"id" => 85,
		"tags"            => "",
		"title" => "Sun Flash",
		"grade" => 5,
		"rank" => "s",
		"weapon" => "mace",
		"armor" => "shield",
		"accessory" => "necklace",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Increase Attack by 2% per enemy in the skill AoE", "stat" => "EnemyInAoEAttack%", "value" => "2")
		),
		"effect2_label" => "Increase Attack by 2% per enemy in the skill AoE",
		"effect3" => array(
			array("label" => "Increase Attack by 4% per enemy in the skill AoE", "stat" => "EnemyInAoEAttack%", "value" => "4")
		),
		"effect3_label" => "Increase Attack by 4% per enemy in the skill AoE",
		"condition_label" => "Area-attack type character only",
		"condition" => "type=Area"
	],
	[
		"id" => 86,
		"tags"            => "",
		"title" => "Omniscient and Omnipotent",
		"grade" => 5,
		"rank" => "ss",
		"weapon" => "bow",
		"armor" => "armor",
		"accessory" => "brooch",
		"weapon_image"    => "5-oao.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Increases Attack damage by 15%", "stat" => "Attack%", "value" => "15")
		),
		"effect2_label" => "Increases Attack damage by 15%",
		"effect3" => array(
			array("label" => "At beginning of every turn, removes 2 debuff on self", "stat" => "RemoveDebuff", "value" => "2")
		),
		"effect3_label" => "At beginning of every turn, removes 2 debuff on self",
		"condition_label" => ""
	],
	[
		"id"              => 88,
		"tags"            => "",
		"title"           => "God of Massacre",
		"grade"           => 5,
		"rank"            => "ss",
		"weapon"          => "sword",
		"armor"           => "shield",
		"accessory"       => "necklace",
		"weapon_image"    => "5-gom.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2_label"   => "Increase Attack by 7190",
		"effect3_label"   => "Increase Crit Rate by 1300 and Critical Damage by 18%",
		"condition_label" => "Attack-type character only",
		"condition"       => "type=Attack",
		"effect2"         => array(
			array("label" => "Increase Attack by 7190", "stat" => "Attack", "value" => "7190"),
		),
		"effect3"         => array(
			array("label" => "Increase Crit Rate by 1300", "stat" => "Crit Rate", "value" => "1300"),
			array("label" => "Increase Critical Damage by 18%", "stat" => "Critical Damage%", "value" => "18"),
		),
	],
	[
		"id" => 89,
		"tags"            => "",
		"title" => "Revelation of the Great Universe",
		"grade" => 5,
		"rank" => "a",
		"weapon" => "bow",
		"armor" => "shield",
		"accessory" => "brooch",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Recover MP by 7 every turn", "stat" => "MP Recovery", "value" => "7")
		),
		"effect2_label" => "Recover MP by 7 every turn",
		"effect3" => array(
			array("label" => "Increase Attack by 2379", "stat" => "Attack", "value" => "2379")
		),
		"effect3_label" => "Increase Attack by 2379",
		"condition_label" => "Area attack-type character only",
		"condition" => "type=Area"
	],
	[
		"id" => 90,
		"tags"            => "",
		"title" => "Hellish Flame's Guide",
		"grade" => 5,
		"rank" => "a",
		"weapon" => "mace",
		"armor" => "armor",
		"accessory" => "ring",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Recover MP by 12 every turn", "stat" => "MP Recovery", "value" => "12", "condition" => "IG")
		),
		"effect2_label" => "Recover MP by 12 every turn",
		"effect3" => array(
			array("label" => "Increase the Final Skill Power by 5823", "stat" => "FSP", "value" => "5823", "condition" => "IG")
		),
		"effect3_label" => "Increase the Final Skill Power by 5823",
		"condition_label" => "Activated when the user's HP falls below 75%",
		"condition" => "IG"
	],
	[
		"id" => 91,
		"tags"            => "",
		"title" => "Sione's Ominous Prophecy",
		"grade" => 5,
		"rank" => "a",
		"weapon" => "mace",
		"armor" => "armor",
		"accessory" => "necklace",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Increase Hit Rate by 900", "stat" => "Hit Rate", "value" => "900")
		),
		"effect2_label" => "Increase Hit Rate by 900",
		"effect3" => array(
			array("label" => "Increase Crit Rate by 1100", "stat" => "Crit Rate", "value" => "1100")
		),
		"effect3_label" => "Increase Crit Rate by 1100",
		"condition_label" => ""
	],
	[
		"id" => 92,
		"tags"            => "",
		"title" => "White Knight's Oath",
		"grade" => 5,
		"rank" => "a",
		"weapon" => "sword",
		"armor" => "armor",
		"accessory" => "ring",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Increase aggro by 31%", "stat" => "Aggro%", "value" => "31")
		),
		"effect2_label" => "Increase aggro by 31%",
		"effect3" => array(
			array("label" => "Íncrease HP by 4016 for all allies", "stat" => "HP", "value" => "4016", "aura")
		),
		"effect3_label" => "Íncrease HP by 4016 for all allies",
		"condition_label" => "Defense-type character only",
		"condition" => "type=Defense"
	],
	[
		"id" => 93,
		"tags"            => "",
		"title" => "Legacy of the High Humans",
		"grade" => 5,
		"rank" => "s",
		"weapon" => "mace",
		"armor" => "boots",
		"accessory" => "ring",
		"weapon_image"    => "5-ectoplasm.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Increase Attack by 719 every turn (up to 5033)", "stat" => "Attack", "value" => "719", "max" => "5033", "condition" => "IG")
		),
		"effect2_label" => "Increase Attack by 719 every turn (up to 5033)",
		"effect3" => array(
			array("label" => "Increase the Final Skill Power by 1055 every turn (up to 7385)", "stat" => "FSP", "value" => "1055", "max" => "7385", "condition" => "IG")
		),
		"effect3_label" => "Increase the Final Skill Power by 1055 every turn (up to 7385)",
		"condition_label" => "Activates the set effect at the beginning of combat",
		"condition" => "IG"
	],
	[
		"id" => 94,
		"tags"            => "",
		"title" => "Eldrika's Trinity",
		"grade" => 5,
		"rank" => "ss",
		"weapon" => "mace",
		"armor" => "boots",
		"accessory" => "brooch",
		"weapon_image"    => "5-eldrika.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Recover HP by 5% for all allies every turn", "stat" => "HP Recovery%", "value" => "5", "aura")
		),
		"effect2_label" => "Recover HP by 5% for all allies every turn",
		"effect3" => array(
			array("label" => "Increase HP by 32056", "stat" => "HP", "value" => "32056")
		),
		"effect3_label" => "Increase HP by 32056",
		"condition_label" => "Defense-type character only",
		"condition" => "type=Defense"
	],
	[
		"id" => 95,
		"tags"            => "",
		"title" => "Die to Live",
		"grade" => 5,
		"rank" => "s",
		"weapon" => "sword",
		"armor" => "armor",
		"accessory" => "ring",
		"weapon_image"    => "5-die-to-live.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Increase Attack by 20%", "stat" => "Attack%", "value" => "20", "condition" => "IG")
		),
		"effect2_label" => "Increase Attack by 20%",
		"effect3" => array(
			array("label" => "Increase Crit Rate by 2500", "stat" => "Crit Rate", "value" => "2500", "condition" => "IG")
		),
		"effect3_label" => "Increase Crit Rate by 2500",
		"condition_label" => "Activated when the user's HP falls below 50%",
		"condition" => "IG"
	],
	[
		"id" => 96,
		"tags"            => "",
		"title" => "Ultimate Weapon Gold Tiger",
		"grade" => 5,
		"rank" => "s",
		"weapon" => "mace",
		"armor" => "armor",
		"accessory" => "brooch",
		"weapon_image"    => "5-ugt.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Increase Attack by 80%", "stat" => "Attack%", "value" => "80", "condition" => "IG")
		),
		"effect2_label" => "Increase Attack by 80%",
		"effect3" => array(
			array("label" => "Increase HP by 130%", "stat" => "HP%", "value" => "130", "condition" => "IG")
		),
		"effect3_label" => "Increase HP by 130%",
		"condition_label" => "Activated if only you are left standing",
		"condition" => "IG"
	],
	[
		"id" => 97,
		"tags"            => "",
		"title" => "Token of Return",
		"grade" => 3,
		"rank" => "a",
		"weapon" => "bow",
		"armor" => "armor",
		"accessory" => "necklace",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Increase Crit Rate by 200 every turn (up to 1200)", "stat" => "Crit Rate", "value" => "200", "max" => "1200", "condition" => "IG")
		),
		"effect2_label" => "Increase Crit Rate by 200 every turn (up to 1200)",
		"effect3" => array(
			array("label" => "Increase Attack by 300 every turn (up to 1800)", "stat" => "Attack", "value" => "300", "max" => "1800", "condition" => "IG")
		),
		"effect3_label" => "Increase Attack by 300 every turn (up to 1800)",
		"condition_label" => "Activated the set effects at the beginning of combat",
		"condition" => "IG"
	],
	[
		"id" => 98,
		"tags"            => "",
		"title" => "Dasomier Battle Suit (Mass Production Type)",
		"grade" => 2,
		"rank" => "a",
		"weapon" => "bow",
		"armor" => "armor",
		"accessory" => "brooch",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Increase Hit Rate by 1000", "stat" => "Hit Rate", "value" => "1000", "condition" => "IG")
		),
		"effect2_label" => "Increase Hit Rate by 1000",
		"effect3" => array(
			array("label" => "Increase Attack by 15%", "stat" => "Attack%", "value" => "15", "condition" => "IG")
		),
		"effect3_label" => "Increase Attack by 15%",
		"condition_label" => "Activated until the second turn of combat",
		"condition" => "IG"
	],
	[
		"id" => 99,
		"tags"            => "",
		"title" => "Knight of the Dawn",
		"grade" => 1,
		"rank" => "ss",
		"weapon" => "sword",
		"armor" => "armor",
		"accessory" => "ring",
		"weapon_image"    => "1-kod.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Increase Crit Rate by 800", "stat" => "Crit Rate", "value" => "800"),
			array("label" => "Additionally increases by 150 for Attack-type hero", "stat" => "Crit Rate", "value" => "150", "condition" => "type=Attack")
		),
		"effect2_label" => "Increase Crit Rate by 800 (Additionally increases by 150 for Attack-type hero)",
		"effect3" => array(
			array("label" => "Increase Attack by 760", "stat" => "Attack", "value" => "760"),
			array("label" => "Additionally increases by 300 for Attack-type hero", "stat" => "Attack", "value" => "300", "condition" => "type=Attack")
		),
		"effect3_label" => "Increase Attack by 760 (Additionally increases by 300 for Attack-type hero)",
		"condition_label" => ""
	],
	[
		"id" => 100,
		"tags"            => "",
		"title"           => "Spirit of Cold Steel",
		"grade"           => 1,
		"rank"            => "s",
		"weapon"          => "mace",
		"armor"           => "armor",
		"accessory"       => "brooch",
		"weapon_image"    => "1-bamboo.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Increase Defense by 600", "stat" => "Defense", "value" => "600")
		),
		"effect2_label" => "Increase Defense by 600",
		"effect3" => array(
			array("label" => "Decrease Critical Damage from enemy attack by 8%", "stat" => "Critical Damage Taken%", "value" => "8")
		),
		"effect3_label" => "Decrease Critical Damage from enemy attack by 8%",
		"condition_label" => ""
	],
	[
		"id" => 101,
		"tags"            => "",
		"title" => "Time for your Shot",
		"grade" => 2,
		"rank" => "ss",
		"weapon" => "bow",
		"armor" => "armor",
		"accessory" => "necklace",
		"weapon_image"    => "2-shu.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Increase Crit Rate by 900", "stat" => "Crit Rate", "value" => "900")
		),
		"effect2_label" => "Increase Crit Rate by 900",
		"effect3" => array(
			array("label" => "Increase Attack by 12%", "stat" => "Attack%", "value" => "12")
		),
		"effect3_label" => "Increase Attack by 12%",
		"condition_label" => ""
	],
	[
		"id"              => 102,
		"tags"            => "",
		"title"           => "Coocoo Ranger: Red!",
		"grade"           => 2,
		"rank"            => "s",
		"weapon"          => "bow",
		"armor"           => "boots",
		"accessory"       => "brooch",
		"weapon_image"    => "2-coocoo-red.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2_label"   => "Increase Defense by 700 (increases additional 150 when used by Balance-type characters)",
		"effect3_label"   => "Increase HP by 2820 (increases additional 687 when used by Balance-type characters)",
		"condition_label" => "",
		"effect2"         => array(
			array("label" => "Increase Defense by 700", "stat" => "Defense", "value" => "700", "condition" => "type=!Balance"),
			array("label" => "Increases additional 150 Defense when used by Balance-type characters", "stat" => "Defense", "value" => "850", "condition" => "type=Balance")
		),
		"effect3" => array(
			array("label" => "Increase HP by 2820", "stat" => "HP", "value" => "2820", "condition" => "type=!Balance"),
			array("label" => "Increases additional 687 HP when used by Balance-type characters", "stat" => "HP", "value" => "3507", "condition" => "type=Balance")
		),
	],
	[
		"id" => 103,
		"tags"            => "",
		"title" => "I Perform the Surgery",
		"grade" => 3,
		"rank" => "ss",
		"weapon" => "sword",
		"armor" => "armor",
		"accessory" => "ring",
		"weapon_image"    => "3-surgery.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Increase HP by 10%", "stat" => "HP%", "value" => "10")
		),
		"effect2_label" => "Increase HP by 10%",
		"effect3" => array(
			array("label" => "Increase Attack by 2568", "stat" => "Attack", "value" => "2568")
		),
		"effect3_label" => "Increase Attack by 2568",
		"condition_label" => ""
	],
	[
		"id" => 104,
		"tags"            => "",
		"title" => "Legendary Ranger",
		"grade" => 3,
		"rank" => "s",
		"weapon" => "bow",
		"armor" => "boots",
		"accessory" => "ring",
		"weapon_image"    => "3-legendary.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Increase Dodge by 2000", "stat" => "Dodge", "value" => "2000")
		),
		"effect2_label" => "Increase Dodge by 2000",
		"effect3" => array(
			array("label" => "Increase Attack by 12%", "stat" => "Attack%", "value" => "12")
		),
		"effect3_label" => "Increase Attack by 12%",
		"condition_label" => "Activated if Wearer's Dodge is at least 30%",
		"condition" => "IG"
	],
	[
		"id" => 105,
		"tags"            => "",
		"title" => "Forgotten Hitter No.4",
		"grade" => 4,
		"rank" => "ss",
		"weapon" => "mace",
		"armor" => "armor",
		"accessory" => "ring",
		"weapon_image"    => "4-hitter.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Increase Crit Rate by 1000", "stat" => "Crit Rate", "value" => "1000"),
			array("label" => "Additionally increase Crit Rate by 300 for Attack-type hero", "stat" => "Crit Rate", "value" => "300", "condition" => "type=Attack")
		),
		"effect2_label" => "Increase Crit Rate by 1000 (additionally increase by 300 for Attack-type hero)",
		"effect3" => array(
			array("label" => "Increase Attack by 2818", "stat" => "Attack", "value" => "2818"),
			array("label" => "Additionally increase Attack by 1082 for Attack-type hero", "stat" => "Attack", "value" => "1082", "condition" => "type=Attack")
		),
		"effect3_label" => "Increase Attack by 2818 (additionally increase by 1082 for Attack-type hero)",
		"condition_label" => ""
	],
	[
		"id" => 106,
		"tags"            => "",
		"title" => "Brushing Wind",
		"grade" => 4,
		"rank" => "s",
		"weapon" => "bow",
		"armor" => "armor",
		"accessory" => "brooch",
		"weapon_image"    => "4-brushing.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Increase Defense by 1500", "stat" => "Defense", "value" => "1500")
		),
		"effect2_label" => "Increase Defense by 1500",
		"effect3" => array(
			array("label" => "Decrease damage from enemy attack by 10%", "stat" => "Damage Taken%", "value" => "10")
		),
		"effect3_label" => "Decrease damage from enemy attack by 10%",
		"condition_label" => "Activated if Wearer's Dodge is at least 40%",
		"condition" => "IG"
	],
	[
		"id" => 107,
		"tags"            => "",
		"title" => "Angel Knight",
		"grade" => 5,
		"rank" => "ss",
		"weapon" => "bow",
		"armor" => "armor",
		"accessory" => "necklace",
		"weapon_image"    => "5-ak.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Increase HP by 21480", "stat" => "HP", "value" => "21480")
		),
		"effect2_label" => "Increase HP by 21480",
		"effect3" => array(
			array("label" => "Increase Attack by 18%", "stat" => "Attack%", "value" => "18")
		),
		"effect3_label" => "Increase Attack by 18%",
		"condition_label" => ""
	],
	[
		"id" => 108,
		"tags"            => "",
		"title" => "Pirate of Pirates",
		"grade" => 5,
		"rank" => "s",
		"weapon" => "sword",
		"armor" => "shield",
		"accessory" => "brooch",
		"weapon_image"    => "5-pirate.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Increase Defense by 1200", "stat" => "Defense", "value" => "1200")
		),
		"effect2_label" => "Increase Defense by 1200",
		"effect3" => array(
			array("label" => "Increase HP by 5831", "stat" => "HP", "value" => "5831"),
			array("label" => "Increase Attack by 1494", "stat" => "Attack", "value" => "1494")
		),
		"effect3_label" => "Increase HP by 5831 and Attack by 1494",
		"condition_label" => ""
	],
	[
		"id" => 109,
		"tags"            => "",
		"title" => "Power of the Ancients",
		"grade" => 1,
		"rank" => "s",
		"weapon" => "mace",
		"armor" => "boots",
		"accessory" => "necklace",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Increase Attack by 320", "stat" => "Attack", "value" => "320")
		),
		"effect2_label" => "Increase Attack by 320",
		"effect3" => array(
			array("label" => "Increase Defense by 750", "stat" => "Defense", "value" => "750")
		),
		"effect3_label" => "Increase Defense by 750",
		"condition_label" => "Balance-type characters only",
		"condition" => "type=Balance"
	],
	[
		"id" => 110,
		"tags"            => "",
		"title" => "Magical Marksman",
		"grade" => 5,
		"rank" => "ss",
		"weapon" => "bow",
		"armor" => "boots",
		"accessory" => "ring",
		"weapon_image"    => "5-mm.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Increase Crit Rate by 1200", "stat" => "Crit Rate", "value" => "1200")
		),
		"effect2_label" => "Increase Crit Rate by 1200",
		"effect3" => array(
			array("label" => "Increase Attack by 30%", "stat" => "Attack%", "value" => "30")
		),
		"effect3_label" => "Increase Attack by 30%",
		"condition_label" => "",
		"condition" => ""
	],
	[
		"id" => 111,
		"tags"            => "",
		"title" => "Calm Guide",
		"grade" => 5,
		"rank" => "s",
		"weapon" => "sword",
		"armor" => "shield",
		"accessory" => "brooch",
		"weapon_image"    => "5-calm-guide.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Increase HP by 11680", "stat" => "HP", "value" => "11680")
		),
		"effect2_label" => "Increase HP by 11680",
		"effect3" => array(
			array("label" => "Decrease Crit Rate for all enemies in the dungeon by 800", "stat" => "Enemy Crit Rate", "value" => "800", "curse")
		),
		"effect3_label" => "Decrease Crit Rate for all enemies in the dungeon by 800",
		"condition_label" => "Activated when at least 3 allies are alive",
		"condition" => "IG"
	],
	[
		"id" => 112,
		"tags"            => "",
		"title" => "Iron Fortress",
		"grade" => 4,
		"rank" => "s",
		"weapon" => "sword",
		"armor" => "shield",
		"accessory" => "brooch",
		"weapon_image"    => "4-iron-fortress.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Increase HP by 7930", "stat" => "HP", "value" => "7930")
		),
		"effect2_label" => "Increase HP by 7930",
		"effect3" => array(
			array("label" => "Increase Defense by 10%", "stat" => "Defense%", "value" => "10")
		),
		"effect3_label" => "Increase Defense by 10%",
		"condition_label" => "Defense-type character only",
		"condition" => "type=Defense"
	],
	[
		"id" => 113,
		"tags"            => "",
		"title" => "Devil Knight",
		"grade" => 4,
		"rank" => "ss",
		"weapon" => "mace",
		"armor" => "armor",
		"accessory" => "brooch",
		"weapon_image"    => "4-dk.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Increase Attack by 2918", "stat" => "Attack", "value" => "2918")
		),
		"effect2_label" => "Increase Attack by 2918",
		"effect3" => array(
			array("label" => "Decrease Damage from skills that target at least 2 units by 25%", "stat" => "Damage Taken%", "value" => "25", "condition" => "IG")
		),
		"effect3_label" => "Decrease Damage from skills that target at least 2 units by 25%",
		"condition_label" => "",
	],
	[
		"id" => 114,
		"tags"            => "heroonly",
		"title" => "Kneel",
		"grade" => 3,
		"rank" => "ss",
		"weapon" => "bow",
		"armor" => "armor",
		"accessory" => "ring",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Decrease attack of enemies within 3 tile of the wearer by 1000. (effect persistent)", "stat" => "Enemy Attack", "value" => "1000", "curse")
		),
		"effect2_label" => "Decrease attack of enemies within 3 tile of the wearer by 1000. (effect persistent)",
		"effect3" => array(
			array("label" => "Increase attack of the wearer by 40% for 3 turns", "stat" => "Attack%", "value" => "40", "condition" => "IG"),
			array("label" => "Decrease incoming damage of the wearer by 15% for 2 turns", "stat" => "Damage Taken%", "value" => "15", "condition" => "IG"),
		),
		"effect3_label" => "Increase attack of the wearer by 40% for 3 turns and decrease incoming damage of the wearer by 15% for 2 turns.",
		"condition_label" => "Raizel only",
		"condition" => "name=Raizel"
	],
	[
		"id" => 115,
		"tags"            => "heroonly",
		"title" => "Roar of Madness",
		"grade" => 3,
		"rank" => "ss",
		"weapon" => "mace",
		"armor" => "armor",
		"accessory" => "necklace",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Every time its wearer loses 10% of HP, this item increases Crit Rate by 180. (Up to 1620)", "stat" => "Crit Rate", "value" => "180", "max" => "1620", "condition" => "IG")
		),
		"effect2_label" => "Every time its wearer loses 10% of HP, this item increases Crit Rate by 180. (Up to 1620)",
		"effect3" => array(
			array("label" => "Increase the wearer's Defense by 1500 on odd-numbered turns", "stat" => "Defense", "value" => "1500", "condition" => "IG"),
			array("label" => "Increase Attack by 2608 on even-numbered turns", "stat" => "Attack", "value" => "2608", "condition" => "IG")
		),
		"effect3_label" => "Increase the wearer's Defense by 1500 on odd-numbered turns and Attack by 2608 on even-numbered turns.",
		"condition_label" => "Krut only",
		"condition" => "name=Krut"
	],
	[
		"id" => 116,
		"tags"            => "heroonly",
		"title" => "Wild Lady",
		"grade" => 3,
		"rank" => "ss",
		"weapon" => "sword",
		"armor" => "armor",
		"accessory" => "ring",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Increase Attack by 2318", "stat" => "Attack", "value" => "2318")
		),
		"effect2_label" => "Increase Attack by 2318",
		"effect3" => array(
			array("label" => "Increase HP by 9710", "stat" => "HP", "value" => "9710"),
			array("label" => "If 3 or more buffs/debuffs cast on the wearer movement increase by 1", "stat" => "Movement", "value" => "1", "condition" => "IG")
		),
		"effect3_label" => "Increase HP by 9710, if 3 or more buffs/debuffs cast on the wearer movement increase by 1",
		"condition_label" => "Reina only",
		"condition" => "name=Reina"
	],
	[
		"id" => 117,
		"tags"            => "heroonly",
		"title" => "Dragon Incarnate",
		"grade" => 3,
		"rank" => "ss",
		"weapon" => "bow",
		"armor" => "armor",
		"accessory" => "brooch",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Restore 5% of HP every turn", "stat" => "HP Recovery%", "value" => "5")
		),
		"effect2_label" => "Restore 5% of HP every turn",
		"effect3" => array(
			array("label" => "Increase Attack of the wearer by 30% for 4 turns", "stat" => "Attack%", "value" => "30", "condition" => "IG"),
			array("label" => "Decrease incoming damage of the wearer by 10% for 3 turns", "stat" => "Damage Taken%", "value" => "10", "condition" => "IG")
		),
		"effect3_label" => "Increase Attack of the wearer by 30% for 4 turns and decrease incoming damage of the wearer by 10% for 3 turns",
		"condition_label" => "Celestial only",
		"condition" => "name=Celestial"
	],
	[
		"id" => 118,
		"tags"            => "",
		"title" => "Phoenix's Apostle",
		"grade" => 4,
		"rank" => "a",
		"weapon" => "mace",
		"armor" => "shield",
		"accessory" => "necklace",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Increase Defense by 750", "stat" => "Defense", "value" => "750"),
			array("label" => "Additionally increase Defense by 800 inside the Nephthys Dungeon", "stat" => "Defense", "value" => "800", "condition" => "IG")
		),
		"effect2_label" => "Increase Defense by 750, and additionally by 800 inside the Nephthys Dungeon",
		"effect3" => array(
			array("label" => "Increase Dodge by 1000", "stat" => "Dodge", "value" => "1000"),
			array("label" => "Additionally Increase Dodge by 1000 inside the Nephthys Dungeon", "stat" => "Dodge", "value" => "1000", "condition" => "IG")
		),
		"effect3_label" => "Increase Dodge by 1000, and additionally by 1000 inside the Nephthys Dungeon",
		"condition_label" => ""
	],
	[
		"id" => 119,
		"tags"            => "",
		"title" => "Winner's Proof",
		"grade" => 4,
		"rank" => "s",
		"weapon" => "mace",
		"armor" => "armor",
		"accessory" => "necklace",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Increase Dodge by 1000", "stat" => "Dodge", "value" => "1000")
		),
		"effect2_label" => "Increase Dodge by 1000",
		"effect3" => array(
			array("label" => "Increase Attack by 611 every turn, up to 1833", "stat" => "Attack", "value" => "611", "max" => "1833", "condition" => "IG")
		),
		"effect3_label" => "Increase Attack by 611 every turn, up to 1833",
		"condition_label" => ""
	],
	[
		"id" => 120,
		"tags"            => "",
		"title" => "I'm a Team Player",
		"grade" => 5,
		"rank" => "a",
		"weapon" => "bow",
		"armor" => "armor",
		"accessory" => "ring",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Increase Defense by 900", "stat" => "Defense", "value" => "900")
		),
		"effect2_label" => "Increase Defense by 900",
		"effect3" => array(
			array("label" => "Decrease skill MP cost by 50% for 3 turns", "stat" => "MP Cost%", "value" => "-50")
		),
		"effect3_label" => "Decrease skill MP cost by 50% for 3 turns",
		"condition_label" => "Support-type character only",
		"condition" => "type=Support"
	],
	[
		"id" => 121,
		"tags"            => "heroonly",
		"title" => "Dragon's Collection",
		"grade" => 4,
		"rank" => "s",
		"weapon" => "mace",
		"armor" => "shield",
		"accessory" => "necklace",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Increase Defense by 1100", "stat" => "Defense", "value" => "1100")
		),
		"effect2_label" => "Increase Defense by 1100",
		"effect3" => array(
			array("label" => "Increase Movement by 1", "stat" => "Movement", "value" => "1")
		),
		"effect3_label" => "Increase Movement by 1",
		"condition_label" => "Celestial only",
		"condition" => "name=Celestial"
	],
	[
		"id" => 122,
		"tags"            => "",
		"title" => "Here Comes a Super Rookie!",
		"grade" => 1,
		"rank" => "a",
		"weapon" => "bow",
		"armor" => "armor",
		"accessory" => "brooch",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Increase Defense by 700", "stat" => "Defense", "value" => "700")
		),
		"effect2_label" => "Increase Defense by 700",
		"effect3" => array(
			array("label" => "Increase Attack by 370", "stat" => "Attack", "value" => "370")
		),
		"effect3_label" => "Increase Attack by 370",
		"condition_label" => ""
	],
	[
		"id" => 123,
		"tags"            => "heroonly",
		"title" => "Defeated Ruler",
		"grade" => 4,
		"rank" => "s",
		"weapon" => "sword",
		"armor" => "shield",
		"accessory" => "brooch",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Increase Crit Rate by 1000", "stat" => "Crit Rate", "value" => "1000")
		),
		"effect2_label" => "Increase Crit Rate by 1000",
		"effect3" => array(
			array("label" => "Increase HP by 7800", "stat" => "HP", "value" => "7800")
		),
		"effect3_label" => "Increase HP by 7800",
		"condition_label" => "Deimos only",
		"condition" => "name=Deimos"
	],
	[
		"id" => 124,
		"tags"            => "heroonly",
		"title" => "Song of Death",
		"grade" => 3,
		"rank" => "ss",
		"weapon" => "mace",
		"armor" => "armor",
		"accessory" => "ring",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Every time its wearer loses 10% of HP, this item increases Crit Rate by 230. (Up to 2070)", "stat" => "Crit Rate", "value" => "230", "max" => "2070", "condition" => "IG") 
		),
		"effect2_label" => "Every time its wearer loses 10% of HP, this item increases Crit Rate by 230. (Up to 2070)",
		"effect3" => array(
			array("label" => "Increase its wearer's Attack by 30% for 4 turns", "stat" => "Attack%", "value" => "30", "condition" => "IG"),
			array("label" => "Increase its wearer's Resistance by 50% for 3 turns", "stat" => "Resistance", "value" => "50", "condition" => "IG")
		),
		"effect3_label" => "Increase its wearer's Attack by 30% for 4 turns and their Resistance by 50% for 3 turns",
		"condition_label" => "Banshee only",
		"condition" => "name=Banshee"
	],
	[
		"id" => 125,
		"tags"            => "heroonly",
		"title" => "Irresistible Power",
		"grade" => 3,
		"rank" => "ss",
		"weapon" => "bow",
		"armor" => "armor",
		"accessory" => "ring",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Increase Dodge by 800 for every enemy in a 1 tile range (Up to 3,200)", "stat" => "Dodge", "value" => "800", "max" => "3200", "condition" => "IG") 
		),
		"effect2_label" => "Increase Dodge by 800 for every enemy in a 1 tile range (Up to 3,200)",
		"effect3" => array(
			array("label" => "Increase Attack by 2510", "stat" => "Attack", "value" => "2510"),
			array("label" => "Increase by additional 1010 if there are 2 or more enemies in a 1 tile range", "stat" => "Attack", "value" => "1010", "condition" => "IG")
		),
		"effect3_label" => "Increase Attack by 2510, and by additional 1010 if there are 2 or more enemies in a 1 tile range",
		"condition_label" => "Deimos only",
		"condition" => "name=Deimos"
	],
	[
		"id" => 126,
		"tags"            => "heroonly",
		"title" => "Separation Anxiety",
		"grade" => 3,
		"rank" => "s",
		"weapon" => "mace",
		"armor" => "armor",
		"accessory" => "brooch",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Restore 6 MP every turn", "stat" => "MP Recovery", "value" => "6") 
		),
		"effect2_label" => "Restore 6 MP every turn",
		"effect3" => array(
			array("label" => "Increase HP by 3824", "stat" => "HP", "value" => "3824"),
			array("label" => "If there is an ally Banshee on the battlefield, Spooky recovers 3% HP every turn", "stat" => "HP Recovery%", "value" => "3", "condition" => "IG")
		),
		"effect3_label" => "Increase HP by 3824. If there is an ally Banshee on the battlefield, Spooky recovers 3% HP every turn",
		"condition_label" => "Spooky only",
		"condition" => "name=Spooky"
	],
	[
		"id" => 127,
		"tags"            => "",
		"title" => "Blue Moonlight Season",
		"grade" => 1,
		"rank" => "s",
		"weapon" => "bow",
		"armor" => "armor",
		"accessory" => "ring",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Decrease Critical Damage from enemy attack by 7%", "stat" => "Critical Damage Taken%", "value" => "7") 
		),
		"effect2_label" => "Decrease Critical Damage from enemy attack by 7%",
		"effect3" => array(
			array("label" => "Increase Attack by 317", "stat" => "Attack", "value" => "317")
		),
		"effect3_label" => "Increase Attack by 317",
		"condition_label" => ""
	],
	[
		"id" => 128,
		"tags"            => "",
		"title" => "Blue Dragon's Blessing",
		"grade" => 2,
		"rank" => "ss",
		"weapon" => "sword",
		"armor" => "armor",
		"accessory" => "ring",
		"weapon_image"    => "2-blue-dragon.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Increase its wearer's Attack by 30% for 4 turns", "stat" => "Attack%", "value" => "30", "condition" => "IG") 
		),
		"effect2_label" => "Increase its wearer's Attack by 30% for 4 turns",
		"effect3" => array(
			array("label" => "Increase Movement by 1", "stat" => "Movement", "value" => "1")
		),
		"effect3_label" => "Increase Movement by 1",
		"condition_label" => ""
	],
	[
		"id"              => 129,
		"tags"            => "",
		"title"           => "Super Ranger: Green",
		"grade"           => 2,
		"rank"            => "s",
		"weapon"          => "bow",
		"armor"           => "shield",
		"accessory"       => "ring",
		"weapon_image"    => "2-green-ranger.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2_label"   => "Increase Defense by 1500",
		"effect3_label"   => "Decrease Critical Damage Taken by 15%",
		"condition_label" => "Activated when the wearer's HP is at least 30%",
		"condition"       => "IG",
		"effect2"         => array(
			array("label" => "Increase Defense by 1500", "stat" => "Defense", "value" => "1500") 
		),
		"effect3" => array(
			array("label" => "Decrease Critical Damage Taken by 15%", "stat" => "Critical Damage Taken%", "value" => "15")
		),
	],
	[
		"id"              => 130,
		"tags"            => "",
		"title"           => "Death Bringer",
		"grade"           => 3,
		"rank"            => "ss",
		"weapon"          => "sword",
		"armor"           => "armor",
		"accessory"       => "brooch",
		"weapon_image"    => "3-db.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2_label"   => "Increase Attack by 1344 and Crit Rate by 600",
		"effect3_label"   => "Revive with 5% HP upon death for the first 2 turns (max once)",
		"condition_label" => "",
		"effect2"         => array(
			array("label" => "Increase Attack by 1344", "stat" => "Attack", "value" => "1344"),
			array("label" => "Increase Crit Rate by 600", "stat" => "Crit Rate", "value" => "600") 
		),
		"effect3" => array(
			array("label" => "Revive with 5% HP upon death for the first 2 turns (max once)", "stat" => "Revive5%HP", "value" => "1")
		),
	],
	[
		"id" => 131,
		"tags"            => "",
		"title" => "Super Ranger: Blue",
		"grade" => 3,
		"rank" => "s",
		"weapon" => "bow",
		"armor" => "armor",
		"accessory" => "ring",
		"weapon_image"    => "3-blue-ranger.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Increase Hit Rate by 700", "stat" => "Hit Rate", "value" => "700")
		),
		"effect2_label" => "Increase Hit Rate by 700",
		"effect3" => array(
			array("label" => "Increase its wearer's Attack by 20% for 4 turns", "stat" => "Attack%", "value" => "20", "condition" => "IG")
		),
		"effect3_label" => "Increase its wearer's Attack by 20% for 4 turns",
		"condition_label" => ""
	],
	[
		"id" => 132,
		"tags"            => "",
		"title" => "Pumpkin Ghost Step",
		"grade" => 2,
		"rank" => "a",
		"weapon" => "mace",
		"armor" => "shield",
		"accessory" => "brooch",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Increase Defense by 600", "stat" => "Defense", "value" => "600")
		),
		"effect2_label" => "Increase Defense by 600",
		"effect3" => array(
			array("label" => "Increase Movement by 1", "stat" => "Movement", "value" => "1")
		),
		"effect3_label" => "Increase Movement by 1",
		"condition_label" => "Activated for 2 turns at the beginning of combat",
		"condition" => "IG"
	],
	[
		"id" => 133,
		"tags"            => "heroonly",
		"title" => "I Like Living Things",
		"grade" => 3,
		"rank" => "ss",
		"weapon" => "bow",
		"armor" => "armor",
		"accessory" => "brooch",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Restore 6% of HP every turn", "stat" => "HP Recovery%", "value" => "6")
		),
		"effect2_label" => "Restore 6% of HP every turn",
		"effect3" => array(
			array("label" => "Increase Attack of the wearer by 40% for 3 turns", "stat" => "Attack%", "value" => "40", "condition" => "IG"),
			array("label" => "Decrease incoming damage of the wearer by 15% for 2 turns", "stat" => "Damage Taken%", "value" => "15", "condition" => "IG")
		),
		"effect3_label" => "Increase Attack of the wearer by 40% for 3 turns and decrease incoming damage of the wearer by 15% for 2 turns",
		"condition_label" => "Lucas only",
		"condition" => "name=Lucas"
	],
	[
		"id" => 134,
		"tags"            => "",
		"title" => "Thank-you Letter",
		"grade" => 1,
		"rank" => "s",
		"weapon" => "bow",
		"armor" => "armor",
		"accessory" => "brooch",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Increase HP by 1452", "stat" => "HP", "value" => "1452")
		),
		"effect2_label" => "Increase HP by 1452",
		"effect3" => array(
			array("label" => "Restore 4% of HP every turn", "stat" => "HP Recovery%", "value" => "4")
		),
		"effect3_label" => "Restore 4% of HP every turn",
		"condition_label" => "",
	],
	[
		"id" => 135,
		"tags"            => "",
		"title" => "Traveler from the East",
		"grade" => 5,
		"rank" => "a",
		"weapon" => "mace",
		"armor" => "armor",
		"accessory" => "brooch",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Swamp accessible", "stat" => "Terrain Swamp", "value" => "1")
		),
		"effect2_label" => "Swamp accessible",
		"effect3" => array(
			array("label" => "Water accessible", "stat" => "Terrain Water", "value" => "1"),
			array("label" => "Increase Attack by 1092", "stat" => "Attack", "value" => "1092")
		),
		"effect3_label" => "Water accessible. Increase Attack by 1092",
		"condition_label" => "",
	],
	[
		"id" => 136,
		"tags"            => "heroonly",
		"title" => "Hero of Ikaruga",
		"grade" => 3,
		"rank" => "ss",
		"weapon" => "mace",
		"armor" => "shield",
		"accessory" => "necklace",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Increase Crit Rate by 1400", "stat" => "Crit Rate", "value" => "1400")
		),
		"effect2_label" => "Increase Crit Rate by 1400",
		"effect3" => array(
			array("label" => "Increase it's wearer Attack by 30% for 4 turns", "stat" => "Attack%", "value" => "30", "condition" => "IG"),
			array("label" => "decreases incoming damage by 10% for 3 turns", "stat" => "Damage Taken%", "value" => "10", "condition" => "IG")
		),
		"effect3_label" => "Increase it's wearer Attack by 30% for 4 turns and decreases incoming damage by 10% for 3 turns",
		"condition_label" => "Jin Kisaragi only",
		"condition" => "name=Jin Kisaragi"
	],
	[
		"id" => 137,
		"tags"            => "heroonly",
		"title" => "Wanted Reaper",
		"grade" => 3,
		"rank" => "ss",
		"weapon" => "bow",
		"armor" => "armor",
		"accessory" => "ring",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Recovers MP by 10 every turn", "stat" => "MP Recovery", "value" => "10"),
			array("label" => "Increase Crit Rate by 200 every attack (up to 800)", "stat" => "Crit Rate", "value" => "800",  "condition" => "IG")
		),
		"effect2_label" => "Recovers MP by 10 every turn. Increase Crit Rate by 200 every attack (up to 800)",
		"effect3" => array(
			array("label" => "Increase it's wearer Attack by 30% for 4 turns", "stat" => "Attack%", "value" => "30", "condition" => "IG")
		),
		"effect3_label" => "Increase it's wearer Attack by 30% for 4 turns",
		"condition_label" => "Ragna only",
		"condition" => "name=Ragna"
	],
	[
		"id" => 138,
		"tags"            => "heroonly",
		"title" => "Reluctant Sparring Partner",
		"grade" => 3,
		"rank" => "s",
		"weapon" => "sword",
		"armor" => "armor",
		"accessory" => "brooch",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Increase Defense by 1100", "stat" => "Defense", "value" => "1100")
		),
		"effect2_label" => "Increase Defense by 1100",
		"effect3" => array(
			array("label" => "Increase Defense by 8%", "stat" => "Defense%", "value" => "8")
		),
		"effect3_label" => "Increase Defense by 8%",
		"condition_label" => "Rachel only",
		"condition" => "name=Rachel"
	],
	[
		"id" => 139,
		"tags"            => "heroonly",
		"title" => "Diligent Secretary",
		"grade" => 3,
		"rank" => "s",
		"weapon" => "sword",
		"armor" => "armor",
		"accessory" => "brooch",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2" => array(
			array("label" => "Increase Hit Rate by 1000", "stat" => "Hit Rate", "value" => "1000")
		),
		"effect2_label" => "Increase Hit Rate by 1000",
		"effect3" => array(
			array("label" => "Increase Attack by 1126", "stat" => "Attack", "value" => "1126")
		),
		"effect3_label" => "Increase Attack by 1126",
		"condition_label" => "Noel only",
		"condition" => "name=Noel"
	],
	[
		"id"              => 140,
		"tags"            => "",
		"title"           => "Changing Future",
		"grade"           => 5,
		"rank"            => "ss",
		"weapon"          => "bow",
		"armor"           => "armor",
		"accessory"       => "brooch",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2_label"   => "Increase Critical Damage by 15%",
		"effect3_label"   => "Increase Attack by 780 every turn, up to 7080",
		"condition_label" => "",
		"condition"       => "",
		"effect2"   => array(
			array("label" => "Increase Critical Damage by 15%", "stat" => "Critical Damage%", "value" => "15")
		),
		"effect3" => array(
			array("label" => "Increase Attack by 780 every turn, up to 7080", "stat" => "Attack", "value" => "780", "max" => "7080", "condition" => "IG")
		),
	],
	[
		"id"              => 141,
		"tags"            => "",
		"title"           => "Progressive Present",
		"grade"           => 5,
		"rank"            => "ss",
		"weapon"          => "sword",
		"armor"           => "armor",
		"accessory"       => "brooch",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2_label"   => "Increase Defense by 2000. Increase Movement by 1",
		"effect3_label"   => "Increase Attack by 40%",
		"condition_label" => "Activated for 5 turns at the beginning of combat",
		"condition"       => "IG",
		"effect2"   => array(
			array("label" => "Increase Defense by 2000", "stat" => "Defense", "value" => "2000"),
			array("label" => "Increase Movement by 1", "stat" => "Movement", "value" => "1"),
		),
		"effect3" => array(
			array("label" => "Increase Attack by 40%", "stat" => "Attack%", "value" => "40")
		),
	],
	[
		"id"              => 142,
		"tags"            => "heroonly",
		"title"           => "Hero's Armament : Legendary Thief",
		"grade"           => 3,
		"rank"            => "ss",
		"weapon"          => "bow",
		"armor"           => "armor",
		"accessory"       => "brooch",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2_label"   => "If owner's has 2 or more buffs from Active skills, Critical Damage increases by 15%",
		"effect3_label"   => "Increase its wearer's Attack by 30% for 4 turns",
		"condition_label" => "Phantom only",
		"condition"       => "name=Phantom",
		"effect2"   => array(
			array("label" => "If owner's has 2 or more buffs from Active skills, Critical Damage increases by 15%", "stat" => "Critical Damage%", "value" => "15", "condition" => "IG"),
		),
		"effect3" => array(
			array("label" => "Increase its wearer's Attack by 30% for 4 turns", "stat" => "Attack%", "value" => "30", "condition" => "IG")
		),
	],
	[
		"id"              => 143,
		"tags"            => "heroonly",
		"title"           => "Hero's Armament : Elf Queen",
		"grade"           => 3,
		"rank"            => "ss",
		"weapon"          => "bow",
		"armor"           => "armor",
		"accessory"       => "brooch",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2_label"   => "Increase Attack by 1344. Increase Hit Rate by 600",
		"effect3_label"   => "Increase its wearer's Attack by 30% for 4 turns",
		"condition_label" => "Mercedes only",
		"condition"       => "name=Mercedes",
		"effect2"   => array(
			array("label" => "Increase Attack by 1344", "stat" => "Attack", "value" => "1344"),
			array("label" => "Increase Hit Rate by 600", "stat" => "Hit Rate", "value" => "600"),
		),
		"effect3" => array(
			array("label" => "Increase its wearer's Attack by 30% for 4 turns", "stat" => "Attack%", "value" => "30", "condition" => "IG")
		),
	],
	[
		"id"              => 144,
		"tags"            => "heroonly",
		"title"           => "Hero's Armament : Dragon Master",
		"grade"           => 3,
		"rank"            => "s",
		"weapon"          => "mace",
		"armor"           => "armor",
		"accessory"       => "ring",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2_label"   => "Increase all allies' HP by 2030 in the dungeon",
		"effect3_label"   => "Increase its wearer's Attack by 20% for 4 turns",
		"condition_label" => "Evan only",
		"condition"       => "name=Evan",
		"effect2"   => array(
			array("label" => "Increase all allies' HP by 2030 in the dungeon", "stat" => "HP", "value" => "2030", "aura"),
		),
		"effect3" => array(
			array("label" => "Increase its wearer's Attack by 20% for 4 turns", "stat" => "Attack%", "value" => "20", "condition" => "IG")
		),
	],
	[
		"id"              => 145,
		"tags"            => "",
		"title"           => "Oblivious Past",
		"grade"           => 5,
		"rank"            => "ss",
		"weapon"          => "mace",
		"armor"           => "armor",
		"accessory"       => "necklace",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2_label"   => "Increase HP by 15800. Increase Attack by 3082",
		"effect3_label"   => "Cannot be counterattacked during its turn",
		"condition_label" => "",
		"condition"       => "",
		"effect2"   => array(
			array("label" => "Increase HP by 15800", "stat" => "HP", "value" => "15800"),
			array("label" => "Increase Attack by 3082", "stat" => "Attack", "value" => "3082"),
		),
		"effect3" => array(
			array("label" => "Cannot be counterattacked during its turn", "stat" => "NoCounter", "value" => "1")
		),
	],
	[
		"id"              => 146,
		"tags"            => "heroonly",
		"title"           => "Hero's Armament: A Light in the Darkness",
		"grade"           => 3,
		"rank"            => "ss",
		"weapon"          => "mace",
		"armor"           => "armor",
		"accessory"       => "brooch",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2_label"   => "Decrease Crit Rate by 800 for all enemies in the dungeon",
		"effect3_label"   => "Increase its wearer's Attack by 40% and decreases skill MP cost by 20% for 3 turns",
		"condition_label" => "Luminous only",
		"condition"       => "name=Luminous",
		"effect2"   => array(
			array("label" => "Decrease Crit Rate by 800 for all enemies in the dungeon", "stat" => "Enemy Crit Rate", "value" => "800", "curse"),
		),
		"effect3" => array(
			array("label" => "Increase its wearer's Attack by 40%", "stat" => "Attack%", "value" => "40", "condition" => "IG"),
			array("label" => "Decreases skill MP cost by 20%", "stat" => "MP Cost%", "value" => "-20", "condition" => "IG"),
		),
	],
	[
		"id"              => 147,
		"tags"            => "heroonly",
		"title"           => "Hero's Armament: The Forgotten One",
		"grade"           => 3,
		"rank"            => "ss",
		"weapon"          => "mace",
		"armor"           => "armor",
		"accessory"       => "brooch",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2_label"   => "Increase Defense by 10%",
		"effect3_label"   => "Increase its wearer's Attack by 30% and Crit Rate by 1000 for 4 turns",
		"condition_label" => "Eunwol only",
		"condition"       => "name=Eunwol",
		"effect2"   => array(
			array("label" => "Increase Defense by 10%", "stat" => "Defense%", "value" => "10"),
		),
		"effect3" => array(
			array("label" => "Increase its wearer's Attack by 30%", "stat" => "Attack%", "value" => "30", "condition" => "IG"),
			array("label" => "Increase Crit Rate by 1000", "stat" => "Crit Rate", "value" => "1000", "condition" => "IG"),
		),
	],
	[
		"id"              => 148,
		"tags"            => "heroonly",
		"title"           => "Magic Knight's Class",
		"grade"           => 3,
		"rank"            => "s",
		"weapon"          => "sword",
		"armor"           => "armor",
		"accessory"       => "ring",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2_label"   => "Increase Defense by 450 for all allies in the dungeon",
		"effect3_label"   => "Increase 1500 HP for all allies in the dungeon",
		"condition_label" => "Serphina only",
		"condition"       => "name=Serphina",
		"effect2"   => array(
			array("label" => "Increase Defense by 450 for all allies in the dungeon", "stat" => "Defense", "value" => "450", "aura"),
		),
		"effect3" => array(
			array("label" => "Increase 1500 HP for all allies in the dungeon", "stat" => "HP", "value" => "1500", "aura"),
		),
	],
	[
		"id"              => 149,
		"tags"            => "heroonly",
		"title"           => "Cat Sidhe Got You!",
		"grade"           => 3,
		"rank"            => "ss",
		"weapon"          => "mace",
		"armor"           => "shield",
		"accessory"       => "necklace",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2_label"   => "Ignore enemy Defense by 1000",
		"effect3_label"   => "Increase wearer's Attack by 30% and Dodge by 1600 for 4 turns",
		"condition_label" => "Cat Sidhe only",
		"condition"       => "name=Cat Sidhe",
		"effect2"   => array(
			array("label" => "Ignore enemy Defense by 1000", "stat" => "Ignore Defense", "value" => "1000"),
		),
		"effect3" => array(
			array("label" => "Increase Attack by 30%", "stat" => "Attack%", "value" => "30", "condition" => "IG"),
			array("label" => "Increase Dodge by 1600", "stat" => "Dodge", "value" => "1600", "condition" => "IG"),
		),
	],
	[
		"id"              => 150,
		"tags"            => "",
		"title"           => "Unassaillable",
		"grade"           => 1,
		"rank"            => "ss",
		"weapon"          => "sword",
		"armor"           => "shield",
		"accessory"       => "ring",
		"weapon_image"    => "1-unassaillable.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2_label"   => "Increases chance of being attacked by enemies by 40%",
		"effect3_label"   => "Increase Max HP by 8%. Immunity to push/pull effect",
		"condition_label" => "Defense-type character only",
		"condition"       => "type=Defense",
		"effect2"   => array(
			array("label" => "Increases chance of being attacked by enemies by 40%", "stat" => "Aggro%", "value" => "40"),
		),
		"effect3" => array(
			array("label" => "Increase Max HP by 8%", "stat" => "HP%", "value" => "8"),
			array("label" => "Immunity to push effect", "stat" => "Push immunity", "value" => "1"),
			array("label" => "Immunity to pull effect", "stat" => "Pull immunity", "value" => "1"),
		),
	],
	[
		"id"              => 151,
		"tags"            => "",
		"title"           => "Iron Fist from the East",
		"grade"           => 1,
		"rank"            => "ss",
		"weapon"          => "mace",
		"armor"           => "armor",
		"accessory"       => "ring",
		"weapon_image"    => "1-iron.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2_label"   => "Increase Critical Damage by 5% and Counter Rate by 5000",
		"effect3_label"   => "Increase Attack by 8% and Counter Damage by 40%",
		"condition_label" => "",
		"condition"       => "",
		"effect2"   => array(
			array("label" => "Increase Critical Damage by 5%", "stat" => "Critical Damage%", "value" => "5"),
			array("label" => "Increase Counter Rate by 5000", "stat" => "Counter Rate", "value" => "5000"),
		),
		"effect3" => array(
			array("label" => "Increase Attack by 8%", "stat" => "Attack%", "value" => "8"),
			array("label" => "Increase Counter Damage by 40%", "stat" => "Counter Damage", "value" => "4000"),
		),
	],
	[
		"id"              => 152,
		"tags"            => "",
		"title"           => "The Immortal",
		"grade"           => 2,
		"rank"            => "ss",
		"weapon"          => "sword",
		"armor"           => "armor",
		"accessory"       => "brooch",
		"weapon_image"    => "2-immortal.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2_label"   => "Can move through enemies",
		"effect3_label"   => "Increases Movement by 1. Increase Direction Strategy Advantage by 9%",
		"condition_label" => "",
		"condition"       => "",
		"effect2"   => array(
			array("label" => "Can move through enemies", "stat" => "Unit Walking", "value" => "1"),
		),
		"effect3" => array(
			array("label" => "Increases Movement by 1", "stat" => "Movement", "value" => "1"),
			array("label" => "Increase Direction Strategy Advantage by 9%", "stat" => "BonusDirection%", "value" => "9"),
		),
	],
	[
		"id"              => 153,
		"tags"            => "",
		"title"           => "Poker-faced Gambler",
		"grade"           => 2,
		"rank"            => "ss",
		"weapon"          => "bow",
		"armor"           => "armor",
		"accessory"       => "ring",
		"weapon_image"    => "2-poker.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2_label"   => "When attacking, this character creates a 40% chance of increasing its own Defense by 2400 for 1 turn, and a 5% chance of ignoring 1100 enemy Defense for 1 turn",
		"effect3_label"   => "When attacking, this character creates a 40% chance of increasing its own Attack by 40% for 1 turn, and a 5% chance of increasing its Critical Damage by 10% for 1 turn",
		"condition_label" => "Activates every time using an active skill on an enemy",
		"condition"       => "IG",
		"effect2"   => array(
			array("label" => "Increase its own Defense by 2400 for 1 turn", "stat" => "Defense", "value" => "2400"),
			array("label" => "Increase Defense ignore by 1100 for 1 turn", "stat" => "Ignore Defense", "value" => "1100"),
		),
		"effect3" => array(
			array("label" => "Increase its own Attack by 40% for 1 turn", "stat" => "Attack%", "value" => "40"),
			array("label" => "Increase its Critical Damage by 10% for 1 turn", "stat" => "Critical Damage%", "value" => "10"),
		),
	],
	[
		"id"              => 154,
		"tags"            => "",
		"title"           => "Vermillion Bird's Red Bloodline",
		"grade"           => 3,
		"rank"            => "ss",
		"weapon"          => "mace",
		"armor"           => "armor",
		"accessory"       => "brooch",
		"weapon_image"    => "3-vermillion.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2_label"   => "Increases the wearer's Skill Final Power by 30% for 3 turns",
		"effect3_label"   => "Revives with 5% HP (1 time only)",
		"condition_label" => "Attack-type character only",
		"condition"       => "type=Attack",
		"effect2"   => array(
			array("label" => "Increases the wearer's Skill Final Power by 30% for 3 turns", "stat" => "FSP%", "value" => "30", "condition" => "IG"),
		),
		"effect3" => array(
			array("label" => "Revives with 5% HP (1 time only)", "stat" => "Revive5%HP", "value" => "1"),
		),
	],
	[
		"id"              => 155,
		"tags"            => "",
		"title"           => "Coocoo Rangers, Go!",
		"grade"           => 3,
		"rank"            => "ss",
		"weapon"          => "bow",
		"armor"           => "boots",
		"accessory"       => "necklace",
		"weapon_image"    => "3-coocoo.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2_label"   => "Increases Defense by 1500 and all allies' Direction Strategy Power by 5% (not stackable)",
		"effect3_label"   => "Increases Attack by 25% and all allies' Attack by 5% (not stackable)",
		"condition_label" => "",
		"condition"       => "",
		"effect2"   => array(
			array("label" => "Increases Defense by 1500", "stat" => "Defense", "value" => "1500"),
			array("label" => "Increases all allies' Direction Strategy Power by 5%", "stat" => "BonusDirection%", "value" => "5", "aura", "unstackable"),
		),
		"effect3" => array(
			array("label" => "Increases Attack by 25%", "stat" => "Attack%", "value" => "25"),
			array("label" => "Increases all allies' Attack by 5%", "stat" => "Attack%", "value" => "5", "aura", "unstackable"),
		),
	],
	[
		"id"              => 156,
		"tags"            => "",
		"title"           => "Elemental Knight",
		"grade"           => 4,
		"rank"            => "ss",
		"weapon"          => "sword",
		"armor"           => "armor",
		"accessory"       => "ring",
		"weapon_image"    => "4-ek.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2_label"   => "Increases Attack by 4200",
		"effect3_label"   => "Increases HP by 10150 and Resistance by 40%",
		"condition_label" => "",
		"condition"       => "",
		"effect2"   => array(
			array("label" => "Increases Attack by 4200", "stat" => "Attack", "value" => "4200"),
		),
		"effect3" => array(
			array("label" => "Increases HP by 10150", "stat" => "HP", "value" => "10150"),
			array("label" => "Increases Resistance by 40%", "stat" => "Resistance", "value" => "40"),
		),
	],
	[
		"id"              => 157,
		"tags"            => "",
		"title"           => "Wisdom from the East",
		"grade"           => 4,
		"rank"            => "ss",
		"weapon"          => "sword",
		"armor"           => "armor",
		"accessory"       => "necklace",
		"weapon_image"    => "4-wisdom.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2_label"   => "Decreases incoming Critical Damage by 10% for all allies in 2 tiles around the wearer",
		"effect3_label"   => "Increases Attack by 2100 for all allies",
		"condition_label" => "",
		"condition"       => "",
		"effect2"   => array(
			array("label" => "Decreases incoming Critical Damage by 10% for all allies in 2 tiles around the wearer", "stat" => "Critical Damage Taken%", "value" => "10", "aura"),
		),
		"effect3" => array(
			array("label" => "Increases Attack by 2100 for all allies", "stat" => "Attack", "value" => "2100", "aura"),
		),
	],
	[
		"id"              => 158,
		"tags"            => "",
		"title"           => "Wind Walker",
		"grade"           => 4,
		"rank"            => "ss",
		"weapon"          => "mace",
		"armor"           => "boots",
		"accessory"       => "ring",
		"weapon_image"    => "4-wind.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2_label"   => "Increases Movement by 1",
		"effect3_label"   => "Increases Skill Final Power by 18%",
		"condition_label" => "Balance-type character only",
		"condition"       => "type=Balance",
		"effect2"   => array(
			array("label" => "Increases Movement by 1", "stat" => "Movement", "value" => "1"),
		),
		"effect3" => array(
			array("label" => "Increases Skill Final Power by 18%", "stat" => "FSP%", "value" => "18"),
		),
	],
	[
		"id"              => 159,
		"tags"            => "",
		"title"           => "Stone Monkey's Treasure",
		"grade"           => 5,
		"rank"            => "ss",
		"weapon"          => "mace",
		"armor"           => "shield",
		"accessory"       => "ring",
		"weapon_image"    => "5-monkey.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2_label"   => "Increases the wearer's Defense by 5% of its current HP (up to +6000)",
		"effect3_label"   => "Increases the wearer's Attack by 5% of its current HP (up to +10000)",
		"condition_label" => "",
		"condition"       => "",
		"effect2"   => array(
			array("label" => "Increases the wearer's Defense by 2% of its current HP (up to +4000)", "stat" => "HPtoDef%", "value" => "2", "max" => "4000"),
		),
		"effect3" => array(
			array("label" => "Increases the wearer's Attack by 2% of its current HP (up to +10000)", "stat" => "HPtoAttack%", "value" => "2", "max" => "10000"),
		),
	],
	[
		"id"              => 160,
		"tags"            => "",
		"title"           => "Unprecedented",
		"grade"           => 5,
		"rank"            => "ss",
		"weapon"          => "mace",
		"armor"           => "armor",
		"accessory"       => "brooch",
		"weapon_image"    => "5-unpre.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2_label"   => "Cast a shield at the beginning of each turn to decrease all damage by 10% once",
		"effect3_label"   => "Increases Attack by 10%. At the beginning of each turn this skill creates a 40% chance of casting Double Attack (...) for 35% damage (Not stackable)",
		"condition_label" => "",
		"condition"       => "",
		"effect2"   => array(
			array("label" => "Cast a shield at the beginning of each turn to decrease all damage by 10% once", "stat" => "Damage Taken% Once", "value" => "10", "condition" => "IG"),
		),
		"effect3" => array(
			array("label" => "Increases Attack by 10%", "stat" => "Attack%", "value" => "10"),
			array("label" => "40% chance of casting Double Attack 35% damage", "stat" => "Double Attack 35%", "value" => "1", "unstackable"),
		),
	],
	[
		"id"              => 161,
		"tags"            => "",
		"title"           => "Prisoner of Avici Hell",
		"grade"           => 5,
		"rank"            => "ss",
		"weapon"          => "sword",
		"armor"           => "armor",
		"accessory"       => "ring",
		"weapon_image"    => "5-avicii.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2_label"   => "Increase HP by 120% and ignore 2500 enemy Defense",
		"effect3_label"   => "Increase Attack by 100% and decrease incoming Critical Damage by 25%",
		"condition_label" => "Activated only if you are left standing",
		"condition"       => "IG",
		"effect2"   => array(
			array("label" => "Increase HP by 120%", "stat" => "HP%", "value" => "120"),
			array("label" => "Ignore 2500 enemy Defense", "stat" => "Ignore Defense", "value" => "2500"),
		),
		"effect3" => array(
			array("label" => "Increase Attack by 100%", "stat" => "Attack%", "value" => "100"),
			array("label" => "Decrease incoming Critical Damage by 25%", "stat" => "Critical Damage Taken%", "value" => "25"),
		),
	],
	[
		"id"              => 162,
		"tags"            => "",
		"title"           => "Primitive Vital Force",
		"grade"           => 1,
		"rank"            => "s",
		"weapon"          => "sword",
		"armor"           => "armor",
		"accessory"       => "ring",
		"weapon_image"    => "1-primitive.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2_label"   => "Increase Attack by 250 (up to 1000) for every tile it travels",
		"effect3_label"   => "Recovers 1% Max HP (up to 10%) for every tile it travels",
		"condition_label" => "",
		"condition"       => "",
		"effect2"   => array(
			array("label" => "Increase Attack by 250 (up to 1000) for every tile it travels", "stat" => "Attack", "value" => "250", "max" => "1000", "condition" => "IG"),
		),
		"effect3" => array(
			array("label" => "Recovers 1% Max HP (up to 10%) for every tile it travels", "stat" => "HP Recovery%", "value" => "1", "max" => "10", "condition" => "IG"),
		),
	],
	[
		"id"              => 163,
		"tags"            => "",
		"title"           => "Dreamy as Dreams",
		"grade"           => 2,
		"rank"            => "s",
		"weapon"          => "bow",
		"armor"           => "armor",
		"accessory"       => "brooch",
		"weapon_image"    => "2-sleep.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2_label"   => "Increase MP by 35",
		"effect3_label"   => "Sleep Immunity. Decrease Skill MP Cost by 5%",
		"condition_label" => "",
		"condition"       => "",
		"effect2"   => array(
			array("label" => "Increase MP by 35", "stat" => "MP", "value" => "35"),
		),
		"effect3" => array(
			array("label" => "Sleep Immunity", "stat" => "Sleep Immunity", "value" => "1"),
			array("label" => "Decrease Skill MP Cost by 5%", "stat" => "MP Cost%", "value" => "-5"),
		),
	],
	[
		"id"              => 164,
		"tags"            => "",
		"title"           => "Hail to the Lord!",
		"grade"           => 3,
		"rank"            => "s",
		"weapon"          => "mace",
		"armor"           => "shield",
		"accessory"       => "ring",
		"weapon_image"    => "3-hail.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2_label"   => "Increase Final Skill Power by 3% for all allies (Not stackable)",
		"effect3_label"   => "Increase all allies' Attack by 800 and Guild Raid clearance score by 8% (Not stackable)",
		"condition_label" => "Activated only in Dark Fortress",
		"condition"       => "IG",
		"effect2"   => array(
			array("label" => "Increase Final Skill Power by 3% for all allies (Not stackable)", "stat" => "FSP%", "value" => "3", "aura", "unstackable"),
		),
		"effect3" => array(
			array("label" => "Increase all allies' Attack by 800", "stat" => "Attack", "value" => "800", "aura", "unstackable"),
			array("label" => "Guild Raid clearance score by 8% (Not stackable)", "stat" => "GR Score%", "value" => "8", "unstackable"),
		),
	],
	[
		"id"              => 165,
		"tags"            => "",
		"title"           => "Ancient Witchdoctor",
		"grade"           => 4,
		"rank"            => "s",
		"weapon"          => "sword",
		"armor"           => "boots",
		"accessory"       => "brooch",
		"weapon_image"    => "4-taunt.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2_label"   => "Recovers 10% Max HP every time an ally or enemy dies",
		"effect3_label"   => "Immunity to taunt",
		"condition_label" => "",
		"condition"       => "",
		"effect2"   => array(
			array("label" => "Recovers 10% Max HP every time an ally or enemy dies", "stat" => "HP Recovery% Per Death", "value" => "10", "condition" => "IG"),
		),
		"effect3" => array(
			array("label" => "Immunity to taunt", "stat" => "Taunt Immunity", "value" => "1"),
		),
	],
	[
		"id"              => 166,
		"tags"            => "",
		"title"           => "Tenacious Explorer",
		"grade"           => 5,
		"rank"            => "s",
		"weapon"          => "bow",
		"armor"           => "boots",
		"accessory"       => "ring",
		"weapon_image"    => "5-explorer.png",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2_label"   => "Incoming damage increases Defense by 1100 for 1 turn",
		"effect3_label"   => "Incoming damage makes Swamp and Water terrain accessible for 1 turn",
		"condition_label" => "Activated each time character is attacked",
		"condition"       => "IG",
		"effect2"   => array(
			array("label" => "Incoming damage increases Defense by 1100 for 1 turn", "stat" => "Defense", "value" => "1100"),
		),
		"effect3" => array(
			array("label" => "Incoming damage makes Swamp terrain accessible for 1 turn", "stat" => "Terrain Swamp", "value" => "1"),
			array("label" => "Incoming damage makes Water terrain accessible for 1 turn", "stat" => "Terrain Water", "value" => "1"),
		),
	],
	[
		"id"              => 167,
		"tags"            => "heroonly",
		"title"           => "Black Dragon's Armament IV",
		"grade"           => 5,
		"rank"            => "s",
		"weapon"          => "mace",
		"armor"           => "shield",
		"accessory"       => "necklace",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2_label"   => "Increase HP by 3250",
		"effect3_label"   => "Increase Crit Rate by 650",
		"condition_label" => "Thanatos only",
		"condition"       => "name=Thanatos",
		"effect2"   => array(
			array("label" => "Increase HP by 3250", "stat" => "HP", "value" => "3250"),
		),
		"effect3" => array(
			array("label" => "Increase Crit Rate by 650", "stat" => "Crit Rate", "value" => "650"),
		),
	],
	[
		"id"              => 168,
		"tags"            => "",
		"title"           => "Toxicologist",
		"grade"           => 1,
		"rank"            => "a",
		"weapon"          => "sword",
		"armor"           => "armor",
		"accessory"       => "brooch",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2_label"   => "Increase Attack by 450",
		"effect3_label"   => "Gains a Poison terrain bonus",
		"condition_label" => "",
		"condition"       => "",
		"effect2"   => array(
			array("label" => "Increase Attack by 450", "stat" => "Attack", "value" => "450"),
		),
		"effect3" => array(
			array("label" => "Gains a Poison terrain bonus", "stat" => "Terrain Poison", "value" => "1"),
		),
	],
	[
		"id"              => 169,
		"tags"            => "",
		"title"           => "Veteran Mountain Climber",
		"grade"           => 2,
		"rank"            => "a",
		"weapon"          => "mace",
		"armor"           => "boots",
		"accessory"       => "ring",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2_label"   => "Increases all allies' Direction Strategy Power by 1%",
		"effect3_label"   => "Increases all allies' Advantage by 1%",
		"condition_label" => "",
		"condition"       => "",
		"effect2"   => array(
			array("label" => "Increases all allies' Direction Strategy Power by 1%", "stat" => "BonusDirection%", "value" => "1", "aura"),
		),
		"effect3" => array(
			array("label" => "Increases all allies' Advantage by 1%", "stat" => "BonusAdvantage%", "value" => "1", "aura"),
		),
	],
	[
		"id"              => 170,
		"tags"            => "heroonly",
		"title"           => "Black Dragon's Armament II",
		"grade"           => 2,
		"rank"            => "a",
		"weapon"          => "mace",
		"armor"           => "shield",
		"accessory"       => "necklace",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2_label"   => "Increases Dodge by 900",
		"effect3_label"   => "Increases Critical Damage by 5%",
		"condition_label" => "Thanatos only",
		"condition"       => "name=Thanatos",
		"effect2"   => array(
			array("label" => "Increases Dodge by 900", "stat" => "Dodge", "value" => "900"),
		),
		"effect3" => array(
			array("label" => "Increases Critical Damage by 5%", "stat" => "Critical Damage%", "value" => "5"),
		),
	],
	[
		"id"              => 171,
		"tags"            => "",
		"title"           => "Expert Rune Collector",
		"grade"           => 3,
		"rank"            => "a",
		"weapon"          => "sword",
		"armor"           => "shield",
		"accessory"       => "ring",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2_label"   => "Increases Direction Strategy Power by 12%",
		"effect3_label"   => "Increases Resistance by 40%",
		"condition_label" => "Activated only in Pit of Nephtys",
		"condition"       => "IG",
		"effect2"   => array(
			array("label" => "Increases Direction Strategy Power by 12%", "stat" => "BonusDirection%", "value" => "12"),
		),
		"effect3" => array(
			array("label" => "Increases Resistance by 40%", "stat" => "Resistance", "value" => "40"),
		),
	],
	[
		"id"              => 172,
		"tags"            => "",
		"title"           => "Explorer Captain",
		"grade"           => 4,
		"rank"            => "a",
		"weapon"          => "mace",
		"armor"           => "boots",
		"accessory"       => "ring",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2_label"   => "Increases Attack by 900 for all allies",
		"effect3_label"   => "Instant Death Terrain Accessible",
		"condition_label" => "Activated as long as there are 4 or more allies alive",
		"condition"       => "IG",
		"effect2"   => array(
			array("label" => "Increases Attack by 900 for all allies", "stat" => "Attack", "value" => "900", "aura"),
		),
		"effect3" => array(
			array("label" => "Instant Death Terrain Accessible", "stat" => "Terrain Instant Death", "value" => "1"),
		),
	],
	[
		"id"              => 173,
		"tags"            => "",
		"title"           => "Anonymous Sniper",
		"grade"           => 5,
		"rank"            => "a",
		"weapon"          => "bow",
		"armor"           => "boots",
		"accessory"       => "ring",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2_label"   => "Increases Crit Rate by 800",
		"effect3_label"   => "Increases the wearer's Attack by 10% when attacking enemies 3 or more tiles away",
		"condition_label" => "",
		"condition"       => "",
		"effect2"   => array(
			array("label" => "Increases Crit Rate by 800", "stat" => "Crit Rate", "value" => "800"),
		),
		"effect3" => array(
			array("label" => "Increases the wearer's Attack by 10% when attacking enemies 3 or more tiles away", "stat" => "Attack%", "value" => "10", "condition" => "IG"),
		),
	],
	[
		"id"              => 174,
		"tags"            => "heroonly",
		"title"           => "Defender's Burden",
		"grade"           => 3,
		"rank"            => "ss",
		"weapon"          => "sword",
		"armor"           => "shield",
		"accessory"       => "ring",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2_label"   => "Casts the 30% Resistance effect when HP becomes 80% or lower",
		"effect3_label"   => "Increases Attack by 60% and Max HP by 25% for the first 3 turns of battle",
		"condition_label" => "Ildo only",
		"condition"       => "name=Ildo",
		"effect2"   => array(
			array("label" => "Casts the 30% Resistance effect when HP becomes 80% or lower", "stat" => "Resistance", "value" => "30", "condition" => "IG"),
		),
		"effect3" => array(
			array("label" => "Increases Attack by 60% for the first 3 turns of battle", "stat" => "Attack%", "value" => "60", "condition" => "IG"),
			array("label" => "Increases Max HP by 25% for the first 3 turns of battle", "stat" => "HP%", "value" => "25", "condition" => "IG"),
		),
	],
	[
		"id"              => 175,
		"tags"            => "heroonly",
		"title"           => "Finishing Touch",
		"grade"           => 3,
		"rank"            => "ss",
		"weapon"          => "mace",
		"armor"           => "armor",
		"accessory"       => "brooch",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2_label"   => "Increases Attack by 10% for every enemy it attacks",
		"effect3_label"   => "Increases Attack by 60% and decreases skill MP cost by 30% for the first 3 turns of battle",
		"condition_label" => "Taehwa only",
		"condition"       => "name=Taehwa",
		"effect2"   => array(
			array("label" => "Increases Attack by 10% for every enemy it attacks", "stat" => "Attack%", "value" => "10", "condition" => "IG"),
		),
		"effect3" => array(
			array("label" => "Increases Attack by 60% for the first 3 turns of battle", "stat" => "Attack%", "value" => "60", "condition" => "IG"),
			array("label" => "Decreases skill MP cost by 30% for the first 3 turns of battle", "stat" => "MP Cost%", "value" => "-30", "condition" => "IG"),
		),
	],
	[
		"id"              => 176,
		"tags"            => "heroonly",
		"title"           => "Fiery Dancer",
		"grade"           => 3,
		"rank"            => "ss",
		"weapon"          => "sword",
		"armor"           => "armor",
		"accessory"       => "brooch",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2_label"   => "Decreases all incoming Damage by 10% while on Lava Terrain",
		"effect3_label"   => "Increases Attack by 60% and increases Critical Damage by 10% for the first 3 turns of battle",
		"condition_label" => "Hongyeom only",
		"condition"       => "name=Hongyeom",
		"effect2"   => array(
			array("label" => "Decreases all incoming Damage by 10% while on Lava Terrain", "stat" => "Damage Taken%", "value" => "10", "condition" => "IG"),
		),
		"effect3" => array(
			array("label" => "Increases Attack by 60% for the first 3 turns of battle", "stat" => "Attack%", "value" => "60", "condition" => "IG"),
			array("label" => "Increases Critical Damage by 10% for the first 3 turns of battle", "stat" => "Critical Damage%", "value" => "10", "condition" => "IG"),
		),
	],
	[
		"id"              => 177,
		"tags"            => "heroonly",
		"title"           => "Doom Bringer",
		"grade"           => 3,
		"rank"            => "ss",
		"weapon"          => "sword",
		"armor"           => "armor",
		"accessory"       => "ring",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2_label"   => "Increases Attack by 2200 for 1 turn per kill",
		"effect3_label"   => "Increases Attack by 50% and Movement by 1 for the first 3 turns of battle",
		"condition_label" => "Thanatos only",
		"condition"       => "name=Thanatos",
		"effect2"   => array(
			array("label" => "Increases Attack by 2200 for 1 turn per kill", "stat" => "Attack", "value" => "2200", "condition" => "IG"),
		),
		"effect3" => array(
			array("label" => "Increases Attack by 50% for the first 3 turns of battle", "stat" => "Attack%", "value" => "50", "condition" => "IG"),
			array("label" => "Increases Movement by 1 for the first 3 turns of battle", "stat" => "Movement", "value" => "1", "condition" => "IG"),
		),
	],
	[
		"id"              => 178,
		"tags"            => "heroonly",
		"title"           => "Young, But Not To Be Underestimated",
		"grade"           => 3,
		"rank"            => "ss",
		"weapon"          => "bow",
		"armor"           => "armor",
		"accessory"       => "ring",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2_label"   => "Decreases incoming damage from Rock enemies by 8%",
		"effect3_label"   => "Increases Attack by 60% and ignore 1200 enemy Defense for the first 3 turns of battle",
		"condition_label" => "Ryeogang only",
		"condition"       => "name=Ryeogang",
		"effect2"   => array(
			array("label" => "Decreases incoming damage from Rock enemies by 8%", "stat" => "Damage Taken From Rock%", "value" => "8", "condition" => "IG"),
		),
		"effect3" => array(
			array("label" => "Increases Attack by 60% for the first 3 turns of battle", "stat" => "Attack%", "value" => "60", "condition" => "IG"),
			array("label" => "Ignore 1200 enemy Defense for the first 3 turns of battle", "stat" => "Ignore Defense", "value" => "1200", "condition" => "IG"),
		),
	],
	[
	    "id"              => 179,
	    "tags"            => "heroonly",
	    "title"           => "Black Dragon's Armament I",
	    "grade"           => 1,
	    "rank"            => "b",
	    "weapon"          => "mace",
	    "armor"           => "shield",
	    "accessory"       => "necklace",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Attack by 250",
	    "effect3_label"   => "Increases Attack by 550",
		"condition_label" => "Thanatos only",
		"condition"       => "name=Thanatos",
	    "effect2"   => array(
	        array("label" => "Increases Attack by 250", "stat" => "Attack", "value" => "250"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Attack by 550", "stat" => "Attack", "value" => "550"),
	    ),
	],
	[
	    "id"              => 180,
	    "tags"            => "",
	    "title"           => "Spirited Recruit's Combat Equipment",
	    "grade"           => 1,
	    "rank"            => "b",
	    "weapon"          => "bow",
	    "armor"           => "armor",
	    "accessory"       => "brooch",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases HP by 290",
	    "effect3_label"   => "Increases HP by 436",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Increases HP by 290", "stat" => "HP", "value" => "290"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases HP by 436", "stat" => "HP", "value" => "436"),
	    ),
	],
	[
	    "id"              => 181,
	    "tags"            => "",
	    "title"           => "Lucid Recruit's Combat Equipment",
	    "grade"           => 1,
	    "rank"            => "b",
	    "weapon"          => "sword",
	    "armor"           => "armor",
	    "accessory"       => "necklace",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases MP by 6",
	    "effect3_label"   => "Increases MP by 9",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Increases MP by 6", "stat" => "MP", "value" => "6"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases MP by 9", "stat" => "MP", "value" => "9"),
	    ),
	],
	[
	    "id"              => 182,
	    "tags"            => "",
	    "title"           => "Tenacious Recruit's Combat Equipment",
	    "grade"           => 1,
	    "rank"            => "b",
	    "weapon"          => "sword",
	    "armor"           => "boots",
	    "accessory"       => "necklace",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Attack by 76",
	    "effect3_label"   => "Increases Attack by 114",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Increases Attack by 76", "stat" => "Attack", "value" => "76"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Attack by 114", "stat" => "Attack", "value" => "114"),
	    ),
	],
	[
	    "id"              => 183,
	    "tags"            => "",
	    "title"           => "Solid Recruit's Combat Equipment",
	    "grade"           => 1,
	    "rank"            => "b",
	    "weapon"          => "mace",
	    "armor"           => "shield",
	    "accessory"       => "brooch",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Defense by 240",
	    "effect3_label"   => "Increases Defense by 360",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Increases Defense by 240", "stat" => "Defense", "value" => "240"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Defense by 360", "stat" => "Defense", "value" => "360"),
	    ),
	],
	[
	    "id"              => 184,
	    "tags"            => "",
	    "title"           => "Keen Recruit's Combat Equipment",
	    "grade"           => 1,
	    "rank"            => "b",
	    "weapon"          => "bow",
	    "armor"           => "boots",
	    "accessory"       => "ring",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Crit Rate by 160",
	    "effect3_label"   => "Increases Crit Rate by 240",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Increases Crit Rate by 160", "stat" => "Crit Rate", "value" => "160"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Crit Rate by 240", "stat" => "Crit Rate", "value" => "240"),
	    ),
	],
	[
	    "id"              => 185,
	    "tags"            => "",
	    "title"           => "Agile Recruit's Combat Equipment",
	    "grade"           => 1,
	    "rank"            => "b",
	    "weapon"          => "bow",
	    "armor"           => "boots",
	    "accessory"       => "brooch",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Dodge by 160",
	    "effect3_label"   => "Increases Dodge by 240",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Increases Dodge by 160", "stat" => "Dodge", "value" => "160"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Dodge by 240", "stat" => "Dodge", "value" => "240"),
	    ),
	],
	[
	    "id"              => 186,
	    "tags"            => "",
	    "title"           => "Clearheaded Recruit's Combat Equipment",
	    "grade"           => 1,
	    "rank"            => "b",
	    "weapon"          => "sword",
	    "armor"           => "armor",
	    "accessory"       => "ring",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Hit Rate by 160",
	    "effect3_label"   => "Increases Hit Rate by 240",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Increases Hit Rate by 160", "stat" => "Hit Rate", "value" => "160"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Hit Rate by 240", "stat" => "Hit Rate", "value" => "240"),
	    ),
	],
	[
	    "id"              => 187,
	    "tags"            => "",
	    "title"           => "Dogged Recruit's Combat Equipment",
	    "grade"           => 1,
	    "rank"            => "b",
	    "weapon"          => "mace",
	    "armor"           => "shield",
	    "accessory"       => "necklace",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Counter Rate by 80",
	    "effect3_label"   => "Increases Counter Rate by 120",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Increases Counter Rate by 80", "stat" => "Counter Rate", "value" => "80"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Counter Rate by 120", "stat" => "Counter Rate", "value" => "120"),
	    ),
	],
	[
	    "id"              => 188,
	    "tags"            => "",
	    "title"           => "Vengeful Recruit's Combat Equipment",
	    "grade"           => 1,
	    "rank"            => "b",
	    "weapon"          => "mace",
	    "armor"           => "shield",
	    "accessory"       => "ring",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Counter Damage by 560",
	    "effect3_label"   => "Increases Counter Damage by 840",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Increases Counter Damage by 560", "stat" => "Counter Damage", "value" => "560"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Counter Damage by 840", "stat" => "Counter Damage", "value" => "840"),
	    ),
	],
	[
	    "id"              => 189,
	    "tags"            => "",
	    "title"           => "Spirited Soldier Combat Equipment",
	    "grade"           => 2,
	    "rank"            => "b",
	    "weapon"          => "bow",
	    "armor"           => "armor",
	    "accessory"       => "brooch",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases HP by 702",
	    "effect3_label"   => "Increases HP by 1055",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Increases HP by 702", "stat" => "HP", "value" => "702"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases HP by 1055", "stat" => "HP", "value" => "1055"),
	    ),
	],
	[
	    "id"              => 190,
	    "tags"            => "",
	    "title"           => "Lucid Soldier Combat Equipment",
	    "grade"           => 2,
	    "rank"            => "b",
	    "weapon"          => "sword",
	    "armor"           => "armor",
	    "accessory"       => "necklace",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases MP by 8",
	    "effect3_label"   => "Increases MP by 12",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Increases MP by 8", "stat" => "MP", "value" => "8"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases MP by 12", "stat" => "MP", "value" => "12"),
	    ),
	],
	[
	    "id"              => 191,
	    "tags"            => "",
	    "title"           => "Tenacious Soldier Combat Equipment",
	    "grade"           => 2,
	    "rank"            => "b",
	    "weapon"          => "sword",
	    "armor"           => "boots",
	    "accessory"       => "necklace",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Attack by 180",
	    "effect3_label"   => "Increases Attack by 271",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Increases Attack by 180", "stat" => "Attack", "value" => "180"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Attack by 271", "stat" => "Attack", "value" => "271"),
	    ),
	],
	[
	    "id"              => 192,
	    "tags"            => "",
	    "title"           => "Solid Soldier Combat Equipment",
	    "grade"           => 2,
	    "rank"            => "b",
	    "weapon"          => "mace",
	    "armor"           => "shield",
	    "accessory"       => "brooch",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Defense by 280",
	    "effect3_label"   => "Increases Defense by 420",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Increases Defense by 280", "stat" => "Defense", "value" => "280"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Defense by 420", "stat" => "Defense", "value" => "420"),
	    ),
	],
	[
	    "id"              => 193,
	    "tags"            => "",
	    "title"           => "Keen Soldier Combat Equipment",
	    "grade"           => 2,
	    "rank"            => "b",
	    "weapon"          => "bow",
	    "armor"           => "boots",
	    "accessory"       => "ring",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Crit Rate by 200",
	    "effect3_label"   => "Increases Crit Rate by 300",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Increases Crit Rate by 200", "stat" => "Crit Rate", "value" => "200"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Crit Rate by 300", "stat" => "Crit Rate", "value" => "300"),
	    ),
	],
	[
	    "id"              => 194,
	    "tags"            => "",
	    "title"           => "Agile Soldier Combat Equipment",
	    "grade"           => 2,
	    "rank"            => "b",
	    "weapon"          => "bow",
	    "armor"           => "boots",
	    "accessory"       => "brooch",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Dodge by 200",
	    "effect3_label"   => "Increases Dodge by 300",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Increases Dodge by 200", "stat" => "Dodge", "value" => "200"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Dodge by 300", "stat" => "Dodge", "value" => "300"),
	    ),
	],
	[
	    "id"              => 195,
	    "tags"            => "",
	    "title"           => "Clearheaded Soldier Combat Equipment",
	    "grade"           => 2,
	    "rank"            => "b",
	    "weapon"          => "sword",
	    "armor"           => "armor",
	    "accessory"       => "ring",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Hit Rate by 200",
	    "effect3_label"   => "Increases Hit Rate by 300",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Increases Hit Rate by 200", "stat" => "Hit Rate", "value" => "200"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Hit Rate by 300", "stat" => "Hit Rate", "value" => "300"),
	    ),
	],
	[
	    "id"              => 196,
	    "tags"            => "",
	    "title"           => "Dogged Soldier Combat Equipment",
	    "grade"           => 2,
	    "rank"            => "b",
	    "weapon"          => "mace",
	    "armor"           => "shield",
	    "accessory"       => "necklace",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Counter Rate by 100",
	    "effect3_label"   => "Increases Counter Rate by 150",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Increases Counter Rate by 100", "stat" => "Counter Rate", "value" => "100"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Counter Rate by 150", "stat" => "Counter Rate", "value" => "150"),
	    ),
	],
	[
	    "id"              => 197,
	    "tags"            => "",
	    "title"           => "Vengeful Soldier Combat Equipment",
	    "grade"           => 2,
	    "rank"            => "b",
	    "weapon"          => "mace",
	    "armor"           => "shield",
	    "accessory"       => "ring",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Counter Damage by 580",
	    "effect3_label"   => "Increases Counter Damage by 870",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Increases Counter Damage by 580", "stat" => "Counter Damage", "value" => "580"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Counter Damage by 870", "stat" => "Counter Damage", "value" => "870"),
	    ),
	],
	[
	    "id"              => 198,
	    "tags"            => "",
	    "title"           => "Spirited Cadet Combat Equipment",
	    "grade"           => 3,
	    "rank"            => "b",
	    "weapon"          => "bow",
	    "armor"           => "armor",
	    "accessory"       => "brooch",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases HP by 1225",
	    "effect3_label"   => "Increases HP by 1838",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Increases HP by 1225", "stat" => "HP", "value" => "1225"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases HP by 1838", "stat" => "HP", "value" => "1838"),
	    ),
	],
	[
	    "id"              => 199,
	    "tags"            => "",
	    "title"           => "Lucid Cadet Combat Equipment",
	    "grade"           => 3,
	    "rank"            => "b",
	    "weapon"          => "sword",
	    "armor"           => "armor",
	    "accessory"       => "necklace",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases MP by 10",
	    "effect3_label"   => "Increases MP by 15",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Increases MP by 10", "stat" => "MP", "value" => "10"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases MP by 15", "stat" => "MP", "value" => "15"),
	    ),
	],
	[
	    "id"              => 200,
	    "tags"            => "",
	    "title"           => "Tenacious Cadet Combat Equipment",
	    "grade"           => 3,
	    "rank"            => "b",
	    "weapon"          => "sword",
	    "armor"           => "boots",
	    "accessory"       => "necklace",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Attack by 312",
	    "effect3_label"   => "Increases Attack by 470",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Increases Attack by 312", "stat" => "Attack", "value" => "312"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Attack by 470", "stat" => "Attack", "value" => "470"),
	    ),
	],
	[
	    "id"              => 201,
	    "tags"            => "",
	    "title"           => "Solid Cadet Combat Equipment",
	    "grade"           => 3,
	    "rank"            => "b",
	    "weapon"          => "mace",
	    "armor"           => "shield",
	    "accessory"       => "brooch",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Defense by 320",
	    "effect3_label"   => "Increases Defense by 480",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Increases Defense by 320", "stat" => "Defense", "value" => "320"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Defense by 480", "stat" => "Defense", "value" => "480"),
	    ),
	],
	[
	    "id"              => 202,
	    "tags"            => "",
	    "title"           => "Keen Cadet Combat Equipment",
	    "grade"           => 3,
	    "rank"            => "b",
	    "weapon"          => "bow",
	    "armor"           => "boots",
	    "accessory"       => "ring",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Crit Rate by 240",
	    "effect3_label"   => "Increases Crit Rate by 360",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Increases Crit Rate by 240", "stat" => "Crit Rate", "value" => "240"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Crit Rate by 360", "stat" => "Crit Rate", "value" => "360"),
	    ),
	],
	[
	    "id"              => 203,
	    "tags"            => "",
	    "title"           => "Agile Cadet Combat Equipment",
	    "grade"           => 3,
	    "rank"            => "b",
	    "weapon"          => "bow",
	    "armor"           => "boots",
	    "accessory"       => "brooch",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Dodge by 240",
	    "effect3_label"   => "Increases Dodge by 360",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Increases Dodge by 240", "stat" => "Dodge", "value" => "240"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Dodge by 360", "stat" => "Dodge", "value" => "360"),
	    ),
	],
	[
	    "id"              => 204,
	    "tags"            => "",
	    "title"           => "Clearheaded Cadet Combat Equipment",
	    "grade"           => 3,
	    "rank"            => "b",
	    "weapon"          => "sword",
	    "armor"           => "armor",
	    "accessory"       => "ring",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Hit Rate by 240",
	    "effect3_label"   => "Increases Hit Rate by 360",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Increases Hit Rate by 240", "stat" => "Hit Rate", "value" => "240"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Hit Rate by 360", "stat" => "Hit Rate", "value" => "360"),
	    ),
	],
	[
	    "id"              => 205,
	    "tags"            => "",
	    "title"           => "Dogged Cadet Combat Equipment",
	    "grade"           => 3,
	    "rank"            => "b",
	    "weapon"          => "mace",
	    "armor"           => "shield",
	    "accessory"       => "necklace",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Counter Rate by 120",
	    "effect3_label"   => "Increases Counter Rate by 180",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Increases Counter Rate by 120", "stat" => "Counter Rate", "value" => "120"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Counter Rate by 180", "stat" => "Counter Rate", "value" => "180"),
	    ),
	],
	[
	    "id"              => 206,
	    "tags"            => "",
	    "title"           => "Vengeful Cadet Combat Equipment",
	    "grade"           => 3,
	    "rank"            => "b",
	    "weapon"          => "mace",
	    "armor"           => "shield",
	    "accessory"       => "ring",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Counter Damage by 660",
	    "effect3_label"   => "Increases Counter Damage by 990",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Increases Counter Damage by 660", "stat" => "Counter Damage", "value" => "660"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Counter Damage by 990", "stat" => "Counter Damage", "value" => "990"),
	    ),
	],
	[
	    "id"              => 207,
	    "tags"            => "",
	    "title"           => "Spirited Sergeant Combat Equipment",
	    "grade"           => 4,
	    "rank"            => "b",
	    "weapon"          => "bow",
	    "armor"           => "armor",
	    "accessory"       => "brooch",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases HP by 1904",
	    "effect3_label"   => "Increases HP by 2854",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Increases HP by 1904", "stat" => "HP", "value" => "1904"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases HP by 2854", "stat" => "HP", "value" => "2854"),
	    ),
	],
	[
	    "id"              => 208,
	    "tags"            => "",
	    "title"           => "Lucid Sergeant Combat Equipment",
	    "grade"           => 4,
	    "rank"            => "b",
	    "weapon"          => "sword",
	    "armor"           => "armor",
	    "accessory"       => "necklace",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases MP by 12",
	    "effect3_label"   => "Increases MP by 18",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Increases MP by 12", "stat" => "MP", "value" => "12"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases MP by 18", "stat" => "MP", "value" => "18"),
	    ),
	],
	[
	    "id"              => 209,
	    "tags"            => "",
	    "title"           => "Tenacious Sergeant Combat Equipment",
	    "grade"           => 4,
	    "rank"            => "b",
	    "weapon"          => "sword",
	    "armor"           => "boots",
	    "accessory"       => "necklace",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Attack by 486",
	    "effect3_label"   => "Increases Attack by 728",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Increases Attack by 486", "stat" => "Attack", "value" => "486"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Attack by 728", "stat" => "Attack", "value" => "728"),
	    ),
	],
	[
	    "id"              => 210,
	    "tags"            => "",
	    "title"           => "Solid Sergeant Combat Equipment",
	    "grade"           => 4,
	    "rank"            => "b",
	    "weapon"          => "mace",
	    "armor"           => "shield",
	    "accessory"       => "brooch",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Defense by 360",
	    "effect3_label"   => "Increases Defense by 540",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Increases Defense by 360", "stat" => "Defense", "value" => "360"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Defense by 540", "stat" => "Defense", "value" => "540"),
	    ),
	],
	[
	    "id"              => 211,
	    "tags"            => "",
	    "title"           => "Keen Sergeant Combat Equipment",
	    "grade"           => 4,
	    "rank"            => "b",
	    "weapon"          => "bow",
	    "armor"           => "boots",
	    "accessory"       => "ring",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Crit Rate by 280",
	    "effect3_label"   => "Increases Crit Rate by 420",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Increases Crit Rate by 280", "stat" => "Crit Rate", "value" => "280"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Crit Rate by 420", "stat" => "Crit Rate", "value" => "420"),
	    ),
	],
	[
	    "id"              => 212,
	    "tags"            => "",
	    "title"           => "Agile Sergeant Combat Equipment",
	    "grade"           => 4,
	    "rank"            => "b",
	    "weapon"          => "bow",
	    "armor"           => "boots",
	    "accessory"       => "brooch",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Dodge by 280",
	    "effect3_label"   => "Increases Dodge by 420",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Increases Dodge by 280", "stat" => "Dodge", "value" => "280"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Dodge by 420", "stat" => "Dodge", "value" => "420"),
	    ),
	],
	[
	    "id"              => 213,
	    "tags"            => "",
	    "title"           => "Clearheaded Sergeant Combat Equipment",
	    "grade"           => 4,
	    "rank"            => "b",
	    "weapon"          => "sword",
	    "armor"           => "armor",
	    "accessory"       => "ring",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Hit Rate by 280",
	    "effect3_label"   => "Increases Hit Rate by 420",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Increases Hit Rate by 280", "stat" => "Hit Rate", "value" => "280"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Hit Rate by 420", "stat" => "Hit Rate", "value" => "420"),
	    ),
	],
	[
	    "id"              => 214,
	    "tags"            => "",
	    "title"           => "Dogged Sergeant Combat Equipment",
	    "grade"           => 4,
	    "rank"            => "b",
	    "weapon"          => "mace",
	    "armor"           => "shield",
	    "accessory"       => "necklace",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Counter Rate by 140",
	    "effect3_label"   => "Increases Counter Rate by 210",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Increases Counter Rate by 140", "stat" => "Counter Rate", "value" => "140"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Counter Rate by 210", "stat" => "Counter Rate", "value" => "210"),
	    ),
	],
	[
	    "id"              => 215,
	    "tags"            => "",
	    "title"           => "Vengeful Sergeant Combat Equipment",
	    "grade"           => 4,
	    "rank"            => "b",
	    "weapon"          => "mace",
	    "armor"           => "shield",
	    "accessory"       => "ring",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Counter Damage by 740",
	    "effect3_label"   => "Increases Counter Damage by 1110",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Increases Counter Damage by 740", "stat" => "Counter Damage", "value" => "740"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Counter Damage by 1110", "stat" => "Counter Damage", "value" => "1110"),
	    ),
	],
	[
	    "id"              => 216,
	    "tags"            => "",
	    "title"           => "Spirited Officer Combat Equipment",
	    "grade"           => 5,
	    "rank"            => "b",
	    "weapon"          => "bow",
	    "armor"           => "armor",
	    "accessory"       => "brooch",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases HP by 2818",
	    "effect3_label"   => "Increases HP by 4226",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Increases HP by 2818", "stat" => "HP", "value" => "2818"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases HP by 4226", "stat" => "HP", "value" => "4226"),
	    ),
	],
	[
	    "id"              => 217,
	    "tags"            => "",
	    "title"           => "Lucid Officer Combat Equipment",
	    "grade"           => 5,
	    "rank"            => "b",
	    "weapon"          => "sword",
	    "armor"           => "armor",
	    "accessory"       => "necklace",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases MP by 14",
	    "effect3_label"   => "Increases MP by 21",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Increases MP by 14", "stat" => "MP", "value" => "14"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases MP by 21", "stat" => "MP", "value" => "21"),
	    ),
	],
	[
	    "id"              => 218,
	    "tags"            => "",
	    "title"           => "Tenacious Officer Combat Equipment",
	    "grade"           => 5,
	    "rank"            => "b",
	    "weapon"          => "sword",
	    "armor"           => "boots",
	    "accessory"       => "necklace",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Attack by 720",
	    "effect3_label"   => "Increases Attack by 1078",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Increases Attack by 720", "stat" => "Attack", "value" => "720"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Attack by 1078", "stat" => "Attack", "value" => "1078"),
	    ),
	],
	[
	    "id"              => 219,
	    "tags"            => "",
	    "title"           => "Solid Officer Combat Equipment",
	    "grade"           => 5,
	    "rank"            => "b",
	    "weapon"          => "mace",
	    "armor"           => "shield",
	    "accessory"       => "brooch",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Defense by 400",
	    "effect3_label"   => "Increases Defense by 600",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Increases Defense by 400", "stat" => "Defense", "value" => "400"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Defense by 600", "stat" => "Defense", "value" => "600"),
	    ),
	],
	[
	    "id"              => 220,
	    "tags"            => "",
	    "title"           => "Keen Officer Combat Equipment",
	    "grade"           => 5,
	    "rank"            => "b",
	    "weapon"          => "bow",
	    "armor"           => "boots",
	    "accessory"       => "ring",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Crit Rate by 320",
	    "effect3_label"   => "Increases Crit Rate by 480",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Increases Crit Rate by 320", "stat" => "Crit Rate", "value" => "320"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Crit Rate by 480", "stat" => "Crit Rate", "value" => "480"),
	    ),
	],
	[
	    "id"              => 221,
	    "tags"            => "",
	    "title"           => "Agile Officer Combat Equipment",
	    "grade"           => 5,
	    "rank"            => "b",
	    "weapon"          => "bow",
	    "armor"           => "boots",
	    "accessory"       => "brooch",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Dodge by 320",
	    "effect3_label"   => "Increases Dodge by 480",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Increases Dodge by 320", "stat" => "Dodge", "value" => "320"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Dodge by 480", "stat" => "Dodge", "value" => "480"),
	    ),
	],
	[
	    "id"              => 222,
	    "tags"            => "",
	    "title"           => "Clearheaded Officer Combat Equipment",
	    "grade"           => 5,
	    "rank"            => "b",
	    "weapon"          => "sword",
	    "armor"           => "armor",
	    "accessory"       => "ring",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Hit Rate by 320",
	    "effect3_label"   => "Increases Hit Rate by 480",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Increases Hit Rate by 320", "stat" => "Hit Rate", "value" => "320"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Hit Rate by 480", "stat" => "Hit Rate", "value" => "480"),
	    ),
	],
	[
	    "id"              => 223,
	    "tags"            => "",
	    "title"           => "Dogged Officer Combat Equipment",
	    "grade"           => 5,
	    "rank"            => "b",
	    "weapon"          => "mace",
	    "armor"           => "shield",
	    "accessory"       => "necklace",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Counter Rate by 160",
	    "effect3_label"   => "Increases Counter Rate by 240",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Increases Counter Rate by 160", "stat" => "Counter Rate", "value" => "160"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Counter Rate by 240", "stat" => "Counter Rate", "value" => "240"),
	    ),
	],
	[
	    "id"              => 224,
	    "tags"            => "",
	    "title"           => "Vengeful Officer Combat Equipment",
	    "grade"           => 5,
	    "rank"            => "b",
	    "weapon"          => "mace",
	    "armor"           => "shield",
	    "accessory"       => "ring",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Counter Damage by 820",
	    "effect3_label"   => "Increases Counter Damage by 1230",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Increases Counter Damage by 820", "stat" => "Counter Damage", "value" => "820"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Counter Damage by 1230", "stat" => "Counter Damage", "value" => "1230"),
	    ),
	],
	[
	    "id"              => 225,
	    "tags"            => "heroonly",
	    "title"           => "Black Dragon's Armament III",
	    "grade"           => 4,
	    "rank"            => "b",
	    "weapon"          => "mace",
	    "armor"           => "shield",
	    "accessory"       => "necklace",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Defense by 350",
	    "effect3_label"   => "Increases Defense by 450",
	    "condition_label" => "Thanatos only",
	    "condition"       => "name=Thanatos",
	    "effect2"   => array(
	        array("label" => "Increases Defense by 350", "stat" => "Defense", "value" => "350"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Defense by 450", "stat" => "Defense", "value" => "450"),
	    ),
	],
	[
	    "id"              => 226,
	    "tags"            => "heroonly",
	    "title"           => "Hero's Armament: Conqueror of Battlefield",
	    "grade"           => 3,
	    "rank"            => "s",
	    "weapon"          => "mace",
	    "armor"           => "armor",
	    "accessory"       => "brooch",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases HP by 6000",
	    "effect3_label"   => "Increases Attack by 400 with each attack (up to 2400)",
	    "condition_label" => "Aran only",
	    "condition"       => "name=Aran",
	    "effect2"   => array(
	        array("label" => "Increases HP by 6000", "stat" => "HP", "value" => "6000"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Attack by 400 with each attack (up to 2400)", "stat" => "Attack", "value" => "400", "max" => "2400", "condition" => "IG"),
	    ),
	],
	[
	    "id"              => 227,
	    "tags"            => "heroonly",
	    "title"           => "Early Archmage Training",
	    "grade"           => 4,
	    "rank"            => "s",
	    "weapon"          => "mace",
	    "armor"           => "armor",
	    "accessory"       => "ring",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Attack by 350 every turn, up to 2800",
	    "effect3_label"   => "Increases Critical Damage by 1% every turn, up to 8%",
	    "condition_label" => "Yeka only",
	    "condition"       => "name=Yeka",
	    "effect2"   => array(
	        array("label" => "Increases Attack by 350 every turn, up to 2800", "stat" => "Attack", "value" => "350", "max" => "2800", "condition" => "IG"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Critical Damage by 1% every turn, up to 8%", "stat" => "Critical Damage%", "value" => "1", "max" => "8", "condition" => "IG"),
	    ),
	],
	[
	    "id"              => 228,
	    "tags"            => "heroonly",
	    "title"           => "Arrogant Calculation",
	    "grade"           => 3,
	    "rank"            => "ss",
	    "weapon"          => "bow",
	    "armor"           => "armor",
	    "accessory"       => "necklace",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Final Skill Power by 10%",
	    "effect3_label"   => "Increases Attack by 60% and all allies' HP by 10% for the first 3 turns in a battle",
	    "condition_label" => "Camelia only",
	    "condition"       => "name=Camelia",
	    "effect2"   => array(
	        array("label" => "Increases Final Skill Power by 10%", "stat" => "FSP%", "value" => "10"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Attack by 60%", "stat" => "Attack%", "value" => "60", "condition" => "IG"),
	        array("label" => "Increases all allies' HP by 10%", "stat" => "HP%", "value" => "10", "aura", "condition" => "IG"),
	    ),
	],
	[
	    "id"              => 229,
	    "tags"            => "heroonly",
	    "title"           => "Gold Dragon's Protection",
	    "grade"           => 3,
	    "rank"            => "ss",
	    "weapon"          => "bow",
	    "armor"           => "armor",
	    "accessory"       => "brooch",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Removes 1 debuffs cast on her if there is an ally Celestial in the battlefield",
	    "effect3_label"   => "Increases Attack by 60% and decreases incoming Critical Damage by 20% for the first 3 turns in a battle",
	    "condition_label" => "Yeka only",
	    "condition"       => "name=Yeka",
	    "effect2"   => array(
	        array("label" => "If ally Celestial on field, remove 1 debuff each turn", "stat" => "RemoveDebuff", "value" => "1"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Attack by 60% for 3 turns at the beginning of combat", "stat" => "Attack%", "value" => "60", "condition" => "IG"),
	        array("label" => "Decreases incoming Critical Damage by 20%", "stat" => "Critical Damage Taken%", "value" => "20"),
	    ),
	],
	[
	    "id"              => 230,
	    "tags"            => "",
	    "title"           => "Gloomy Shaman",
	    "grade"           => 4,
	    "rank"            => "a",
	    "weapon"          => "mace",
	    "armor"           => "boots",
	    "accessory"       => "brooch",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Max HP by 1000 every time an enemy dies (Up to 10000)",
	    "effect3_label"   => "Immunity to Curse",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Increases Max HP by 1000 every time an enemy dies (Up to 10000)", "stat" => "HP", "value" => "1000", "max" => "10000", "condition" => "IG"),
	    ),
	    "effect3" => array(
	        array("label" => "Immunity to Curse", "stat" => "Curse Immunity", "value" => "1"),
	    ),
	],
	[
	    "id"              => 231,
	    "tags"            => "",
	    "title"           => "End of Passion",
	    "grade"           => 5,
	    "rank"            => "a",
	    "weapon"          => "bow",
	    "armor"           => "shield",
	    "accessory"       => "ring",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "The wearer dies after 2 turns",
	    "effect3_label"   => "Increases Attack by 20%",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "The wearer dies after 2 turns", "stat" => "Self Destruct", "value" => "1"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Attack by 20%", "stat" => "Attack%", "value" => "20"),
	    ),
	],
	[
	    "id"              => 232,
	    "tags"            => "",
	    "title"           => "Hot Hot Summer",
	    "grade"           => 2,
	    "rank"            => "s",
	    "weapon"          => "sword",
	    "armor"           => "armor",
	    "accessory"       => "ring",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Mastery by 2150",
	    "effect3_label"   => "Starting a turn on a Water tile increases Movement by 1 for 2 turns",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Increases Mastery by 2150", "stat" => "Mastery", "value" => "2150"),
	    ),
	    "effect3" => array(
	        array("label" => "Starting a turn on a Water tile increases Movement by 1 for 2 turns", "stat" => "Movement", "value" => "1", "condition" => "IG"),
	    ),
	],
	[
	    "id"              => 233,
	    "tags"            => "",
	    "title"           => "Redemption of Darkness",
	    "grade"           => 2,
	    "rank"            => "s",
	    "weapon"          => "bow",
	    "armor"           => "armor",
	    "accessory"       => "brooch",
	    "weapon_image"    => "2-redemption.png",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Movement by 1, and Dodge by 1000",
	    "effect3_label"   => "Increases Attack by 40%",
	    "condition_label" => "Activates 4 turns before the end of combat",
	    "condition"       => "IG",
	    "effect2"   => array(
	        array("label" => "Increases Movement by 1", "stat" => "Movement", "value" => "1"),
	        array("label" => "Increases Dodge by 1000", "stat" => "Dodge", "value" => "1000"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Attack by 40%", "stat" => "Attack%", "value" => "40"),
	    ),
	],
	[
	    "id"              => 234,
	    "tags"            => "",
	    "title"           => "Fair Play",
	    "grade"           => 3,
	    "rank"            => "s",
	    "weapon"          => "bow",
	    "armor"           => "armor",
	    "accessory"       => "ring",
	    "weapon_image"    => "3-fair-play.png",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Decreases Dodge by 100000 and increases Defense by 2000",
	    "effect3_label"   => "Decreases Counter Rate by 100000. Decreases Critical Damage from enemy attacks by 5%",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Decreases Dodge by 100000", "stat" => "Dodge", "value" => "-100000"),
	        array("label" => "Increases Defense by 2000", "stat" => "Defense", "value" => "2000"),
	    ),
	    "effect3" => array(
	        array("label" => "Decreases Counter Rate by 100000", "stat" => "Counter Rate", "value" => "-100000"),
	        array("label" => "Decreases Critical Damage from enemy attacks by 5%", "stat" => "Critical Damage Taken%", "value" => "5"),
	    ),
	],
	[
	    "id"              => 235,
	    "tags"            => "",
	    "title"           => "Imperfect Test Subject",
	    "grade"           => 4,
	    "rank"            => "s",
	    "weapon"          => "mace",
	    "armor"           => "armor",
	    "accessory"       => "necklace",
	    "weapon_image"    => "4-its.png",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Dodge by 3000 for 1 turn",
	    "effect3_label"   => "Decreases Critical Damage by 50% for 1 turn",
	    "condition_label" => "Activated only in Battle of Honor",
	    "condition"       => "IG",
	    "effect2"   => array(
	        array("label" => "Increases Dodge by 3000 for 1 turn", "stat" => "Dodge", "value" => "3000", "condition" => "IG"),
	    ),
	    "effect3" => array(
	        array("label" => "Decreases Critical Damage by 50% for 1 turn", "stat" => "Critical Damage Taken%", "value" => "50", "condition" => "IG"),
	    ),
	],
	[
	    "id"              => 236,
	    "tags"            => "",
	    "title"           => "Part-time Lifeguard",
	    "grade"           => 5,
	    "rank"            => "s",
	    "weapon"          => "mace",
	    "armor"           => "armor",
	    "accessory"       => "necklace",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Defense by 1000",
	    "effect3_label"   => "Starting a turn on a Water tile removes 1 debuffs cast on allies in a 1 tile-range",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Increases Defense by 1000", "stat" => "Defense", "value" => "1000"),
	    ),
	    "effect3" => array(
	        array("label" => "Starting a turn on a Water tile removes 1 debuffs cast on allies in a 1 tile-range", "stat" => "RemoveDebuff", "value" => "1", "condition" => "IG"),
	    ),
	],
	[
	    "id"              => 237,
	    "tags"            => "",
	    "title"           => "Secret Experiment",
	    "grade"           => 5,
	    "rank"            => "s",
	    "weapon"          => "mace",
	    "armor"           => "armor",
	    "accessory"       => "necklace",
	    "weapon_image"    => "5-secret-experiment.png",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Recovers 10 MP every turn",
	    "effect3_label"   => "Decreases damage received from map objects by 50%",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Recovers 10 MP every turn", "stat" => "MP Recovery", "value" => "10"),
	    ),
	    "effect3" => array(
	        array("label" => "Decreases damage received from map objects by 50%", "stat" => "Map Damage Taken%", "value" => "50"),
	    ),
	],
	[
	    "id"              => 238,
	    "tags"            => "",
	    "title"           => "Bloody Melee",
	    "grade"           => 1,
	    "rank"            => "ss",
	    "weapon"          => "bow",
	    "armor"           => "armor",
	    "accessory"       => "ring",
	    "weapon_image"    => "1-bloody.png",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Decreases damage by 10% from skills with 2 or greater range",
	    "effect3_label"   => "Increases Final Skill Power on nearby enemies by 10%",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Decreases damage by 10% from skills with 2 or greater range", "stat" => "Skill Damage Taken%", "value" => "10", "range" => "2+"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Final Skill Power on nearby enemies by 10%", "stat" => "FSP%", "value" => "10", "condition" => "IG"),
	    ),
	],
	[
	    "id"              => 239,
	    "tags"            => "",
	    "title"           => "Kingdom's Tailor",
	    "grade"           => 1,
	    "rank"            => "ss",
	    "weapon"          => "sword",
	    "armor"           => "armor",
	    "accessory"       => "brooch",
	    "weapon_image"    => "1-tailor.png",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Ignore enemy Defense by 2500",
	    "effect3_label"   => "Increases Critical Damage by 8%",
	    "condition_label" => "Activates when attacking an enemy with Defense 70% or above",
	    "condition"       => "IG",
	    "effect2"   => array(
	        array("label" => "Ignore enemy Defense by 2500", "stat" => "Ignore Defense", "value" => "2500"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Critical Damage by 8%", "stat" => "Critical Damage%", "value" => "8"),
	    ),
	],
	[
	    "id"              => 240,
	    "tags"            => "",
	    "title"           => "Oath of Flame",
	    "grade"           => 1,
	    "rank"            => "ss",
	    "weapon"          => "sword",
	    "armor"           => "armor",
	    "accessory"       => "necklace",
	    "weapon_image"    => "1-oath.png",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Attack by 5% while on Lava terrain",
	    "effect3_label"   => "Increases Attack by 5% while on Lava terrain, and gives a Lava terrain bonus",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Increases Attack by 5% while on Lava terrain", "stat" => "Attack%", "value" => "5", "condition" => "IG"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Attack by 5% while on Lava terrain", "stat" => "Attack%", "value" => "5", "condition" => "IG"),
	        array("label" => "Gives a Lava terrain bonus", "stat" => "Terrain Lava", "value" => "1"),
	    ),
	],
	[
	    "id"              => 241,
	    "tags"            => "",
	    "title"           => "Sense of Inferiority",
	    "grade"           => 2,
	    "rank"            => "ss",
	    "weapon"          => "mace",
	    "armor"           => "boots",
	    "accessory"       => "ring",
	    "weapon_image"    => "2-sense.png",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Decreases Skill MP Cost by 10%",
	    "effect3_label"   => "When attacking a target with higher HP than yourself, increases Critical Damage by 10%",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Decreases Skill MP Cost by 10%", "stat" => "MP Cost%", "value" => "-10"),
	    ),
	    "effect3" => array(
	        array("label" => "When attacking a target with higher HP than yourself, increases Critical Damage by 10%", "stat" => "Critical Damage%", "value" => "10", "condition" => "IG"),
	    ),
	],
	[
	    "id"              => 242,
	    "tags"            => "",
	    "title"           => "Nomad's Rest",
	    "grade"           => 2,
	    "rank"            => "ss",
	    "weapon"          => "bow",
	    "armor"           => "armor",
	    "accessory"       => "ring",
	    "weapon_image"    => "2-nomad.png",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Attack by 8%",
	    "effect3_label"   => "Decreases Movement by 1 every turn (up to 10)",
	    "condition_label" => "Activates only in Dark Fortress",
	    "condition"       => "IG",
	    "effect2"   => array(
	        array("label" => "Increases Attack by 8%", "stat" => "Attack%", "value" => "8"),
	    ),
	    "effect3" => array(
	        array("label" => "Decreases Movement by 1 every turn (up to 10)", "stat" => "Movement", "value" => "-1", "max" => "-10", "condition" => "IG"),
	    ),
	],
	[
	    "id"              => 243,
	    "tags"            => "",
	    "title"           => "Hungry Predator",
	    "grade"           => 2,
	    "rank"            => "ss",
	    "weapon"          => "sword",
	    "armor"           => "armor",
	    "accessory"       => "ring",
	    "weapon_image"    => "2-predator.png",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Recovers 15% HP when defeating an enemy",
	    "effect3_label"   => "Increases Attack by 12% for 3 turns when attacking enemies (up to 36%)",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Recovers 15% HP when defeating an enemy", "stat" => "Instant HP Recovery%", "value" => "15", "condition" => "IG"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Attack by 12% for 3 turns when attacking enemies (up to 36%)", "stat" => "Attack%", "value" => "12", "max" => "36", "condition" => "IG"),
	    ),
	],
	[
	    "id"              => 245,
	    "tags"            => "heroonly",
	    "title"           => "White Tiger's Pride",
	    "grade"           => 3,
	    "rank"            => "ss",
	    "weapon"          => "sword",
	    "armor"           => "armor",
	    "accessory"       => "ring",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Standing on an Ice tile decreases all incoming damage by 10%",
	    "effect3_label"   => "Increases Attack by 60% and Crit Rate by 1200 for the first 3 turns in battle",
	    "condition_label" => "Sogoon only",
	    "condition"       => "name=Sogoon",
	    "effect2"   => array(
	        array("label" => "Standing on an Ice tile decreases all incoming damage by 10%", "stat" => "Damage Taken%", "value" => "10", "condition" => "IG"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Attack by 60% for the first 3 turns in battle", "stat" => "Attack%", "value" => "60", "condition" => "IG"),
	        array("label" => "Increases Crit Rate by 1200 for the first 3 turns in battle", "stat" => "Crit Rate", "value" => "1200", "condition" => "IG"),
	    ),
	],
	[
	    "id"              => 246,
	    "tags"            => "heroonly",
	    "title"           => "Endless Training",
	    "grade"           => 3,
	    "rank"            => "ss",
	    "weapon"          => "mace",
	    "armor"           => "armor",
	    "accessory"       => "brooch",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases all allies Defense by 1100",
	    "effect3_label"   => "Increases Attack by 60% and decreases incoming Critical Damage by 10% for the first 3 turns in battle",
	    "condition_label" => "Churyeok only",
	    "condition"       => "name=Churyeok",
	    "effect2"   => array(
	        array("label" => "Increases all allies Defense by 1100", "stat" => "Damage Taken%", "value" => "10", "condition" => "IG", "aura"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Attack by 60% for the first 3 turns in battle", "stat" => "Attack%", "value" => "60", "condition" => "IG"),
	        array("label" => "Decreases incoming Critical Damage by 10% for the first 3 turns in battle", "stat" => "Critical Damage Taken%", "value" => "10", "condition" => "IG"),
	    ),
	],
	[
	    "id"              => 247,
	    "tags"            => "",
	    "title"           => "Mana Storage",
	    "grade"           => 3,
	    "rank"            => "ss",
	    "weapon"          => "sword",
	    "armor"           => "armor",
	    "accessory"       => "brooch",
	    "weapon_image"    => "3-mana.png",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Recovers 15 MP every turn",
	    "effect3_label"   => "If MP is 20% or above at the beginning of a turn, casts a Shield that decreases incoming damage by 25% once",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Recovers 15 MP every turn", "stat" => "MP Recovery", "value" => "15", "condition" => "IG"),
	    ),
	    "effect3" => array(
	        array("label" => "If MP is 20% or above at the beginning of a turn, casts a Shield that decreases incoming damage by 25% once", "stat" => "Damage Taken% Once", "value" => "25", "condition" => "IG"),
	    ),
	],
	[
	    "id"              => 248,
	    "tags"            => "",
	    "title"           => "King's Arrogance",
	    "grade"           => 3,
	    "rank"            => "ss",
	    "weapon"          => "mace",
	    "armor"           => "armor",
	    "accessory"       => "ring",
	    "weapon_image"    => "3-king.png",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Attack by 20%",
	    "effect3_label"   => "When attacked by target with lower Attack than yourself, decreases Critical Damage by 10%",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Increases Attack by 20%", "stat" => "Attack%", "value" => "20"),
	    ),
	    "effect3" => array(
	        array("label" => "When attacked by target with lower Attack than yourself, decreases Critical Damage by 10%", "stat" => "Critical Damage Taken%", "value" => "10", "condition" => "IG"),
	    ),
	],
	[
	    "id"              => 249,
	    "tags"            => "",
	    "title"           => "Lightning Guardian",
	    "grade"           => 3,
	    "rank"            => "ss",
	    "weapon"          => "bow",
	    "armor"           => "armor",
	    "accessory"       => "ring",
	    "weapon_image"    => "3-lightning.png",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Movement by 1",
	    "effect3_label"   => "Increases Direction Strategy Power by 10%",
	    "condition_label" => "Deactivates whe ending a turn without moving",
	    "condition"       => "IG",
	    "effect2"   => array(
	        array("label" => "Increases Movement by 1", "stat" => "Movement", "value" => "1"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Direction Strategy Power by 10%", "stat" => "BonusDirection%", "value" => "10"),
	    ),
	],
	[
	    "id"              => 250,
	    "tags"            => "",
	    "title"           => "Sharp Advance",
	    "grade"           => 4,
	    "rank"            => "ss",
	    "weapon"          => "sword",
	    "armor"           => "boots",
	    "accessory"       => "necklace",
	    "weapon_image"    => "4-sharp.png",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Final Skill Power by 12% when attacking without moving",
	    "effect3_label"   => "Changes the tile below into Thornbush for 1 turn at the beginning of each turn",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Increases Final Skill Power by 12% when attacking without moving", "stat" => "FSP%", "value" => "12", "condition" => "IG"),
	    ),
	    "effect3" => array(
	        array("label" => "Changes the tile below into Thornbush for 1 turn at the beginning of each turn", "stat" => "Transform Tile Thornbush", "value" => "1"),
	    ),
	],
	[
	    "id"              => 251,
	    "tags"            => "",
	    "title"           => "Blood Seduction",
	    "grade"           => 4,
	    "rank"            => "ss",
	    "weapon"          => "bow",
	    "armor"           => "armor",
	    "accessory"       => "ring",
	    "weapon_image"    => "4-blood.png",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "The wearer receives DoT damage at 200% Attack every turn",
	    "effect3_label"   => "Increases Critical Damage by 35% while receiving DoT damage",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "The wearer receives DoT damage at 200% Attack every turn", "stat" => "Self DoT Attack%", "value" => "200"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Critical Damage by 35% while receiving DoT damage", "stat" => "Critical Damage%", "value" => "35", "condition" => "IG"),
	    ),
	],
	[
	    "id"              => 252,
	    "tags"            => "",
	    "title"           => "Pioneer of Power",
	    "grade"           => 4,
	    "rank"            => "ss",
	    "weapon"          => "bow",
	    "armor"           => "armor",
	    "accessory"       => "ring",
	    "weapon_image"    => "4-pioneer.png",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Attack by 2742, and Defense by 1435",
	    "effect3_label"   => "Decreases Awakening Skill Cooldown by 1",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Increases Attack by 2742", "stat" => "Attack", "value" => "2742"),
	        array("label" => "Increases Defense by 1435", "stat" => "Defense", "value" => "1435"),
	    ),
	    "effect3" => array(
	        array("label" => "Decreases Awakening Skill Cooldown by 1", "stat" => "Awakening Skill CD", "value" => "-1"),
	    ),
	],
	[
	    "id"              => 253,
	    "tags"            => "",
	    "title"           => "Master of Strategy",
	    "grade"           => 5,
	    "rank"            => "ss",
	    "weapon"          => "bow",
	    "armor"           => "armor",
	    "accessory"       => "ring",
	    "weapon_image"    => "5-master.png",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Decreases damage by 20% for 2 turns",
	    "effect3_label"   => "Increases Attack by 10% for all allies in 2 tiles around the wearer",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Decreases damage by 20% for 2 turns", "stat" => "Damage Taken%", "value" => "20", "condition" => "IG"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Attack by 10% for all allies in 2 tiles around the wearer", "stat" => "Attack%", "value" => "10", "aura"),
	    ),
	],
	[
	    "id"              => 254,
	    "tags"            => "",
	    "title"           => "Double Spy",
	    "grade"           => 5,
	    "rank"            => "ss",
	    "weapon"          => "bow",
	    "armor"           => "boots",
	    "accessory"       => "ring",
	    "weapon_image"    => "5-double.png",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "If there is an enemy within 2 tiles from the wearer, increases Defense by 3112",
	    "effect3_label"   => "If there is an ally within 2 tiles from the wearer, increases Attack by 5992",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "If there is an enemy within 2 tiles from the wearer, increases Defense by 3112", "stat" => "Defense", "value" => "3112", "condition" => "IG"),
	    ),
	    "effect3" => array(
	        array("label" => "If there is an ally within 2 tiles from the wearer, increases Attack by 5992", "stat" => "Attack", "value" => "5992", "condition" => "IG"),
	    ),
	],
	[
	    "id"              => 255,
	    "tags"            => "heroonly",
	    "title"           => "Flames of Corruption",
	    "grade"           => 3,
	    "rank"            => "ss",
	    "weapon"          => "sword",
	    "armor"           => "armor",
	    "accessory"       => "necklace",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Incoming Damage increases Counter Damage by 10% for 2 turns (up to 20%)",
	    "effect3_label"   => "Increases Attack by 60% and resistance by 20% for the first 3 turns in battle",
	    "condition_label" => "Sol Badguy only",
	    "condition"       => "name=Sol Badguy",
	    "effect2"   => array(
	        array("label" => "Incoming Damage increases Counter Damage by 10% for 2 turns (up to 20%)", "stat" => "Counter Damage", "value" => "10", "max" => "20", "condition" => "IG"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Attack by 60% for the first 3 turns in battle", "stat" => "Attack%", "value" => "60", "condition" => "IG"),
	        array("label" => "Increases Resistance by 20% for the first 3 turns in battle", "stat" => "Resistance", "value" => "20", "condition" => "IG"),
	    ),
	],
	[
	    "id"              => 256,
	    "tags"            => "heroonly",
	    "title"           => "Silvery Trust",
	    "grade"           => 3,
	    "rank"            => "ss",
	    "weapon"          => "sword",
	    "armor"           => "armor",
	    "accessory"       => "ring",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Final Skill Power by 4% for every buff cast on its wearer (up to 16%)",
	    "effect3_label"   => "Increases Attack by 60% and decreases skill MP costs by 20% for the first 3 turns in battle",
	    "condition_label" => "Ky Kiske only",
	    "condition"       => "name=Ky Kiske",
	    "effect2"   => array(
	        array("label" => "Increases Final Skill Power by 4% for every buff cast on its wearer (up to 16%)", "stat" => "Counter Damage", "value" => "10", "max" => "20", "condition" => "IG"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Attack by 60% for the first 3 turns in battle", "stat" => "Attack%", "value" => "60", "condition" => "IG"),
	        array("label" => "Increases Decreases skill MP costs by 20% for the first 3 turns in battle", "stat" => "MP Cost%", "value" => "-20", "condition" => "IG"),
	    ),
	],
	[
	    "id"              => 257,
	    "tags"            => "heroonly",
	    "title"           => "Charging Girl in Love",
	    "grade"           => 3,
	    "rank"            => "ss",
	    "weapon"          => "mace",
	    "armor"           => "armor",
	    "accessory"       => "brooch",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Final Skill Power on nearby enemies by 15%",
	    "effect3_label"   => "Increases Attack by 60% and Defense by 2000 for the first 3 turns in battle",
	    "condition_label" => "May only",
	    "condition"       => "name=May",
	    "effect2"   => array(
	        array("label" => "Increases Final Skill Power on nearby enemies by 15%", "stat" => "Counter Damage", "value" => "10", "max" => "20", "condition" => "IG"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Attack by 60% for the first 3 turns in battle", "stat" => "Attack%", "value" => "60", "condition" => "IG"),
	        array("label" => "Increases Defense by 2000 for the first 3 turns in battle", "stat" => "Defense", "value" => "2000", "condition" => "IG"),
	    ),
	],
	[
	    "id"              => 258,
	    "tags"            => "heroonly",
	    "title"           => "Future Resister",
	    "grade"           => 3,
	    "rank"            => "ss",
	    "weapon"          => "sword",
	    "armor"           => "armor",
	    "accessory"       => "ring",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Attacking without moving increases Attack by 15% for 2 turns (up to 30%)",
	    "effect3_label"   => "Increases Attack by 60% and Movement by 1 for the first 3 turns in battle",
	    "condition_label" => "Ramlethal Valentine only",
	    "condition"       => "name=Ramlethal Valentine",
	    "effect2"   => array(
	        array("label" => "Attacking without moving increases Attack by 15% for 2 turns (up to 30%)", "stat" => "Attack%", "value" => "15", "max" => "30", "condition" => "IG"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Attack by 60% for the first 3 turns in battle", "stat" => "Attack%", "value" => "60", "condition" => "IG"),
	        array("label" => "Increases Movement by 1 for the first 3 turns in battle", "stat" => "Movement", "value" => "1", "condition" => "IG"),
	    ),
	],
	[
	    "id"              => 259,
	    "tags"            => "",
	    "title"           => "Take Care of Summer",
	    "grade"           => 3,
	    "rank"            => "s",
	    "weapon"          => "mace",
	    "armor"           => "boots",
	    "accessory"       => "ring",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Crit Rate by 800",
	    "effect3_label"   => "Recovers MP by 9 each turn",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Increases Crit Rate by 800", "stat" => "Crit Rate", "value" => "800"),
	    ),
	    "effect3" => array(
	        array("label" => "Recovers MP by 9 each turn", "stat" => "MP Recovery", "value" => "9"),
	    ),
	],
	[
	    "id"              => 260,
	    "tags"            => "heroonly",
	    "title"           => "Bird Out of Its Nest",
	    "grade"           => 3,
	    "rank"            => "ss",
	    "weapon"          => "bow",
	    "armor"           => "boots",
	    "accessory"       => "ring",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Can move through enemies for 1 turn by kill",
	    "effect3_label"   => "Increases Attack by 60% and increase directional strategy power by 15 for the first 3 turns of battle",
	    "condition_label" => "Parsifal only",
	    "condition"       => "name=Parsifal",
	    "effect2"   => array(
	        array("label" => "Can move through enemies for 1 turn by kill", "stat" => "Unit Walking", "value" => "1", "condition" => "IG"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Attack by 60% for the first 3 turns of battle", "stat" => "Attack%", "value" => "60", "condition" => "IG"),
	        array("label" => "Increase directional strategy power by 15 for the first 3 turns of battle", "stat" => "BonusDirection%", "value" => "15", "condition" => "IG"),
	    ),
	],
	[
	    "id"              => 261,
	    "tags"            => "",
	    "title"           => "Battle Priest",
	    "grade"           => 1,
	    "rank"            => "s",
	    "weapon"          => "bow",
	    "armor"           => "shield",
	    "accessory"       => "brooch",
	    "weapon_image"    => "1-battle-priest.png",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Max HP by 10% until the beginning of the next turn",
	    "effect3_label"   => "Increases Max MP by 25 until the beginning of the next turn",
	    "condition_label" => "Activated upon successful defense",
	    "condition"       => "IG",
	    "effect2"   => array(
	        array("label" => "Increases Max HP by 10% until the beginning of the next turn", "stat" => "HP%", "value" => "10", "condition" => "IG"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Max MP by 25 until the beginning of the next turn", "stat" => "MP", "value" => "25", "condition" => "IG"),
	    ),
	],
	[
	    "id"              => 262,
	    "tags"            => "heroonly",
	    "title"           => "Cold Hase",
	    "grade"           => 3,
	    "rank"            => "ss",
	    "weapon"          => "bow",
	    "armor"           => "armor",
	    "accessory"       => "necklace",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Decreases all incoming damage by 10% while on a Water terrain",
	    "effect3_label"   => "Increases Attack by 60%, and Resistance by 20% for the first 3 turns of battle",
	    "condition_label" => "Woryeong only",
	    "condition"       => "name=Woryeong",
	    "effect2"   => array(
	        array("label" => "Decreases all incoming damage by 10% while on a Water terrain", "stat" => "Damage Taken%", "value" => "10", "condition" => "IG"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Attack by 60% for the first 3 turns of battle", "stat" => "Attack%", "value" => "60", "condition" => "IG"),
	        array("label" => "Increases Resistance by 20% for the first 3 turns of battle", "stat" => "Resistance", "value" => "20", "condition" => "IG"),
	    ),
	],
	[
	    "id"              => 263,
	    "tags"            => "heroonly",
	    "title"           => "Ancient Vampire",
	    "grade"           => 3,
	    "rank"            => "ss",
	    "weapon"          => "sword",
	    "armor"           => "armor",
	    "accessory"       => "brooch",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Incoming damage increases Critical Damage by 8% for 1 turn (up to 16%)",
	    "effect3_label"   => "Increases Attack by 60% and all allies' Attack by 10% for the first 3 turns of battle",
	    "condition_label" => "Camilla only",
	    "condition"       => "name=Camilla",
	    "effect2"   => array(
	        array("label" => "Incoming damage increases Critical Damage by 8% for 1 turn (up to 16%)", "stat" => "Critical Damage%", "value" => "8", "max" => "16", "condition" => "IG"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Attack by 60% for the first 3 turns of battle", "stat" => "Attack%", "value" => "60", "condition" => "IG"),
	        array("label" => "Increases all allies' Attack by 10% for the first 3 turns of battle", "stat" => "Attack%", "value" => "10", "condition" => "IG", "aura"),
	    ),
	],
	[
	    "id"              => 264,
	    "tags"            => "",
	    "title"           => "Girl Who Dreamed of Freedom",
	    "grade"           => 1,
	    "rank"            => "s",
	    "weapon"          => "bow",
	    "armor"           => "armor",
	    "accessory"       => "brooch",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Attack by 6%",
	    "effect3_label"   => "Increases Direction Strategy by 5%",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Increases Attack by 6%", "stat" => "Attack%", "value" => "6"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Direction Strategy by 5%", "stat" => "BonusDirection%", "value" => "5"),
	    ),
	],
	[
	    "id"              => 265,
	    "tags"            => "heroonly",
	    "title"           => "Black Snake's Hunting",
	    "grade"           => 3,
	    "rank"            => "ss",
	    "weapon"          => "sword",
	    "armor"           => "armor",
	    "accessory"       => "brooch",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Drains as much HP as 28% outgoing damage",
	    "effect3_label"   => "Increases Attack by 60% and Max HP by 10% for the first 3 turns in a battle",
	    "condition_label" => "Heuksa only",
	    "condition"       => "name=Heuksa",
	    "effect2"   => array(
	        array("label" => "Drains as much HP as 28% outgoing damage", "stat" => "Drain%", "value" => "28"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Attack by 60% for the first 3 turns in a battle", "stat" => "Attack%", "value" => "60"),
	        array("label" => "Increases Max HP by 10% for the first 3 turns in a battle", "stat" => "HP%", "value" => "10"),
	    ),
	],
	[
	    "id"              => 266,
	    "tags"            => "",
	    "title"           => "Lonesome Sniper",
	    "grade"           => 1,
	    "rank"            => "ss",
	    "weapon"          => "bow",
	    "armor"           => "armor",
	    "accessory"       => "brooch",
	    "weapon_image"    => "1-lonesome.png",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Attack by 5%, and by 5% on enemies 3 or more tile away",
	    "effect3_label"   => "Increases Crit Rate by 800. Increases Final Skill Power by 10% if there is no enemy in 1-tile range",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Increases Attack by 5%", "stat" => "Attack%", "value" => "5"),
	        array("label" => "Increases Attack by 5% on enemies 3 or more tile away", "stat" => "Attack%", "value" => "5", "condition" => "IG"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Crit Rate by 800", "stat" => "Crit Rate", "value" => "800"),
	        array("label" => "Increases Final Skill Power by 10% if there is no enemy in 1-tile range", "stat" => "FSP%", "value" => "10", "condition" => "IG"),
	    ),
	],
	[
	    "id"              => 267,
	    "tags"            => "",
	    "title"           => "Goblin King's Order",
	    "grade"           => 1,
	    "rank"            => "ss",
	    "weapon"          => "bow",
	    "armor"           => "armor",
	    "accessory"       => "necklace",
	    "weapon_image"    => "1-goblin-king.png",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "At the beginning of each turn, 2 or more allies in a 1-tile range increase Movement by 1 for 1 turn for all allies in its range",
	    "effect3_label"   => "Increases Final Skill Power by 5% for all allies in a 2-tile range (Not stackable)",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "At the beginning of each turn, 2 or more allies in a 1-tile range increase Movement by 1 for 1 turn for all allies in its range", "stat" => "Movement", "value" => "1", "condition" => "IG"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Final Skill Power by 5% for all allies in a 2-tile range (Not stackable)", "stat" => "FSP%", "value" => "5", "aura", "unstackable"),
	    ),
	],
	[
	    "id"              => 268,
	    "tags"            => "",
	    "title"           => "CooCoo Pirates",
	    "grade"           => 2,
	    "rank"            => "ss",
	    "weapon"          => "bow",
	    "armor"           => "armor",
	    "accessory"       => "brooch",
	    "weapon_image"    => "2-pirate.png",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases the wearer's Max HP by 10% and all allies' by 5% (Not stackable)",
	    "effect3_label"   => "Restores HP at the beginning of each turn by 10% for its wearer and by 5% for all allies (Not stackable)",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Increases the wearer's Max HP by 10%", "stat" => "HP%", "value" => "10"),
	        array("label" => "Increases all allies' Max HP by 5% (Not stackable)", "stat" => "HP%", "value" => "5", "aura", "unstackable"),
	    ),
	    "effect3" => array(
	        array("label" => "Restores HP at the beginning of each turn by 10% for its wearer", "stat" => "HP Recovery%", "value" => "10"),
	        array("label" => "Restores HP at the beginning of each turn by 5% for all allies (Not stackable)", "stat" => "HP Recovery%", "value" => "5", "aura", "unstackable"),
	    ),
	],
	[
	    "id"              => 269,
	    "tags"            => "",
	    "title"           => "Rose Knight",
	    "grade"           => 2,
	    "rank"            => "ss",
	    "weapon"          => "sword",
	    "armor"           => "armor",
	    "accessory"       => "brooch",
	    "weapon_image"    => "2-rose-knight.png",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Reflects 35% of all incoming damage",
	    "effect3_label"   => "Enemies' debuffs on the wearer create a 70% chance of affecting their casters as well",
	    "condition_label" => "Activated if there are 2 or more enemies in a 2-tile range",
	    "condition"       => "IG",
	    "effect2"   => array(
	        array("label" => "Reflects 35% of all incoming damage", "stat" => "Reflect%", "value" => "35"),
	    ),
	    "effect3" => array(
	        array("label" => "Enemies' debuffs on the wearer create a 70% chance of affecting their casters as well", "stat" => "Reflect Debuff Chance%", "value" => "70"),
	    ),
	],
	[
	    "id"              => 270,
	    "tags"            => "",
	    "title"           => "Red Flash",
	    "grade"           => 3,
	    "rank"            => "ss",
	    "weapon"          => "sword",
	    "armor"           => "boots",
	    "accessory"       => "necklace",
	    "weapon_image"    => "3-red-flash.png",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Decreases damage from enemy skills with 2 or greater range by 15%",
	    "effect3_label"   => "Increases Direction Strategy Power by 12%. Increases Critical Damage bt 7% for every tile the wearer moves (up to 35%)",
	    "condition_label" => "Activated if the wearer has 5 or higher Movement",
	    "condition"       => "IG",
	    "effect2"   => array(
	        array("label" => "Decreases damage from enemy skills with 2 or greater range by 15%", "stat" => "Damage Taken%", "value" => "15", "condition" => "IG"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Direction Strategy Power by 12%", "stat" => "BonusDirection%", "value" => "12"),
	        array("label" => "Increases Critical Damage bt 7% for every tile the wearer moves (up to 35%)", "stat" => "Critical Damage%", "value" => "7", "max" => "35", "condition" => "IG"),
	    ),
	],
	[
	    "id"              => 271,
	    "tags"            => "",
	    "title"           => "Raging Netherworld Overlord",
	    "grade"           => 3,
	    "rank"            => "ss",
	    "weapon"          => "mace",
	    "armor"           => "shield",
	    "accessory"       => "necklace",
	    "weapon_image"    => "3-raging.png",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Debuff skills create a 55% chance of double-casting certain debuffs",
	    "effect3_label"   => "Increases Final Skill Power by 18%",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Debuff skills create a 55% chance of double-casting certain debuffs", "stat" => "Debuff Double Cast Chance%", "value" => "55", "condition" => "IG"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Final Skill Power by 18%", "stat" => "FSP%", "value" => "18"),
	    ),
	],
	[
	    "id"              => 272,
	    "tags"            => "heroonly",
	    "title"           => "Split Personality Crybaby Maid",
	    "grade"           => 3,
	    "rank"            => "ss",
	    "weapon"          => "bow",
	    "armor"           => "armor",
	    "accessory"       => "necklace",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Attack by 1600 for allies in a 2-tile range",
	    "effect3_label"   => "Increases Attack by 60%, and decreases skill MP costs by 20% for the first 3 turns of battle",
	    "condition_label" => "Mary only",
	    "condition"       => "name=Mary",
	    "effect2"   => array(
	        array("label" => "Increases Attack by 1600 for allies in a 2-tile range", "stat" => "Attack", "value" => "1600", "condition" => "IG", "aura"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Attack by 60% for the first 3 turns of battle", "stat" => "Attack%", "value" => "60", "condition" => "IG"),
	        array("label" => "Decreases skill MP costs by 20% for the first 3 turns of battle", "stat" => "MP Cost%", "value" => "-20", "condition" => "IG"),
	    ),
	],
	[
	    "id"              => 273,
	    "tags"            => "",
	    "title"           => "Master of Tranquil Paradise",
	    "grade"           => 4,
	    "rank"            => "ss",
	    "weapon"          => "mace",
	    "armor"           => "armor",
	    "accessory"       => "necklace",
	    "weapon_image"    => "4-paradise.png",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Using skills on allies adds 20% Resistance for 1 turn",
	    "effect3_label"   => "Buff skills create a 75% chance of double-casting certains buff",
	    "condition_label" => "Support-type character only",
	    "condition"       => "type=Support",
	    "effect2"   => array(
	        array("label" => "Using skills on allies adds 20% Resistance for 1 turn", "stat" => "Resistance", "value" => "20", "condition" => "IG"),
	    ),
	    "effect3" => array(
	        array("label" => "Buff skills create a 75% chance of double-casting certains buff", "stat" => "Buff Double Cast Chance%", "value" => "75", "condition" => "IG"),
	    ),
	],
	[
	    "id"              => 274,
	    "tags"            => "",
	    "title"           => "Detective's Insight",
	    "grade"           => 4,
	    "rank"            => "ss",
	    "weapon"          => "bow",
	    "armor"           => "armor",
	    "accessory"       => "ring",
	    "weapon_image"    => "4-detective.png",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "At the beginning of each turn, if there are 1 or more enemies in a 2-tile range, increases Attack by 3800 and Max HP by 13850 for 2 turns",
	    "effect3_label"   => "At the beginning of each turn, if there are 2 or more enemies in a 2-tile range, removes 1 debuff cast on the wearer, and increases Movement by 2 for 2 turns",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "At the beginning of each turn, if there are 1 or more enemies in a 2-tile range, increases Attack by 3800 for 2 turns", "stat" => "Attack", "value" => "3800", "condition" => "IG"),
	        array("label" => "At the beginning of each turn, if there are 1 or more enemies in a 2-tile range, increases Max HP by 13850 for 2 turns", "stat" => "HP", "value" => "13850", "condition" => "IG"),
	    ),
	    "effect3" => array(
	        array("label" => "At the beginning of each turn, if there are 2 or more enemies in a 2-tile range, removes 1 debuff cast on the wearer", "stat" => "RemoveDebuff", "value" => "1", "condition" => "IG"),
	        array("label" => "At the beginning of each turn, if there are 2 or more enemies in a 2-tile range, increases Movement by 2 for 2 turns", "stat" => "Movement", "value" => "2", "condition" => "IG"),
	    ),
	],
	[
	    "id"              => 275,
	    "tags"            => "",
	    "title"           => "Noble Kin",
	    "grade"           => 5,
	    "rank"            => "ss",
	    "weapon"          => "bow",
	    "armor"           => "armor",
	    "accessory"       => "ring",
	    "weapon_image"    => "5-noble-kin.png",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Ignores enemy Defense by 3000",
	    "effect3_label"   => "Drains as much HP as 28% outgoing damage and increases Attack by 4350",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Ignores enemy Defense by 3000", "stat" => "Ignore Defense", "value" => "3000"),
	    ),
	    "effect3" => array(
	        array("label" => "Drains as much HP as 28% outgoing damage", "stat" => "Drain%", "value" => "28"),
	        array("label" => "Increases Attack by 4350", "stat" => "Attack", "value" => "4350"),
	    ),
	],
	[
	    "id"              => 276,
	    "tags"            => "heroonly",
	    "title"           => "Conqueror of the North Sea",
	    "grade"           => 3,
	    "rank"            => "ss",
	    "weapon"          => "bow",
	    "armor"           => "armor",
	    "accessory"       => "brooch",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases HP by 10%",
	    "effect3_label"   => "Increases Attack by 60% and increases Attack by 10% for every enemy it attacks on first 3 turns of battle",
	    "condition_label" => "Amora only",
	    "condition"       => "name=Amora",
	    "effect2"   => array(
	        array("label" => "Increases HP by 10%", "stat" => "HP%", "value" => "10"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Attack by 60% on first 3 turns of battle", "stat" => "Attack%", "value" => "60", "condition" => "IG"),
	        array("label" => "Increases Attack by 10% for every enemy it attacks on first 3 turns of battle", "stat" => "Attack%", "value" => "10", "condition" => "IG"),
	    ),
	],
	[
	    "id"              => 277,
	    "tags"            => "heroonly",
	    "title"           => "Pink Pink Pink Bean",
	    "grade"           => 3,
	    "rank"            => "ss",
	    "weapon"          => "mace",
	    "armor"           => "armor",
	    "accessory"       => "brooch",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Decreases all incoming damage by 10% while on a field tile",
	    "effect3_label"   => "Increases Attack by 60% for the first 3 turns of battle. Enemy debuff cast on the wearer create a 70% chance of also affecting their casters",
	    "condition_label" => "Pink Bean only",
	    "condition"       => "name=Pink Bean",
	    "effect2"   => array(
	        array("label" => "Decreases all incoming damage by 10% while on a field tile", "stat" => "HP%", "value" => "10"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Attack by 60% for the first 3 turns of battle", "stat" => "Attack%", "value" => "60", "condition" => "IG"),
	        array("label" => "Enemy debuff cast on the wearer create a 70% chance of also affecting their casters", "stat" => "Reflect Debuff Chance%", "value" => "70"),
	    ),
	],
	[
	    "id"              => 278,
	    "tags"            => "heroonly",
	    "title"           => "Annihilating Darkness",
	    "grade"           => 3,
	    "rank"            => "ss",
	    "weapon"          => "sword",
	    "armor"           => "armor",
	    "accessory"       => "brooch",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Decreases all incoming damage by 10% while standing on Special Terrain",
	    "effect3_label"   => "Increases Attack by 60% and Max HP by 25% for the first 3 turns of battle",
	    "condition_label" => "Jinyo only",
	    "condition"       => "name=Jinyo",
	    "effect2"   => array(
	        array("label" => "Decreases all incoming damage by 10% while standing on Special Terrain", "stat" => "HP%", "value" => "10"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Attack by 60% for the first 3 turns of battle", "stat" => "Attack%", "value" => "60", "condition" => "IG"),
	        array("label" => "Increases Max HP by 25% for the first 3 turns of battle", "stat" => "HP%", "value" => "25", "condition" => "IG"),
	    ),
	],
	[
	    "id"              => 279,
	    "tags"            => "heroonly",
	    "title"           => "Nomadic Wind",
	    "grade"           => 3,
	    "rank"            => "ss",
	    "weapon"          => "mace",
	    "armor"           => "armor",
	    "accessory"       => "ring",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Direction Strategy Power by 10%",
	    "effect3_label"   => "Increases Attack by 60% and decreases incoming Critical Damage by 20% for the first 3 turns of battle",
	    "condition_label" => "Lynn only",
	    "condition"       => "name=Lynn",
	    "effect2"   => array(
	        array("label" => "Increases Direction Strategy Power by 10%", "stat" => "BonusDirection%", "value" => "10"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Attack by 60% for the first 3 turns of battle", "stat" => "Attack%", "value" => "60", "condition" => "IG"),
	        array("label" => "Decreases incoming Critical Damage by 20% for the first 3 turns of battle", "stat" => "Critical Damage Taken%", "value" => "20", "condition" => "IG"),
	    ),
	],
	[
	    "id"              => 280,
	    "tags"            => "",
	    "title"           => "Potter's Warrior Sword",
	    "grade"           => 1,
	    "rank"            => "ss",
	    "weapon"          => "sword",
	    "armor"           => "armor",
	    "accessory"       => "necklace",
	    "weapon_image"    => "1-potter.png",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases its wearer's Defense by 1000 and Movement by 2 for 2 turns (Not stackable)",
	    "effect3_label"   => "Increases its wearer's Attack by 800 for 2 turns. Creates a 50% chance of casting Double Attack buff that inflicts 30% of skill damage. (Effect not stackable)",
	    "condition_label" => "Activated each time character is attacked",
	    "condition"       => "IG",
	    "effect2"   => array(
	        array("label" => "Increases its wearer's Defense by 1000 for 2 turns (Not stackable)", "stat" => "Defense", "value" => "1000", "condition" => "IG", "unstackable"),
	        array("label" => "Increases its wearer's Movement by 2 for 2 turns (Not stackable)", "stat" => "Movement", "value" => "2", "condition" => "IG", "unstackable"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases its wearer's Attack by 800 for 2 turns", "stat" => "Attack", "value" => "800", "condition" => "IG"),
	        array("label" => "Creates a 50% chance of casting Double Attack buff that inflicts 30% of skill damage. (Effect not stackable)", "stat" => "Double Attack 30%", "value" => "1", "condition" => "IG", "unstackable"),
	    ),
	],
	[
	    "id"              => 281,
	    "tags"            => "",
	    "title"           => "Torn Throne",
	    "grade"           => 1,
	    "rank"            => "ss",
	    "weapon"          => "sword",
	    "armor"           => "armor",
	    "accessory"       => "ring",
	    "weapon_image"    => "1-throne.png",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Removes 1 Debuff cast on self",
	    "effect3_label"   => "Increases Attack by 1000 (up to 12000)",
	    "condition_label" => "Activated when eliminating enemies",
	    "condition"       => "IG",
	    "effect2"   => array(
	        array("label" => "Removes 1 Debuff cast on self", "stat" => "RemoveDebuff", "value" => "1"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Attack by 1000 (up to 12000)", "stat" => "Attack", "value" => "1000", "max" => "12000"),
	    ),
	],
	[
	    "id"              => 282,
	    "tags"            => "",
	    "title"           => "Worst Slayer",
	    "grade"           => 2,
	    "rank"            => "ss",
	    "weapon"          => "bow",
	    "armor"           => "armor",
	    "accessory"       => "necklace",
	    "weapon_image"    => "2-slayer.png",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Attack by 400 for each 6-Star item equipped",
	    "effect3_label"   => "Increases HP by 600 for each 6-Star item equipped",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Increases Attack by 400 for each 6-Star item equipped", "stat" => "Attack", "value" => "6000", "condition" => "IG", "todo"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases HP by 600 for each 6-Star item equipped", "stat" => "HP", "value" => "9000", "condition" => "IG", "todo"),
	    ),
	],
	[
	    "id"              => 283,
	    "tags"            => "",
	    "title"           => "Red-light District Prince",
	    "grade"           => 2,
	    "rank"            => "ss",
	    "weapon"          => "bow",
	    "armor"           => "armor",
	    "accessory"       => "ring",
	    "weapon_image"    => "2-red-light.png",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Reflect 30% of all damage",
	    "effect3_label"   => "Increases the chance of being attacked by 20% and HP by 6000",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Reflect 30% of all damage", "stat" => "Reflect%", "value" => "30"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases the chance of being attacked by 20%", "stat" => "Aggro%", "value" => "20"),
	        array("label" => "Increases HP by 6000", "stat" => "HP", "value" => "6000"),
	    ),
	],
	[
	    "id"              => 284,
	    "tags"            => "",
	    "title"           => "He Who Opened the Door of the Tower",
	    "grade"           => 3,
	    "rank"            => "ss",
	    "weapon"          => "bow",
	    "armor"           => "armor",
	    "accessory"       => "necklace",
	    "weapon_image"    => "3-door.png",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Resistance by 25%",
	    "effect3_label"   => "Increases Attack by 60% and ignores 1500 enemy Defense for the first 3 turns in a battle",
	    "condition_label" => "Attack-type character only",
	    "condition"       => "type=Attack",
	    "effect2"   => array(
	        array("label" => "Increases Resistance by 25%", "stat" => "Resistance%", "value" => "25"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Attack by 60% for first 3 turns", "stat" => "Attack%", "value" => "60", "condition" => "IG"),
	        array("label" => "Ignores 1500 enemy Defense for first 3 turns", "stat" => "Ignore Defense", "value" => "1500", "condition" => "IG"),
	    ),
	],
	[
	    "id"              => 285,
	    "tags"            => "",
	    "title"           => "Ruler's Style",
	    "grade"           => 3,
	    "rank"            => "ss",
	    "weapon"          => "sword",
	    "armor"           => "armor",
	    "accessory"       => "ring",
	    "weapon_image"    => "3-ruler.png",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Decreases Crit Rate by 2000 for all enemies in the dungeon",
	    "effect3_label"   => "Increases Attack by 60% and all allies' HP by 10% for the first 3 turns in a battle",
	    "condition_label" => "Support-type character only",
	    "condition"       => "type=Support",
	    "effect2"   => array(
	        array("label" => "Decreases Crit Rate by 2000 for all enemies in the dungeon", "stat" => "Enemy Crit Rate", "value" => "2000"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Attack by 60% for first 3 turns", "stat" => "Attack%", "value" => "60", "condition" => "IG"),
	        array("label" => "Increases all allies' HP by 10% for first 3 turns", "stat" => "HP%", "value" => "10", "condition" => "IG", "aura"),
	    ),
	],
	[
	    "id"              => 286,
	    "tags"            => "",
	    "title"           => "Jahad's Effenberg",
	    "grade"           => 3,
	    "rank"            => "ss",
	    "weapon"          => "sword",
	    "armor"           => "armor",
	    "accessory"       => "brooch",
	    "weapon_image"    => "3-jahad.png",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Final Skill Power by 25%",
	    "effect3_label"   => "Increases Attack by 60% and Movement by 2 for the first 3 turns in a battle",
	    "condition_label" => "Area attack-type character only",
	    "condition"       => "type=Area",
	    "effect2"   => array(
	        array("label" => "Increases Final Skill Power by 25%", "stat" => "FSP%", "value" => "25"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Attack by 60% for first 3 turns", "stat" => "Attack%", "value" => "60", "condition" => "IG"),
	        array("label" => "Increases Movement by 2 for first 3 turns", "stat" => "Movement", "value" => "2", "condition" => "IG"),
	    ),
	],
	[
	    "id"              => 287,
	    "tags"            => "",
	    "title"           => "Mankind's Cultural Heritage",
	    "grade"           => 3,
	    "rank"            => "ss",
	    "weapon"          => "sword",
	    "armor"           => "armor",
	    "accessory"       => "brooch",
	    "weapon_image"    => "3-mankind.png",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Attack by 7% for all allies in a 2-tile range (Not stackable)",
	    "effect3_label"   => "Increases Attack by 60% and decreases incoming Critical Damage by 10% for the first 3 turns in a battle",
	    "condition_label" => "Balance-type character only",
	    "condition"       => "type=Balance",
	    "effect2"   => array(
	        array("label" => "Increases Attack by 7% for all allies in a 2-tile range (Not stackable)", "stat" => "Attack%", "value" => "7", "condition" => "IG", "unstackable"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Attack by 60% for first 3 turns", "stat" => "Attack%", "value" => "60", "condition" => "IG"),
	        array("label" => "Decreases incoming Critical Damage by 10% for first 3 turns", "stat" => "Critical Damage Taken%", "value" => "10", "condition" => "IG"),
	    ),
	],
	[
	    "id"              => 288,
	    "tags"            => "",
	    "title"           => "Mysterious Guide",
	    "grade"           => 3,
	    "rank"            => "ss",
	    "weapon"          => "mace",
	    "armor"           => "shield",
	    "accessory"       => "brooch",
	    "weapon_image"    => "3-guide.png",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases MP by 15 and restores 10 MP every turn",
	    "effect3_label"   => "Using the same skill that was used most recently increases Final Skill Power by 30%",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Increases MP by 15", "stat" => "MP", "value" => "15"),
	        array("label" => "Restores 10 MP every turn", "stat" => "MP Recovery", "value" => "10"),
	    ),
	    "effect3" => array(
	        array("label" => "Using the same skill that was used most recently increases Final Skill Power by 30%", "stat" => "FSP%", "value" => "30", "condition" => "IG"),
	    ),
	],
	[
	    "id"              => 289,
	    "tags"            => "",
	    "title"           => "Dignified Crocodile",
	    "grade"           => 3,
	    "rank"            => "ss",
	    "weapon"          => "sword",
	    "armor"           => "armor",
	    "accessory"       => "brooch",
	    "weapon_image"    => "3-crocodile.png",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Incoming damage increases HP by 5% (up to 40%)",
	    "effect3_label"   => "Attacking restores 10% HP and eliminating enemies restores additional 10% HP",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Incoming damage increases HP by 5% (up to 40%)", "stat" => "HP%", "value" => "5", "max" => "40", "condition" => "IG"),
	    ),
	    "effect3" => array(
	        array("label" => "Attacking restores 10% HP", "stat" => "HP Recovery%", "value" => "10", "condition" => "IG"),
	        array("label" => "Eliminating enemies restores additional 10% HP", "stat" => "HP Recovery%", "value" => "10", "condition" => "IG"),
	    ),
	],
	[
	    "id"              => 290,
	    "tags"            => "",
	    "title"           => "Shoes That Escaped the Rack",
	    "grade"           => 4,
	    "rank"            => "ss",
	    "weapon"          => "sword",
	    "armor"           => "armor",
	    "accessory"       => "necklace",
	    "weapon_image"    => "4-shoes.png",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Dodge by 7000",
	    "effect3_label"   => "Increases Attack by 12%. Successful dodge increases Ignore Defense by 500 (up to 2000)",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Increases Dodge by 7000", "stat" => "Dodge", "value" => "7000"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Attack by 12%", "stat" => "Attack%", "value" => "12"),
	        array("label" => "Successful dodge increases Ignore Defense by 500 (up to 2000)", "stat" => "Ignore Defense", "value" => "500", "max" => "2000", "condition" => "IG"),
	    ),
	],
	[
	    "id"              => 291,
	    "tags"            => "",
	    "title"           => "Evankhell Floor's Submerged Fish",
	    "grade"           => 4,
	    "rank"            => "ss",
	    "weapon"          => "mace",
	    "armor"           => "armor",
	    "accessory"       => "ring",
	    "weapon_image"    => "4-evankhell.png",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Decreases Defense by 3000 for all enemies within 2 tiles around the wearer",
	    "effect3_label"   => "Increases Movement by 4",
	    "condition_label" => "Activated for 2 turns at the beginning of combat",
	    "condition"       => "IG",
	    "effect2"   => array(
	        array("label" => "Decreases Defense by 3000 for all enemies within 2 tiles around the wearer", "stat" => "Enemy Defense", "value" => "-3000", "condition" => "IG"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Movement by 4", "stat" => "Movement", "value" => "4"),
	    ),
	],
	[
	    "id"              => 292,
	    "tags"            => "",
	    "title"           => "Wol Ha Ik Song Deputy Master",
	    "grade"           => 5,
	    "rank"            => "ss",
	    "weapon"          => "bow",
	    "armor"           => "boots",
	    "accessory"       => "brooch",
	    "weapon_image"    => "5-wol-ha-ik.png",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Decreases incoming Critical Damage by 25%",
	    "effect3_label"   => "Receives only 40% of incoming damage first, and then the rest at the end of the next turn",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Decreases incoming Critical Damage by 25%", "stat" => "Critical Damage Taken%", "value" => "25"),
	    ),
	    "effect3" => array(
	        array("label" => "Receives only 40% of incoming damage first, and then the rest at the end of the next turn", "stat" => "Delay Damage%", "value" => "40"),
	    ),
	],
	[
	    "id"              => 293,
	    "tags"            => "",
	    "title"           => "Royal Family's Fast Ship",
	    "grade"           => 5,
	    "rank"            => "ss",
	    "weapon"          => "bow",
	    "armor"           => "armor",
	    "accessory"       => "ring",
	    "weapon_image"    => "5-royal.png",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Critical Damage by 12% (Increases additionally by 8% for 1 turn with each kill)",
	    "effect3_label"   => "Decreases incoming Critical Damage by 15% (Additionally decreases by 10% for 1 turn with each kill)",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Increases Critical Damage by 12% (Increases additionally by 8% for 1 turn with each kill)", "stat" => "Critical Damage%", "value" => "12", "todo"),
	    ),
	    "effect3" => array(
	        array("label" => "Decreases incoming Critical Damage by 15% (Additionally decreases by 10% for 1 turn with each kill)", "stat" => "Critical Damage Taken%", "value" => "15", "todo"),
	    ),
	],
	[
	    "id"              => 294,
	    "tags"            => "heroonly",
	    "title"           => "Guard of Determination",
	    "grade"           => 3,
	    "rank"            => "ss",
	    "weapon"          => "sword",
	    "armor"           => "shield",
	    "accessory"       => "necklace",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases the character's chance of being attacked by 30% and HP by 8000",
	    "effect3_label"   => "Increases Attack by 50%, and decreases incoming Critical Damage by 15% for the first 3 turns of battle",
	    "condition_label" => "Valkyrie only",
	    "condition"       => "name=Valkyrie",
	    "effect2"   => array(
	        array("label" => "Increases the character's chance of being attacked by 30%", "stat" => "Aggro%", "value" => "30"),
	        array("label" => "Increases the character's HP by 8000", "stat" => "HP", "value" => "8000"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Attack by 50% for the first 3 turns of battle", "stat" => "Attack%", "value" => "50", "condition" => "IG"),
	        array("label" => "Decreases incoming Critical Damage by 15% for the first 3 turns of battle", "stat" => "Critical Damage Taken%", "value" => "15", "condition" => "IG"),
	    ),
	],
	[
	    "id"              => 295,
	    "tags"            => "heroonly",
	    "title"           => "Love in the Shadow",
	    "grade"           => 3,
	    "rank"            => "ss",
	    "weapon"          => "bow",
	    "armor"           => "armor",
	    "accessory"       => "necklace",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Critical Damage by 15% and Ignore Defense by 1000",
	    "effect3_label"   => "Increases Attack by 60% and all allies' Attack by 10% for the first 3 turns of battle",
	    "condition_label" => "Cleo only",
	    "condition"       => "name=Cleo",
	    "effect2"   => array(
	        array("label" => "Increases Critical Damage by 15%", "stat" => "Critical Damage%", "value" => "15"),
	        array("label" => "Increases Ignore Defense by 1000", "stat" => "Ignore Defense", "value" => "1000"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Attack by 60% for the first 3 turns of battle", "stat" => "Attack%", "value" => "60", "condition" => "IG"),
	        array("label" => "Increases all allies' Attack by 10% for the first 3 turns of battle", "stat" => "Attack%", "value" => "10", "condition" => "IG", "aura"),
	    ),
	],
	[
	    "id"              => 296,
	    "tags"            => "heroonly",
	    "title"           => "Tiny Tinder",
	    "grade"           => 3,
	    "rank"            => "ss",
	    "weapon"          => "sword",
	    "armor"           => "armor",
	    "accessory"       => "brooch",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Decreases incoming Critical Damage by 25%",
	    "effect3_label"   => "Increases Attack by 40% for the first 3 turns of battle and Revives with 25% HP (1 time only)",
	    "condition_label" => "Sehee only",
	    "condition"       => "name=Sehee",
	    "effect2"   => array(
	        array("label" => "Decreases incoming Critical Damage by 25%", "stat" => "Critical Damage%", "value" => "12", "todo"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Attack by 40% for the first 3 turns of battle", "stat" => "Attack%", "value" => "40", "condition" => "IG"),
	        array("label" => "Revives with 25% HP (1 time only)", "stat" => "Revive", "value" => "25"),
	    ),
	],
	[
	    "id"              => 297,
	    "tags"            => "",
	    "title"           => "Red Gift of Winter",
	    "grade"           => 5,
	    "rank"            => "ss",
	    "weapon"          => "mace",
	    "armor"           => "shield",
	    "accessory"       => "brooch",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Terrain Strategy power by 19%",
	    "effect3_label"   => "Increases Direction Strategy power by 19%",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Increases Terrain Strategy power by 19%", "stat" => "Terrain Strategy%", "value" => "19", "aura"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Direction Strategy power by 19%", "stat" => "Direction Strategy%", "value" => "19", "aura"),
	    ),
	],
	[
	    "id"              => 298,
	    "tags"            => "",
	    "title"           => "Green Gift of Festival",
	    "grade"           => 5,
	    "rank"            => "ss",
	    "weapon"          => "sword",
	    "armor"           => "armor",
	    "accessory"       => "necklace",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Decreases incoming Critical Damage by 25%",
	    "effect3_label"   => "Receives only 40% of incoming damage first, and then the rest at the end of the next turn",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Decreases incoming Critical Damage by 25%", "stat" => "Critical Damage Taken%", "value" => "25"),
	    ),
	    "effect3" => array(
	        array("label" => "Receives only 40% of incoming damage first, and then the rest at the end of the next turn", "stat" => "Delay Damage%", "value" => "40"),
	    ),
	],
	[
	    "id"              => 299,
	    "tags"            => "",
	    "title"           => "White Gift of Happiness",
	    "grade"           => 5,
	    "rank"            => "ss",
	    "weapon"          => "bow",
	    "armor"           => "boots",
	    "accessory"       => "ring",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Critical Damage by 12% (Increases additionally by 8% for 1 turn with each kill)",
	    "effect3_label"   => "Decreases incoming Critical Damage by 15% (Additionally decreases by 10% for 1 turn with each kill)",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Increases Critical Damage by 12% (Increases additionally by 8% for 1 turn with each kill)", "stat" => "Critical Damage%", "value" => "12", "todo"),
	    ),
	    "effect3" => array(
	        array("label" => "Decreases incoming Critical Damage by 15% (Additionally decreases by 10% for 1 turn with each kill)", "stat" => "Critical Damage Taken%", "value" => "15", "todo"),
	    ),
	],
	[
	    "id"              => 300,
	    "tags"            => "",
	    "title"           => "Romance of Red and Black",
	    "grade"           => 1,
	    "rank"            => "ss",
	    "weapon"          => "mace",
	    "armor"           => "armor",
	    "accessory"       => "ring",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases all allies' attack by 20% while in dungeon",
	    "effect3_label"   => "Equiped Hero have a 70% chance to avoid Instant Death",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Increases all allies' attack by 20% while in dungeon", "stat" => "Attack%", "value" => "20", "aura"),
	    ),
	    "effect3" => array(
	        array("label" => "Equiped Hero have a 70% chance to avoid Instant Death", "stat" => "Instant Death Resist%", "value" => "70"),
	    ),
	],
	[
	    "id"              => 301,
	    "tags"            => "",
	    "title"           => "Pink Invitation",
	    "grade"           => 4,
	    "rank"            => "ss",
	    "weapon"          => "bow",
	    "armor"           => "shield",
	    "accessory"       => "necklace",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Casts a shield at the beginning of each turn to decrease all damage by 10% once",
	    "effect3_label"   => "Increases Final Skill Power by 12% for all allies in a 2-tile range (Not stackable)",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Casts a shield at the beginning of each turn to decrease all damage by 10% once", "stat" => "Damage Taken% Once", "value" => "10"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Final Skill Power by 12% for all allies in a 2-tile range (Not stackable)", "stat" => "FSP%", "value" => "12", "aura", "unstackable"),
	    ),
	],
	[
	    "id"              => 302,
	    "tags"            => "",
	    "title"           => "Absolute Aegis",
	    "grade"           => 5,
	    "rank"            => "ss",
	    "weapon"          => "sword",
	    "armor"           => "armor",
	    "accessory"       => "brooch",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Casts a shield at the beginning of each turn to decrease all damage by 55% once",
	    "effect3_label"   => "Reflects 55% of all damage",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Casts a shield at the beginning of each turn to decrease all damage by 55% once", "stat" => "Damage Taken% Once", "value" => "55"),
	    ),
	    "effect3" => array(
	        array("label" => "Reflects 55% of all incoming damage", "stat" => "Reflect%", "value" => "55"),
	    ),
	],
	[
	    "id"              => 303,
	    "tags"            => "",
	    "title"           => "Firmament",
	    "grade"           => 5,
	    "rank"            => "ss",
	    "weapon"          => "mace",
	    "armor"           => "armor",
	    "accessory"       => "bow",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Crit Rate by 480",
	    "effect3_label"   => "Avoid Instant Death",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Increases Crit Rate by 480", "stat" => "Crit Rate", "value" => "480"),
	    ),
	    "effect3" => array(
	        array("label" => "Avoid Instant Death", "stat" => "Instant Death Resist%", "value" => "100"),
	    ),
	],
	[
	    "id"              => 304,
	    "tags"            => "",
	    "title"           => "The Stars",
	    "grade"           => 5,
	    "rank"            => "ss",
	    "weapon"          => "mace",
	    "armor"           => "armor",
	    "accessory"       => "bow",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Decreases all incoming damage by 30% while on a Field tile",
	    "effect3_label"   => "Increases Attack by 100% for the first 3 turns of battle. Enemy debuffs cast on the wearer create a 55% chance of also affecting their casters",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
	        array("label" => "Decreases all incoming damage by 30% while on a Field tile", "stat" => "Damage Taken%", "value" => "40", "condition" => "IG"),
	    ),
	    "effect3" => array(
	        array("label" => "Increases Attack by 100% for the first 3 turns of battle. Enemy debuffs cast on the wearer create a 55% chance of also affecting their casters", "stat" => "Attack%", "value" => "100", "condition" => "IG"),
	        array("label" => "Enemy debuffs cast on the wearer create a 55% chance of also affecting their casters", "stat" => "Reflect Debuff Chance%", "value" => "55"),
		),
	],
    [
		"id"              => 305,
		"tags"            => "",
		"title"           => "Invitation of Light",
		"grade"           => 4,
		"rank"            => "ss",
		"weapon"          => "sword",
		"armor"           => "armor",
		"accessory"       => "necklace",
		"weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
		"effect2_label"   => "Increases Resistance by 35%",
		"effect3_label"   => "Revives with 55% HP when killed (max once)",
		"condition_label" => "",
		"condition"       => "",
		"effect2"   => array(
			array("label" => "Increases Resistance by 35%", "stat" => "Resistance", "value" => "35"),
		),
		"effect3" => array(
			array("label" => "Revives with 55% HP when killed (max once)", "stat" => "Revive", "value" => "55"),
		),
	],
	[
	    "id"              => 306,
	    "tags"            => "heroonly",
	    "title"           => "Holiday Miracle",
	    "grade"           => 5,
	    "rank"            => "ss",
	    "weapon"          => "mace",
	    "armor"           => "shield",
	    "accessory"       => "necklace",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Casts a shield at the beginning of each turn to decrease all damage by 55% once",
	    "effect3_label"   => "Increase Defense by 2000. Increase Movement by 1",
	    "condition_label" => "Hella only",
	    "condition"       => "name=Hella",
	    "effect2"   => array(
	        array("label" => "Casts a shield at the beginning of each turn to decrease all damage by 55% once", "stat" => "Damage Taken% Once", "value" => "55"),
	    ),
	    "effect3" => array(
			array("label" => "Increase Defense by 2000", "stat" => "Defense", "value" => "2000"),
			array("label" => "Increase Movement by 1", "stat" => "Movement", "value" => "1"),
		),
	],
[
	    "id"              => 307,
	    "tags"            => "heronly",
	    "title"           => "Christmas Elf on Vacation",
	    "grade"           => 5,
	    "rank"            => "ss",
	    "weapon"          => "bow",
	    "armor"           => "shield",
	    "accessory"       => "necklace",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Attack by 100% for the first 3 turns of battle. Enemy debuffs cast on the wearer create a 55% chance of also affecting their casters",
	    "effect3_label"   => "Increases Crit Rate by 1200",
	    "condition_label" => "Fruel only",
	    "condition"       => "name=Fruel",
	    "effect2"   => array(
	        array("label" => "Increases Attack by 100% for the first 3 turns of battle. Enemy debuffs cast on the wearer create a 55% chance of also affecting their casters", "stat" => "Attack%", "value" => "100", "condition" => "IG"),
	        array("label" => "Enemy debuffs cast on the wearer create a 55% chance of also affecting their casters", "stat" => "Reflect Debuff Chance%", "value" => "55"),
		),
	    "effect3" => array(
				array("label" => "Increases Crit Rate by 1200", "stat" => "Crit Rate", "value" => "1200")
			),
	],
[
	    "id"              => 308,
	    "tags"            => "",
	    "title"           => "Christmas Wonder",
	    "grade"           => 5,
	    "rank"            => "ss",
	    "weapon"          => "mace",
	    "armor"           => "shield",
	    "accessory"       => "necklace",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases HP by 4225",
	    "effect3_label"   => "Increases MP by 14",
	    "condition_label" => "",
	    "condition"       => "",
	    "effect2"   => array(
				array("label" => "Increases HP by 4225", "stat" => "HP", "value" => "4225")
			),
	    "effect3" => array(
				array("label" => "Increase MP by 14", "stat" => "MP", "value" => "14")
			),
	],
[
	    "id"              => 309,
	    "tags"            => "new", //heroonly
	    "title"           => "A Dragoon's Legacy",
	    "grade"           => 5,
	    "rank"            => "ss",
	    "weapon"          => "bow",
	    "armor"           => "shield",
	    "accessory"       => "necklace",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Drains as much HP as 28% outgoing damage and increases Attack by 4350",
	    "effect3_label"   => "Decreases Awakening Skill Cooldown by 1",
	    "condition_label" => "Kai only",
	    "condition"       => "name=Kai",
	    "effect2"   => array(
	        array("label" => "Drains as much HP as 28% outgoing damage", "stat" => "Drain%", "value" => "28"),
	        array("label" => "Increases Attack by 4350", "stat" => "Attack", "value" => "4350"),
	    ),
	    "effect3" => array(
	        array("label" => "Decreases Awakening Skill Cooldown by 1", "stat" => "Awakening Skill CD", "value" => "-1"),
	    ),
	],
[
	    "id"              => 310,
	    "tags"            => "new", //heroonly
	    "title"           => "Golden Dragoon Guidance",
	    "grade"           => 5,
	    "rank"            => "ss",
	    "weapon"          => "sword",
	    "armor"           => "armor",
	    "accessory"       => "ring",
	    "weapon_image"    => "",
		"armor_image"     => "",
		"accessory_image" => "",
	    "effect2_label"   => "Increases Attack by 10%. At the beginning of each turn this skill creates a 40% chance of casting Double Attack (...) for 35% damage (Not stackable)",
	    "effect3_label"   => "Revives with 30% HP when killed (max once)",
	    "condition_label" => "Ian only",
	    "condition"       => "name=Ian",
	    "effect2"   => array(
			array("label" => "Increases Attack by 10%", "stat" => "Attack%", "value" => "10"),
			array("label" => "40% chance of casting Double Attack 35% damage", "stat" => "Double Attack 35%", "value" => "1", "unstackable"),
		),
	    "effect3" => array(
			array("label" => "Revives with 30% HP when killed (max once)", "stat" => "Revive", "value" => "30"),
		),
	],
];

function doPull( $quantity ) {
	global $sets;

	$pullable_ss_sets = [
		5,8,15,16,24,27,29,31,34,36,47,50,56,58,65,68,71,72,83,86,88,94,99,101,103,105,107,110,113,128,130,150,151,152,153,154,155,156,157,158,159,160,161,238,239,240,241,242,243,247,248,249,250,251,252,253,254,266,267,268,269,270,271,273,274,275
	];

	$pullable_s_sets = [
		4,7,11,17,18,23,26,32,35,46,49,53,59,60,64,67,73,76,82,93,95,96,100,102,104,106,108,111,112,129,131,162,163,164,165,166,233,234,235,237,261
	];

	$quantity = (int) $quantity;
	$quantity = $quantity <= 0 || $quantity > 10 ? 1 : $quantity;

	$data = [];
	// echo PULL_RATE_BROWN.PHP_EOL;
	// echo PULL_RATE_B.PHP_EOL;
	// echo PULL_RATE_A.PHP_EOL;
	// echo PULL_RATE_S.PHP_EOL;
	// echo PULL_RATE_SS.PHP_EOL;
	for ( $i = 1 ; $i <= $quantity ; $i++ )
	{
		$luck = rand(1,10000) / 100;
		// $luck = 100;
		$info = [];
		// echo $luck.PHP_EOL;

		if ($luck <= PULL_RATE_BROWN)
		{
			$info = array(
				"caps"  => "/assets/images/icon/s2caps_brown.png",
				"equip" => array(
					"rank" => "brown",
					"name" => "Shiny 1 star stuff",
					"pic"  => SETS_IMAGE_DIRECTORY . "brown-bow.png",
				),
			);
		}
		else if ($luck <= PULL_RATE_B)
		{
			$info = array(
				"caps" => "/assets/images/icon/s2caps_brown.png",
				"equip" => array(
					"rank" => "b",
					"name" => "Cool 3 stars stuff",
					"pic"  => SETS_IMAGE_DIRECTORY . "some-set-b.png",
				),
			);
		}
		else if ($luck <= PULL_RATE_A)
		{
			$info = array(
				"caps" => "/assets/images/icon/s2caps_silver.png",
				"equip" => array(
					"rank" => "a",
					"name" => "Awesome 4 stars stuff",
					"pic"  => SETS_IMAGE_DIRECTORY . "some-set-a.png",
				),
			);
		}
		else if ($luck <= PULL_RATE_S)
		{
			$randomSetId = $pullable_s_sets[ array_rand($pullable_s_sets) ];
			$set         = $sets[ searchForId( $randomSetId, $sets ) ];

			$info = array(
				"caps" => "/assets/images/icon/s2caps_gold.png",
				"equip" => array(
					"rank" => $set['rank'],
					"name" => $set['title'],
					"pic"  => SETS_IMAGE_DIRECTORY . $set['rank'] . "/" . $set['weapon_image'],
				),
			);
		}
		else if ($luck <= PULL_RATE_SS)
		{
			$randomSetId = $pullable_ss_sets[ array_rand($pullable_ss_sets) ];
			$set         = $sets[ searchForId( $randomSetId, $sets ) ];
			
			$info = array(
				"caps" => "/assets/images/icon/s2caps_gold.png",
				"equip" => array(
					"rank" => $set['rank'],
					"name" => $set['title'],
					"pic"  => SETS_IMAGE_DIRECTORY . $set['rank'] . "/" . $set['weapon_image'],
				),
			);
		}
		else {
			echo "coucou";
		}

		$data[] = $info;
	}

	return $data;
	// echo json_encode($data, JSON_HEX_APOS|JSON_HEX_QUOT);

	// Pour générer les objets dans le JS
	// var setsList = JSON.parse( '<?php echo json_encode($sets, JSON_HEX_APOS|JSON_HEX_QUOT); ¿>' );
}

/* Liste tous les sets avec une image
 *
 * foreach ($sets as $set) 
 * {
 * 	if ($set['weapon_image'] != "") {
 * 		$tab[ $set['rank'] ][] = $set;
 * 	}
 * }
 *
 * foreach ($tab as $rank => $sets_list) 
 * {
 * 	echo strtoupper($rank);
 * 	echo "<br>";
 * 	$aaa = [];
 * 	foreach ($sets_list as $set) {
 * 		$aaa[] = $set['id'];
 * 	}
 *
 * 	echo implode(",", $aaa);
 * 	echo "<br>";
 * }
 * var_dump($tab);
 **/
