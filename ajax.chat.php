<?php
session_start();

include_once "include.maintenance-check.php";

// CREATE OR REPLACE VIEW view_user_chat_config AS
// SELECT user.nickname, user.account_status, ucc.*
// FROM user_chat_config AS ucc
// INNER JOIN user ON (ucc.user_id = user.user_id)

// Utilisateur connecté ?
// Vérifier si déjà connecté
if (!isset($_SESSION['user']['id']))
{
  $tab['success'] = FALSE;
  $tab['message'] = "You are not logged in";
  echo json_encode($tab);exit;
}

// Charger les paramètres
$sql = "SELECT * FROM view_user_chat_config WHERE user_id = :user_id";

try
{
  $database = new Database();
  $database->query($sql);
  $database->bind(":user_id", $_SESSION['user']['id']);
  $result = $database->fetch();

  if (count($result) == 0)
  {
    $tab['success'] = FALSE;
    $tab['message'] = "Nothing to load";
    echo json_encode($tab);exit;    
  }

  $user = $result[0];

  unset($database);
}

// User a les droits ?
if ($user["is_blocked"])
{
  $tab['success'] = FALSE;
  $tab['message'] = "You can't access the chat anymore";
  echo json_encode($tab);exit;
}

// Récupérer les conv dont l'id est supérieur est passé en paramètre si paramètre
// Sinon les X dernières

// Vérifier s'il y a quelque chose a charger (en session normalement)
if (!isset($_SESSION['user']['calculator']))
{
	$tab['success'] = FALSE;
	$tab['message'] = "Nothing to load";
	echo json_encode($tab);exit;
}

$tab['success'] = TRUE;
$tab['data']    = $_SESSION['user']['calculator'];
$tab['message'] = "Load successful";
echo json_encode($tab);exit;


// Load stats
$sql = "SELECT data FROM user_hero_stats WHERE user_id = :user_id";

try
{
  $database = new Database();
  $database->query($sql);
  $database->bind(":user_id", $_SESSION['user']['id']);
	$result = $database->fetch();

	if (count($result) == 0)
	{
		$tab['success'] = FALSE;
		$tab['message'] = "Nothing to load";
		echo json_encode($tab);exit;		
	}

  unset($database);
}

// $tab['success']         = TRUE;
// $tab['data']            = $result[0]['data'];
// $_SESSION['calculator'] = $result[0]['data'];
// $tab['message']         = "Load successful";
// echo json_encode($tab);exit;
?>