<?php
session_start();

define("PATH_INFO", "");

include_once "include.functions.php";
include_once "include.maintenance-check.php";

include_once "data.equip-stats.php";
include_once "data.equip-sets.php";
include_once "data.upcoming-sets.php";
include_once "data.stats.php";


?>

<!DOCTYPE html>
<html lang="en">
<head>
	<!-- common head -->
	<?php include "template.head.php"; ?>

	<!-- custom css -->
	<link rel="stylesheet" href="/assets/css/calculator.css?<?php echo filemtime('assets/css/calculator.css'); ?>">
	<link rel="stylesheet" href="/assets/css/fluff.css?<?php echo filemtime('assets/css/fluff.css'); ?>"> 

	<title>FWT Sets</title>
</head>
<body>
	<div class="loader" style="display:none;"><img src="<?php echo ICONS_DIRECTORY; ?>black-caps.png" class="ncaps roll"></div>
	<header>
		<?php include "template.header-belt.php"; ?>
	</header>
	<?php include "template.upcoming-sets.php"; ?>
	<div class="container-fluid" style="position:relative;overflow:hidden;">
		<?php include "template.notification.php"; ?>

		<h1>Sets list</h1>

		<div id='sets-list'>
			<div class="sets-container">
				<div id="sets-filter" class="btn-toolbar">
					<input type="text" class="form-control" placeholder="Filter here" />
					<?php 
					/* filter part */
					foreach ($equip_details as $type => $stuff_list) 
					{
						echo "<div class='btn-group' role='group'>";

						foreach ($stuff_list as $stuff) 
						{
							echo "<button class='btn btn-default' data-type='" . tdf($type) . "' data-stuff='" . tdf($stuff) . "' title='" . $stuff . "' data-role='filter' data-toggle='tooltip' data-placement='top'><img src='" . ICONS_DIRECTORY . $stuff . ".png' class='icon' /></button>";
						}

						echo "</div>";
					}
					?>

					<div class='btn-group btn-radio' role='group'>
						<button class='btn btn-default' data-type='rank' data-stuff='b' data-role='filter'><img src="<?php echo ICONS_DIRECTORY; ?>rank-b.png" style="width:20px;" alt="B"/></button>
						<button class='btn btn-default' data-type='rank' data-stuff='a' data-role='filter'><img src="<?php echo ICONS_DIRECTORY; ?>rank-a.png" style="width:20px;" alt="A"/></button>
						<button class='btn btn-default' data-type='rank' data-stuff='s' data-role='filter'><img src="<?php echo ICONS_DIRECTORY; ?>rank-s.png" style="width:20px;" alt="S"/></button>
						<button class='btn btn-default' data-type='rank' data-stuff='ss' data-role='filter'><img src="<?php echo ICONS_DIRECTORY; ?>rank-ss.png" style="width:20px;" alt="SS"/></button>
					</div>

					<div class='btn-group btn-radio' role='group'>
					<?php
						foreach ($equip_slots as $num => $slot)
						{
							echo "<button class='slot btn btn-default' data-type='grade' data-stuff='" . $num . "' data-role='filter'>" . $slot . "</button>";
						}
					?>
					</div>

					<div class='btn-group btn-radio' role='group'>
						<button class='btn btn-default' data-role='filter' data-type='tags' data-stuff='new' title="New sets" data-toggle='tooltip' data-placement='top'>New</button>
						<button class='btn btn-default' data-role='filter' data-type='tags' data-stuff='heroonly' title="Hero specific" data-toggle='tooltip' data-placement='top'>Hero specific</button>
					</div>

					<button class='btn btn-default' data-role='reset' title="Reset filters" data-toggle='tooltip' data-placement='top'><span class="glyphicon glyphicon-remove"></span></button>
				</div>

				<?php 
				renderSets(); 
				?>
			</div>
		</div>
	</div>
</div>
<footer>
	<!-- common postload -->
	<?php include "template.postload.php"; ?>
	<script src="/assets/js/sets.min.js?<?php echo filemtime('assets/js/sets.min.js'); ?>"></script>  
</footer>
</body>
</html>