<div id="hero-pick" class="form-group">
  <legend>Hero pick</legend>
  <div>
    <div id="hero-awaken">
      <span></span>
    </div>

    <select class="form-control" id="select-hero" name="select-hero">
      <option></option>
      <?php 
      foreach ($hero_stats as $hero => $attribute_list)
      {
        $data_stats = "";
        foreach ($attribute_list as $attribute => $value) 
        {
          $value = isset($awaken_stats[$hero][$attribute]) ? $awaken_stats[$hero][$attribute] : $value;

          $data_stats.= "data-" . tdf($attribute) . "='" . $value . "' ";
        }

        echo "<option " . $data_stats . " value='" . tdf($hero) . "' class='" . tdf($attribute_list['element']) . "'>" . $hero . "</option>";
      }
      ?>
    </select>

    <button type="button" id="btn-apply-stats" class="btn btn-primary">Apply lvl 70 stats<span class="floating-tooltip">Don't forget to apply stats !</span></button>

    <button type="button" id="btn-type-stats" class="btn btn-default">Type stats</button>
  </div>

  <div id="hero-stats-type" class="form-horizontal" style="display: none;">
    <div class="form-group">
      <div class='col-left'>&nbsp;</div>
      <div class='col-xs-2 text-center'><b>White number</b></div>
      <div class='col-xs-2 text-center text-red'><b>Red number</b></div>
    </div>
    <?php
      foreach ($stats_editable as $stat)
      {
        echo "<div class='form-group stat' data-stat='" . tdf($stat) . "'>";
          echo "<div class='col-left text-right'>";
            echo "<label>" . $stat . "</label>";
          echo "</div>";
          echo "<div class='col-xs-2'>";
            echo "<input type='number' class='form-control white-stat' />";
          echo "</div>";
          echo "<div class='col-xs-2'>";
            echo "<input type='number' class='form-control red-stat text-red' />";
          echo "</div>";
        echo "</div>";
      }
    ?>
    <div class="form-group">
      <div class="col-xs-12 text-left">
        <button type="button" id="btn-apply-typed-stats" class="btn btn-primary">Apply stats</button>
      </div>
    </div>      
  </div>
</div>