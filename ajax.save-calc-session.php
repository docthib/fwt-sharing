<?php
session_start();

include_once "include.maintenance-check.php";

// Control and sanitize data
$dataform = $_POST['dataform'];
$dataform = filter_var($dataform, FILTER_SANITIZE_STRING);
$dataform = str_replace("&#34;", '"', $dataform);

// Store in session even if not logged in
$_SESSION['user']['calculator'] = $dataform;