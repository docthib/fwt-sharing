<div id="lord-costumes">
	<legend>Lord Costumes</legend>

	<?php
		foreach ($lord_costumes as $key => $costume)
		{
			// Préparation des données de stats
			$data_stats = "";
			$aff_stats  = "";
			foreach ($costume["stats"] as $stat => $value) 
			{
				$data_stats.= "data-" . tdf($stat) . "='" . $value . "' ";
				$aff_stats .= $stat . " : " . $value . "<br />";
			}

			echo "<div class='costume' data-id='" . $key . "' title='" . $costume["name"] . "' data-toggle='tooltip' data-placement='top'>";
				echo "<img class='pull-left' src='" . LORD_COSTUMES_DIRECTORY . $costume["img"] . "' alt='" . $costume["name"] . "'>";
				echo "<span class='stats' " . $data_stats . ">" . $aff_stats . "</span>";
			echo "</div>";
		}
	?>
</div>