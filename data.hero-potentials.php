<?php
$hero_potentials = array(
	"Attack" => array(
		250 => "legendary",
		245 => "unique",
		240 => "unique",
		235 => "unique",
		230 => "epic",
		225 => "epic",
		220 => "epic",
		215 => "epic",
		210 => "rare",
		205 => "rare",
		200 => "rare",
		195 => "rare",
		185 => "normal",
		180 => "normal",
		175 => "normal",
		170 => "normal",
		165 => "normal",
		160 => "normal",
		155 => "normal"
	),
	"Counter Damage" => array(
		1600 => "legendary",
		1500 => "unique",
		1400 => "unique",
		1300 => "epic",
		1200 => "epic",
		1100 => "rare",
		1000 => "rare",
		900 => "normal",
		800 => "normal",
		700 => "normal"
	),
	"Counter Rate" => array(
		400 => "legendary",
		390 => "unique",
		380 => "unique",
		370 => "epic",
		360 => "epic",
		350 => "rare",
		340 => "rare",
		330 => "normal",
		320 => "normal",
		310 => "normal"
	),
	"Crit Rate" => array(
		400 => "legendary",
		390 => "unique",
		380 => "unique",
		370 => "epic",
		360 => "epic",
		350 => "rare",
		340 => "rare",
		330 => "normal",
		320 => "normal",
		310 => "normal"
	),
	"Damage" => array(
		200 => "legendary",
		190 => "unique",
		180 => "unique",
		170 => "epic",
		160 => "epic",
		150 => "rare",
		140 => "rare",
		130 => "normal",
		120 => "normal",
		110 => "normal"
	),
	"Damage taken" => array(
		200 => "legendary",
		190 => "unique",
		180 => "unique",
		170 => "epic",
		160 => "epic",
		150 => "rare",
		140 => "rare",
		130 => "normal",
		120 => "normal",
		110 => "normal"
	),
	"Defense" => array(
		400 => "legendary",
		390 => "unique",
		380 => "unique",
		370 => "epic",
		360 => "epic",
		350 => "rare",
		340 => "rare",
		330 => "normal",
		320 => "normal",
		310 => "normal"
	),
	"Dodge" => array(
		400 => "legendary",
		390 => "unique",
		380 => "unique",
		370 => "epic",
		360 => "epic",
		350 => "rare",
		340 => "rare",
		330 => "normal",
		320 => "normal",
		310 => "normal"
	),
	"Hit Rate" => array(
		400 => "legendary",
		390 => "unique",
		380 => "unique",
		370 => "epic",
		360 => "epic",
		350 => "rare",
		340 => "rare",
		330 => "normal",
		320 => "normal",
		310 => "normal"
	),
	"HP" => array(
		800 => "legendary",
		760 => "unique",
		720 => "epic",
		680 => "epic",
		640 => "rare",
		600 => "rare",
		560 => "rare",
		520 => "normal",
		500 => "normal",
		480 => "normal",
		460 => "normal",
		440 => "normal",
		420 => "normal"
	),
	"MP" => array(
		30 => "legendary",
		29 => "unique",
		28 => "unique",
		27 => "epic",
		26 => "epic",
		25 => "rare",
		24 => "rare",
		23 => "normal",
		22 => "normal",
		21 => "normal"
	),
	"Stun immunity (3rd slot only)" => 3,
	"Confusion immunity (4th slot only)" => 4,
	"Charm immunity (5th slot only)" => 5
);

if (isset($_POST['action']) && $_POST['action'] == 'select')
{
	if (isset($_POST['stat']) && isset($_POST['num']))
	{
		selectHeroPotential($_POST['stat'], $_POST['num']);
	}
}

function selectHeroPotential($stat, $num)
{
	global $hero_potentials;

	if (!isset($hero_potentials[$stat]))
	{
		$json['success'] = FALSE;
		echo json_encode($json);
		return;
	}

	$json['message'] = "<select class='form-control' name='hero[potentials][" . $num . "][stat]'>";
	foreach ($hero_potentials[$stat] as $val => $rarity)
	{
		$json['message'].= "<option class='bg-" . $rarity . "' value='" . $val . "'>" . $val . "</option>";
	}
	$json['message'].= "</select>";

	$json['success'] = TRUE;
	echo json_encode($json);
}

function toFormat($val)
{
	$val = str_replace("-", " ", $val);
	$val = ucwords($val);

	return $val;
}