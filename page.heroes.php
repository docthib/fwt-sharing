<?php
session_start();
define("PATH_INFO", "");

include_once "include.functions.php";
include_once "include.maintenance-check.php";

include_once "data.hero-stats.php";
include_once "data.stats.php";

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- common head -->
    <?php include "template.head.php"; ?>

    <!-- custom css -->
    <link rel="stylesheet" href="/assets/css/calculator.css?<?php echo filemtime('assets/css/calculator.css'); ?>">
    <link rel="stylesheet" href="/assets/css/fluff.css?<?php echo filemtime('assets/css/fluff.css'); ?>"> 

    <title>FWT Heroes</title>
  </head>
  <body>
    <div class="loader" style="display:none;"><img src="/assets/images/icon/black-caps.png" class="ncaps roll"></div>
    <header>
      <?php include "template.header-belt.php"; ?>
    </header>
    <div class="container-fluid" style="position:relative;overflow:hidden;">
      <?php include "template.notification.php"; ?>

      <h1>Heroes list</h1>
      
      <div id="heroes-filter" class="btn-toolbar">
        <div class="btn-group" style="max-width: 400px;">
          <input type="text" class="form-control" placeholder="Hero name" /> 
        </div>

        <div class="btn-group">
        <?php
          foreach ($hero_elements as $element) 
          {
            echo "<button class='btn btn-default' title='" . $element . "' data-type='element' data-stuff='" . $element . "' data-toggle='tooltip' data-role='filter'><img src='" . ICONS_DIRECTORY . "icon-" . $element . ".png' style='width:20px;' alt='" . $element . "'/></button>";
          }
        ?>            
        </div>

        <div class="btn-group">
        <?php
          foreach ($hero_types as $type) 
          {
            echo "<button class='btn btn-default' title='" . $type . "'  data-type='type' data-stuff='" . $type . "' data-toggle='tooltip' data-role='filter'><img src='" . ICONS_DIRECTORY . $type . "-type.png' style='width:20px;' alt='" . $type . "'/></button>";
          }
        ?> 
        </div>

        <div class="btn-group">
        <?php
          foreach ($terrain_list as $terrain) 
          {
            echo "<button class='btn btn-default' title='" . $terrain . "'  data-type='Terrain' data-stuff='" . $terrain . "' data-toggle='tooltip' data-role='filter'><img src='" . ICONS_DIRECTORY . "terrain-" . $terrain . ".png' style='width:20px;' alt='" . $terrain . "'/></button>";
          }
        ?> 
        </div>

        <div class="btn-group">
          <button class='btn btn-default' title='Awaken only' data-type='awakenable' data-stuff='1' data-toggle='tooltip' data-role='filter'>
            <span class='glyphicon glyphicon-star awake-star'></span>
          </button>
        </div>

        <div class="legend">
          <div class="icon">
            <span class="glyphicon glyphicon-info-sign" title="Click on a hero to pin it (will stay even if filtered)" data-toggle="tooltip" data-placement="right"></span>
          </div>
        </div>
      </div>

      <div class="clearfix"></div>
      
      <div class="btn-toolbar">
        <div class="btn-group">
          <button id="heroes-table-overview" class="btn btn-default active" title="Table overview" data-toggle='tooltip' data-placement='top'><span class="glyphicon glyphicon-th-list"></span></button>
          <!-- <button id="heroes-list-overview" class="btn btn-default" title="List overview" data-toggle='tooltip' data-placement='top'><span class="glyphicon glyphicon-th-large"></span></button> -->
        </div>
      </div>

      <div id='heroes-table' style="overflow-x:auto;">
        <table class="table table-striped table-hover">
          <thead>
            <tr>
              <?php
              foreach ($hero_stats_list as $nom_champ => $nom_stat) 
              {
                echo "<th data-stat='" . $nom_champ . "'>" . $nom_stat . "</th>";
              }
              ?>
            </tr>
          </thead>
          <tbody>
            <?php
            foreach ($hero_stats as $nom_hero => $stats) 
            {
              $awakenable = isset( $awaken_stats[ $nom_hero ] );

              echo "<tr>";
              foreach ($hero_stats_list as $nom_champ => $nom_stat)
              {
                $val_stat = isset( $stats[$nom_champ] ) ? $stats[$nom_champ] : 0;

                switch ($nom_champ)
                {
                  case 'awakenable':
                    $val      = $val_stat ? "<span class='glyphicon glyphicon-star awake-star' alt='Awaken' title='Awaken'></span>" : "<span title=''></span>";
                    $val_data = $val_stat;
                  break;
                  case 'type':
                    $val      = "<img src='" . ICONS_DIRECTORY . tdf($val_stat) . "-type.png' alt='" . $val_stat . "' title='" . $val_stat . "' />";
                    $val_data = $val_stat;
                  break;
                  case 'element':
                    $val      = "<img src='" . ICONS_DIRECTORY . "icon-" . tdf($val_stat) . ".png' alt='" . $val_stat . "' title='" . $val_stat . "' />";
                    $val_data = $val_stat;
                  break;
                  case 'Terrain':
                    $val_stat = "";

                    if (isset($hero_terrains[$nom_hero]))
                    {
                      $val_data = implode(", ", $hero_terrains[$nom_hero]);
                      $val      = "";

                      foreach ($hero_terrains[$nom_hero] as $terrain) 
                      {
                        $val .= "<img class='icon-terrain' src='" . ICONS_DIRECTORY . "terrain-" . tdf($terrain) . ".png' alt='" . $terrain . "' title='" . $terrain . "' />";
                      }
                    }
                    else
                    {
                      $val_data = "";
                      $val      = "<i>No terrain :(</i>";
                    }
                  break;
                  default:
                    if ( $awakenable && isset( $awaken_stats[ $nom_hero ][ $nom_champ ] ) )
                    {
                      $val = $awaken_stats[ $nom_hero ][ $nom_champ ];
                    }
                    else
                    {
                      $val = $val_stat;
                    }

                    $val_data = $val;
                  break;
                }

                echo "<td data-stat='" . $nom_champ . "' data-" . tdf($nom_champ) . "='" . $val_data . "' >" . $val . "</td>";
              }
              echo "</tr>";
            }
            ?>
          </tbody>
        </table>
      </div>
      <div id='heroes-list'>
        <div class="heroes-container"> </div>
      </div>
    </div>
    </div>
    <footer>
      <!-- common postload -->
      <?php include "template.postload.php"; ?>
      <script src="/assets/js/heroes.min.js?<?php echo filemtime('assets/js/heroes.min.js'); ?>"></script>  
    </footer>
  </body>
</html>