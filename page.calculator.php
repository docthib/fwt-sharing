<?php
session_start();

define("PATH_INFO", "");

include_once "include.functions.php";
include_once "include.maintenance-check.php";

// CREATE OR REPLACE VIEW view_user_login AS
// SELECT user.user_id, data AS data_stats, nickname, password, account_status
// FROM user
// LEFT JOIN user_hero_stats ON (user.user_id = user_hero_stats.user_id)

include_once "data.soul-gears.php";
include_once "data.lord-costumes.php";
include_once "data.lord-battle-mastery.php";
include_once "data.hero-costumes.php";
include_once "data.hero-stats.php";
include_once "data.hero-potentials.php";
include_once "data.equip-stats.php";
include_once "data.equip-sets.php";
include_once "data.runes-stats.php";
include_once "data.stats.php";

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- common head -->
    <?php include "template.head.php"; ?>

    <!-- custom css -->
    <link rel="stylesheet" href="/assets/css/calculator.css?<?php echo filemtime('assets/css/calculator.css'); ?>">
    <link rel="stylesheet" href="/assets/css/fluff.css?<?php echo filemtime('assets/css/fluff.css'); ?>"> 

    <title>FWT Stats Calculator - by docthib</title>
  </head>
  <body>
    <div class="loader" style="display:none;"><img src="/assets/images/icon/black-caps.png" class="ncaps roll"></div>
    <?php include "template.modal-calc.php"; ?>
    <?php include "template.modal-sets.php"; ?>
    <header>
      <?php include "template.header-belt.php"; ?>
    </header>
    <div class="container-fluid" style="position:relative;overflow:hidden;">
      <?php include "template.notification.php"; ?>
      
      <form id="calc-form" action="" role="form" onsubmit="return false;">
        <div class="mid-bloc">
          <?php include "template.hero-pick.php"; ?>
          <?php include "template.equipment.php"; ?>
          <?php include "template.runes.php"; ?>
        </div>
        <div class="left-bloc">
          <div class="switch btn btn-default"><span class="glyphicon glyphicon-triangle-left"></span></div>
          <?php include "template.hero-stats.php"; ?>
        </div>
        <div class="right-bloc">
          <div class="switch btn btn-default"><span class="glyphicon glyphicon-triangle-right"></span></div>
          <?php include "template.hero-potentials.php"; ?>
          <?php include "template.hero-costumes.php"; ?>
          <?php include "template.soul-gears.php"; ?>
          <?php include "template.lord-costumes.php"; ?>
          <?php include "template.lord-battle-mastery.php"; ?>
        </div>
      </form>
    </div>
    <footer>
      <!-- common postload -->
      <?php include "template.postload.php"; ?>

      <script src="/assets/js/calculator.min.js?<?php echo filemtime('assets/js/calculator.min.js'); ?>"></script>
      <script src="/assets/js/sets.min.js?<?php echo filemtime('assets/js/sets.min.js'); ?>"></script>

      <?php if (isset($_SESSION['user']['calculator'])) { ?>
      <script type="text/javascript">
        $(document).ready(loadJSONpls(0));
      </script>
      <?php } ?>  
    </footer>
    <div class="overlay"></div>
  </body>
</html>