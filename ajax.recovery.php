<?php
session_start();

include_once "include.maintenance-check.php";

// Vérifier si déjà connecté
if (isset($_SESSION['user']['id']))
{
	$tab['success'] = FALSE;
	$tab['message'] = "You are already logged in omg !";
	echo json_encode($tab);exit;
}

// Database connexion
include_once "class.database.php";

parse_str($_POST['data'], $data);

// Control and sanitize data
$password   = trim(filter_var($data['newpass'], FILTER_SANITIZE_STRING));
$repassword = trim(filter_var($data['repass'], FILTER_SANITIZE_STRING));
$pass_hash  = hash("sha384", $password . SALT);

if (empty($password))
{
  $tab['success'] = FALSE;
  $tab['message'] = "Password can't be empty";
  echo json_encode($tab);exit;
}

if ($password != $repassword)
{
  $tab['success'] = FALSE;
  $tab['message'] = "Both passwords doesn't match !";
  echo json_encode($tab);exit;
}

if ($_SESSION['recovery']['token'] == "" || $_SESSION['recovery']['email'] == "")
{
    $tab['success'] = FALSE;
    $tab['message'] = "Nop";
    echo json_encode($tab);exit;
}

// Vérifier le nombre de try
$sql = "SELECT user_id
FROM user
WHERE email = :email
AND token_recovery = :token_recovery";

try
{
  $database = new Database();
  $database->query($sql);
  $database->bind(":email", $_SESSION['recovery']['email']);
  $database->bind(":token_recovery", $_SESSION['recovery']['token']);
  $result = $database->fetch();

  // Si l'user n'est pas retrouvé on balance une erreur
  if (count($result) == 0)
  {
    $tab['success'] = FALSE;
    $tab['message'] = "Unknown account";
    echo json_encode($tab);exit;
  }

  // On stock ces données pour une future utilisation
  $user_id = $result[0]["user_id"];

  unset($database);
}
catch(Exception $e)
{
  if (!DEBUG)
  {
    $tab['success'] = FALSE;
    $tab['message'] = "Error while recov a.r1";
    echo json_encode($tab);exit;
  }
}

// INSERT connexion log
$sql = "UPDATE user SET
  password = :password,
  account_status = :account_status,
  login_failed = :login_failed,
  token_recovery = :token_recovery
  WHERE user_id = :user_id";

try
{
  $database = new Database();
  $database->query($sql);
  $database->bind(":user_id", $user_id);
  $database->bind(":password", $pass_hash);
  $database->bind(":account_status", 1);
  $database->bind(":login_failed", 0);
  $database->bind(":token_recovery", "");
  
  $database->execute();

  unset($database);
}
catch(Exception $e)
{
  if (!DEBUG)
  {
    $tab['success'] = FALSE;
    $tab['message'] = "Error while login a.r2";
    echo json_encode($tab);exit;
  }
}

$tab['success'] = TRUE;
$tab['message'] = "New password successfully saved. Gogo login now! redirection in 2 seconds";
echo json_encode($tab);exit;