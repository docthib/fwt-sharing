<div id="hero-potentials" class="form-inline">
	<legend>Hero Potentials</legend>

	<?php
		for ($i = 1 ; $i <= 5 ; $i++)
		{
			echo "<div class='potential' data-num='" . $i . "'>";
				echo "<div class='form-group potential-stat'>";
					echo "<select class='form-control rarity' name='hero[potentials][" . $i . "][stat]'>";
						echo "<option value='-1' selected disabled hidden>Pick a potential...</option>";
						foreach ($hero_potentials as $stat => $val_list)
						{
							if (is_array($val_list))
							{
								echo "<option class='bg-normal' value='" . tdf($stat) . "'>" . $stat . "</option>";
							}
							else if ($val_list == $i)
							{
								echo "<option class='bg-legendary' value='" . tdf($stat) . "'>" . $stat . "</option>";
							}
						}
					echo "</select>";
				echo "</div>";

				echo "<div class='form-group potential-val'>";
				foreach ($hero_potentials as $stat => $val_list)
				{
					if (is_array($val_list))
					{
						echo "<select class='form-control rarity' data-stat='" . tdf($stat) . "' name='hero[potentials][" . $i . "][val]' style='display:none;'>";
							echo "<option class='bg-normal'></option>";
							foreach ($val_list as $val => $rarity)
							{
								echo "<option class='bg-" . $rarity . "' value='" . $val . "'>" . $val . "</option>";
							}
						echo "</select>";
					}
				}
				echo "</div>";
			echo "</div>";
		}
		
	?>
</div>