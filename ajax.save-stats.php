<?php
session_start();

include_once "include.maintenance-check.php";

// Control and sanitize data
$dataform = $_POST['dataform'];
$dataform = filter_var($dataform, FILTER_SANITIZE_STRING);
$dataform = str_replace("&#34;", '"', $dataform);

// Store in session even if not logged in
$_SESSION['user']['calculator'] = $dataform;

// Vérifier si déjà connecté
if (!isset($_SESSION['user']['id']))
{
	$tab['success'] = FALSE;
	$tab['message'] = "You are not logged in";
	echo json_encode($tab);exit;
}

// Database connexion
include_once "class.database.php";

// Database insert
$sql = "INSERT INTO user_hero_stats (user_id, data) VALUES (:user_id, :data) ON DUPLICATE KEY UPDATE data = :data2";

try
{
  $database = new Database();
  $database->query($sql);
  $database->bind(":user_id", $_SESSION['user']['id']);
  $database->bind(":data", $dataform);
  $database->bind(":data2", $dataform);
	$database->execute();

  unset($database);
}
catch(Exception $e)
{
  if (!DEBUG)
  {
    $tab['success'] = FALSE;
    $tab['message'] = "Error while saving";
    echo json_encode($tab);exit;
  }
}

$tab['success'] = TRUE;
$tab['message'] = "Save successful";
echo json_encode($tab);exit;