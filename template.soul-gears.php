<div id="hero-soul-gears">
	<legend>Soul Gears</legend>

<?php
	foreach ($soul_gears as $hero => $soul_gear_list)
	{
		echo "<div class='" . tdf($hero) . "' style='display:none;'>";
		foreach ($soul_gear_list as $sg_num => $soul_gear_set)
		{
			foreach ($soul_gear_set as $key => $soul_gear) 
			{
				// Préparation des données de stats
				$data_stats = "";
				$aff_stats  = "";
				
				if (isset($soul_gear["stats"])) {
					foreach ($soul_gear["stats"] as $stat => $value) 
					{
						$data_stats.= "data-" . tdf($stat) . "='" . $value . "' ";
						$aff_stats .= $stat . " : " . $value . "<br />";
					}
				}

				echo "<div class='costume' data-hero='" . tdf($hero) . "' data-num='" . $sg_num . "' data-type='" . $key . "' title=\"" . $soul_gear["name"] . "\" data-toggle='tooltip' data-placement='top'>";
					if (isset($soul_gear["img"]))
					{
						echo "<img class='pull-left' src='" . SOUL_GEAR_DIRECTORY . $soul_gear["img"] . "' alt=\"" . $soul_gear["name"] . "\">";
					}
					echo "<span class='stats' " . $data_stats . ">" . $aff_stats . "</span>";
				echo "</div>";
			}
		}
		echo "</div>";
	}
?>

</div>