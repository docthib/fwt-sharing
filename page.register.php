<?php
session_start();
define("PATH_INFO", "");

include_once "include.functions.php";
include_once "include.maintenance-check.php";

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- common head -->
    <?php include "template.head.php"; ?>

    <!-- custom header meta -->
    <title>FWT Stats Calculator - Login/Register</title>
    <!-- custom css -->
    <link rel="stylesheet" href="/assets/css/calculator.css?<?php echo filemtime('assets/css/calculator.css'); ?>">
    <link rel="stylesheet" href="/assets/css/fluff.css?<?php echo filemtime('assets/css/fluff.css'); ?>">

  </head>
  <body>
    <header>
      <?php include "template.header-belt.php"; ?>
    </header>
    <div class="container">
      <?php include "template.notification.php"; ?>

      <div class="row">
        <div class="col-sm-6">
          <?php include "template.user-login.php"; ?>
        </div>
        <div class="col-sm-6">
          <?php include "template.user-register.php"; ?>
        </div>
      </div>
    </div>
    <footer>
      <!-- common postload -->
      <?php include "template.postload.php"; ?>
      <!-- custom js -->
      <script src="/assets/js/user.js?<?php echo filemtime('assets/js/user.js'); ?>"></script>
    </footer>
  </body>
</html>