<div id="equip-stats" class="form-inline">
	<legend>Fast build</legend>

	<div id="equip-btn-set">
		<div class="btn-toolbar" role="toolbar">
			<label for="">Set all stuff to</label>

			<?php 
			foreach ($equip_details as $type => $stuff_list) 
			{
				echo "<div class='btn-group' role='group'>";
				foreach ($stuff_list as $stuff) 
				{
					echo "<button class='btn btn-default' data-type='" . tdf($type) . "' data-role='" . tdf($stuff) . "' title='" . $stuff . "' data-toggle='tooltip' data-placement='top'><img src='/assets/images/icon/" . $stuff . ".png' class='icon' /></button>";
				}
				echo "</div>";
			}
			?>

			<div class="btn-group" role="group">
				<button class='btn btn-default bg-default' data-role='platinum' title='Platinum' data-toggle='tooltip' data-placement='top'>
					<?php echo "<img src='" . ICONS_DIRECTORY . "plat-on.png' />"; ?>
				</button>
			</div>

			
				
		</div>

		<div class="btn-toolbar" role="toolbar">
			<button class='btn btn-primary btn-sets-list'>Sets list</button>

			<div class="btn-group" role="group">
				<button class='btn bg-legendary' data-role='legendary_max' title='Legendary +10' data-toggle='tooltip' data-placement='top'>+10</button>
				<button class='btn bg-transcended' data-role='transcended_max' title='Transcended @MAX' data-toggle='tooltip' data-placement='top'>MAX</button>
			</div>

			<button class='btn bg-rare' data-role='costumes' title='Select all costumes / SG / Max Masteries' data-toggle='tooltip' data-placement='top'>Costumes / SG / Masteries</button>

			<div class="btn-group" role="group">
				<button class='btn bg-unique' data-role='potential_common_build' title='Most used item potentials @tier 2' data-toggle='tooltip' data-placement='top'>Common item potentials @tier 2</button>
				<button class='btn bg-legendary' data-role='potential_max_common_build' title='Most used item potentials @tier 1' data-toggle='tooltip' data-placement='top'>@tier 1</button>
			</div>

			<button class='btn bg-epic' data-role='reset_equips'>Reset equips</button>
			<button class='btn bg-epic' data-role='reset_potentials'>Reset potentials</button>
		</div>
	</div>

	<legend>Equipped Sets</legend>
	<button id="button-combat-condition" class='btn btn-default' title='Activate / Deactivate combat condition' data-toggle='tooltip' data-placement='top'>Toggle Set Condition</button>
	<div class="sets-activated flex"></div>
	
	<legend>Equipment</legend>
	<?php
		foreach ($equip_slots as $i => $slot_name)
		{
			$sets_list = filterByValue($sets, "grade", $i);
			usort($sets_list, "sortBySetTitle");

			echo "<div class='grade row'>";
				echo "<div class='set-effects col-xs-12'>";

				echo "</div>";
			foreach ($equip_details as $type => $stat_list)
			{
				echo "<div class='equip col-xs-12 col-sm-4' data-type='" . tdf($type) . "' data-grade='" . $i . "'>";
					echo "<p>Slot <span class='slot'>" . $slot_name . "</span> " . $type . "</p>";
					echo "<div>";
						echo "<div class='form-group set'>";
							echo "<select class='form-control' name='equip_set[" . $i . "][" . $type . "]'>";
								echo "<option></option>";
								foreach ($sets_list as $set => $arg_list) 
								{
									echo "<option value='" . $arg_list['id'] . "'>" . $arg_list['title'] . "</option>";
								}
							echo "</select>";
						echo "</div>";

						echo "<div class='form-group stuff'>";
							echo "<select class='form-control' name='equip_stuff[" . $i . "][" . $type . "]'>";
								echo "<option></option>";
								foreach ($equip_details[$type] as $stuff) 
								{
									echo "<option value='" . tdf($stuff) . "'>" . $stuff . "</option>";
								}
							echo "</select>";
						echo "</div>";
						echo "<div class='form-group item-rarity'>";
							echo "<select class='form-control rarity' name='equip_rarity[" . $i . "][" . $type . "]'>";
								echo "<option value='' class='bg-normal'></option>";
								foreach ($equip_rarity as $rarity => $arg_list) 
								{
									$data_args = "";
									foreach ($arg_list as $argument => $value) 
									{
										$data_args.= "data-" . tdf($argument) . "='" . $value . "' ";
									}

									echo "<option class='bg-" . tdf($rarity) . "' value='" . tdf($rarity) . "' " . $data_args . ">" . $rarity . "</option>";
								}
							echo "</select>";
						echo "</div>";
						echo "<div class='form-group enhancement'>";
							echo "<select class='form-control' name='equip_enhancement[" . $i . "][" . $type . "]'>";
								echo "<option></option>";
								for ($lvl = 15 ; $lvl >= 0 ; $lvl--) 
								{
									echo "<option value='" . $lvl . "'>" . $lvl . "</option>";
								}
							echo "</select>";
						echo "</div>";

						echo "<div class='checkbox platinum'>";
							echo "<img data-target='plat_" . $i . "_" . $type . "' src='" . ICONS_DIRECTORY . "plat-on.png' />";
							echo "<input type='number' id='plat_" . $i . "_" . $type . "' name='equip_plat[" . $i . "][" . $type . "]' style='display:none;'>";
						echo "</div>";
					echo "</div>";

					/* People doesn't care about stats being displayed */
					// echo "<p>Stats</p>";
					echo "<div class='stats' style='display:none;' >";
						echo "<i>Use dropdowns above</i>";
					echo "</div>";

					echo "<div class='potentials'>";
					echo "<p>Potentials</p>";
						for ($j = 1 ; $j <= 4 ; $j++)
						{
							echo "<div class='potential' data-num='" . $j . "'>";
								echo "<div class='form-group potential-stat'>";
									echo "<select class='form-control' name='equip[" . $i . "][" . tdf($type) . "][potentials][" . $j . "][stat]' disabled>";
										echo "<option class='bg-normal'></option>";
										foreach ($equip_potentials[$type] as $stat => $condition) 
										{
											if ($condition == 0 || $condition == $i)
											{
												echo "<option class='bg-normal' value='" . tdf($stat) . "'>" . $stat . "</option>";												
											}
										}
									echo "</select>";
								echo "</div>";
								echo "<div class='form-group potential-val text-center'>";
									echo "<i>&lt;&lt; Use dropdowns</i>";
								echo "</div>";
							echo "</div>";
						}
					echo "</div>";
				echo "</div>";
			}
			echo "</div>";
		}
	?>
	<div>
		Equipment Attack Bonus : <span id="equip-attack-bonus">0</span> 
	</div>
</div>