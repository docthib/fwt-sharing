<?php
session_start();
define("PATH_INFO", "");

include_once "include.functions.php";
include_once "include.maintenance-check.php";
include_once "data.equip-sets.php";

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- common head -->
    <?php include "template.head.php"; ?>

    <!-- custom header meta -->
		<title>FWT Stats Calculator - Coocoo Machine</title>

		<!-- custom js/css -->
		<link rel="stylesheet" href="/assets/css/calculator.css?<?php echo filemtime('assets/css/calculator.css'); ?>">
		<link rel="stylesheet" href="/assets/css/fluff.css?<?php echo filemtime('assets/css/fluff.css'); ?>">

  </head>
	<body>
		<header>
			<?php include "template.header-belt.php"; ?>
		</header>
		<div class="container">
			<?php // include "template.notification.php"; ?>

			<div class="wrap text-center">
				<img class="caps result" src="/assets/images/icon/s2caps_gold.png" alt="">
				<img class="caps result" src="/assets/images/icon/s2caps_silver.png" alt="">
				<img class="caps result" src="/assets/images/icon/s2caps_brown.png" alt="">
				<img class="caps result" src="/assets/images/icon/black-caps.png" alt="">

				<img class="caps result" src="/assets/images/icon/noblesse-caps.png" alt="">
				<img class="caps result" src="/assets/images/icon/s2caps_brown.png" alt="">
				<img class="caps result" src="/assets/images/icon/s2caps_silver.png" alt="">
				<img class="caps result" src="/assets/images/icon/s2caps_gold.png" alt="">
			</div>
			<div class="machine">
				<legend>Coocoo Machine !</legend>
				<div class="stats">
					<b style="display:block;line-height: 160px;font-size:24px;">#neverpull</b>
				</div>
				<button class="btn-pull" data-nb="1">X 1</button>
				<button class="btn-pull" data-nb="10">X 10</button>
				<!-- <button class="btn btn-success">Trothon button</button> -->
			</div>
		</div>

		<footer>
      <!-- common postload -->
      <?php include "template.postload.php"; ?>
    

			<script>
				$(document).ready(function()
				{
					$(".machine button").removeAttr("disabled");

					$("body").on("click", ".machine button", function() {
						
						// disable button
						$(".machine button").attr("disabled", "true");

						// empty machine caps
						$(".machine .stats").empty();

						capsPull( $(this).data("nb") );

						// random effect
						var effects = new Array("rotate-x", "rotate-y", "roll");
						$(".caps").addClass(effects[Math.floor(Math.random() * 3)]);
					});
				});

				function endOfPull()
				{
					$(".machine button").removeAttr("disabled");
					$(".machine .caps").remove();
					$(".caps.result").attr("class", "caps result");
					$('[data-toggle="tooltip"]').tooltip({container: 'body', trigger : 'hover'});
					// $(".caps").attr("class", "caps result");
				}

				function capsPull( nb )
				{
					$.ajax({
						method : "POST",
						url    : "ajax.pull.php",
						data   : { "nb" : nb }
					})
					.done(function(data) {
						var data = JSON.parse( data );
						console.log(data);

						var nbPull = data.length;
						var onePullDuration = 750;
						for (i = 0 ; i < nbPull ; i++)
						{
							var time = onePullDuration * i;
							setTimeOutWithClosure( time, data, i );
						}

						time += 5000;
						setTimeout("endOfPull()", time);
					})
					.fail(function(data) {
						$(".machine button").removeAttr("disabled");
					});
				}

                function setTimeOutWithClosure( time, data, i ) 
                { 
                    setTimeout(function() { makeAnimation(data, i); }, time) 
                }

                function makeAnimation( data, i )
                {
                	// add the caps roll
					var el_caps = $("<img>");
					el_caps.attr({
						'src' : data[ i ].caps
					}).addClass('caps pull');

					$(".machine .stats").append(el_caps);

					// prepare the equip image
					// if S or SS need to add a div container
					var el_equip = $("<img>");
					el_equip.attr({
						'src' : data[ i ].equip.pic,
					});

					var el_div = $("<div>").addClass("ready set set-" + data[ i ].equip.rank).attr({
						'data-toggle' : 'tooltip',
						'title'       : data[ i ].equip.name,
					});
					if ( data[ i ].equip.rank == "s" || data[ i ].equip.rank == "ss" ) {
						el_div.append("<div class='image'></div>");
						el_div.find(".image").append( el_equip );
					} else {
						el_div.append(el_equip);
					}

					$(".machine .stats").append(el_div);
                }

			</script>
		</footer>
	</body>
</html>