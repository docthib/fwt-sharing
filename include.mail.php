<?php
	//////////////////////////////////////////////////////////////////////////////////
	//                                                                              //
	// Super fonction de mail réalisée par Damien                                   //
	// Permet, à partir d'un texte en HTML d'envoyer un e-mail PROPRE et compatible //
	//                                                                              //
	// Paramètres                                                                   //
	//    - expediteur :                                                            //
	//        C'est la personne qui envoit le mail. Plusieurs formats               //
	//		  sont disponibles ("Nom Prénom <adresse@email.com>" ou tout simplement //
	//		  "adresse@email.com").                                                 //
	// 	  - destinataire :                                                          //
	//        Adresse e-mail du destinataire.                                       //
	//    - sujet :                                                                 //
	//        Objet du mail.                                                        //
	// 	  - message_html :                                                          //
	//        Corps du mail AVEC ou SANS les balises <html> et <body>.              //
	//    - a_remplacer (optionnel) :                                               //
	//        Array de TAGS à remplacer par des valeurs de variables.               //
	//    - remplacement (optionnel) :                                              //
	//        Array de variables remplaçant les TAGS par des valeurs.               //
	//    - piece_jointe (optionnel) :                                              //
	//        Chemin vers une pièce jointe à envoyer avec le mail.                  //
	//        Extension supportées : jpeg, jpg, gif, png, txt, html, htm, pdf,      //
	//                               doc, docx, xls, xlsx, zip.                     //
	//    - cc :                                             						//
	//        Ajouter un ou plusieurs destinataires en copie cachée.                //
    //                                                                              //
	//    - bcc :                                              						//
	//        Ajouter un ou plusieurs destinataires en copie cachée                 //
	//        Extension supportées : jpeg, jpg, gif, png, txt, html, htm, pdf,      //
	//                               doc, docx, xls, xlsx, zip.                     //
	//                                                                              //
	// Valeur de retour                                                             //
	//    - Renvoie TRUE ou FALSE selon si le message a été envoyé ou pas.          //
	//                                                                              //
	// Fonctionnalités avancées                                                     //
	//    - Remplacement de chaînes de caractères grâce à des "tags",               //
	//    - Ajoute automatiquement les balises html et body si besoin,              //
	//    - Conversion automatique de l'objet et du message en utf-8 si besoin,     //
	//    - Supporte les pièces jointes.                                            //
	//    - supporte CC et BCC                                                      //
	//                                                                              //
	//////////////////////////////////////////////////////////////////////////////////
	//                                                                              //
	//   UPDATES                                                                    //
	//                                                                              //
	//  10/07/2012 by Thibaut                                                       //
	//    - Ajout des PJ multiples                                                  //
	//                                                                              //
	//  11/10/2012 by Damien                                                        //
	//    - Ajout des CC et BCC		                                                //
	//                                                                              //
	//////////////////////////////////////////////////////////////////////////////////

	function get_mime_type($nom_fichier)
	{
		// Identification du MIME Type de la pièce jointe
		$ext = strtolower(substr($nom_fichier, strrpos($nom_fichier, '.')));
		switch ($ext)
		{
			case '.jpeg':
			case '.jpg':
				$mimetype = 'image/jpeg';
				break;
			case '.gif':
				$mimetype = 'image/gif';
				break;
			case '.png':
				$mimetype = 'image/png';
				break;
			case '.txt':
				$mimetype = 'text/plain';
				break;
			case '.html':
			case '.htm':
				$mimetype = 'text/html';
				break;
			case '.pdf':
				$mimetype = 'application/pdf';
				break;
			case '.xls':
				$mimetype = 'application/vnd.ms-excel';
				break;
			case '.xlsx':
				$mimetype = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
				break;
			case '.doc':
				$mimetype = 'application/msword';
				break;
			case '.docx':
				$mimetype = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
				break;
			case '.zip':
				$mimetype = 'application/x-zip-compressed';
				break;
			default:
				$mimetype = 'application/octet-stream';
		}
		return ($mimetype);
	}
	
	function mail_s($expediteur, $destinataire, $sujet, $message_html, $a_remplacer = "", $remplacement = "", $piece_jointe = "", $cc = "", $bcc = "")
	{
		// Création du boundary pour délimiter les parties du mail HTML / Texte
		$frontiere = md5(uniqid(mt_rand()));
		$frontiere_alt = md5(uniqid(mt_rand()));
		
		// Remplacements de texte
		$sujet = str_replace($a_remplacer, $remplacement, $sujet);
		$message_html = str_replace($a_remplacer, $remplacement, $message_html);
		
		// Encodage du sujet
		$sujet_encode = '=?UTF-8?B?'.base64_encode($sujet).'?=';
		
		// Encodage de textes en UTF-8 (si besoin)
		if(!mb_detect_encoding($message_html, 'UTF-8', true))
		{
			$message_html = utf8_encode($message_html);
		}
		if(!mb_detect_encoding($sujet, 'UTF-8', true))
		{
			$sujet = utf8_encode($sujet);
		}
		
		// Préparation du message au format text/plain
		$message_texte = str_replace("\n", '', $message_html); 						// On enlève tous les retours à la ligne
		$message_texte = str_replace('&rsquo;', '\'', $message_texte); 				// Remplacement des apostrophes (car html_entity_decode ne les prend pas en compte)
		$message_texte = str_replace('</p>', "\n\n", $message_texte); 				// Remplacement des P par 2 retours à la ligne
		$message_texte = str_replace('<br />', "\n", $message_texte); 				// Remplacement des <br /> par 1 retour à la ligne
		$message_texte = str_replace('	', "", $message_texte);						// Retirer les tabulations qui s'affichent
		$message_texte = html_entity_decode($message_texte, ENT_COMPAT, 'UTF-8');	// Conversion des &eacute; etc...
		$message_texte = preg_replace('/<.*?>/', '', $message_texte);				// On enlève toutes les autres balises HTML
		
		// Entêtes
		$headers  = 'From: '.$expediteur."\n";
		if($cc != "") {$headers .= 'Cc: '.$cc."\n";}
		if($bcc != "") {$headers .= 'Bcc: '.$bcc."\n";}
		$headers .= 'MIME-Version: 1.0'."\n";
		$headers .= 'Content-Type: multipart/mixed; boundary="'.$frontiere.'"'."\n\n";
		
		$message  = '--'.$frontiere."\n";
		$message .= 'Content-Type: multipart/alternative; boundary="'.$frontiere_alt.'"'."\n\n";
		
		$message .= '--'.$frontiere_alt."\n";
		$message .= 'Content-Type: text/plain; charset=UTF-8'."\n\n";
		
		$message .= $message_texte."\n\n";
		
		$message .= '--'.$frontiere_alt."\n";
		$message .= 'Content-Type: text/html; charset=UTF-8'."\n\n";
		
		$message .= $message_html."\n\n";
		
		$message .= '--'.$frontiere_alt.'--'."\n";
		
		// Ajout de la pièce jointe (si présente)
		if($piece_jointe != "")
		{
			// Si la variable piece_jointe n'est pas un tableau, traitement EZ...
			if (!is_array($piece_jointe))
			{
				$chemin_fichier = $piece_jointe;
				$nom_fichier = basename($piece_jointe);
				
				// Récupération du MIME type du fichier
				$mimetype = get_mime_type($nom_fichier);
				
				$message .= '--'.$frontiere."\n";
				$message .= 'Content-Type: application/pdf; name="'.$nom_fichier.'"'."\n";
				$message .= 'Content-Disposition: attachment; filename="'.$nom_fichier.'"'."\n";
				$message .= 'Content-Transfer-Encoding: base64'."\n\n";
				$message .= chunk_split(base64_encode(file_get_contents($chemin_fichier)))."\n";
			}
			else // ...sinon on fait un for each pour checker les données dans le tableau
			{
				foreach ($piece_jointe as $i => $valeur)
				{
					$chemin_fichier = $valeur;
					$nom_fichier = basename($valeur);
					
					// Récupération du MIME type du fichier
					$mimetype = get_mime_type($nom_fichier);
					
					$message .= '--'.$frontiere."\n";
					$message .= 'Content-Type: application/pdf; name="'.$nom_fichier.'"'."\n";
					$message .= 'Content-Disposition: attachment; filename="'.$nom_fichier.'"'."\n";
					$message .= 'Content-Transfer-Encoding: base64'."\n\n";
					$message .= chunk_split(base64_encode(file_get_contents($chemin_fichier)))."\n";
				}
			}
		}
		
		$message .= '--'.$frontiere.'--'."\n";
		
		if (mail($destinataire, $sujet_encode, $message, $headers)) // Envoi du mail
		{
			return true;
		}
		else
		{
			return false;
		}
	}
?>