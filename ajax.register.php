<?php
session_start();

include_once "include.maintenance-check.php";

// Vérifier si déjà connecté
if (isset($_SESSION['user']['id']))
{
	$tab['success'] = FALSE;
	$tab['message'] = "You are already logged in omg !";
	echo json_encode($tab);exit;
}

// Vérifier si déjà initié une procédure d'inscription
if (isset($_SESSION['registration']) && $_SESSION['registration'] == 1)
{
	$tab['success'] = FALSE;
	$tab['message'] = "You already initiated registration process, please check your mailbox";
	echo json_encode($tab);exit;
}

// Database connexion
include_once "class.database.php";

parse_str($_POST['data'], $data);

// Control and sanitize data
$email    = trim(filter_var($data['email'], FILTER_SANITIZE_EMAIL));
$nickname = trim(filter_var($data['nickname'], FILTER_SANITIZE_STRING));
$password = trim(filter_var($data['pass'], FILTER_SANITIZE_STRING));

if (empty($nickname))
{
  $tab['success'] = FALSE;
  $tab['message'] = "Nickname can't be empty";
  echo json_encode($tab);exit;
}

if (empty($password))
{
  $tab['success'] = FALSE;
  $tab['message'] = "Password can't be empty";
  echo json_encode($tab);exit;
}

if (!filter_var($email, FILTER_VALIDATE_EMAIL))
{
	$tab['success'] = FALSE;
	$tab['message'] = "Email format incorrect";
	echo json_encode($tab);exit;
}



// Vérifier si email n'existe pas deja
$sql = "SELECT user_id FROM user
WHERE email = :email";

try
{
  $database = new Database();
  $database->query($sql);
  $database->bind(":email", $email);
  $result = $database->fetch();

  unset($database);
}
catch(Exception $e)
{
  if (!DEBUG){echo "An error occured";}
}

if (count($result) != 0)
{
	$tab['success'] = FALSE;
	$tab['message'] = "Email already used";
	echo json_encode($tab);exit;
}

// Vérifier si nickname n'existe pas deja
$sql = "SELECT user_id FROM user
WHERE nickname = :nickname";

try
{
  $database = new Database();
  $database->query($sql);
  $database->bind(":nickname", $nickname);
  $result = $database->fetch();

  unset($database);
}
catch(Exception $e)
{
  if (!DEBUG)
  {
  	$tab['success'] = FALSE;
    $tab['message'] = "Error while saving a.r1";
    echo json_encode($tab);exit;
  }
}

if (count($result) != 0)
{
	$tab['success'] = FALSE;
	$tab['message'] = "Nickname already used";
	echo json_encode($tab);exit;
}

// If here, everything is OK
// Password HASH
$pass_hash = hash("sha384", $password . SALT);

// Database insert
$sql = "INSERT INTO user (nickname, password, email) VALUES (:nickname, :password, :email)";

try
{
  $database = new Database();
  $database->query($sql);
  $database->bind(":nickname", $nickname);
  $database->bind(":password", $pass_hash);
  $database->bind(":email", $email);
	$database->execute();

	$user_id = $database->lastInsertId();
  unset($database);
}
catch(Exception $e)
{
  if (!DEBUG)
  {
  	$tab['success'] = FALSE;
    $tab['message'] = "Error while saving a.r2";
    echo json_encode($tab);exit;
 	}
}

// Email verification code
$code = md5($email . SALT);

// Email verification insert
$sql = "INSERT INTO user_email_validation (email_sent, user_id, code) VALUES (:email_sent, :user_id, :code)";

try
{
  $database = new Database();
  $database->query($sql);
  $database->bind(":email_sent", time());
  $database->bind(":user_id", $user_id);
  $database->bind(":code", $code);
  $database->execute();

  unset($database);
}
catch(Exception $e)
{
  if (!DEBUG)
	{
		$tab['success'] = FALSE;
    $tab['message'] = "Error while saving a.r3";
    echo json_encode($tab);exit;
	}
}

include_once "include.mail.php";

$expe    = "your@email.com";
$subject = "[FWT-SC] Email validation";
$bcc     = "docthib@hotmail.com";
$html    = "<html><body><p>Hello " . $nickname . ",</p>";
$html    .= "<p>Please click on the following link to activate your account : <a href='http://yoururl.com/activation?e=" . $email . "&c=" . $code . "'>Link</a></p>";
$html    .= "<p>Thanks ! :)</p>";
$html    .= "</body></html>";

mail_s($expe, $email, $subject, $html, "", "", "", "", $bcc);

$tab['success'] = TRUE;
$tab['message'] = "Account created, please verify your email";
echo json_encode($tab);exit;