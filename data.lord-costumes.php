<?php

define("LORD_COSTUMES_DIRECTORY", "/assets/images/lc/");

$lord_costumes[0]["name"]                  = "A Refreshing Trip to School";
$lord_costumes[0]["img"]                   = "lord-costume-0.png";
$lord_costumes[0]["stats"]["Defense"]      = 200;
$lord_costumes[0]["stats"]["Crit Rate"]    = 150;

$lord_costumes[1]["name"]                  = "Black Magician";
$lord_costumes[1]["img"]                   = "lord-costume-1.png";
$lord_costumes[1]["stats"]["MP"]           = 16;
$lord_costumes[1]["stats"]["Attack"]       = 285;

$lord_costumes[2]["name"]                  = "White Magician";
$lord_costumes[2]["img"]                   = "lord-costume-2.png";
$lord_costumes[2]["stats"]["HP"]           = 600;
$lord_costumes[2]["stats"]["Dodge"]        = 150;

$lord_costumes[3]["name"]                  = "Noblesse Raizel";
$lord_costumes[3]["img"]                   = "lord-costume-3.png";
$lord_costumes[3]["stats"]["HP"]           = 900;
$lord_costumes[3]["stats"]["MP"]           = 24;

$lord_costumes[4]["name"]                  = "Ye Ran High School Raizel";
$lord_costumes[4]["img"]                   = "lord-costume-4.png";
$lord_costumes[4]["stats"]["Attack"]       = 285;
$lord_costumes[4]["stats"]["MP Recovery"]  = 3;

$lord_costumes[5]["name"]                  = "Magician Girl";
$lord_costumes[5]["img"]                   = "lord-costume-5.png";
$lord_costumes[5]["stats"]["Hit Rate"]     = 100;
$lord_costumes[5]["stats"]["Dodge"]        = 100;

$lord_costumes[6]["name"]                  = "White Witch";
$lord_costumes[6]["img"]                   = "lord-costume-6.png";
$lord_costumes[6]["stats"]["HP"]           = 900;
$lord_costumes[6]["stats"]["Attack"]       = 285;

$lord_costumes[7]["name"]                  = "Black Witch";
$lord_costumes[7]["img"]                   = "lord-costume-7.png";
$lord_costumes[7]["stats"]["HP"]           = 900;
$lord_costumes[7]["stats"]["Attack"]       = 285;

$lord_costumes[8]["name"]                  = "Onmyoji Lord";
$lord_costumes[8]["img"]                   = "lord-costume-8.png";
$lord_costumes[8]["stats"]["Attack"]       = 285;
$lord_costumes[8]["stats"]["MP Recovery"]  = 3;

$lord_costumes[9]["name"]                  = "Jellylike Pink Bean";
$lord_costumes[9]["img"]                   = "lord-costume-9.png";
$lord_costumes[9]["stats"]["Defense"]      = 200;
$lord_costumes[9]["stats"]["Crit Rate"]    = 150;

$lord_costumes[10]["name"]                 = "Lord in White";
$lord_costumes[10]["img"]                  = "lord-costume-10.png";
$lord_costumes[10]["stats"]["HP"]          = 900;
$lord_costumes[10]["stats"]["Attack"]      = 150;

$lord_costumes[11]["name"]                 = "Warrior Emperor Lord";
$lord_costumes[11]["img"]                  = "lord-costume-10.png";
$lord_costumes[11]["stats"]["Attack"]      = 285;
$lord_costumes[11]["stats"]["MP Recovery"] = 3;