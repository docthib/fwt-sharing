<?php

if (isset($_SESSION['notification']) && isset($_SESSION['notification']['type']))
{
?>
	<div class="alert alert-<?php echo $_SESSION['notification']['type']; ?> alert-dismissible fade in notif">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		  <span aria-hidden="true">&times;</span>
		</button>
		
		<h4><?php echo $_SESSION['notification']['titre']; ?></h4>
		<?php echo $_SESSION['notification']['message']; ?>
	</div>
<?php
}

unset($_SESSION['notification']);

if (!isset($_SESSION['info']))
{
  $_SESSION['info'] = 1;
 ?> 
 <!--
 <div class="alert alert-success alert-dismissible fade in notif">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
    
    <h4>v0.7 is out !</h4>
    <p>Hi guys ! I'm super excited to bring you this version with a lot of new features : sets picker, stats comparison etc =)<br />
    As it's fresh and may be not so stable, please help me improve it by reporting bugs and suggestions. Many thanks :)</p>

    <h4>v0.7.1</h4>  
    <p>Fixed bugs on some browsers</p>
  </div>
  -->
<?php
}

// Changelog

// v0.6.1 - 1464367860
// - Item potential propagation button added (clone potential to any non disabled dropdown who contain the option)
// - Reset potential button added
// - Reset equip button added
// - New Chris/Cleo costumes added
// - Optimization and fixes

// v0.6 - 1463675123
// - Multiple effects sets
// - Hero conditions on sets are now active
// - Krut added
// - Last sets added
// - Ian/Krut costumes added
// - Added some more data to "Simple Overview"
// - Quick fixes

// <p>I did not fully tested new set system, please report any bugs, thanks :)</p>

// v0.5 - 1461849426
// - Item potentials list added
// - Fast equip buttons added
// - Dropdown sorted by Desc now
// - Many fixes

// v0.4.5 - 1461183618
// - New costumes added
// - Terrain 30% removed
// <p>Thanks <b>/u/ydobonrehtonatsuj</b> for % to flat formula ^^</p>

// v0.4.4 - 1460736899
// - Platinum added
// - Sets values updated
// - New display available for stats (simple / detailed)
// - Damage formula updated (thanks <b>@TorikSAn</b>)
// - Help on damage tab
// - Minor fix on hero potential
// <p>Still looking for <b>def% to flat def formula</b> guys !</p>

// v0.4.3 - 1460487204
// Dang, I broke save with the new import/export feature.. sorry guys! :(<br />
// Thanks <b>@Gimmix</b> for reporting it!


// v0.4.2 - 1459950642
// - New feature ! Import and Export JSON (no account required)
// - Alert on page unload (to prevent unwanted quit)

// v0.4.1 - 1459874150
// - Added new Lord Costume
// - New sets added (Dasomier2, Token of return)
// - Sets items not included in conversions anymore
// (platinum will come soon)

// v0.4 - 1459249200
// Background by @Venevolence / Fizintine
// - Stats UI updated
// - Sets added
// - Accounts added
// - Save and load added
// - Coocoo machine added
// - Various fixes
// Please report any bug on fwtforum (Help link). Thanks and enjoy :)
?>

