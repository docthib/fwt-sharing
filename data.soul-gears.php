<?php

define("SOUL_GEAR_DIRECTORY", "/assets/images/sg/");

$soul_gears["Angela"][0]["weapon"]["name"]                          = "Valkyrie's Staff";
$soul_gears["Angela"][0]["weapon"]["img"]                           = "staff.png";
$soul_gears["Angela"][0]["weapon"]["stats"]["Defense"]              = 400;
$soul_gears["Angela"][0]["weapon"]["stats"]["Crit Rate"]            = 200;

$soul_gears["Angela"][0]["wings"]["name"]                           = "Valkyrie's Nimble Wings";
$soul_gears["Angela"][0]["wings"]["img"]                            = "wing-paper.png";
$soul_gears["Angela"][0]["wings"]["stats"]["Dodge"]                 = 200;
$soul_gears["Angela"][0]["wings"]["stats"]["Counter Damage"]        = 600;
$soul_gears_set["Angela"][0]["effect2"]                             = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Bearman"][0]["weapon"]["name"]                         = "Valkyrie's Gauntlets";
$soul_gears["Bearman"][0]["weapon"]["img"]                          = "gauntlets.png";
$soul_gears["Bearman"][0]["weapon"]["stats"]["HP"]                  = 1200;
$soul_gears["Bearman"][0]["weapon"]["stats"]["Counter Rate"]        = 200;

$soul_gears["Bearman"][0]["wings"]["name"]                          = "Valkyrie's Gorgeous Wings";
$soul_gears["Bearman"][0]["wings"]["img"]                           = "wing-scissors.png";
$soul_gears["Bearman"][0]["wings"]["stats"]["Defense"]              = 400;
$soul_gears["Bearman"][0]["wings"]["stats"]["Counter Rate"]         = 200;
$soul_gears_set["Bearman"][0]["effect2"]                            = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Carrot"][0]["weapon"]["name"]                          = "Valkyrie's Crossbow";
$soul_gears["Carrot"][0]["weapon"]["img"]                           = "crossbow.png";
$soul_gears["Carrot"][0]["weapon"]["stats"]["Attack"]               = 380;
$soul_gears["Carrot"][0]["weapon"]["stats"]["Counter Damage"]       = 600;

$soul_gears["Carrot"][0]["wings"]["name"]                           = "Valkyrie's Nimble Wings";
$soul_gears["Carrot"][0]["wings"]["img"]                            = "wing-paper.png";
$soul_gears["Carrot"][0]["wings"]["stats"]["Dodge"]                 = 200;
$soul_gears["Carrot"][0]["wings"]["stats"]["Counter Damage"]        = 600;
$soul_gears_set["Carrot"][0]["effect2"]                             = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Chenny"][0]["weapon"]["name"]                          = "Valkyrie's Staff";
$soul_gears["Chenny"][0]["weapon"]["img"]                           = "staff.png";
$soul_gears["Chenny"][0]["weapon"]["stats"]["Defense"]              = 400;
$soul_gears["Chenny"][0]["weapon"]["stats"]["Crit Rate"]            = 200;

$soul_gears["Chenny"][0]["wings"]["name"]                           = "Valkyrie's Gorgeous Wings";
$soul_gears["Chenny"][0]["wings"]["img"]                            = "wing-scissors.png";
$soul_gears["Chenny"][0]["wings"]["stats"]["Defense"]               = 400;
$soul_gears["Chenny"][0]["wings"]["stats"]["Counter Rate"]          = 200;
$soul_gears_set["Chenny"][0]["effect2"]                             = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Chris"][0]["weapon"]["name"]                           = "Valkyrie's Sword";
$soul_gears["Chris"][0]["weapon"]["img"]                            = "sword.png";
$soul_gears["Chris"][0]["weapon"]["stats"]["HP"]                    = 1200;
$soul_gears["Chris"][0]["weapon"]["stats"]["Counter Rate"]          = 200;

$soul_gears["Chris"][0]["wings"]["name"]                            = "Valkyrie's Nimble Wings";
$soul_gears["Chris"][0]["wings"]["img"]                             = "wing-paper.png";
$soul_gears["Chris"][0]["wings"]["stats"]["Dodge"]                  = 200;
$soul_gears["Chris"][0]["wings"]["stats"]["Counter Damage"]         = 600;
$soul_gears_set["Chris"][0]["effect2"]                              = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Cleo"][0]["weapon"]["name"]                            = "Valkyrie's Wand";
$soul_gears["Cleo"][0]["weapon"]["img"]                             = "wand.png";
$soul_gears["Cleo"][0]["weapon"]["stats"]["Defense"]                = 400;
$soul_gears["Cleo"][0]["weapon"]["stats"]["Crit Rate"]              = 200;

$soul_gears["Cleo"][0]["wings"]["name"]                             = "Valkyrie's Nimble Wings";
$soul_gears["Cleo"][0]["wings"]["img"]                              = "wing-paper.png";
$soul_gears["Cleo"][0]["wings"]["stats"]["Dodge"]                   = 200;
$soul_gears["Cleo"][0]["wings"]["stats"]["Counter Damage"]          = 600;
$soul_gears_set["Cleo"][0]["effect2"]                               = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Deborah"][0]["weapon"]["name"]                         = "Valkyrie's Staff";
$soul_gears["Deborah"][0]["weapon"]["img"]                          = "staff.png";
$soul_gears["Deborah"][0]["weapon"]["stats"]["MP"]                  = 32;
$soul_gears["Deborah"][0]["weapon"]["stats"]["Hit Rate"]            = 200;

$soul_gears["Deborah"][0]["wings"]["name"]                          = "Valkyrie's Gorgeous Wings";
$soul_gears["Deborah"][0]["wings"]["img"]                           = "wing-scissors.png";
$soul_gears["Deborah"][0]["wings"]["stats"]["Defense"]              = 400;
$soul_gears["Deborah"][0]["wings"]["stats"]["Counter Rate"]         = 200;
$soul_gears_set["Deborah"][0]["effect2"]                            = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Frankenstein"][0]["weapon"]["name"]                    = "Bloodstained Spear";
$soul_gears["Frankenstein"][0]["weapon"]["img"]                     = "red-spear.png";
$soul_gears["Frankenstein"][0]["weapon"]["stats"]["Attack"]         = 380;
$soul_gears["Frankenstein"][0]["weapon"]["stats"]["Counter Damage"] = 600;

$soul_gears["Frankenstein"][0]["wings"]["name"]                     = "Bloodstained Merciless Wings";
$soul_gears["Frankenstein"][0]["wings"]["img"]                      = "red-wing-scissors.png";
$soul_gears["Frankenstein"][0]["wings"]["stats"]["Defense"]         = 400;
$soul_gears["Frankenstein"][0]["wings"]["stats"]["Counter Rate"]    = 200;
$soul_gears_set["Frankenstein"][0]["effect2"]                       = ["label" => "Decrease incoming Crit Damage by 7%", "stat" => "Critical Damage Taken%", "value" => "7"];

$soul_gears["Fruel"][0]["weapon"]["name"]                           = "Valkyrie's Bow";
$soul_gears["Fruel"][0]["weapon"]["img"]                            = "bow.png";
$soul_gears["Fruel"][0]["weapon"]["stats"]["Attack"]                = 380;
$soul_gears["Fruel"][0]["weapon"]["stats"]["Counter Damage"]        = 600;

$soul_gears["Fruel"][0]["wings"]["name"]                            = "Valkyrie's Gorgeous Wings";
$soul_gears["Fruel"][0]["wings"]["img"]                             = "wing-scissors.png";
$soul_gears["Fruel"][0]["wings"]["stats"]["Defense"]                = 400;
$soul_gears["Fruel"][0]["wings"]["stats"]["Counter Rate"]           = 200;
$soul_gears_set["Fruel"][0]["effect2"]                              = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Jack"][0]["weapon"]["name"]                            = "Valkyrie's Claws";
$soul_gears["Jack"][0]["weapon"]["img"]                             = "claws.png";
$soul_gears["Jack"][0]["weapon"]["stats"]["HP"]                     = 1200;
$soul_gears["Jack"][0]["weapon"]["stats"]["Counter Rate"]           = 200;

$soul_gears["Jack"][0]["wings"]["name"]                             = "Valkyrie's Elegant Wings";
$soul_gears["Jack"][0]["wings"]["img"]                              = "wing-rock.png";
$soul_gears["Jack"][0]["wings"]["stats"]["Crit Rate"]               = 200;
$soul_gears["Jack"][0]["wings"]["stats"]["Hit Rate"]                = 200;
$soul_gears_set["Jack"][0]["effect2"]                               = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Jenny"][0]["weapon"]["name"]                           = "Valkyrie's Rifle";
$soul_gears["Jenny"][0]["weapon"]["img"]                            = "rifle.png";
$soul_gears["Jenny"][0]["weapon"]["stats"]["MP"]                    = 32;
$soul_gears["Jenny"][0]["weapon"]["stats"]["Hit Rate"]              = 200;

$soul_gears["Jenny"][0]["wings"]["name"]                            = "Valkyrie's Nimble Wings";
$soul_gears["Jenny"][0]["wings"]["img"]                             = "wing-paper.png";
$soul_gears["Jenny"][0]["wings"]["stats"]["Dodge"]                  = 200;
$soul_gears["Jenny"][0]["wings"]["stats"]["Counter Damage"]         = 600;
$soul_gears_set["Jenny"][0]["effect2"]                              = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Kai"][0]["weapon"]["name"]                             = "Valkyrie's Greatsword";
$soul_gears["Kai"][0]["weapon"]["img"]                              = "great-sword.png";
$soul_gears["Kai"][0]["weapon"]["stats"]["Defense"]                 = 400;
$soul_gears["Kai"][0]["weapon"]["stats"]["Crit Rate"]               = 200;

$soul_gears["Kai"][0]["wings"]["name"]                              = "Valkyrie's Elegant Wings";
$soul_gears["Kai"][0]["wings"]["img"]                               = "wing-rock.png";
$soul_gears["Kai"][0]["wings"]["stats"]["Crit Rate"]                = 200;
$soul_gears["Kai"][0]["wings"]["stats"]["Hit Rate"]                 = 200;
$soul_gears_set["Kai"][0]["effect2"]                                = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Lena"][0]["weapon"]["name"]                            = "Valkyrie's Mic";
$soul_gears["Lena"][0]["weapon"]["img"]                             = "mic.png";
$soul_gears["Lena"][0]["weapon"]["stats"]["Defense"]                = 400;
$soul_gears["Lena"][0]["weapon"]["stats"]["Crit Rate"]              = 200;

$soul_gears["Lena"][0]["wings"]["name"]                             = "Valkyrie's Gorgeous Wings";
$soul_gears["Lena"][0]["wings"]["img"]                              = "wing-scissors.png";
$soul_gears["Lena"][0]["wings"]["stats"]["Defense"]                 = 400;
$soul_gears["Lena"][0]["wings"]["stats"]["Counter Rate"]            = 200;
$soul_gears_set["Lena"][0]["effect2"]                               = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Mas"][0]["weapon"]["name"]                             = "Valkyrie's Harp";
$soul_gears["Mas"][0]["weapon"]["img"]                              = "harp.png";
$soul_gears["Mas"][0]["weapon"]["stats"]["Dodge"]                   = 200;
$soul_gears["Mas"][0]["weapon"]["stats"]["MP Recovery"]             = 4;

$soul_gears["Mas"][0]["wings"]["name"]                              = "Valkyrie's Nimble Wings";
$soul_gears["Mas"][0]["wings"]["img"]                               = "wing-paper.png";
$soul_gears["Mas"][0]["wings"]["stats"]["Dodge"]                    = 200;
$soul_gears["Mas"][0]["wings"]["stats"]["Counter Damage"]           = 600;
$soul_gears_set["Mas"][0]["effect2"]                                = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Momo"][0]["weapon"]["name"]                            = "Valkyrie's Pistol";
$soul_gears["Momo"][0]["weapon"]["img"]                             = "gun.png";
$soul_gears["Momo"][0]["weapon"]["stats"]["Attack"]                 = 380;
$soul_gears["Momo"][0]["weapon"]["stats"]["Counter Damage"]         = 600;

$soul_gears["Momo"][0]["wings"]["name"]                             = "Valkyrie's Elegant Wings";
$soul_gears["Momo"][0]["wings"]["img"]                              = "wing-rock.png";
$soul_gears["Momo"][0]["wings"]["stats"]["Crit Rate"]               = 200;
$soul_gears["Momo"][0]["wings"]["stats"]["Hit Rate"]                = 200;
$soul_gears_set["Momo"][0]["effect2"]                               = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Mu"][0]["weapon"]["name"]                              = "Valkyrie's Sword";
$soul_gears["Mu"][0]["weapon"]["img"]                               = "sword.png";
$soul_gears["Mu"][0]["weapon"]["stats"]["Defense"]                  = 400;
$soul_gears["Mu"][0]["weapon"]["stats"]["Crit Rate"]                = 200;

$soul_gears["Mu"][0]["wings"]["name"]                               = "Valkyrie's Nimble Wings";
$soul_gears["Mu"][0]["wings"]["img"]                                = "wing-paper.png";
$soul_gears["Mu"][0]["wings"]["stats"]["Dodge"]                     = 200;
$soul_gears["Mu"][0]["wings"]["stats"]["Counter Damage"]            = 600;
$soul_gears_set["Mu"][0]["effect2"]                                 = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Muzaka"][0]["weapon"]["name"]                          = "Bloodstained Arm Knife";
$soul_gears["Muzaka"][0]["weapon"]["img"]                           = "red-arm-knife.png";
$soul_gears["Muzaka"][0]["weapon"]["stats"]["HP"]                   = 1200;
$soul_gears["Muzaka"][0]["weapon"]["stats"]["Counter Rate"]         = 200;

$soul_gears["Muzaka"][0]["wings"]["name"]                           = "Bloodstained Ruthless Wings";
$soul_gears["Muzaka"][0]["wings"]["img"]                            = "red-wing-rock.png";
$soul_gears["Muzaka"][0]["wings"]["stats"]["Crit Rate"]             = 200;
$soul_gears["Muzaka"][0]["wings"]["stats"]["Hit Rate"]              = 200;
$soul_gears_set["Muzaka"][0]["effect2"]                             = ["label" => "Decrease incoming Crit Damage by 7%", "stat" => "Critical Damage Taken%", "value" => "7"];

$soul_gears["Persona"][0]["weapon"]["name"]                         = "Valkyrie's Staff";
$soul_gears["Persona"][0]["weapon"]["img"]                          = "staff.png";
$soul_gears["Persona"][0]["weapon"]["stats"]["Defense"]             = 400;
$soul_gears["Persona"][0]["weapon"]["stats"]["Crit Rate"]           = 200;

$soul_gears["Persona"][0]["wings"]["name"]                          = "Valkyrie's Elegant Wings";
$soul_gears["Persona"][0]["wings"]["img"]                           = "wing-rock.png";
$soul_gears["Persona"][0]["wings"]["stats"]["Crit Rate"]            = 200;
$soul_gears["Persona"][0]["wings"]["stats"]["Hit Rate"]             = 200;
$soul_gears_set["Persona"][0]["effect2"]                            = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Rage"][0]["weapon"]["name"]                            = "Valkyrie's Pistol";
$soul_gears["Rage"][0]["weapon"]["img"]                             = "gun.png";
$soul_gears["Rage"][0]["weapon"]["stats"]["Attack"]                 = 380;
$soul_gears["Rage"][0]["weapon"]["stats"]["Counter Damage"]         = 600;

$soul_gears["Rage"][0]["wings"]["name"]                             = "Valkyrie's Nimble Wings";
$soul_gears["Rage"][0]["wings"]["img"]                              = "wing-paper.png";
$soul_gears["Rage"][0]["wings"]["stats"]["Crit Rate"]               = 200;
$soul_gears["Rage"][0]["wings"]["stats"]["Hit Rate"]                = 200;
$soul_gears_set["Rage"][0]["effect2"]                               = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Raizel"][0]["weapon"]["name"]                          = "Bloodstained Sword";
$soul_gears["Raizel"][0]["weapon"]["img"]                           = "red-sword.png";
$soul_gears["Raizel"][0]["weapon"]["stats"]["Attack"]               = 380;
$soul_gears["Raizel"][0]["weapon"]["stats"]["Counter Damage"]       = 600;

$soul_gears["Raizel"][0]["wings"]["name"]                           = "Bloodstained Cruel Wings";
$soul_gears["Raizel"][0]["wings"]["img"]                            = "red-wing-paper.png";
$soul_gears["Raizel"][0]["wings"]["stats"]["Dodge"]                 = 200;
$soul_gears["Raizel"][0]["wings"]["stats"]["Counter Damage"]        = 600;
$soul_gears_set["Raizel"][0]["effect2"]                             = ["label" => "Decrease incoming Crit Damage by 7%", "stat" => "Critical Damage Taken%", "value" => "7"];

$soul_gears["Raskreia"][0]["weapon"]["name"]                        = "Bloodstained Greatsword";
$soul_gears["Raskreia"][0]["weapon"]["img"]                         = "red-sword.png";
$soul_gears["Raskreia"][0]["weapon"]["stats"]["MP"]                 = 32;
$soul_gears["Raskreia"][0]["weapon"]["stats"]["Hit Rate"]           = 200;

$soul_gears["Raskreia"][0]["wings"]["name"]                         = "Bloodstained Merciless wings";
$soul_gears["Raskreia"][0]["wings"]["img"]                          = "red-wing-scissors.png";
$soul_gears["Raskreia"][0]["wings"]["stats"]["Defense"]             = 400;
$soul_gears["Raskreia"][0]["wings"]["stats"]["Counter Rate"]        = 200;
$soul_gears_set["Raskreia"][0]["effect2"]                           = ["label" => "Decrease incoming Crit Damage by 7%", "stat" => "Critical Damage Taken%", "value" => "7"];

$soul_gears["Serendi"][0]["weapon"]["name"]                         = "Valkyrie's Wand";
$soul_gears["Serendi"][0]["weapon"]["img"]                          = "wand.png";
$soul_gears["Serendi"][0]["weapon"]["stats"]["Defense"]             = 400;
$soul_gears["Serendi"][0]["weapon"]["stats"]["Crit Rate"]           = 200;

$soul_gears["Serendi"][0]["wings"]["name"]                          = "Valkyrie's Elegant Wings";
$soul_gears["Serendi"][0]["wings"]["img"]                           = "wing-rock.png";
$soul_gears["Serendi"][0]["wings"]["stats"]["Dodge"]                = 200;
$soul_gears["Serendi"][0]["wings"]["stats"]["Counter Damage"]       = 200;
$soul_gears_set["Serendi"][0]["effect2"]                            = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Seira"][0]["weapon"]["name"]                           = "Bloodstained Scythe";
$soul_gears["Seira"][0]["weapon"]["img"]                            = "red-scythe.png";
$soul_gears["Seira"][0]["weapon"]["stats"]["Attack"]                = 380;
$soul_gears["Seira"][0]["weapon"]["stats"]["Counter Damage"]        = 600;

$soul_gears["Seira"][0]["wings"]["name"]                            = "Bloodstained Cruel wings";
$soul_gears["Seira"][0]["wings"]["img"]                             = "red-wing-paper.png";
$soul_gears["Seira"][0]["wings"]["stats"]["Dodge"]                  = 200;
$soul_gears["Seira"][0]["wings"]["stats"]["Counter Damage"]         = 600;
$soul_gears_set["Seira"][0]["effect2"]                              = ["label" => "Decrease incoming Crit Damage by 7%", "stat" => "Critical Damage Taken%", "value" => "7"];

$soul_gears["Unknown"][0]["weapon"]["name"]                         = "Valkyrie's Gauntlets";
$soul_gears["Unknown"][0]["weapon"]["img"]                          = "gauntlets.png";
$soul_gears["Unknown"][0]["weapon"]["stats"]["Attack"]              = 380;
$soul_gears["Unknown"][0]["weapon"]["stats"]["Counter Damage"]      = 600;

$soul_gears["Unknown"][0]["wings"]["name"]                          = "Valkyrie's Nimble Wings";
$soul_gears["Unknown"][0]["wings"]["img"]                           = "wing-paper.png";
$soul_gears["Unknown"][0]["wings"]["stats"]["Dodge"]                = 200;
$soul_gears["Unknown"][0]["wings"]["stats"]["Counter Damage"]       = 600;
$soul_gears_set["Unknown"][0]["effect2"]                            = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Yekaterina"][0]["weapon"]["name"]                      = "Valkyrie's Staff";
$soul_gears["Yekaterina"][0]["weapon"]["img"]                       = "staff.png";
$soul_gears["Yekaterina"][0]["weapon"]["stats"]["MP"]               = 32;
$soul_gears["Yekaterina"][0]["weapon"]["stats"]["Hit Rate"]         = 200;

$soul_gears["Yekaterina"][0]["wings"]["name"]                       = "Valkyrie's Elegant Wings";
$soul_gears["Yekaterina"][0]["wings"]["img"]                        = "wing-rock.png";
$soul_gears["Yekaterina"][0]["wings"]["stats"]["Crit Rate"]         = 200;
$soul_gears["Yekaterina"][0]["wings"]["stats"]["Hit Rate"]          = 200;
$soul_gears_set["Yekaterina"][0]["effect2"]                         = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

// 2nd generation SG
$soul_gears["Alex"][0]["weapon"]["name"]                            = "Valkyrie's Spear";
$soul_gears["Alex"][0]["weapon"]["img"]                             = "spear.png";
$soul_gears["Alex"][0]["weapon"]["stats"]["Defense"]                = 400;
$soul_gears["Alex"][0]["weapon"]["stats"]["Crit Rate"]              = 200;

$soul_gears["Alex"][0]["wings"]["name"]                             = "Valkyrie's Nimble Wings";
$soul_gears["Alex"][0]["wings"]["img"]                              = "wing-paper.png";
$soul_gears["Alex"][0]["wings"]["stats"]["Dodge"]                   = 200;
$soul_gears["Alex"][0]["wings"]["stats"]["Counter Damage"]          = 600;
$soul_gears_set["Alex"][0]["effect2"]                               = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Alfred"][0]["weapon"]["name"]                          = "Valkyrie's Staff";
$soul_gears["Alfred"][0]["weapon"]["img"]                           = "staff.png";
$soul_gears["Alfred"][0]["weapon"]["stats"]["MP"]                   = 32;
$soul_gears["Alfred"][0]["weapon"]["stats"]["Hit Rate"]             = 200;

$soul_gears["Alfred"][0]["wings"]["name"]                           = "Valkyrie's Elegant Wings";
$soul_gears["Alfred"][0]["wings"]["img"]                            = "wing-rock.png";
$soul_gears["Alfred"][0]["wings"]["stats"]["Crit Rate"]             = 200;
$soul_gears["Alfred"][0]["wings"]["stats"]["Hit Rate"]              = 200;
$soul_gears_set["Alfred"][0]["effect2"]                             = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Azrael"][0]["weapon"]["name"]                          = "Valkyrie's Spear";
$soul_gears["Azrael"][0]["weapon"]["img"]                           = "spear.png";
$soul_gears["Azrael"][0]["weapon"]["stats"]["Attack"]               = 380;
$soul_gears["Azrael"][0]["weapon"]["stats"]["Counter Damage"]       = 600;

$soul_gears["Azrael"][0]["wings"]["name"]                           = "Valkyrie's Nimble Wings";
$soul_gears["Azrael"][0]["wings"]["img"]                            = "wing-paper.png";
$soul_gears["Azrael"][0]["wings"]["stats"]["Dodge"]                 = 200;
$soul_gears["Azrael"][0]["wings"]["stats"]["Counter Damage"]        = 600;
$soul_gears_set["Azrael"][0]["effect2"]                             = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Belle"][0]["weapon"]["name"]                           = "Valkyrie's Staff";
$soul_gears["Belle"][0]["weapon"]["img"]                            = "staff.png";
$soul_gears["Belle"][0]["weapon"]["stats"]["Dodge"]                 = 200;
$soul_gears["Belle"][0]["weapon"]["stats"]["MP Recovery"]           = 4;

$soul_gears["Belle"][0]["wings"]["name"]                            = "Valkyrie's Gorgeous Wings";
$soul_gears["Belle"][0]["wings"]["img"]                             = "wing-scissors.png";
$soul_gears["Belle"][0]["wings"]["stats"]["Defense"]                = 400;
$soul_gears["Belle"][0]["wings"]["stats"]["Counter Rate"]           = 200;
$soul_gears_set["Belle"][0]["effect2"]                              = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Dolores"][0]["weapon"]["name"]                         = "Valkyrie's Wand";
$soul_gears["Dolores"][0]["weapon"]["img"]                          = "wand.png";
$soul_gears["Dolores"][0]["weapon"]["stats"]["Defense"]             = 400;
$soul_gears["Dolores"][0]["weapon"]["stats"]["Crit Rate"]           = 200;

$soul_gears["Dolores"][0]["wings"]["name"]                          = "Valkyrie's Nimble Wings";
$soul_gears["Dolores"][0]["wings"]["img"]                           = "wing-paper.png";
$soul_gears["Dolores"][0]["wings"]["stats"]["Dodge"]                = 200;
$soul_gears["Dolores"][0]["wings"]["stats"]["Counter Damage"]       = 600;
$soul_gears_set["Dolores"][0]["effect2"]                            = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Dominique"][0]["weapon"]["name"]                       = "Valkyrie's Dagger";
$soul_gears["Dominique"][0]["weapon"]["img"]                        = "dagger.png";
$soul_gears["Dominique"][0]["weapon"]["stats"]["Attack"]            = 380;
$soul_gears["Dominique"][0]["weapon"]["stats"]["Counter Damage"]    = 600;

$soul_gears["Dominique"][0]["wings"]["name"]                        = "Valkyrie's Elegant Wings";
$soul_gears["Dominique"][0]["wings"]["img"]                         = "wing-rock.png";
$soul_gears["Dominique"][0]["wings"]["stats"]["Crit Rate"]          = 200;
$soul_gears["Dominique"][0]["wings"]["stats"]["Hit Rate"]           = 200;
$soul_gears_set["Dominique"][0]["effect2"]                          = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Gillan"][0]["weapon"]["name"]                          = "Valkyrie's Dagger";
$soul_gears["Gillan"][0]["weapon"]["img"]                           = "dagger.png";
$soul_gears["Gillan"][0]["weapon"]["stats"]["Dodge"]                = 200;
$soul_gears["Gillan"][0]["weapon"]["stats"]["MP Recovery"]          = 4;

$soul_gears["Gillan"][0]["wings"]["name"]                           = "Valkyrie's Elegant Wings";
$soul_gears["Gillan"][0]["wings"]["img"]                            = "wing-rock.png";
$soul_gears["Gillan"][0]["wings"]["stats"]["Crit Rate"]             = 200;
$soul_gears["Gillan"][0]["wings"]["stats"]["Hit Rate"]              = 200;
$soul_gears_set["Gillan"][0]["effect2"]                             = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Ian"][0]["weapon"]["name"]                             = "Valkyrie's Crossbow";
$soul_gears["Ian"][0]["weapon"]["img"]                              = "crossbow.png";
$soul_gears["Ian"][0]["weapon"]["stats"]["Defense"]                 = 400;
$soul_gears["Ian"][0]["weapon"]["stats"]["Crit Rate"]               = 200;

$soul_gears["Ian"][0]["wings"]["name"]                              = "Valkyrie's Elegant Wings";
$soul_gears["Ian"][0]["wings"]["img"]                               = "wing-rock.png";
$soul_gears["Ian"][0]["wings"]["stats"]["Crit Rate"]                = 200;
$soul_gears["Ian"][0]["wings"]["stats"]["Hit Rate"]                 = 200;
$soul_gears_set["Ian"][0]["effect2"]                                = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Jin"][0]["weapon"]["name"]                             = "Valkyrie's Two-handed Axe";
$soul_gears["Jin"][0]["weapon"]["img"]                              = "battle-axe.png";
$soul_gears["Jin"][0]["weapon"]["stats"]["Attack"]                  = 380;
$soul_gears["Jin"][0]["weapon"]["stats"]["Counter Damage"]          = 600;

$soul_gears["Jin"][0]["wings"]["name"]                              = "Valkyrie's Gorgeous Wings";
$soul_gears["Jin"][0]["wings"]["img"]                               = "wing-scissors.png";
$soul_gears["Jin"][0]["wings"]["stats"]["Defense"]                  = 400;
$soul_gears["Jin"][0]["wings"]["stats"]["Counter Rate"]             = 200;
$soul_gears_set["Jin"][0]["effect2"]                                = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Kitty"][0]["weapon"]["name"]                           = "Valkyrie's Bracelet";
$soul_gears["Kitty"][0]["weapon"]["img"]                            = "bracelet.png";
$soul_gears["Kitty"][0]["weapon"]["stats"]["Defense"]               = 400;
$soul_gears["Kitty"][0]["weapon"]["stats"]["Crit Rate"]             = 200;

$soul_gears["Kitty"][0]["wings"]["name"]                            = "Valkyrie's Elegant Wings";
$soul_gears["Kitty"][0]["wings"]["img"]                             = "wing-rock.png";
$soul_gears["Kitty"][0]["wings"]["stats"]["Crit Rate"]              = 200;
$soul_gears["Kitty"][0]["wings"]["stats"]["Hit Rate"]               = 200;
$soul_gears_set["Kitty"][0]["effect2"]                              = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Lilid"][0]["weapon"]["name"]                           = "Valkyrie's Claws";
$soul_gears["Lilid"][0]["weapon"]["img"]                            = "claws.png";
$soul_gears["Lilid"][0]["weapon"]["stats"]["HP"]                    = 1200;
$soul_gears["Lilid"][0]["weapon"]["stats"]["Counter Rate"]          = 200;

$soul_gears["Lilid"][0]["wings"]["name"]                            = "Valkyrie's Elegant Wings";
$soul_gears["Lilid"][0]["wings"]["img"]                             = "wing-rock.png";
$soul_gears["Lilid"][0]["wings"]["stats"]["Crit Rate"]              = 200;
$soul_gears["Lilid"][0]["wings"]["stats"]["Hit Rate"]               = 200;
$soul_gears_set["Lilid"][0]["effect2"]                              = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Muang"][0]["weapon"]["name"]                           = "Valkyrie's Rifle";
$soul_gears["Muang"][0]["weapon"]["img"]                            = "rifle.png";
$soul_gears["Muang"][0]["weapon"]["stats"]["MP"]                    = 32;
$soul_gears["Muang"][0]["weapon"]["stats"]["Hit Rate"]              = 200;

$soul_gears["Muang"][0]["wings"]["name"]                            = "Valkyrie's Gorgeous Wings";
$soul_gears["Muang"][0]["wings"]["img"]                             = "wing-scissors.png";
$soul_gears["Muang"][0]["wings"]["stats"]["Defense"]                = 400;
$soul_gears["Muang"][0]["wings"]["stats"]["Counter Rate"]           = 200;
$soul_gears_set["Muang"][0]["effect2"]                              = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Nirvana"][0]["weapon"]["name"]                         = "Valkyrie's Staff";
$soul_gears["Nirvana"][0]["weapon"]["img"]                          = "staff.png";
$soul_gears["Nirvana"][0]["weapon"]["stats"]["Dodge"]               = 200;
$soul_gears["Nirvana"][0]["weapon"]["stats"]["MP Recover"]          = 4;

$soul_gears["Nirvana"][0]["wings"]["name"]                          = "Valkyrie's Nimble Wings";
$soul_gears["Nirvana"][0]["wings"]["img"]                           = "wing-paper.png";
$soul_gears["Nirvana"][0]["wings"]["stats"]["Dodge"]                = 200;
$soul_gears["Nirvana"][0]["wings"]["stats"]["Counter Damage"]       = 600;
$soul_gears_set["Nirvana"][0]["effect2"]                            = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Nox"][0]["weapon"]["name"]                             = "Valkyrie's Wand";
$soul_gears["Nox"][0]["weapon"]["img"]                              = "wand.png";
$soul_gears["Nox"][0]["weapon"]["stats"]["Attack"]                  = 380;
$soul_gears["Nox"][0]["weapon"]["stats"]["Counter Damage"]          = 600;

$soul_gears["Nox"][0]["wings"]["name"]                              = "Valkyrie's Nimble Wings";
$soul_gears["Nox"][0]["wings"]["img"]                               = "wing-paper.png";
$soul_gears["Nox"][0]["wings"]["stats"]["Dodge"]                    = 200;
$soul_gears["Nox"][0]["wings"]["stats"]["Counter Damage"]           = 600;
$soul_gears_set["Nox"][0]["effect2"]                                = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Raboff"][0]["weapon"]["name"]                          = "Valkyrie's Greatsword";
$soul_gears["Raboff"][0]["weapon"]["img"]                           = "great-sword.png";
$soul_gears["Raboff"][0]["weapon"]["stats"]["HP"]                   = 1200;
$soul_gears["Raboff"][0]["weapon"]["stats"]["Counter Rate"]         = 200;

$soul_gears["Raboff"][0]["wings"]["name"]                           = "Valkyrie's Nimble Wings";
$soul_gears["Raboff"][0]["wings"]["img"]                            = "wing-paper.png";
$soul_gears["Raboff"][0]["wings"]["stats"]["Dodge"]                 = 200;
$soul_gears["Raboff"][0]["wings"]["stats"]["Counter Damage"]        = 600;
$soul_gears_set["Raboff"][0]["effect2"]                             = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Shark"][0]["weapon"]["name"]                           = "Valkyrie's Two-handed Axe";
$soul_gears["Shark"][0]["weapon"]["img"]                            = "battle-axe.png";
$soul_gears["Shark"][0]["weapon"]["stats"]["Defense"]               = 400;
$soul_gears["Shark"][0]["weapon"]["stats"]["Crit Rate"]             = 200;

$soul_gears["Shark"][0]["wings"]["name"]                            = "Valkyrie's Gorgeous Wings";
$soul_gears["Shark"][0]["wings"]["img"]                             = "wing-scissors.png";
$soul_gears["Shark"][0]["wings"]["stats"]["Defense"]                = 400;
$soul_gears["Shark"][0]["wings"]["stats"]["Counter Rate"]           = 200;
$soul_gears_set["Shark"][0]["effect2"]                              = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Shu Shu"][0]["weapon"]["name"]                         = "Valkyrie's Rifle";
$soul_gears["Shu Shu"][0]["weapon"]["img"]                          = "rifle.png";
$soul_gears["Shu Shu"][0]["weapon"]["stats"]["Dodge"]               = 200;
$soul_gears["Shu Shu"][0]["weapon"]["stats"]["MP Recovery"]         = 4;

$soul_gears["Shu Shu"][0]["wings"]["name"]                          = "Valkyrie's Elegant Wings";
$soul_gears["Shu Shu"][0]["wings"]["img"]                           = "wing-rock.png";
$soul_gears["Shu Shu"][0]["wings"]["stats"]["Crit Rate"]            = 200;
$soul_gears["Shu Shu"][0]["wings"]["stats"]["Hit Rate"]             = 200;
$soul_gears_set["Shu Shu"][0]["effect2"]                            = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Sione"][0]["weapon"]["name"]                           = "Valkyrie's Orb";
$soul_gears["Sione"][0]["weapon"]["img"]                            = "orb.png";
$soul_gears["Sione"][0]["weapon"]["stats"]["Attack"]                = 380;
$soul_gears["Sione"][0]["weapon"]["stats"]["Counter Damage"]        = 600;

$soul_gears["Sione"][0]["wings"]["name"]                            = "Valkyrie's Gorgeous Wings";
$soul_gears["Sione"][0]["wings"]["img"]                             = "wing-scissors.png";
$soul_gears["Sione"][0]["wings"]["stats"]["Defense"]                = 400;
$soul_gears["Sione"][0]["wings"]["stats"]["Counter Rate"]           = 200;
$soul_gears_set["Sione"][0]["effect2"]                              = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Tao"][0]["weapon"]["name"]                             = "Valkyrie's Bracelet";
$soul_gears["Tao"][0]["weapon"]["img"]                              = "bracelet.png";
$soul_gears["Tao"][0]["weapon"]["stats"]["Attack"]                  = 380;
$soul_gears["Tao"][0]["weapon"]["stats"]["Counter Damage"]          = 600;

$soul_gears["Tao"][0]["wings"]["name"]                              = "Valkyrie's Gorgeous Wings";
$soul_gears["Tao"][0]["wings"]["img"]                               = "wing-scissors.png";
$soul_gears["Tao"][0]["wings"]["stats"]["Defense"]                  = 400;
$soul_gears["Tao"][0]["wings"]["stats"]["Counter Rate"]             = 200;
$soul_gears_set["Tao"][0]["effect2"]                                = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

// 3rd generation SG

$soul_gears["Lance"][0]["weapon"]["name"]                           = "Valkyrie's Crown";
$soul_gears["Lance"][0]["weapon"]["img"]                            = "crown.png";
$soul_gears["Lance"][0]["weapon"]["stats"]["HP"]                    = 1200;
$soul_gears["Lance"][0]["weapon"]["stats"]["Counter Rate"]          = 200;

$soul_gears["Lance"][0]["wings"]["name"]                            = "Valkyrie's Gorgeous Wings";
$soul_gears["Lance"][0]["wings"]["img"]                             = "wing-scissors.png";
$soul_gears["Lance"][0]["wings"]["stats"]["Defense"]                = 400;
$soul_gears["Lance"][0]["wings"]["stats"]["Counter Rate"]           = 200;
$soul_gears_set["Lance"][0]["effect2"]                              = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Henry"][0]["weapon"]["name"]                           = "Valkyrie's Gauntlets";
$soul_gears["Henry"][0]["weapon"]["img"]                            = "gauntlets.png";
$soul_gears["Henry"][0]["weapon"]["stats"]["Attack"]                = 380;
$soul_gears["Henry"][0]["weapon"]["stats"]["Counter Damage"]        = 600;

$soul_gears["Henry"][0]["wings"]["name"]                            = "Valkyrie's Elegant Wings";
$soul_gears["Henry"][0]["wings"]["img"]                             = "wing-rock.png";
$soul_gears["Henry"][0]["wings"]["stats"]["Crit Rate"]              = 200;
$soul_gears["Henry"][0]["wings"]["stats"]["Hit Rate"]               = 200;
$soul_gears_set["Henry"][0]["effect2"]                              = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Lee"][0]["weapon"]["name"]                             = "Valkyrie's Orb";
$soul_gears["Lee"][0]["weapon"]["img"]                              = "orb.png";
$soul_gears["Lee"][0]["weapon"]["stats"]["Attack"]                  = 380;
$soul_gears["Lee"][0]["weapon"]["stats"]["Counter Damage"]          = 600;

$soul_gears["Lee"][0]["wings"]["name"]                              = "Valkyrie's Gorgeous Wings";
$soul_gears["Lee"][0]["wings"]["img"]                               = "wing-scissors.png";
$soul_gears["Lee"][0]["wings"]["stats"]["Defense"]                  = 400;
$soul_gears["Lee"][0]["wings"]["stats"]["Counter Rate"]             = 200;
$soul_gears_set["Lee"][0]["effect2"]                                = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Poni"][0]["weapon"]["name"]                            = "Valkyrie's Armor";
$soul_gears["Poni"][0]["weapon"]["img"]                             = "armor.png";
$soul_gears["Poni"][0]["weapon"]["stats"]["Defense"]                = 400;
$soul_gears["Poni"][0]["weapon"]["stats"]["Crit Rate"]              = 200;

$soul_gears["Poni"][0]["wings"]["name"]                             = "Valkyrie's Gorgeous Wings";
$soul_gears["Poni"][0]["wings"]["img"]                              = "wing-scissors.png";
$soul_gears["Poni"][0]["wings"]["stats"]["Defense"]                 = 400;
$soul_gears["Poni"][0]["wings"]["stats"]["Counter Rate"]            = 200;
$soul_gears_set["Poni"][0]["effect2"]                               = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Lily"][0]["weapon"]["name"]                            = "Valkyrie's Wand";
$soul_gears["Lily"][0]["weapon"]["img"]                             = "wand.png";
$soul_gears["Lily"][0]["weapon"]["stats"]["Attack"]                 = 380;
$soul_gears["Lily"][0]["weapon"]["stats"]["Counter Damage"]         = 200;

$soul_gears["Lily"][0]["wings"]["name"]                             = "Valkyrie's Nimble Wings";
$soul_gears["Lily"][0]["wings"]["img"]                              = "wing-paper.png";
$soul_gears["Lily"][0]["wings"]["stats"]["Dodge"]                   = 200;
$soul_gears["Lily"][0]["wings"]["stats"]["Counter Damage"]          = 600;
$soul_gears_set["Lily"][0]["effect2"]                               = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Klein"][0]["weapon"]["name"]                           = "Valkyrie's Staff";
$soul_gears["Klein"][0]["weapon"]["img"]                            = "staff.png";
$soul_gears["Klein"][0]["weapon"]["stats"]["MP"]                    = 32;
$soul_gears["Klein"][0]["weapon"]["stats"]["Hit Rate"]              = 200;

$soul_gears["Klein"][0]["wings"]["name"]                            = "Valkyrie's Nimble Wings";
$soul_gears["Klein"][0]["wings"]["img"]                             = "wing-paper.png";
$soul_gears["Klein"][0]["wings"]["stats"]["Dodge"]                  = 200;
$soul_gears["Klein"][0]["wings"]["stats"]["Counter Damage"]         = 600;
$soul_gears_set["Klein"][0]["effect2"]                              = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["SonicBoom"][0]["weapon"]["name"]                       = "Valkyrie's Shuriken";
$soul_gears["SonicBoom"][0]["weapon"]["img"]                        = "shuriken.png";
$soul_gears["SonicBoom"][0]["weapon"]["stats"]["Defense"]           = 400;
$soul_gears["SonicBoom"][0]["weapon"]["stats"]["Crit Rate"]         = 200;

$soul_gears["SonicBoom"][0]["wings"]["name"]                        = "Valkyrie's Gorgeous Wings";
$soul_gears["SonicBoom"][0]["wings"]["img"]                         = "wing-scissors.png";
$soul_gears["SonicBoom"][0]["wings"]["stats"]["Defense"]            = 400;
$soul_gears["SonicBoom"][0]["wings"]["stats"]["Counter Rate"]       = 200;
$soul_gears_set["SonicBoom"][0]["effect2"]                          = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Sraka"][0]["weapon"]["name"]                           = "Valkyrie's Axe";
$soul_gears["Sraka"][0]["weapon"]["img"]                            = "axe.png";
$soul_gears["Sraka"][0]["weapon"]["stats"]["Attack"]                = 380;
$soul_gears["Sraka"][0]["weapon"]["stats"]["Counter Damage"]        = 600;

$soul_gears["Sraka"][0]["wings"]["name"]                            = "Valkyrie's Gorgeous Wings";
$soul_gears["Sraka"][0]["wings"]["img"]                             = "wing-scissors.png";
$soul_gears["Sraka"][0]["wings"]["stats"]["Defense"]                = 400;
$soul_gears["Sraka"][0]["wings"]["stats"]["Counter Rate"]           = 200;
$soul_gears_set["Sraka"][0]["effect2"]                              = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Elektra"][0]["weapon"]["name"]                         = "Valkyrie's Sword";
$soul_gears["Elektra"][0]["weapon"]["img"]                          = "sword.png";
$soul_gears["Elektra"][0]["weapon"]["stats"]["Dodge"]               = 200;
$soul_gears["Elektra"][0]["weapon"]["stats"]["MP Recovery"]         = 4;

$soul_gears["Elektra"][0]["wings"]["name"]                          = "Valkyrie's Gorgeous Wings";
$soul_gears["Elektra"][0]["wings"]["img"]                           = "wing-scissors.png";
$soul_gears["Elektra"][0]["wings"]["stats"]["Defense"]              = 400;
$soul_gears["Elektra"][0]["wings"]["stats"]["Counter Rate"]         = 200;
$soul_gears_set["Elektra"][0]["effect2"]                            = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Zero"][0]["weapon"]["name"]                            = "Valkyrie's Dagger";
$soul_gears["Zero"][0]["weapon"]["img"]                             = "dagger.png";
$soul_gears["Zero"][0]["weapon"]["stats"]["MP"]                     = 32;
$soul_gears["Zero"][0]["weapon"]["stats"]["Hit Rate"]               = 200;

$soul_gears["Zero"][0]["wings"]["name"]                             = "Valkyrie's Elegant Wings";
$soul_gears["Zero"][0]["wings"]["img"]                              = "wing-rock.png";
$soul_gears["Zero"][0]["wings"]["stats"]["Crit Rate"]               = 200;
$soul_gears["Zero"][0]["wings"]["stats"]["Hit Rate"]                = 200;
$soul_gears_set["Zero"][0]["effect2"]                               = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Frankenstein"][1]["weapon"]["name"]                    = "Valkyrie's Spear";
$soul_gears["Frankenstein"][1]["weapon"]["img"]                     = "spear.png";
$soul_gears["Frankenstein"][1]["weapon"]["stats"]["Attack"]         = 380;
$soul_gears["Frankenstein"][1]["weapon"]["stats"]["Counter Damage"] = 600;

$soul_gears["Frankenstein"][1]["wings"]["name"]                     = "Valkyrie's Gorgeous Wings";
$soul_gears["Frankenstein"][1]["wings"]["img"]                      = "wing-scissors.png";
$soul_gears["Frankenstein"][1]["wings"]["stats"]["Defense"]         = 400;
$soul_gears["Frankenstein"][1]["wings"]["stats"]["Counter Rate"]    = 200;
$soul_gears_set["Frankenstein"][1]["effect2"]                       = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Seira"][1]["weapon"]["name"]                           = "Valkyrie's Sickle";
// $soul_gears["Seira"][1]["weapon"]["img"]                         = "sickle.png";
$soul_gears["Seira"][1]["weapon"]["stats"]["Attack"]                = 380;
$soul_gears["Seira"][1]["weapon"]["stats"]["Counter Damage"]        = 600;

$soul_gears["Seira"][1]["wings"]["name"]                            = "Valkyrie's Nimble Wings";
$soul_gears["Seira"][1]["wings"]["img"]                             = "wing-paper.png";
$soul_gears["Seira"][1]["wings"]["stats"]["Dodge"]                  = 200;
$soul_gears["Seira"][1]["wings"]["stats"]["Counter Damage"]         = 600;
$soul_gears_set["Seira"][1]["effect2"]                              = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Muzaka"][1]["weapon"]["name"]                          = "Valkyrie's Arm Knife";
$soul_gears["Muzaka"][1]["weapon"]["img"]                           = "arm-knife.png";
$soul_gears["Muzaka"][1]["weapon"]["stats"]["HP"]                   = 1200;
$soul_gears["Muzaka"][1]["weapon"]["stats"]["Counter Rate"]         = 200;

$soul_gears["Muzaka"][1]["wings"]["name"]                           = "Valkyrie's Elegant Wings";
$soul_gears["Muzaka"][1]["wings"]["img"]                            = "wing-rock.png";
$soul_gears["Muzaka"][1]["wings"]["stats"]["Crit Rate"]             = 200;
$soul_gears["Muzaka"][1]["wings"]["stats"]["Hit Rate"]              = 200;
$soul_gears_set["Muzaka"][1]["effect2"]                             = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Raskreia"][1]["weapon"]["name"]                        = "Valkyrie's Greatsword";
$soul_gears["Raskreia"][1]["weapon"]["img"]                         = "sword.png";
$soul_gears["Raskreia"][1]["weapon"]["stats"]["MP"]                 = 32;
$soul_gears["Raskreia"][1]["weapon"]["stats"]["Hit Rate"]           = 200;

$soul_gears["Raskreia"][1]["wings"]["name"]                         = "Valkyrie's Gorgeous Wings";
$soul_gears["Raskreia"][1]["wings"]["img"]                          = "wing-scissors.png";
$soul_gears["Raskreia"][1]["wings"]["stats"]["Defense"]             = 400;
$soul_gears["Raskreia"][1]["wings"]["stats"]["Counter Rate"]        = 200;
$soul_gears_set["Raskreia"][1]["effect2"]                           = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

// 4th generation SG

$soul_gears["Raizel"][1]["weapon"]["name"]                          = "Valkyrie's Sword";
$soul_gears["Raizel"][1]["weapon"]["img"]                           = "sword.png";
$soul_gears["Raizel"][1]["weapon"]["stats"]["Attack"]               = 380;
$soul_gears["Raizel"][1]["weapon"]["stats"]["Counter Damage"]       = 600;

$soul_gears["Raizel"][1]["wings"]["name"]                           = "Valkyrie's Nimble Wings";
$soul_gears["Raizel"][1]["wings"]["img"]                            = "wing-paper.png";
$soul_gears["Raizel"][1]["wings"]["stats"]["Dodge"]                 = 200;
$soul_gears["Raizel"][1]["wings"]["stats"]["Counter Damage"]        = 600;
$soul_gears_set["Raizel"][1]["effect2"]                             = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Moa"][0]["weapon"]["name"]                             = "Valkyrie's Staff";
$soul_gears["Moa"][0]["weapon"]["img"]                              = "staff.png";
$soul_gears["Moa"][0]["weapon"]["stats"]["MP"]                      = 32;
$soul_gears["Moa"][0]["weapon"]["stats"]["Hit Rate"]                = 200;

$soul_gears["Moa"][0]["wings"]["name"]                              = "Valkyrie's Nimble Wings";
$soul_gears["Moa"][0]["wings"]["img"]                               = "wing-paper.png";
$soul_gears["Moa"][0]["wings"]["stats"]["Dodge"]                    = 200;
$soul_gears["Moa"][0]["wings"]["stats"]["Counter Damage"]           = 600;
$soul_gears_set["Moa"][0]["effect2"]                                = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Reina"][0]["weapon"]["name"]                           = "Valkyrie's Axe";
$soul_gears["Reina"][0]["weapon"]["img"]                            = "axe.png";
$soul_gears["Reina"][0]["weapon"]["stats"]["Defense"]               = 400;
$soul_gears["Reina"][0]["weapon"]["stats"]["Crit Rate"]             = 200;

$soul_gears["Reina"][0]["wings"]["name"]                            = "Valkyrie's Gorgeous Wings";
$soul_gears["Reina"][0]["wings"]["img"]                             = "wing-scissors.png";
$soul_gears["Reina"][0]["wings"]["stats"]["Defense"]                = 400;
$soul_gears["Reina"][0]["wings"]["stats"]["Counter Rate"]           = 200;
$soul_gears_set["Reina"][0]["effect2"]                              = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Krut"][0]["weapon"]["name"]                            = "Valkyrie's Axe";
$soul_gears["Krut"][0]["weapon"]["img"]                             = "axe.png";
$soul_gears["Krut"][0]["weapon"]["stats"]["Attack"]                 = 380;
$soul_gears["Krut"][0]["weapon"]["stats"]["Counter Damage"]         = 600;

$soul_gears["Krut"][0]["wings"]["name"]                             = "Valkyrie's Elegant Wings";
$soul_gears["Krut"][0]["wings"]["img"]                              = "wing-rock.png";
$soul_gears["Krut"][0]["wings"]["stats"]["Crit Rate"]               = 200;
$soul_gears["Krut"][0]["wings"]["stats"]["Hit Rate"]                = 200;
$soul_gears_set["Krut"][0]["effect2"]                               = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Valkyrie"][0]["weapon"]["name"]                        = "Valkyrie's Sword";
$soul_gears["Valkyrie"][0]["weapon"]["img"]                         = "sword.png";
$soul_gears["Valkyrie"][0]["weapon"]["stats"]["HP"]                 = 1200;
$soul_gears["Valkyrie"][0]["weapon"]["stats"]["Counter Rate"]       = 200;

$soul_gears["Valkyrie"][0]["wings"]["name"]                         = "Valkyrie's Nimble Wings";
$soul_gears["Valkyrie"][0]["wings"]["img"]                          = "wing-paper.png";
$soul_gears["Valkyrie"][0]["wings"]["stats"]["Dodge"]               = 200;
$soul_gears["Valkyrie"][0]["wings"]["stats"]["Counter Damage"]      = 600;
$soul_gears_set["Valkyrie"][0]["effect2"]                           = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Celestial"][0]["weapon"]["name"]                       = "Valkyrie's Staff";
$soul_gears["Celestial"][0]["weapon"]["img"]                        = "staff.png";
$soul_gears["Celestial"][0]["weapon"]["stats"]["Dodge"]             = 200;
$soul_gears["Celestial"][0]["weapon"]["stats"]["MP Recovery"]       = 4;

$soul_gears["Celestial"][0]["wings"]["name"]                        = "Valkyrie's Nimble Wings";
$soul_gears["Celestial"][0]["wings"]["img"]                         = "wing-paper.png";
$soul_gears["Celestial"][0]["wings"]["stats"]["Dodge"]              = 200;
$soul_gears["Celestial"][0]["wings"]["stats"]["Counter Damage"]     = 600;
$soul_gears_set["Celestial"][0]["effect2"]                          = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Deimos"][0]["weapon"]["name"]                          = "Valkyrie's Dagger";
$soul_gears["Deimos"][0]["weapon"]["img"]                           = "dagger.png";
$soul_gears["Deimos"][0]["weapon"]["stats"]["HP"]                   = 1200;
$soul_gears["Deimos"][0]["weapon"]["stats"]["Counter Rate"]         = 200;

$soul_gears["Deimos"][0]["wings"]["name"]                           = "Valkyrie's Gorgeous Wings";
$soul_gears["Deimos"][0]["wings"]["img"]                            = "wing-scissors.png";
$soul_gears["Deimos"][0]["wings"]["stats"]["Defense"]               = 400;
$soul_gears["Deimos"][0]["wings"]["stats"]["Counter Rate"]          = 200;
$soul_gears_set["Deimos"][0]["effect2"]                             = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Banshee"][0]["weapon"]["name"]                         = "Valkyrie's Staff";
$soul_gears["Banshee"][0]["weapon"]["img"]                          = "staff.png";
$soul_gears["Banshee"][0]["weapon"]["stats"]["Defense"]             = 400;
$soul_gears["Banshee"][0]["weapon"]["stats"]["Crit Rate"]           = 200;

$soul_gears["Banshee"][0]["wings"]["name"]                          = "Valkyrie's Gorgeous Wings";
$soul_gears["Banshee"][0]["wings"]["img"]                           = "wing-scissors.png";
$soul_gears["Banshee"][0]["wings"]["stats"]["Defense"]              = 400;
$soul_gears["Banshee"][0]["wings"]["stats"]["Counter Rate"]         = 200;
$soul_gears_set["Banshee"][0]["effect2"]                            = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Spooky"][0]["weapon"]["name"]                          = "Valkyrie's Staff";
$soul_gears["Spooky"][0]["weapon"]["img"]                           = "staff.png";
$soul_gears["Spooky"][0]["weapon"]["stats"]["Dodge"]                = 200;
$soul_gears["Spooky"][0]["weapon"]["stats"]["MP Recovery"]          = 4;

$soul_gears["Spooky"][0]["wings"]["name"]                           = "Valkyrie's Elegant Wings";
$soul_gears["Spooky"][0]["wings"]["img"]                            = "wing-rock.png";
$soul_gears["Spooky"][0]["wings"]["stats"]["Crit Rate"]             = 200;
$soul_gears["Spooky"][0]["wings"]["stats"]["Hit Rate"]              = 200;
$soul_gears_set["Spooky"][0]["effect2"]                             = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

// BlazBlue SG

$soul_gears["Ragna"][0]["weapon"]["name"]                         = "Reddened Aramasa";
$soul_gears["Ragna"][0]["weapon"]["img"]                          = "xx.png";
$soul_gears["Ragna"][0]["weapon"]["stats"]["Attack"]              = 380;
$soul_gears["Ragna"][0]["weapon"]["stats"]["Counter Damage"]      = 600;

$soul_gears["Ragna"][0]["wings"]["name"]                          = "Reaper's Emblem";
$soul_gears["Ragna"][0]["wings"]["img"]                           = "ww.png";
$soul_gears["Ragna"][0]["wings"]["stats"]["Defense"]              = 400;
$soul_gears["Ragna"][0]["wings"]["stats"]["Counter Rate"]         = 200;
$soul_gears_set["Ragna"][0]["effect2"]                            = ["label" => "Increase Max MP by 25", "stat" => "MP", "value" => "25"];

$soul_gears["Jin Kisaragi"][0]["weapon"]["name"]                  = "Reddened Yukianesa";
$soul_gears["Jin Kisaragi"][0]["weapon"]["img"]                   = "xx.png";
$soul_gears["Jin Kisaragi"][0]["weapon"]["stats"]["HP"]           = 1200;
$soul_gears["Jin Kisaragi"][0]["weapon"]["stats"]["Counter Rate"] = 200;

$soul_gears["Jin Kisaragi"][0]["wings"]["name"]                   = "Hero's Emblem";
$soul_gears["Jin Kisaragi"][0]["wings"]["img"]                    = "ww.png";
$soul_gears["Jin Kisaragi"][0]["wings"]["stats"]["Crit Rate"]     = 200;
$soul_gears["Jin Kisaragi"][0]["wings"]["stats"]["Hit Rate"]      = 200;
$soul_gears_set["Jin Kisaragi"][0]["effect2"]                     = ["label" => "Increase Max MP by 25", "stat" => "MP", "value" => "25"];

$soul_gears["Noel"][0]["weapon"]["name"]                          = "Reddened Bolverk";
$soul_gears["Noel"][0]["weapon"]["img"]                           = "xx.png";
$soul_gears["Noel"][0]["weapon"]["stats"]["Attack"]               = 380;
$soul_gears["Noel"][0]["weapon"]["stats"]["Counter Damage"]       = 600;

$soul_gears["Noel"][0]["wings"]["name"]                           = "Successor's Emblem";
$soul_gears["Noel"][0]["wings"]["img"]                            = "ww.png";
$soul_gears["Noel"][0]["wings"]["stats"]["Crit Rate"]             = 200;
$soul_gears["Noel"][0]["wings"]["stats"]["Hit Rate"]              = 200;
$soul_gears_set["Noel"][0]["effect2"]                             = ["label" => "Increase Max MP by 25", "stat" => "MP", "value" => "25"];

$soul_gears["Rachel"][0]["weapon"]["name"]                        = "Reddened Vampire";
$soul_gears["Rachel"][0]["weapon"]["img"]                         = "xx.png";
$soul_gears["Rachel"][0]["weapon"]["stats"]["MP"]                 = 32;
$soul_gears["Rachel"][0]["weapon"]["stats"]["Hit Rate"]           = 200;

$soul_gears["Rachel"][0]["wings"]["name"]                         = "Vampire's Emblem";
$soul_gears["Rachel"][0]["wings"]["img"]                          = "ww.png";
$soul_gears["Rachel"][0]["wings"]["stats"]["Dodge"]               = 200;
$soul_gears["Rachel"][0]["wings"]["stats"]["Counter Damage"]      = 600;
$soul_gears_set["Rachel"][0]["effect2"]                           = ["label" => "Increase Max MP by 25", "stat" => "MP", "value" => "25"];

// MapleStory SG

$soul_gears["Phantom"][0]["weapon"]["name"]                    = "Saint Cane";
$soul_gears["Phantom"][0]["weapon"]["img"]                     = "xx.png";
$soul_gears["Phantom"][0]["weapon"]["stats"]["Attack"]         = 380;
$soul_gears["Phantom"][0]["weapon"]["stats"]["Counter Damage"] = 600;

$soul_gears["Phantom"][0]["wings"]["name"]                     = "Phantom's Husky";
$soul_gears["Phantom"][0]["wings"]["img"]                      = "ww.png";
$soul_gears["Phantom"][0]["wings"]["stats"]["Defense"]         = 400;
$soul_gears["Phantom"][0]["wings"]["stats"]["Counter Rate"]    = 200;
$soul_gears_set["Phantom"][0]["effect2"]                       = ["label" => "Decrease skill MP cost by 10%", "stat" => "MP Cost%", "value" => "-10"];

$soul_gears["Mercedes"][0]["weapon"]["name"]                   = "Saint Dual Bowguns";
$soul_gears["Mercedes"][0]["weapon"]["img"]                    = "xx.png";
$soul_gears["Mercedes"][0]["weapon"]["stats"]["HP"]            = 1200;
$soul_gears["Mercedes"][0]["weapon"]["stats"]["Counter Rate"]  = 200;

$soul_gears["Mercedes"][0]["wings"]["name"]                    = "Mercedes's Yeti";
$soul_gears["Mercedes"][0]["wings"]["img"]                     = "ww.png";
$soul_gears["Mercedes"][0]["wings"]["stats"]["Crit Rate"]      = 200;
$soul_gears["Mercedes"][0]["wings"]["stats"]["Hit Rate"]       = 200;
$soul_gears_set["Mercedes"][0]["effect2"]                      = ["label" => "Decrease skill MP cost by 10%", "stat" => "MP Cost%", "value" => "-10"];

$soul_gears["Evan"][0]["weapon"]["name"]                       = "Saint Wand";
$soul_gears["Evan"][0]["weapon"]["img"]                        = "xx.png";
$soul_gears["Evan"][0]["weapon"]["stats"]["Attack"]            = 380;
$soul_gears["Evan"][0]["weapon"]["stats"]["Counter Damage"]    = 600;

$soul_gears["Evan"][0]["wings"]["name"]                        = "Evan's Brown Kitty";
$soul_gears["Evan"][0]["wings"]["img"]                         = "ww.png";
$soul_gears["Evan"][0]["wings"]["stats"]["Crit Rate"]          = 200;
$soul_gears["Evan"][0]["wings"]["stats"]["Hit Rate"]           = 200;
$soul_gears_set["Evan"][0]["effect2"]                          = ["label" => "Decrease skill MP cost by 10%", "stat" => "MP Cost%", "value" => "-10"];

// Lucas

$soul_gears["Lucas"][0]["weapon"]["name"]                   = "Valkyrie's Rifle";
$soul_gears["Lucas"][0]["weapon"]["img"]                    = "rifle.png";
$soul_gears["Lucas"][0]["weapon"]["stats"]["Defense"]       = 400;
$soul_gears["Lucas"][0]["weapon"]["stats"]["Crit Rate"]     = 200;

$soul_gears["Lucas"][0]["wings"]["name"]                    = "Valkyrie's Nimble Wings";
$soul_gears["Lucas"][0]["wings"]["img"]                     = "wing-paper.png";
$soul_gears["Lucas"][0]["wings"]["stats"]["Dodge"]          = 200;
$soul_gears["Lucas"][0]["wings"]["stats"]["Counter Damage"] = 600;
$soul_gears_set["Lucas"][0]["effect2"]                      = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

// MapleStory2 SG

$soul_gears["Aran"][0]["weapon"]["name"]                     = "Saint Polearm";
$soul_gears["Aran"][0]["weapon"]["img"]                      = "xx.png";
$soul_gears["Aran"][0]["weapon"]["stats"]["Defense"]         = 400;
$soul_gears["Aran"][0]["weapon"]["stats"]["Crit Rate"]       = 200;

$soul_gears["Aran"][0]["wings"]["name"]                      = "Aran's Black Pig";
$soul_gears["Aran"][0]["wings"]["img"]                       = "ww.png";
$soul_gears["Aran"][0]["wings"]["stats"]["Crit Rate"]        = 200;
$soul_gears["Aran"][0]["wings"]["stats"]["Hit Rate"]         = 200;
$soul_gears_set["Aran"][0]["effect2"]                        = ["label" => "Decrease skill MP cost by 10%", "stat" => "MP Cost%", "value" => "-10"];

$soul_gears["Eunwol"][0]["weapon"]["name"]                   = "Saint Grip";
$soul_gears["Eunwol"][0]["weapon"]["img"]                    = "xx.png";
$soul_gears["Eunwol"][0]["weapon"]["stats"]["HP"]            = 1200;
$soul_gears["Eunwol"][0]["weapon"]["stats"]["Counter Rate"]  = 200;

$soul_gears["Eunwol"][0]["wings"]["name"]                    = "Eunwol's Persian Cat";
$soul_gears["Eunwol"][0]["wings"]["img"]                     = "ww.png";
$soul_gears["Eunwol"][0]["wings"]["stats"]["Dodge"]          = 200;
$soul_gears["Eunwol"][0]["wings"]["stats"]["Counter Damage"] = 600;
$soul_gears_set["Eunwol"][0]["effect2"]                      = ["label" => "Decrease skill MP cost by 10%", "stat" => "MP Cost%", "value" => "-10"];

$soul_gears["Luminous"][0]["weapon"]["name"]                 = "Saint Shining Rod";
$soul_gears["Luminous"][0]["weapon"]["img"]                  = "xx.png";
$soul_gears["Luminous"][0]["weapon"]["stats"]["Dodge"]       = 200;
$soul_gears["Luminous"][0]["weapon"]["stats"]["MP Recovery"] = 4;

$soul_gears["Luminous"][0]["wings"]["name"]                  = "Luminous's Welsh Corgi";
$soul_gears["Luminous"][0]["wings"]["img"]                   = "ww.png";
$soul_gears["Luminous"][0]["wings"]["stats"]["Defense"]      = 400;
$soul_gears["Luminous"][0]["wings"]["stats"]["Counter Rate"] = 200;
$soul_gears_set["Luminous"][0]["effect2"]                    = ["label" => "Decrease skill MP cost by 10%", "stat" => "MP Cost%", "value" => "-10"];

// Cat & Serphina

$soul_gears["Cat Sidhe"][0]["weapon"]["name"]                  = "Valkyrie's Claws";
$soul_gears["Cat Sidhe"][0]["weapon"]["img"]                   = "claws.png";
$soul_gears["Cat Sidhe"][0]["weapon"]["stats"]["HP"]           = 1200;
$soul_gears["Cat Sidhe"][0]["weapon"]["stats"]["Counter Rate"] = 200;

$soul_gears["Cat Sidhe"][0]["wings"]["name"]                   = "Valkyrie's Elegant Wings";
$soul_gears["Cat Sidhe"][0]["wings"]["img"]                    = "wing-rock.png";
$soul_gears["Cat Sidhe"][0]["wings"]["stats"]["Crit Rate"]     = 200;
$soul_gears["Cat Sidhe"][0]["wings"]["stats"]["Hit Rate"]      = 200;
$soul_gears_set["Cat Sidhe"][0]["effect2"]                     = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Serphina"][0]["weapon"]["name"]                   = "Valkyrie's Saint Polearm";
$soul_gears["Serphina"][0]["weapon"]["img"]                    = "sword.png";
$soul_gears["Serphina"][0]["weapon"]["stats"]["MP"]            = 32;
$soul_gears["Serphina"][0]["weapon"]["stats"]["Hit Rate"]      = 200;

$soul_gears["Serphina"][0]["wings"]["name"]                    = "Valkyrie's Gorgeous Wings";
$soul_gears["Serphina"][0]["wings"]["img"]                     = "wing-scissors.png";
$soul_gears["Serphina"][0]["wings"]["stats"]["Defense"]        = 400;
$soul_gears["Serphina"][0]["wings"]["stats"]["Counter Rate"]   = 200;
$soul_gears_set["Serphina"][0]["effect2"]                      = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

// S2 heroes

$soul_gears["Ildo"][0]["weapon"]["name"]                        = "Valkyrie's Saber";
$soul_gears["Ildo"][0]["weapon"]["img"]                         = "xx.png";
$soul_gears["Ildo"][0]["weapon"]["stats"]["Defense"]            = 400;
$soul_gears["Ildo"][0]["weapon"]["stats"]["Crit Rate"]          = 200;

$soul_gears["Ildo"][0]["wings"]["name"]                         = "Valkyrie's Nimble Wings";
$soul_gears["Ildo"][0]["wings"]["img"]                          = "wing-paper.png";
$soul_gears["Ildo"][0]["wings"]["stats"]["Defense"]             = 400;
$soul_gears["Ildo"][0]["wings"]["stats"]["Counter Rate"]        = 200;
$soul_gears_set["Ildo"][0]["effect2"]                           = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Taehwa"][0]["weapon"]["name"]                      = "Valkyrie's Staff";
$soul_gears["Taehwa"][0]["weapon"]["img"]                       = "staff.png";
$soul_gears["Taehwa"][0]["weapon"]["stats"]["HP"]               = 1200;
$soul_gears["Taehwa"][0]["weapon"]["stats"]["Counter Rate"]     = 200;

$soul_gears["Taehwa"][0]["wings"]["name"]                       = "Valkyrie's Gorgeous Wings";
$soul_gears["Taehwa"][0]["wings"]["img"]                        = "wing-scissors.png";
$soul_gears["Taehwa"][0]["wings"]["stats"]["Crit Rate"]         = 200;
$soul_gears["Taehwa"][0]["wings"]["stats"]["Hit Rate"]          = 200;
$soul_gears_set["Taehwa"][0]["effect2"]                         = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Thanatos"][0]["weapon"]["name"]                    = "Valkyrie's Greatsword";
$soul_gears["Thanatos"][0]["weapon"]["img"]                     = "great-sword.png";
$soul_gears["Thanatos"][0]["weapon"]["stats"]["Attack"]         = 380;
$soul_gears["Thanatos"][0]["weapon"]["stats"]["Counter Damage"] = 600;

$soul_gears["Thanatos"][0]["wings"]["name"]                     = "Valkyrie's Elegant Wings";
$soul_gears["Thanatos"][0]["wings"]["img"]                      = "wing-rock.png";
$soul_gears["Thanatos"][0]["wings"]["stats"]["Defense"]         = 400;
$soul_gears["Thanatos"][0]["wings"]["stats"]["Counter Rate"]    = 200;
$soul_gears_set["Thanatos"][0]["effect2"]                       = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Ryeogang"][0]["weapon"]["name"]                    = "Valkyrie's Tome";
$soul_gears["Ryeogang"][0]["weapon"]["img"]                     = "xx.png";
$soul_gears["Ryeogang"][0]["weapon"]["stats"]["MP"]             = 32;
$soul_gears["Ryeogang"][0]["weapon"]["stats"]["Hit Rate"]       = 200;

$soul_gears["Ryeogang"][0]["wings"]["name"]                     = "Valkyrie's Nimble Wings";
$soul_gears["Ryeogang"][0]["wings"]["img"]                      = "wing-paper.png";
$soul_gears["Ryeogang"][0]["wings"]["stats"]["Dodge"]           = 200;
$soul_gears["Ryeogang"][0]["wings"]["stats"]["Counter Damage"]  = 600;
$soul_gears_set["Ryeogang"][0]["effect2"]                       = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Hongyeom"][0]["weapon"]["name"]                    = "Valkyrie's Spear";
$soul_gears["Hongyeom"][0]["weapon"]["img"]                     = "spear.png";
$soul_gears["Hongyeom"][0]["weapon"]["stats"]["Attack"]         = 380;
$soul_gears["Hongyeom"][0]["weapon"]["stats"]["Counter Damage"] = 600;

$soul_gears["Hongyeom"][0]["wings"]["name"]                     = "Valkyrie's Elegant Wings";
$soul_gears["Hongyeom"][0]["wings"]["img"]                      = "wing-rock.png";
$soul_gears["Hongyeom"][0]["wings"]["stats"]["Crit Rate"]       = 200;
$soul_gears["Hongyeom"][0]["wings"]["stats"]["Hit Rate"]        = 200;
$soul_gears_set["Hongyeom"][0]["effect2"]                       = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

// Guilty Gear (24/08/2017)

$soul_gears["Ky Kiske"][0]["weapon"]["name"]                               = "Red Energy Magnolia Eclair";
$soul_gears["Ky Kiske"][0]["weapon"]["img"]                                = "xx.png";
$soul_gears["Ky Kiske"][0]["weapon"]["stats"]["HP"]                        = 1200;
$soul_gears["Ky Kiske"][0]["weapon"]["stats"]["Counter Rate"]              = 200;

$soul_gears["Ky Kiske"][0]["wings"]["name"]                                = "High King's Aura";
$soul_gears["Ky Kiske"][0]["wings"]["img"]                                 = "ww.png";
$soul_gears["Ky Kiske"][0]["wings"]["stats"]["Crit Rate"]                  = 200;
$soul_gears["Ky Kiske"][0]["wings"]["stats"]["Hit Rate"]                   = 200;
$soul_gears_set["Ky Kiske"][0]["effect2"]                                  = ["label" => "HP +5%", "stat" => "HP%", "value" => "5"];

$soul_gears["May"][0]["weapon"]["name"]                                    = "Red Energy Pirate Ship Anchor";
$soul_gears["May"][0]["weapon"]["img"]                                     = "xx.png";
$soul_gears["May"][0]["weapon"]["stats"]["Attack"]                         = 380;
$soul_gears["May"][0]["weapon"]["stats"]["Counter Damage"]                 = 600;

$soul_gears["May"][0]["wings"]["name"]                                     = "Jellyfish Pirate's Aura";
$soul_gears["May"][0]["wings"]["img"]                                      = "ww.png";
$soul_gears["May"][0]["wings"]["stats"]["Crit Rate"]                       = 200;
$soul_gears["May"][0]["wings"]["stats"]["Hit Rate"]                        = 200;
$soul_gears_set["May"][0]["effect2"]                                       = ["label" => "HP +5%", "stat" => "HP%", "value" => "5"];

$soul_gears["Sol Badguy"][0]["weapon"]["name"]                             = "Red Energy Junkyard";
$soul_gears["Sol Badguy"][0]["weapon"]["img"]                              = "xx.png";
$soul_gears["Sol Badguy"][0]["weapon"]["stats"]["Defense"]                 = 400;
$soul_gears["Sol Badguy"][0]["weapon"]["stats"]["Crit Rate"]               = 200;

$soul_gears["Sol Badguy"][0]["wings"]["name"]                              = "Bounty Hunter's Aura";
$soul_gears["Sol Badguy"][0]["wings"]["img"]                               = "ww.png";
$soul_gears["Sol Badguy"][0]["wings"]["stats"]["Crit Rate"]                = 200;
$soul_gears["Sol Badguy"][0]["wings"]["stats"]["Hit Rate"]                 = 200;
$soul_gears_set["Sol Badguy"][0]["effect2"]                                = ["label" => "HP +5%", "stat" => "HP%", "value" => "5"];

$soul_gears["Ramlethal Valentine"][0]["weapon"]["name"]                    = "Red Energy Belpha and Venus";
$soul_gears["Ramlethal Valentine"][0]["weapon"]["img"]                     = "xx.png";
$soul_gears["Ramlethal Valentine"][0]["weapon"]["stats"]["Attack"]         = 380;
$soul_gears["Ramlethal Valentine"][0]["weapon"]["stats"]["Counter Damage"] = 600;

$soul_gears["Ramlethal Valentine"][0]["wings"]["name"]                     = "Valentine's Aura";
$soul_gears["Ramlethal Valentine"][0]["wings"]["img"]                      = "ww.png";
$soul_gears["Ramlethal Valentine"][0]["wings"]["stats"]["Defense"]         = 400;
$soul_gears["Ramlethal Valentine"][0]["wings"]["stats"]["Counter Rate"]    = 200;
$soul_gears_set["Ramlethal Valentine"][0]["effect2"]                       = ["label" => "HP +5%", "stat" => "HP%", "value" => "5"];

// S2 wave 2 (21/09/2017)

$soul_gears["Sogoon"][0]["weapon"]["name"]                    = "Valkyrie's Dagger";
$soul_gears["Sogoon"][0]["weapon"]["img"]                     = "dagger.png";
$soul_gears["Sogoon"][0]["weapon"]["stats"]["Attack"]         = 380;
$soul_gears["Sogoon"][0]["weapon"]["stats"]["Counter Damage"] = 600;

$soul_gears["Sogoon"][0]["wings"]["name"]                     = "Valkyrie's Nimble Wings";
$soul_gears["Sogoon"][0]["wings"]["img"]                      = "wing-scissors.png";
$soul_gears["Sogoon"][0]["wings"]["stats"]["Crit Rate"]       = 200;
$soul_gears["Sogoon"][0]["wings"]["stats"]["Hit Rate"]        = 200;
$soul_gears_set["Sogoon"][0]["effect2"]                       = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Churyeok"][0]["weapon"]["name"]                  = "Valkyrie's Spear";
$soul_gears["Churyeok"][0]["weapon"]["img"]                   = "spear.png";
$soul_gears["Churyeok"][0]["weapon"]["stats"]["HP"]           = 1200;
$soul_gears["Churyeok"][0]["weapon"]["stats"]["Counter Rate"] = 200;

$soul_gears["Churyeok"][0]["wings"]["name"]                   = "Valkyrie's Nimble Wings";
$soul_gears["Churyeok"][0]["wings"]["img"]                    = "wing-scissors.png";
$soul_gears["Churyeok"][0]["wings"]["stats"]["Defense"]       = 400;
$soul_gears["Churyeok"][0]["wings"]["stats"]["MP Recovery"]   = 4;
$soul_gears_set["Churyeok"][0]["effect2"]                     = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

// Yeka & Camelia (12/10/2017)

$soul_gears["Yeka"][0]["weapon"]["name"]                    = "Valkyrie's Wand";
$soul_gears["Yeka"][0]["weapon"]["img"]                     = "wand.png";
$soul_gears["Yeka"][0]["weapon"]["stats"]["Attack"]         = 380;
$soul_gears["Yeka"][0]["weapon"]["stats"]["Crit Rate"] = 200;

$soul_gears["Yeka"][0]["wings"]["name"]                     = "Valkyrie's Nimble Wings";
$soul_gears["Yeka"][0]["wings"]["img"]                      = "wing-rock.png";
$soul_gears["Yeka"][0]["wings"]["stats"]["Defense"]       = 400;
$soul_gears["Yeka"][0]["wings"]["stats"]["MP Recovery"]        = 4;
$soul_gears_set["Yeka"][0]["effect2"]                       = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Camelia"][0]["weapon"]["name"]                  = "Valkyrie's Wand";
$soul_gears["Camelia"][0]["weapon"]["img"]                   = "wand.png";
$soul_gears["Camelia"][0]["weapon"]["stats"]["HP"]           = 1200;
$soul_gears["Camelia"][0]["weapon"]["stats"]["Counter Rate"] = 200;

$soul_gears["Camelia"][0]["wings"]["name"]                   = "Valkyrie's Nimble Wings";
$soul_gears["Camelia"][0]["wings"]["img"]                    = "wing-paper.png";
$soul_gears["Camelia"][0]["wings"]["stats"]["Defense"]       = 400;
$soul_gears["Camelia"][0]["wings"]["stats"]["MP Recovery"]   = 4;
$soul_gears_set["Camelia"][0]["effect2"]                     = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

// BlazBlue blue SGs (26/10/2017)

$soul_gears["Jin Kisaragi"][1]["weapon"]["name"]                  = "Valkyrie's Yukianesa";
$soul_gears["Jin Kisaragi"][1]["weapon"]["img"]                   = "xx.png";
$soul_gears["Jin Kisaragi"][1]["weapon"]["stats"]["HP"]           = 1200;
$soul_gears["Jin Kisaragi"][1]["weapon"]["stats"]["Counter Rate"] = 200;

$soul_gears["Jin Kisaragi"][1]["wings"]["name"]                   = "Valkyrie's Elegant Emblem";
$soul_gears["Jin Kisaragi"][1]["wings"]["img"]                    = "ww.png";
$soul_gears["Jin Kisaragi"][1]["wings"]["stats"]["Crit Rate"]     = 200;
$soul_gears["Jin Kisaragi"][1]["wings"]["stats"]["Hit Rate"]      = 200;
$soul_gears_set["Jin Kisaragi"][1]["effect2"]                     = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Noel"][1]["weapon"]["name"]                          = "Valkyrie's Bolverk";
$soul_gears["Noel"][1]["weapon"]["img"]                           = "xx.png";
$soul_gears["Noel"][1]["weapon"]["stats"]["Attack"]               = 380;
$soul_gears["Noel"][1]["weapon"]["stats"]["Counter Damage"]       = 600;

$soul_gears["Noel"][1]["wings"]["name"]                           = "Valkyrie's Elegant Emblem";
$soul_gears["Noel"][1]["wings"]["img"]                            = "ww.png";
$soul_gears["Noel"][1]["wings"]["stats"]["Crit Rate"]             = 200;
$soul_gears["Noel"][1]["wings"]["stats"]["Hit Rate"]              = 200;
$soul_gears_set["Noel"][1]["effect2"]                             = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Ragna"][1]["weapon"]["name"]                         = "Valkyrie's Aramasa";
$soul_gears["Ragna"][1]["weapon"]["img"]                          = "xx.png";
$soul_gears["Ragna"][1]["weapon"]["stats"]["Attack"]              = 380;
$soul_gears["Ragna"][1]["weapon"]["stats"]["Counter Damage"]      = 600;

$soul_gears["Ragna"][1]["wings"]["name"]                          = "Valkyrie's Gorgeous Emblem";
$soul_gears["Ragna"][1]["wings"]["img"]                           = "ww.png";
$soul_gears["Ragna"][1]["wings"]["stats"]["Defense"]              = 400;
$soul_gears["Ragna"][1]["wings"]["stats"]["Counter Rate"]         = 200;
$soul_gears_set["Ragna"][1]["effect2"]                            = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Rachel"][1]["weapon"]["name"]                        = "Valkyrie's Vampire";
$soul_gears["Rachel"][1]["weapon"]["img"]                         = "xx.png";
$soul_gears["Rachel"][1]["weapon"]["stats"]["MP"]                 = 32;
$soul_gears["Rachel"][1]["weapon"]["stats"]["Hit Rate"]           = 200;

$soul_gears["Rachel"][1]["wings"]["name"]                         = "Valkyrie's Nimble Emblem";
$soul_gears["Rachel"][1]["wings"]["img"]                          = "ww.png";
$soul_gears["Rachel"][1]["wings"]["stats"]["Dodge"]               = 200;
$soul_gears["Rachel"][1]["wings"]["stats"]["Counter Damage"]      = 600;
$soul_gears_set["Rachel"][1]["effect2"]                           = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

// Special Moon (Cel, Lena, Yeka)

$soul_gears["Celestial"][1]["weapon"]["name"]              = "Moon Dust Staff";
$soul_gears["Celestial"][1]["weapon"]["img"]               = "xx.png";
$soul_gears["Celestial"][1]["weapon"]["stats"]["Attack"]   = 380;
$soul_gears["Celestial"][1]["weapon"]["stats"]["Defense"]  = 400;

$soul_gears["Celestial"][1]["wings"]["name"]               = "Moon Dust Guardian CooCoo";
$soul_gears["Celestial"][1]["wings"]["img"]                = "ww.png";
$soul_gears["Celestial"][1]["wings"]["stats"]["HP"]        = 1200;
$soul_gears["Celestial"][1]["wings"]["stats"]["Dodge"]     = 200;
$soul_gears_set["Celestial"][1]["effect2"]                 = ["label" => "Increases Crit Rate by 500", "stat" => "Crit Rate", "value" => "500"];

$soul_gears["Lena"][1]["weapon"]["name"]                   = "Red Planet Mic";
$soul_gears["Lena"][1]["weapon"]["img"]                    = "xx.png";
$soul_gears["Lena"][1]["weapon"]["stats"]["Attack"]        = 380;
$soul_gears["Lena"][1]["weapon"]["stats"]["Crit Rate"]     = 200;

$soul_gears["Lena"][1]["wings"]["name"]                    = "Red Planet Guardian CooCoo";
$soul_gears["Lena"][1]["wings"]["img"]                     = "ww.png";
$soul_gears["Lena"][1]["wings"]["stats"]["Counter Damage"] = 600;
$soul_gears["Lena"][1]["wings"]["stats"]["MP Recovery"]    = 4;
$soul_gears_set["Lena"][1]["effect2"]                      = ["label" => "Increases Crit Rate by 500", "stat" => "Crit Rate", "value" => "500"];

$soul_gears["Yeka"][1]["weapon"]["name"]                   = "Dream Weaver Wand";
$soul_gears["Yeka"][1]["weapon"]["img"]                    = "xx.png";
$soul_gears["Yeka"][1]["weapon"]["stats"]["HP"]            = 1200;
$soul_gears["Yeka"][1]["weapon"]["stats"]["MP"]            = 32;

$soul_gears["Yeka"][1]["wings"]["name"]                    = "Dream Weaver Guardian CooCoo";
$soul_gears["Yeka"][1]["wings"]["img"]                     = "ww.png";
$soul_gears["Yeka"][1]["wings"]["stats"]["Dodge"]          = 380;
$soul_gears["Yeka"][1]["wings"]["stats"]["Counter Damage"] = 400;
$soul_gears_set["Yeka"][1]["effect2"]                      = ["label" => "Increases Crit Rate by 500", "stat" => "Crit Rate", "value" => "500"];

// Last Hero Batch (Parsifal, Wory, Heuksa, Camilla, Mary, Amora, Pink Bean)

$soul_gears["Parsifal"][0]["weapon"]["name"]                   = "Valkyrie's Bow";
$soul_gears["Parsifal"][0]["weapon"]["img"]                    = "bow.png";
$soul_gears["Parsifal"][0]["weapon"]["stats"]["Attack"]        = 380;
$soul_gears["Parsifal"][0]["weapon"]["stats"]["Hit Rate"]      = 200;

$soul_gears["Parsifal"][0]["wings"]["name"]                    = "Valkyrie's Nimble Wings";
$soul_gears["Parsifal"][0]["wings"]["img"]                     = "wing-scissors.png";
$soul_gears["Parsifal"][0]["wings"]["stats"]["MP"]             = 32;
$soul_gears["Parsifal"][0]["wings"]["stats"]["Defense"]        = 400;
$soul_gears_set["Parsifal"][0]["effect2"]                      = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Woryeong"][0]["weapon"]["name"]                   = "Valkyrie's Orb";
$soul_gears["Woryeong"][0]["weapon"]["img"]                    = "orb.png";
$soul_gears["Woryeong"][0]["weapon"]["stats"]["HP"]            = 1200;
$soul_gears["Woryeong"][0]["weapon"]["stats"]["MP Recovery"]   = 4;

$soul_gears["Woryeong"][0]["wings"]["name"]                    = "Valkyrie's Nimble Wings";
$soul_gears["Woryeong"][0]["wings"]["img"]                     = "wing-paper.png";
$soul_gears["Woryeong"][0]["wings"]["stats"]["Attack"]         = 380;
$soul_gears["Woryeong"][0]["wings"]["stats"]["Defense"]        = 400;
$soul_gears_set["Woryeong"][0]["effect2"]                      = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Heuksa"][0]["weapon"]["name"]                     = "Valkyrie's Saber";
$soul_gears["Heuksa"][0]["weapon"]["img"]                      = "xx.png";
$soul_gears["Heuksa"][0]["weapon"]["stats"]["HP"]              = 1200;
$soul_gears["Heuksa"][0]["weapon"]["stats"]["Attack"]          = 380;

$soul_gears["Heuksa"][0]["wings"]["name"]                      = "Valkyrie's Nimble Wings";
$soul_gears["Heuksa"][0]["wings"]["img"]                       = "wing-rock.png";
$soul_gears["Heuksa"][0]["wings"]["stats"]["Defense"]          = 400;
$soul_gears["Heuksa"][0]["wings"]["stats"]["Counter Rate"]     = 200;
$soul_gears_set["Heuksa"][0]["effect2"]                        = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Camilla"][0]["weapon"]["name"]                    = "Valkyrie's Saber";
$soul_gears["Camilla"][0]["weapon"]["img"]                     = "xx.png";
$soul_gears["Camilla"][0]["weapon"]["stats"]["HP"]             = 1200;
$soul_gears["Camilla"][0]["weapon"]["stats"]["Defense"]        = 400;

$soul_gears["Camilla"][0]["wings"]["name"]                     = "Valkyrie's Nimble Wings";
$soul_gears["Camilla"][0]["wings"]["img"]                      = "wing-rock.png";
$soul_gears["Camilla"][0]["wings"]["stats"]["Crit Rate"]       = 200;
$soul_gears["Camilla"][0]["wings"]["stats"]["Hit Rate"]        = 200;
$soul_gears_set["Camilla"][0]["effect2"]                       = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Mary"][0]["weapon"]["name"]                       = "Valkyrie's Orb";
$soul_gears["Mary"][0]["weapon"]["img"]                        = "orb.png";
$soul_gears["Mary"][0]["weapon"]["stats"]["Dodge"]             = 200;
$soul_gears["Mary"][0]["weapon"]["stats"]["MP Recovery"]       = 4;

$soul_gears["Mary"][0]["wings"]["name"]                        = "Valkyrie's Nimble Wings";
$soul_gears["Mary"][0]["wings"]["img"]                         = "wing-rock.png";
$soul_gears["Mary"][0]["weapon"]["stats"]["HP"]                = 1200;
$soul_gears["Mary"][0]["weapon"]["stats"]["Defense"]           = 400;
$soul_gears_set["Mary"][0]["effect2"]                          = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Amora"][0]["weapon"]["name"]                      = "Valkyrie's Orb";
$soul_gears["Amora"][0]["weapon"]["img"]                       = "orb.png";
$soul_gears["Amora"][0]["weapon"]["stats"]["HP"]               = 1200;
$soul_gears["Amora"][0]["weapon"]["stats"]["MP Recovery"]      = 4;

$soul_gears["Amora"][0]["wings"]["name"]                       = "Valkyrie's Nimble Wings";
$soul_gears["Amora"][0]["wings"]["img"]                        = "wing-scissors.png";
$soul_gears["Amora"][0]["weapon"]["stats"]["Attack"]           = 380;
$soul_gears["Amora"][0]["weapon"]["stats"]["Dodge"]            = 200;
$soul_gears_set["Amora"][0]["effect2"]                         = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];

$soul_gears["Pink Bean"][1]["weapon"]["name"]                  = "Valkyrie's Headset";
$soul_gears["Pink Bean"][1]["weapon"]["img"]                   = "xx.png";
$soul_gears["Pink Bean"][1]["weapon"]["stats"]["MP"]           = 32;
$soul_gears["Pink Bean"][1]["weapon"]["stats"]["Attack"]       = 380;

$soul_gears["Pink Bean"][1]["wings"]["name"]                   = "Valkyrie's Nimble Wings";
$soul_gears["Pink Bean"][1]["wings"]["img"]                    = "wing-paper.png";
$soul_gears["Pink Bean"][1]["weapon"]["stats"]["Counter Rate"] = 200;
$soul_gears["Pink Bean"][1]["weapon"]["stats"]["MP Recovery"]  = 4;
$soul_gears_set["Pink Bean"][1]["effect2"]                     = ["label" => "Increases Resistance +5%", "stat" => "Resistance", "value" => "5"];