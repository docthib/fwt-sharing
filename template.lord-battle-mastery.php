<div id="lord-battle-mastery">
	<legend>Lord Mastery</legend>
	
	<?php
		foreach ($lord_battle_mastery as $key => $battle_mastery)
		{
			echo "<div class='battle-mastery'>";
				echo "<img src='" . LORD_BATTLE_MASTERY_DIRECTORY . $battle_mastery['img'] . "' alt='" . $battle_mastery['name'] . "' data-toggle='tooltip' data-placement='top' title='" . $battle_mastery['description'] . "'>";
				echo "<div class='form-group'>";
					echo "<select name='battle_mastery[" . $key . "]' data-condition='" . $battle_mastery['condition'] . "' data-stat='" . tdf($battle_mastery['stat']) . "' data-valperlevel='" . $battle_mastery['valperlevel'] . "' data-lvlmax='" . $battle_mastery['levelmax'] . "' class='form-control' />";
					echo "<option></option>";
						for ($i = $battle_mastery['levelmax'] ; $i >= 0 ; $i--)
						{
							echo "<option value='" . $i . "'>" . $i . "</option>";
						}
					echo "</select>";
				echo "</div>";
			echo "</div>";
		}
	?>
</div>