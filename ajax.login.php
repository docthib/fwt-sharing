<?php
session_start();

include_once("include.maintenance-check.php");

// Vérifier si déjà connecté
if (isset($_SESSION['user']['id']))
{
	$tab['success'] = FALSE;
	$tab['message'] = "You are already logged in omg !";
	echo json_encode($tab);exit;
}

// Database connexion
include_once("class.database.php");

parse_str($_POST['data'], $data);

// Control and sanitize data
$nickname  = trim(filter_var($data['nickname'], FILTER_SANITIZE_STRING));
$password  = trim(filter_var($data['pass'], FILTER_SANITIZE_STRING));
$pass_hash = hash("sha384", $password . SALT);

if (empty($nickname))
{
  $tab['success'] = FALSE;
  $tab['message'] = "Nickname can't be empty";
  echo json_encode($tab);exit;
}

if (empty($password))
{
  $tab['success'] = FALSE;
  $tab['message'] = "Password can't be empty";
  echo json_encode($tab);exit;
}

// Vérifier le nombre de try
$sql = "SELECT user_id, login_failed, account_status
FROM user
WHERE nickname = :nickname";

try
{
  $database = new Database();
  $database->query($sql);
  $database->bind(":nickname", $nickname);
  $result = $database->fetch();

  // Si le nick n'est pas retrouvé on balance une erreur
  if (count($result) == 0)
  {
    $tab['success'] = FALSE;
    $tab['message'] = "Unknown account";
    echo json_encode($tab);exit;
  }

  // On stock ces données pour une future utilisation
  $account_status = $result[0]["account_status"];
  $login_failed   = $result[0]["login_failed"];
  $user_id        = $result[0]["user_id"];

  unset($database);
}
catch(Exception $e)
{
  if (DEBUG)
  {
    var_dump($e);
  }

  $tab['success'] = FALSE;
  $tab['message'] = "Error while login a.l1";
  echo json_encode($tab);exit;
}

// Si account = 0
if ($account_status == 0)
{
  $tab['success'] = FALSE;
  $tab['message'] = "Account suspended, contact admin";
  echo json_encode($tab);exit;
}

// Si 5 essais ou plus on bloque le compte
// if ($login_failed >= 5)
// {
//    $sql = "UPDATE user SET 
//    account_status = 0
//   WHERE user_id = :user_id";

//   try
//   {
//     $login_failed++;

//     $database = new Database();
//     $database->query($sql);
//     $database->bind(":user_id", $user_id);
    
//     $database->execute();

//     unset($database);
//   }
//   catch(Exception $e)
//   {
//     if (DEBUG)
//     {
//       var_dump($e);
//     }

//     $tab['success'] = FALSE;
//     $tab['message'] = "Error while login a.l4";
//     echo json_encode($tab);exit;
//   }

//   $tab['success'] = FALSE;
//   $tab['message'] = "Account is now suspended, contact admin";
//   echo json_encode($tab);exit;
// }

// Vérifier si la correspondance existe
$sql = "SELECT * FROM view_user_login
WHERE user_id = :user_id
AND password = :password
AND account_status = 1";

try
{
  $database = new Database();
  $database->query($sql);
  $database->bind(":user_id", $user_id);
  $database->bind(":password", $pass_hash);
  
  $result = $database->fetch();

  unset($database);
}
catch(Exception $e)
{  
  if (DEBUG)
  {
    var_dump($e);
  }

	$tab['success'] = FALSE;
  $tab['message'] = "Error while login a.l2";
  echo json_encode($tab);exit;
}

if (count($result) == 0)
{
  $sql = "UPDATE user SET 
    login_failed = :login_failed
  WHERE user_id = :user_id";

  try
  {
    $login_failed++;

    $database = new Database();
    $database->query($sql);
    $database->bind(":user_id", $user_id);
    $database->bind(":login_failed", $login_failed);
    
    $database->execute();

    unset($database);
  }
  catch(Exception $e)
  {
    if (DEBUG)
    {
      var_dump($e);
    }

    $tab['success'] = FALSE;
    $tab['message'] = "Error while login a.l3";
    echo json_encode($tab);exit;
  }

  $tab['success'] = FALSE;
  $tab['message'] = "Incorrect password";
  echo json_encode($tab);exit;
}

// If here, everything is OK
$_SESSION['user']['id']       = $result[0]['user_id'];
$_SESSION['user']['nickname'] = $result[0]['nickname'];


// Load stats if there is
$sql = "SELECT data FROM user_hero_stats WHERE user_id = :user_id";

try
{
  $database = new Database();
  $database->query($sql);
  $database->bind(":user_id", $user_id);
  $result = $database->fetch();

  if (count($result) != 0)
  {
    $_SESSION['user']['calculator'] = $result[0]['data'];
  }

  unset($database);
}
catch(Exception $e)
{
  if (DEBUG)
  {
    var_dump($e);
  }

  $tab['success'] = FALSE;
  $tab['message'] = "Error while login a.l4";
  echo json_encode($tab);exit;
}

// INSERT connexion log
$sql = "INSERT INTO user_log (
  user_id,
  timestamp,
  action,
  ip
) VALUES (
  :user_id,
  :timestamp,
  :action,
  :ip
)";

try
{
  $database = new Database();
  $database->query($sql);
  $database->bind(":user_id", $user_id);
  $database->bind(":timestamp", time());
  $database->bind(":action", "Login");
  $database->bind(":ip", $_SERVER['REMOTE_ADDR']);
  
  $database->execute();

  unset($database);
}
catch(Exception $e)
{
  if (DEBUG)
  {
    var_dump($e);
  }

  $tab['success'] = FALSE;
  $tab['message'] = "Error while login a.l5";
  echo json_encode($tab);exit;
}

$tab['success'] = TRUE;
$tab['message'] = "Login successful, redirection in 2 seconds";
echo json_encode($tab);exit;