<div>
	<h1>Login</h1>	

	<form role="form" id="userlogin" action="" onsubmit="return false;">
		<div class="form-group">
			<input type="text" class="form-control" name="nickname" id="lnickname" placeholder="Nickname" />
		</div>
		<div class="form-group">
			<input type="password" class="form-control" name="pass" id="lpassword" placeholder="Password" />
		</div>

		<div class="alert alert-warning"><span class="glyphicon glyphicon-info-sign"></span> Login will erase your data if you started using the tool - Please export your data first</div>

		<button role="submit" class="btn btn-primary">Login</button>
	</form>
</div>