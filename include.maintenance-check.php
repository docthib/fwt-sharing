<?php

define("APP_VERSION", "0.9.2.16");

include_once "class.database.php";

$filename = pathinfo( $_SERVER['PHP_SELF'], PATHINFO_BASENAME );

$ip_allowed = array();

$exception_pages = array(
    "page.fluff.php",
	"page.maintenance.php"
);

// var_dump($ip_allowed);
// var_dump($_SERVER['REMOTE_ADDR']);exit;


if (MAINTENANCE && !in_array($_SERVER['REMOTE_ADDR'], $ip_allowed) && !in_array($filename, $exception_pages))
{
	if ((isset($_GET) && !empty($_GET)) || (isset($_POST) && !empty($_POST)))
	{
		$tab['success'] = FALSE;
		$tab['message'] = "Website is currently under maintenance, try again later";
		echo json_encode($tab);exit;
	}

	header("location:/maintenance");exit;
}