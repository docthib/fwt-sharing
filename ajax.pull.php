<?php
session_start();

include_once "include.maintenance-check.php";
include_once "data.equip-sets.php";

$nbPull = (int) $_POST['nb'];
$data   = doPull( $nbPull );

echo json_encode($data, JSON_HEX_APOS|JSON_HEX_QUOT);
exit;
?>