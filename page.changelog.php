<?php
session_start();

include_once "include.maintenance-check.php";

$json = file_get_contents('_changelogxx.json');
$tab  = json_decode($json, true);

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- common head -->
    <?php include "template.head.php"; ?>

    <!-- custom css -->
    <link rel="stylesheet" href="/assets/css/calculator.css?<?php echo filemtime('assets/css/calculator.css'); ?>">
    <link rel="stylesheet" href="/assets/css/fluff.css?<?php echo filemtime('assets/css/fluff.css'); ?>"> 

    <title>Changelog</title>
  </head>

  <body>
    <header>
      <?php include "template.header-belt.php"; ?>
    </header>
    <div class="container" style="position:relative;">
      <style>
        .bs-callout-info{background-color:#F1FCFF;}
        .bs-callout-warning{background-color:#FFF6EA;}
      </style>
      <h1>Changelog</h1>

      <p>Please write me on <a href="https://www.reddit.com/user/docthib/">reddit</a> if you have any problem / bug / question</p>

      <div id="changelog">
      <?php
        foreach ( $tab as $changelog ) { 
          $diff = time() - $changelog['date'];
          $new  = $diff < 604800 ? "bs-callout-warning" : ( $diff < 2419200  ? "bs-callout-info" : "" );
      ?>
          <div class="bs-callout <?php echo $new; ?>">
            <b><?php echo $changelog['version']; ?></b><i class="date"> - <?php echo date("Y-m-d", $changelog['date']); ?></i><br />
            <?php echo $changelog['log']; ?>
          </div>
      <?php } ?>
      </div>
    </div>
    <footer>
      <!-- common postload -->
      <?php include "template.postload.php"; ?>
      <script>
        
      </script>
    </footer>
  </body>
</html>
