<div id="runes" class="form-inline">
	<legend>Runes</legend>

	<div class="runes-container">
		<div class="runes-unavailable" style="display: none;"><p>Runes are only available for awaken heroes :/</p></div>
		<div class="runes-available">
			<i>*Runes are expected to be at enhancement max (15)</i>
			<div id="runes-btn-set">
				<div class="btn-toolbar" role="toolbar">
					<label for="">Set all runes to</label>

					<div class='btn-group' role='group'>
					<?php 
					foreach ($rune_grade as $grade) 
					{
						echo "<button class='btn btn-default' data-type='" . tdf($grade) . "' data-role='grade' title='" . $grade . " star(s)' data-toggle='tooltip' data-placement='top'>";
						for ($i = 1 ; $i <= $grade; $i++)
						{
							echo "<span class='glyphicon glyphicon-star grade-star'></span>";
						}
						echo "</button>";
					}
					?>
					</div>

					<div class='btn-group' role='group'>
					<?php 
					foreach ($rune_type as $type) 
					{
						echo "<button class='btn btn-default' data-type='" . tdf($type) . "' data-role='type' title='All " . $type . "' data-toggle='tooltip' data-placement='top'>" . $type . "</button>";
					}
						echo "<button class='btn btn-default' data-type='balanced' data-role='type' title='One of each' data-toggle='tooltip' data-placement='top'>Balanced</button>";
					?>
					</div>	

					<div class='btn-group' role='group'>
						<button class='btn bg-epic' data-role='reset_runes'>Reset runes</button>
					</div>
				</div>
			</div>

			<div class='row'>
			<?php
				foreach ($rune_slot as $rune_slot_name)
				{
				echo "<div class='rune col-xs-12 col-sm-4' data-rune-slot='" . tdf($rune_slot_name) . "'>";
					echo "<p>" . $rune_slot_name . " Rune</p>";
					echo "<div>";
						echo "<div class='form-group rune-grade'>";
							echo "<select class='form-control' name='rune_grade[" . tdf($rune_slot_name) . "]'>";
								echo "<option></option>";
								foreach ($rune_grade as $grade) 
								{
									echo "<option value='" . $grade . "'>" . $grade . "</option>";
								}
							echo "</select>";
						echo "</div>";

						echo "<div class='form-group rune-type'>";
							echo "<select class='form-control' name='rune_type[" . tdf($rune_slot_name) . "]'>";
								echo "<option></option>";
								foreach ($rune_type as $type) 
								{
									echo "<option value='" . tdf($type) . "'>" . $type . "</option>";
								}
							echo "</select>";
						echo "</div>";
					echo "</div>";		

					echo "<p>Stats</p>";
					echo "<div class='stats'>";
						echo "<i>Use dropdowns above</i>";
					echo "</div>";
				echo "</div>";
				}
			?>
			</div>
		</div>
	</div>
</div>