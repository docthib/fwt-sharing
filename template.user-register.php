<div>
	<h1>Register</h1>	

	<form role="form" id="registration" action="" onsubmit="return false;">
		<div class="form-group">
			<input type="email" class="form-control" name="email" id="remail" placeholder="Email" required />
		</div>
		<div class="form-group">
			<input type="text" class="form-control" name="nickname" id="rnickname" placeholder="Nickname" />
		</div>
		<div class="form-group">
			<input type="password" class="form-control" name="pass" id="rpassword" placeholder="Password" />
		</div>

		<button role="submit" class="btn btn-primary">Register</button>
	</form>

	<div>
		<h4 class="text-info">Why register ?</h4>
		<p class="text-justify">To benefit from Save and Load feature (and maybe much more coming). Email is required for password recovery issues only. I'm not here to spam you or anything else.</p>
	</div>
</div>