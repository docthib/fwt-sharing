<?php

// Define configuration

class Chat
{
  private $id;
  private $userId;
  private $creationDate;
  private $message;
  private $editDate;
  private $isActive;

  function __construct($array)
  {
    foreach ($array as $key => $value) {
      $method = 'set'.ucfirst($key);
     
      if (method_exists($this, $method))
      {
        $this->$method($value);
      }
    }

  }

  public function getId()
  {
    return $this->id;
  }
  public function setId($var)
  {
    $var = (int) $var;

    $this->id = $var;
  }

  public function getUserId()
  {
    return $this->userId;
  }
  public function setUserId($var)
  {
    $var = (int) $var;

    $this->userId = $var;
  }

  public function getCreationDate()
  {
    return $this->creationDate;
  }
  public function setCreationDate($var)
  {
    $var = (int) $var;

    $this->creationDate = $var;
  }

  public function getMessage()
  {
    return $this->message;
  }
  public function setMessage($var)
  {
    if (mb_detect_encoding($var, 'UTF-8', true) == FALSE) 
    {
      $var = mb_convert_encoding($var, "UTF-8");
    }

    $this->message = $var;
  }

  public function getEditDate()
  {
    return $this->editDate;
  }
  public function setEditDate($var)
  {
    $var = (int) $var;

    $this->editDate = $var;
  }

  public function getIsActive()
  {
    return $this->isActive;
  }
  public function setIsActive($var)
  {
    $var = ((int) $var  != 0) ? 1 : 0;

    $this->isActive = $var;
  }


}
?>