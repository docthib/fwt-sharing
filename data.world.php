<?php



$continents = array(
  0 => array(
    "title" => "Mustache Island",
    "heroes" => array()
  ),
  1 => array(
    "title" => "Verna",
    "heroes" => array(
      "Kai"
    )
  ),
  2 => array(
    "title" => "Tezen",
    "heroes" => array(
      "Carrot",
      "Lance",
      "Deborah"
    )
  ),
  3 => array(
    "title" => "Company",
    "heroes" => array(
      "Mas",
      "Jenny",
      "Dolores"
    )
  ),
  4 => array(
    "title" => "Vulcan",
    "heroes" => array(
      "Sonic Boom",
      "Poni",
      "Jack"
    )
  ),
  5 => array(
    "title" => "Foose",
    "heroes" => array(
      "Serendi",
      "Gillan",
      "Lily"
    )
  ),
  6 => array(
    "title" => "Glory",
    "heroes" => array(
      "Raboff",
      "Dominique",
      "Cleo"
    )
  ),
  7 => array(
    "title" => "Girgas",
    "heroes" => array(
      "Jin",
      "Bearman",
      "Moa"
    )
  ),
  8 => array(
    "title" => "Nereid",
    "heroes" => array(
      "Lilid",
      "Kitty",
      "Henry"
    )
  ),
  9 => array(
    "title" => "Lista",
    "heroes" => array(
      "Elektra"
    )
  ),
  10 => array(
    "title" => "Ashan",
    "heroes" => array(
      "Angela",
      "Lee",
      "Azrael"
    )
  ),
  11 => array(
    "title" => "Moon Island",
    "heroes" => array(
      "Klein"
    )
  )
);

$genes_farm = array(
  "monday" => array(
    "continents" => array(
      1, 7
    )
  ),
  "tuesday" => array(
    "continents" => array(
      3, 8
    )
  ), 
  "wednesday" => array(
    "continents" => array(
      2, 9
    )
  ), 
  "thursday" => array(
    "continents" => array(
      5, 6
    )
  ), 
  "friday" => array(
    "continents" => array(
      4, 10
    )
  )
);

$xp_farm = array(
  'saturday' => array(
    'continents' => array(
      1, 3, 5, 7, 9
    )
  ),
  'sunday' => array(
    'continents' => array(
      2, 4, 6, 8, 10
    )
  )
);

$nephtys = array(
  "monday" => array(
    array(
      "title" => "Moonlight Garden", // scissors
      "image" => ""
    ),
    array(
      "title" => "Weapon Storage",
      "image" => ""
    )
  ),
  "tuesday" => array(
    array(
      "title" => "Flame Garden", // rock
      "image" => ""
    ),
    array(
      "title" => "Armor Storage",
      "image" => ""
    )
  ),
  "wednesday" => array(
    array(
      "title" => "Underwater Garden", // paper
      "image" => ""
    ),
    array(
      "title" => "Accessory Storage",
      "image" => ""
    )
  ),
  "thursday" => array(
    array(
      "title" => "Moonlight Garden", // scissors
      "image" => ""
    ),
    array(
      "title" => "Weapon Storage",
      "image" => ""
    )
  ),
  "friday" => array(
    array(
      "title" => "Flame Garden", // rock
      "image" => ""
    ),
    array(
      "title" => "Armor Storage",
      "image" => ""
    )
  ),
  "saturday" => array(
    array(
      "title" => "Underwater Garden", // paper
      "image" => ""
    ),
    array(
      "title" => "Accessory Storage",
      "image" => ""
    )
  ),
  "sunday" => array(
    array(
      "title" => "King's Bedchamber", // mana
      "image" => ""
    ),
    array(
      "title" => "Queen's Bedchamber", // gold
      "image" => ""
    ),
    array(
      "title" => "Sphinx's Maze", // gold
      "image" => ""
    )
  ),
);

// 1209600 = 14j
// 3628800 = 14j x3
// 172800 = 48h
// commence a 4h du mat + 23 secondes par rapport au serveur

$lost_island = array(
  'Muang' => 1458957623, 
  'Chenny' => 1460167223, 
  'Nirvana' => 1457748023  
);

$j42 = 3628800;
$j2  = 172800;

