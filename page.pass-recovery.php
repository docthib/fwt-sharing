<?php
session_start();
// http://fwt.docthib.com/recovery?m=xalord@gmail.com&c=abd
// http://fwt.docthib.com/recovery?m=docthib@hotmail.com&c=aaa
define("PATH_INFO", "");

include_once "include.functions.php";
include_once "include.maintenance-check.php";

$_SESSION['recovery']['try'] = (isset($_SESSION['recovery']['try'])) ? $_SESSION['recovery']['try'] + 1 : 1;
$_SESSION['recovery']['email'] = $_GET['m'];
$_SESSION['recovery']['token'] = $_GET['c'];

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- common head -->
    <?php include "template.head.php"; ?>

		<!-- custom header meta -->
		<title>FWT Stats Calculator - Password Recovery</title>
  </head>
	<body>
		<header>
			<?php include "template.header-belt.php"; ?>
		</header>
		<div class="container">
			<?php include "template.notification.php"; ?>

			<div class="row">
				<div class="col-sm-6">
					<?php include "template.user-recovery.php"; ?>
				</div>
				<div class="col-sm-6">
				</div>
			</div>
		</div>
		<footer>
        <!-- common postload -->
        <?php include "template.postload.php"; ?>
        
        <!-- custom js/css -->
        <script src="/assets/js/user.js?<?php echo filemtime('assets/js/user.js'); ?>"></script>
        <link rel="stylesheet" href="/assets/css/calculator.css?<?php echo filemtime('assets/css/calculator.css'); ?>">
    </footer>
	</body>
</html>