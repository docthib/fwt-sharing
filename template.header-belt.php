<?php
$filename = pathinfo($_SERVER['PHP_SELF'], PATHINFO_BASENAME );
?>

<nav class="navbar navbar-inverse">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#nav-button" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>

			<a class="navbar-brand" href="calc">FWT Stats Calculator</a>
		</div>

		<div class="collapse navbar-collapse" id="nav-button">
      <ul class="nav navbar-nav navbar-right">
        <li class="<?php if ($filename == "page.calculator.php"){echo "active";} ?> dropdown">
          <a id="nav-calculator" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-th-list"></span> Calculator <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="calc"><span class="glyphicon glyphicon-th-list"></span> Calculator</a></li>
            <li <?php if (!isset($_SESSION['user']['id']) || $filename != "page.calculator.php") {echo "class='disabled'";} ?> ><a id="calc-save" href="#"><span class="glyphicon glyphicon-cloud-upload"></span> Save</a></li>
            <li <?php if (!isset($_SESSION['user']['id']) || $filename != "page.calculator.php") {echo "class='disabled'";} ?> ><a id="calc-load" href="#"><span class="glyphicon glyphicon-cloud-download"></span> Load</a></li>
            <li <?php if ($filename != "page.calculator.php") {echo "class='disabled'";} ?> ><a id="calc-export" href="#"><span class="glyphicon glyphicon-cloud-upload"></span> Export JSON</a></li>
            <li <?php if ($filename != "page.calculator.php") {echo "class='disabled'";} ?> ><a id="calc-import" href="#"><span class="glyphicon glyphicon-cloud-download"></span> Import JSON</a></li>
          </ul>
        </li>
        <li class="<?php if ($filename == "page.heroes.php"){echo "active";} ?>"><a href="heroes"><span class="glyphicon glyphicon-star"></span> Heroes</a></li>
        <li class="<?php if ($filename == "page.sets.php"){echo "active";} ?>"><a href="sets"><span class="glyphicon glyphicon-plane"></span> Sets</a></li>
				<li class="<?php if ($filename == "page.fluff.php"){echo "active";} ?>"><a href="machine"><span class="glyphicon glyphicon-gift"></span> Machine</a></li>
				<li class="<?php if ($filename == "page.register.php"){echo "active";} ?> dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-user"></span> 
					<?php if (isset($_SESSION['user']['id'])) {
						echo $_SESSION['user']['nickname'];
					} else { 
						echo "Account";
					} ?>
					&nbsp;<span class="caret"></span></a>
          <ul class="dropdown-menu">
          	<li <?php if (isset($_SESSION['user']['id'])) {echo "class='disabled'";} ?> ><a href="register"><span class="glyphicon glyphicon-user"></span> Login/Register</a></li>
          	<li <?php if (!isset($_SESSION['user']['id'])) {echo "class='disabled'";} ?> ><a href="logout"><span class="glyphicon glyphicon-off"></span> Logout</a></li>
          </ul>
         </li>
        <!-- <li><a href="http://fantasywartacticsforum.com/threads/online-stats-calculator.1035/" target="_blank"><span class="glyphicon glyphicon-question-sign"></span> Help</a></li> -->
				<li><a href="changelog" target="_blank"><span class="glyphicon glyphicon-question-sign"></span> Changelog</a></li>
				<li><a id="app-version">v<?php echo APP_VERSION; ?></a></li>
			</ul>
		</div>
	</div>
</nav>