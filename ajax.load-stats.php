<?php
session_start();

include_once "include.maintenance-check.php";

// Vérifier s'il y a quelque chose a charger (en session normalement)
if (!isset($_SESSION['user']['calculator']))
{
	$tab['success'] = FALSE;
	$tab['message'] = "Nothing to load";
	echo json_encode($tab);exit;
}

$tab['success'] = TRUE;
$tab['data']    = $_SESSION['user']['calculator'];
$tab['message'] = "Load successful";
echo json_encode($tab);exit;

?>