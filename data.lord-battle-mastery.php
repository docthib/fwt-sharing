<?php

define("LORD_BATTLE_MASTERY_DIRECTORY", "/assets/images/lbm/");

$lord_battle_mastery = array(
	0 => array(
		"name"        => "Increase Attack",
		"levelmax"    => 15,
		"stat"        => "Attack%",
		"valperlevel" => 0.6,
		"img"         => "atk.png",
		"description" => "Increase Attack of heroes by 0.6% per level",
		"condition"   => "",
	),
	1 => array(
		"name"        => "Increase Defense",
		"levelmax"    => 15,
		"stat"        => "Defense!",
		"valperlevel" => 0.3,
		"img"         => "def.png",
		"description" => "Increase Pure Defense of heroes by 0.3% per level",
		"condition"   => "",
	),
	2 => array(
		"name"        => "Increase HP",
		"levelmax"    => 15,
		"stat"        => "HP%",
		"valperlevel" => 1,
		"img"         => "hp.png",
		"description" => "Increase HP of heroes by 1% per level",
		"condition"   => "",
	),
	3 => array(
		"name"        => "Increase Crit Rate",
		"levelmax"    => 15,
		"stat"        => "Crit Rate",
		"valperlevel" => 30,
		"img"         => "crit.png",
		"description" => "Increase Crit Rate of heroes by 30 per level",
		"condition"   => "",
	),
	4 => array(
		"name"        => "Specialization: Scissors",
		"levelmax"    => 15,
		"stat"        => "Damage%",
		"valperlevel" => 1,
		"img"         => "dmg-scissors.png",
		"description" => "Increase Damage of Scissors heroes by 1% per level",
		"condition"   => "element=Scissors",
	),
	5 => array(
		"name"        => "Specialization: Rock",
		"levelmax"    => 15,
		"stat"        => "Damage%",
		"valperlevel" => 1,
		"img"         => "dmg-rock.png",
		"description" => "Increase Damage of Rock heroes by 1% per level",
		"condition"   => "element=Rock",
	),
	6 => array(
		"name"        => "Specialization: Paper",
		"levelmax"    => 15,
		"stat"        => "Damage%",
		"valperlevel" => 1,
		"img"         => "dmg-paper.png",
		"description" => "Increase Damage of Papers heroes by 1% per level",
		"condition"   => "element=Paper",
	)
);

