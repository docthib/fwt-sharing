<?php

// Define configuration
define("DB_HOST", "");
define("DB_USER", "");
define("DB_PASS", "");
define("DB_NAME", "");

define("DEBUG", FALSE);
define("MAINTENANCE", FALSE);
define("SALT", "+");

class Database
{
  private $host   = DB_HOST;
  private $user   = DB_USER;
  private $pass   = DB_PASS;
  private $dbname = DB_NAME;

  private $dbh;
  private $eror;

  public function __construct()
  {
    // Set DSN
    $dsn = 'mysql:host=' . $this->host . ';dbname=' . $this->dbname;
    // Set options
    $options = array(
      PDO::ATTR_EMULATE_PREPARES   => false, // important! use actual prepared statements (default: emulate prepared statements)
      PDO::ATTR_PERSISTENT         => true,
      PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
      PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
      PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"
    );
    // Create a new PDO instance
    try
    {
      $this->dbh = new PDO($dsn, $this->user, $this->pass, $options);
    }
    // Catch any errors
    catch(PDOException $e)
    {
      $this->error = $e->getMessage();
    }
  }

  public function query($query)
  {
    $this->stmt = $this->dbh->prepare($query);
  }

  public function bind($param, $value, $type = null)
  {
    if (is_null($type))
    {
      switch (true)
      {
        case is_int($value):
          $type = PDO::PARAM_INT;
        break;
        case is_bool($value):
          $type = PDO::PARAM_BOOL;
        break;
        case is_null($value):
          $type = PDO::PARAM_NULL;
        break;
        default:
          $type = PDO::PARAM_STR;
        break;
      }
    }
    $this->stmt->bindValue($param, $value, $type);
  }

  function bindMultiple($params, &$value, $type = null)
  {
    foreach ($params as $param) {
      if (is_null($type))
      {
        switch (true)
        {
          case is_int($value):
            $type = PDO::PARAM_INT;
          break;
          case is_bool($value):
            $type = PDO::PARAM_BOOL;
          break;
          case is_null($value):
            $type = PDO::PARAM_NULL;
          break;
          default:
            $type = PDO::PARAM_STR;
          break;
        }
      }
      $this->stmt->bindValue($param, $value, $type);
    }
  }

  public function execute()
  {
    return $this->stmt->execute();
  }

  public function fetch(){
    $this->execute();
    return $this->stmt->fetchAll(PDO::FETCH_ASSOC);
  }

  public function rowCount(){
    return $this->stmt->rowCount();
  }

  public function lastInsertId(){
    return $this->dbh->lastInsertId();
  }

  public function beginTransaction(){
    return $this->dbh->beginTransaction();
  }

  public function endTransaction(){
    return $this->dbh->commit();
  }

  public function cancelTransaction(){
    return $this->dbh->rollBack();
  }
}
?>