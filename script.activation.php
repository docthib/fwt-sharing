<?php
session_start();

include_once "include.maintenance-check.php";

// Vérifier si déjà connecté
// Vérifier si existe
// Vérifier si déjà activé

// email & code

// update status

// redirect sur la page register


if (isset($_SESSION['user']['id']))
{
	$_SESSION['notification']['type'] = "danger";
  $_SESSION['notification']['titre'] = "Error!";
	$_SESSION['notification']['message'] = "You are already logged in omg !";
	redirect();
}

// Database connexion
include_once "class.database.php";

// Control and sanitize data
$email = filter_var($_GET['e'], FILTER_SANITIZE_EMAIL);
$code  = filter_var($_GET['c'], FILTER_SANITIZE_STRING);

// Récupérer l'ID
$sql = "SELECT user_id FROM user
WHERE email = :email";

try
{
  $database = new Database();
  $database->query($sql);
  $database->bind(":email", $email);

  $result = $database->fetch();

  // Si l'email n'est pas retrouvé on balance une erreur
  if (count($result) == 0)
  {
    $_SESSION['notification']['type'] = "danger";
    $_SESSION['notification']['titre'] = "Error!";
    $_SESSION['notification']['message'] = "Email not found";                            
    redirect();
  }

  // On stock l'user id
  $user_id = $result[0]["user_id"];

  unset($database);
}
catch(Exception $e)
{
  if (!DEBUG)
  {
    $_SESSION['notification']['type'] = "danger";
    $_SESSION['notification']['titre'] = "Error!";
    $_SESSION['notification']['message'] = "Error while activation s.a1";    
    redirect();
  }
}

// Récupérer et vérifier les données
$sql = "SELECT code, activation_time, failed
FROM user_email_validation
WHERE user_id = :user_id";

try
{
  $database = new Database();
  $database->query($sql);
  $database->bind(":user_id", $user_id);

  $result = $database->fetch();

  // Si le nick n'est pas retrouvé on balance une erreur
  if (count($result) == 0)
  {
    $_SESSION['notification']['type'] = "danger";
    $_SESSION['notification']['titre'] = "Error!";
    $_SESSION['notification']['message'] = "Impossible";                                      
    redirect();
  }

  // On stock ces données pour une future utilisation
  $codebdd         = $result[0]["code"];
  $activation_time = $result[0]["activation_time"];
  $failed          = $result[0]["failed"];

  unset($database);
}
catch(Exception $e)
{
  if (!DEBUG)
  {
    $_SESSION['notification']['type'] = "danger";
    $_SESSION['notification']['titre'] = "Error!";
    $_SESSION['notification']['message'] = "Error while activation s.a2";    
    redirect();
  }
}

// Already activated
if (!is_null($activation_time))
{
  $_SESSION['notification']['type'] = "danger";
  $_SESSION['notification']['titre'] = "Error!";
  $_SESSION['notification']['message'] = "Already activated";                            
  redirect();
}

// Code is wrong
if ($code != $codebdd)
{
  $failed++;

  $sql = "UPDATE user_email_validation
  SET failed = :failed
  WHERE user_id = :user_id";
  try
  {
    $database = new Database();
    $database->query($sql);
    $database->bind(":failed", $failed);
    $database->bind(":user_id", $user_id);

    $database->execute();

    unset($database);
  }
  catch(Exception $e)
  {
    if (!DEBUG)
    {
      $_SESSION['notification']['type'] = "danger";
      $_SESSION['notification']['titre'] = "Error!";
      $_SESSION['notification']['message'] = "Error while activation s.a3";
      redirect();
    }
  }

  $_SESSION['notification']['type'] = "danger";
  $_SESSION['notification']['titre'] = "Error!";
  $_SESSION['notification']['message'] = "Wrong code";                                          
  redirect();
}

// Everything OK, update activation_time
$sql = "UPDATE user_email_validation
SET activation_time = :activation_time
WHERE user_id = :user_id";

try
{
  $database = new Database();
  $database->query($sql);
  $database->bind(":user_id", $user_id);
  $database->bind(":activation_time", time());

  $database->execute();

  unset($database);
}
catch(Exception $e)
{
  if (!DEBUG)
  {
    $_SESSION['notification']['type'] = "danger";
    $_SESSION['notification']['titre'] = "Error!";
    $_SESSION['notification']['message'] = "Error while activation s.a4";    
    redirect();
  }
}

$sql = "UPDATE user
SET account_status = 1
WHERE user_id = :user_id";

try
{
  $database = new Database();
  $database->query($sql);
  $database->bind(":user_id", $user_id);

  $database->execute();

  unset($database);
}
catch(Exception $e)
{
  if (!DEBUG)
  {
    $_SESSION['notification']['type'] = "danger";
    $_SESSION['notification']['titre'] = "Error!";
    $_SESSION['notification']['message'] = "Error while activation s.a5";    
    redirect();
  }
}

$_SESSION['notification']['type'] = "success";
$_SESSION['notification']['titre'] = "Success!";
$_SESSION['notification']['message'] = "Activation successful :)<br />You can login now";
redirect();

function redirect()
{
  header("location:/register");exit;
}