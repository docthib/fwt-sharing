 <div>
	<h1>Setup new password</h1>	

	<form role="form" id="userrecovery" action="" onsubmit="return false;">
		<div class="form-group">
			<input type="password" class="form-control" name="newpass" id="newpass" placeholder="New password" />
		</div>
		<div class="form-group">
			<input type="password" class="form-control" name="repass" id="repass" placeholder="Retype new password" />
		</div>

		<button role="submit" class="btn btn-primary">Save</button>
	</form>
</div>
