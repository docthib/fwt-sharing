$(document).ready(function()
{
	$("body").on("submit", "form#registration", function(){
		register();
	})
	.on("submit", "form#userlogin", function(){
		login();
	})
	.on("submit", "form#userrecovery", function(){
		recovery();
	});
});

function register()
{
	var dataform = $("form#registration").serialize();

	if (!dataform){return;}

	$.ajax({
	  method: "POST",
	  url: "ajax.register.php",
	  data: { 
			"data" : dataform
	  }
	})
	.done(function(data) 
	{
		var json = JSON.parse(data);

		if (json.success)
		{
			var html = "<div class='alert alert-success alert-dismissible fade in'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>" + json.message + "</div>";
		}
		else
		{
			// error
			var html = "<div class='alert alert-danger alert-dismissible fade in'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>" + json.message + "</div>";
		}

		if ($(".alert").length){$(".alert").remove();}
		
		$("body > div:first").prepend(html);
	});
}

function login()
{
	var dataform = $("form#userlogin").serialize();

	if (!dataform){return;}

	$.ajax({
	  method: "POST",
	  url: "ajax.login.php",
	  data: { 
			"data" : dataform
	  }
	})
	.done(function(data) 
	{
		var json = JSON.parse(data);

		if (json.success)
		{
			$("body > div:first").empty();
			var html = "<div class='alert alert-success alert-dismissible fade in'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>" + json.message + "</div>";
			setTimeout("redirect('./')", 2000);
		}
		else
		{
			// error
			var html = "<div class='alert alert-danger alert-dismissible fade in'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>" + json.message + "</div>";
		}

		if ($(".alert").length){$(".alert").remove();}

		$("body > div:first").prepend(html);
	});
}

function recovery()
{
	var dataform = $("form#userrecovery").serialize();

	if (!dataform){return;}

	$.ajax({
	  method: "POST",
	  url: "ajax.recovery.php",
	  data: { 
			"data" : dataform
	  }
	})
	.done(function(data) 
	{
		var json = JSON.parse(data);

		if (json.success)
		{
			buildNotification("success", json.message, undefined , 1);
			setTimeout("redirect('./register')", 2000);
		}
		else
		{
			// error
			buildNotification("danger", json.message, undefined , 1);
		}
	});
}

function redirect(url)
{
	window.location.href = url;
}

function buildNotification(type, message, title, is_temp)
{
  var timeout = 5000;

  // remove old notif
  if ($(".notif").length){$(".notif").remove();}

  if (is_temp === undefined)
  {
    is_temp = false;
  }

  var html = $("<div></div>");
  $(html).addClass("notif alert alert-dismissible fade in alert-" + type);
  $(html).append("<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>");

  if (title !== undefined)
  {
    $(html).append( "<h4>" + title + "</h4>" );
  }

  $(html).append( "<p>" + message + "</p>" );
  
  $("body > div.container:first, body > div.container-fluid:first").prepend(html);

  if (is_temp)
  {
    setTimeout(function() { 
      $(".alert").fadeOut(1000); 
    }, timeout);
  }
}