window.onbeforeunload = function(e) 
{
    saveInSession();
    return 'No unsaved changes ?';
};

var stats_translation = {};
var current_language = "en";

// load translation
$.ajax({
  method: "POST",
  url: "data.stats.php",
  data: { 
    "action" : "getTranslation"
  }
})
.done(function(data) 
{
  stats_translation = JSON.parse(data);
});

// translate
function t(stat)
{
  var val = stat;
  if (stats_translation[stat] == undefined)
  {
    console.log(stat);
  }
  else
  {
    val = stats_translation[stat][current_language];
  }
  return val;
}

var data_compare = {
  json : "",
  stats : {},
  active : false,
  button_save    : $("#build-save"),
  button_restore : $("#build-restore"),
  button_delete  : $("#build-delete"),
  saveBuild : function()
  {
    if (this.checkBuild())
    {
      if (!confirm("A build is already saved, do you want to overwrite it ?"))
      {
        return;
      }
    }

    this.active = true;
    this.json   = toJSONpls();
    this.stats  = arrayClone(calculatePls());
    this.compareStats(this.stats);

    $(this.button_save).addClass("bg-rare");
    $(this.button_restore).removeClass("disabled");
    $(this.button_delete).removeClass("disabled");
  },

  restoreBuild : function()
  {
    loader.init();
    loadCalcFromJSON(this.json);
    loader.stop();
  },

  deleteBuild : function()
  {
    this.active = false;
    this.json   = "";
    this.stats  = [];
    this.emptyDisplay();

    $(this.button_save).removeClass("bg-rare");
    $(this.button_restore).addClass("disabled");
    $(this.button_delete).addClass("disabled");
  },

  compareStats : function(total)
  {
    if (!this.checkBuild())
    {
      alert("save stats first");
      return; 
    }

    this.emptyDisplay();
    var stats_name = this.getAllStatsName(total);
    this.createStatsDisplay(stats_name, total);
  },

  checkBuild : function()
  {
    return this.active;
  },

  getAllStatsName : function(total)
  {
    stats_name = [];

    for (var stat in total)
    {
      stats_name[stat] = true;
    }

    for (var stat in this.stats)
    {
      stats_name[stat] = true;
    }

    return stats_name;
  },

  emptyDisplay : function()
  {
    $("#hero-stats #comparison-table tbody").empty();
  },

  createStatsDisplay : function(stats_name, total)
  {
    var $comparison_table = $("#hero-stats #comparison-table tbody");
    i = 0;

    for (var stat in stats_name)
    {
      i++;
      var val_before = this.stats[stat] || 0;
      var val_after  = total[stat] || 0;

      var $row     = $("<tr>").addClass("stat").data("stat", tdf(stat));
      var $td_name = $("<td>").text( t(stat) );
      var $td_col1 = $("<td>").addClass("total1").text(roundIfFloat(val_before));
      var $td_col2 = $("<td>").addClass("total2").text(roundIfFloat(val_after));

      if (i > 10)
      {
        $row.addClass("stat-add");
      }

      if (val_before > val_after)
      {
        $($td_col1).addClass("text-green");
        $($td_col2).addClass("text-red");
      }
      else if (val_before < val_after)
      {
        $($td_col1).addClass("text-red");
        $($td_col2).addClass("text-green");
      }

      $($row).append($td_name, $td_col1, $td_col2);

      $($comparison_table).append($row);
    }
  },
};

$(document).ready(function()
{
  // on load
  $('[data-toggle="tooltip"]').tooltip({container: 'body', trigger : 'hover'});

  $("body")
  .on("click", ".costume", function() 
  {
    $(this).toggleClass("active");
    calculatePls();
  })
  .on("change", "select.rarity", function()
  {
    applyBgRarity(this);
  })
  .on("change", "#hero-potentials select, #lord-battle-mastery select, #hero-stats-damage input, #equip-stats .potential-val select", function()
  {
    calculatePls();
  })
  .on("click", "#calc-json-textarea", function()
  {
    this.select();
  })
  .on("click", "#btn-calc-import", function()
  {
    var json = $("#calc-json-textarea").val();

    if (json == "")
    {
      return;
    }

    loadCalcFromJSON(json);

    $('#modal-calc').modal('hide');
  })
  .on("click", ".div-close", function()
  {
    $(this).parent().toggle();
  });

  ///////////////////////////////
  // HERO PICK
  //

  $("#hero-pick")
  .on("change", "#select-hero", function()
  {
    displayIllustration();
    displayCostumes();
    displaySoulGears();

    $("#btn-apply-stats").removeClass("btn-primary").addClass("btn-danger");
    checkAwakening();
    // theater($("#btn-apply-stats"));
  })
  .on("click", "#btn-apply-stats", function()
  {
    var hero = $("#select-hero").val();
    var opt  = $("#select-hero option:selected");

    // populate fields
    var datas = $(opt).data();
    for (var stat in datas)
    {
      if ($("#hero-stats #hero-" + tdf(stat)).length)
      {
        $("#hero-stats #hero-" + tdf(stat)).val(datas[stat]);
      }
    }

    $(this).removeClass("btn-danger").addClass("btn-primary");

    calculatePls();
  })
  .on("click", "#btn-type-stats", function()
  {
    $("#hero-stats-type").toggle();
  })
  .on("click", "#btn-apply-typed-stats", function()
  {
    $("#hero-stats-type .stat").each(function()
    {
      var stat  = $(this).data("stat");
      var white = parseInt($(this).find(".white-stat").val()) || 0;
      var red   = parseInt($(this).find(".red-stat").val()) || 0;
      var val   = white - red;

      $("#hero-stats-details #hero-" + tdf(stat)).val(val);
    });

    $("#hero-stats-type").hide();
    $("#btn-apply-stats").removeClass("btn-danger").addClass("btn-primary");

    calculatePls();
  });

  ///////////////////////////////
  // HERO POTENTIAL
  //

  $("#hero-potentials").on("change", ".potential-stat select", function()
  {
    var num = $(this).closest(".potential").data("num");

    displayHeroPotential(num);
  });

  ///////////////////////////////
  // EQUIPMENT
  //

  $("#equip-stats")
  .on("change", ".set select", function()
  {
    getSetStuff(this);
  })
  .on("change", ".potential-stat select", function()
  {
    loader.init();

    var stat = $(this).val();

    if (stat == "")
    {
      $(this).attr("class", "form-control");
    }

    addCloneButton(this);
    updateEquipPotential(this);

    $.when(getEquipPotentials(this)).done(function()
    {
      calculatePls();
      loader.stop();
    });      
  })
  .on("change", ".stuff select, .item-rarity select, .enhancement select, .set select", function()
  {
    loader.init();

    updateItemSelects(this);

    $.when(getEquipStats(this)).done(function()
    {
      updateActivatedSets();
      calculatePls();
      loader.stop();
    });    
  })
  .on("click", ".action-clone-potential", function()
  {
    cloneEquipPotential(this);
  })
  .on("click", ".platinum img", function()
  {
    togglePlatinum(this);
    updateItemSelects(this);
    getEquipStats(this);
    calculatePls();
  })
  .on("click", "#equip-btn-set button", function()
  {
    var role = $(this).data("role");
    var type = $(this).data("type");

    if (role == "legendary_max")
    {
      setEquipRarity("legendary");
      setEnhancement(10);
    }
    else if (role == "transcended_max")
    {
      setEquipRarity("transcended");
      setEnhancement(12);
      setEnhancement(15);
    }
    else if (role == "potential_common_build")
    {
      data           = {};
      data.weapon    = ["attack%","critrate%","criticaldamage%","damage%"];
      data.armor     = ["hp%","defense%","damagetaken%","dodge%"];
      data.accessory = ["attackbyhp","hp%","critrate%","criticaldamage%","damagetaken%","boss%","attack%","defense%","dodge%"];
      setItemPotentialsBuild(data, 1);
    }
    else if (role == "potential_max_common_build")
    {
      data           = {};
      data.weapon    = ["attack%","critrate%","criticaldamage%","damage%"];
      data.armor     = ["hp%","defense%","damagetaken%","dodge%"];
      data.accessory = ["attackbyhp","hp%","critrate%","criticaldamage%","damagetaken%","boss%","attack%","defense%","dodge%"];
      setItemPotentialsBuild(data, 0);
    }
    else if (role == "costumes")
    {
      $("#hero-costumes .costume:visible").addClass("active");
      $("#lord-costumes .costume:visible").addClass("active");
      $("#hero-soul-gears .costume:visible").addClass("active");

      $(".battle-mastery select").each(function() {
        var lvlmax = $(this).data("lvlmax");
        $(this).val(lvlmax);
      });
      calculatePls();
    }
    else if (role == "platinum")
    {
      setPlatinum(1);
    }
    else if (role == "reset_equips")
    {
      resetEquips();
      resetPotentials(); 
      calculatePls();
    }
    else if (role == "reset_potentials")
    {
      resetPotentials();
      calculatePls();
    }
    else
    {
      setEquipStuff(type, role);
    }
  })
  .on("click", ".btn-sets-list", function()
  {
    var modal = $("#modal-sets");

    $(modal).modal('show');
  })
  .on("click", ".sets-activated .set-info", function()
  {
    $(this).toggleClass("activated");
    calculatePls();
  })
  .on("click", "#button-combat-condition", function()
  {
    $(this).toggleClass("bg-rare").toggleClass("btn-default");
    if ($(this).hasClass("bg-rare"))
    {
      $(".sets-activated .set-info").addClass("activated");
    }
    else
    {
      $(".sets-activated .set-info").removeClass("activated");
    }
    calculatePls();
  });

  ///////////////////////////////
  // RUNES
  //

  $("#runes")
  .on("click", "#runes-btn-set button", function()
  {
    var role = $(this).data("role");
    var type = $(this).data("type");

    if (role == "grade")
    {
      setRunesGrade(type);
    }
    else if (role == "type")
    {
      setRunesType(type);
    }
    else if (role == "reset_runes")
    {
      resetRunes();
      calculatePls();
    }
    else
    {
      // something...
    }
  })
  .on("change", ".grade select, .type select", function()
  {
    loader.init();

    updateRuneSelects(this);

    $.when(getRuneStats(this)).done(function()
    {
      calculatePls();
      loader.stop();
    });    
  });

  //
  // DAMAGE CALCULATIONS
  //

  $("#group-atk #advantages #advantage, #group-atk #advantages #opposite-advantage").on("click", function()
  {
    if ($(this).is(":checked"))
    {
      if ($(this).attr("id") == "advantage")
      {
        $("#opposite-advantage").removeAttr("checked");
      }
      else
      {
        $("#advantage").removeAttr("checked");
      }
    }
  });

  $("#group-atk").on("click", "#advantages input", function()
  {
    calculatePls();
  });

  $("#hero-stats-details").on("click", ".display-mode button", function()
  {
    toggleStatsTable(this);
  });

  //
  // MENU
  //

  $("#calc-save").on("click", function()
  {
    saveJSONpls();
  });

  $("#calc-load").on("click", function()
  {
    loadJSONpls();
  });

  $("#calc-export").on("click", function()
  {
    openExportDialog();
  });

  $("#calc-import").on("click", function()
  {
    openImportDialog();
  });

  $("#sets-list").on("click", ".set-info", function()
  {
    if (!confirm("Do you want to equip this set ?"))
    {
      return;
    }

    $(this).closest(".sets-grade").find(".set-info").removeClass("active");
    $(this).addClass("active");

    var grade  = $(this).data("grade");
    var set_id = $(this).data("id");

    var equip = $(".equip[data-grade='" + grade + "']");
    setSet(equip, set_id);
    calculatePls();
  });

  $(".left-bloc .switch").on("click", function()
  {
    var parent = $(this).parent();
    
    $(parent).toggleClass("left-hidden"); 
    $(".mid-bloc").toggleClass("left-hidden"); 

    $(this).find("span").toggleClass("glyphicon-triangle-right");
    $(this).find("span").toggleClass("glyphicon-triangle-left");
  });
  
  $(".right-bloc .switch").on("click", function()
  {
    var parent = $(this).parent();
    
    $(parent).toggleClass("right-hidden"); 
    $(".mid-bloc").toggleClass("right-hidden"); 

    $(this).find("span").toggleClass("glyphicon-triangle-right");
    $(this).find("span").toggleClass("glyphicon-triangle-left");
  });

  $("#build-save").on("click", function()
  {
    data_compare.saveBuild();
  });

  $("#build-restore").on("click", function()
  {
    data_compare.restoreBuild();
  });

  $("#build-delete").on("click", function()
  {
    data_compare.deleteBuild();
  });
});

function checkAwakening()
{
  var $el = $("#select-hero option:selected");
  var awak = $el.data("awakenable");

  if (awak)
  {
    $("#runes.disabled").removeClass('disabled');
    $(".runes-unavailable").hide();
    $("#hero-awaken span").attr("class", "glyphicon glyphicon-star awake-star");
  }
  else
  {
    $("#runes").addClass('disabled');
    $(".runes-unavailable").show();
    $("#hero-awaken span").attr("class", "glyphicon glyphicon-star-empty non-awake-star");
  }
}

function setItemPotentialsBuild(data, tier)
{
  loader.init();
  var functionList = [];

  $("#equip-stats .equip").each(function() 
  {
    var $equip       = $(this);
    var equip_rarity = $equip.find(".item-rarity select").val();

    if (equip_rarity == "") return;

    var type         = $equip.data("type");
    var potentials   = $equip.find(".potentials");

    // duplicate data because we are gonne remove used elements for a equip type
    var data_clone = JSON.parse(JSON.stringify(data));

    $(potentials).find(".potential").each(function()
    {
      var potential_stat = $(this).find(".potential-stat select");

      var ok = false;
      for (var index in data_clone[type])
      {
        var stat = data_clone[type][index];
        if (ok) 
        {
          return;
        }

        if ($(potential_stat).find("option[value='" + stat + "']").length)
        {
          $(potential_stat).val(stat);
          functionList.push(getEquipPotentials(potential_stat));

          delete data_clone[type][index];
          ok = true;
        }
      }
    });
  });

  // tier starts at 0
  $.when.apply(null, functionList).done(function() 
  {
    $(".potential-val select option[data-level='" + tier + "']").attr("selected", "selected");
    applyAllBgRarity();
    calculatePls();
    loader.stop();
  });
}

function setSet(element, set_id)
{
  $(element).find(".set select").each(function() 
  {
    $(this).val(set_id);
    getSetStuff($(this));
  });

  var platinums = $(element).find(".platinum input");
  setPlatinum("", platinums);
  updateActivatedSets();
}

function displayIllustration()
{
  var hero = $("#select-hero").val();

  if (hero != "")
  {
    $("body").css("background-image", "url(assets/images/illust/" + hero + ".png)");
  }
  else
  {
    $("body").css("background-image", "url(assets/images/bg/yekaterina-by-fizintine.jpg)");
  }
}

function resetEquips()
{
  $("#equip-stats .equip").each(function() 
  {
    $(this).find(".set select, .stuff select, .item-rarity select, .enhancement select").val("").attr("class", "form-control").removeAttr("disabled");
  });

  $("#equip-stats .sets-activated").empty();
  setPlatinum(0);
  applyAllBgRarity();
}

function resetRunes()
{
  $("#runes .rune").each(function() 
  {
    $(this).find(".rune-grade select, .rune-type select").val("").attr("class", "form-control").removeAttr("disabled");
    getRuneStats( this );
  });
}

function resetPotentials()
{
  $("#equip-stats .potential-stat select").each(function() 
  {
    $(this).val("").attr("class", "form-control");
    updateEquipPotential(this);
    getEquipPotentials(this);
  });

  $(".btn-action-container").remove();
}

function addCloneButton(element)
{
  var potential = $(element).closest(".potential");
  var stat      = $(potential).find(".potential-stat select").val();

  // if no stat, remove button
  if (stat == "")
  {
    $(potential).find(".btn-action-container").remove();
    return;
  }

  // if already exist leave
  if ($(potential).find(".btn-action-container").length)
  {
    return;
  }

  var div = $("<div>").addClass("btn-action-container");
  var btn = $("<div>").addClass("btn-action action-clone-potential").html("<span class='glyphicon glyphicon-fullscreen'></span>");
  $(div).append(btn);
  $(potential).append(div);
}

function cloneEquipPotential(element)
{
  var potential = $(element).closest(".potential");
  var stat      = $(potential).find(".potential-stat select").val();
  var level     = $(potential).find(".potential-val select option:selected").data("level");

  if (stat == "")
  {
    return;
  }

  var functionList = [];
  loader.init();

  $("#equip-stats .equip").each(function()
  {
    var equip = $(this);

    if ($(equip).find(".potential-stat select:not(:disabled) option[value='" + stat + "']").length == 0 || $(equip).find(".potential-stat select option[value='" + stat + "']:selected").length)
    {
      return;
    }

    var check = false;
    $(equip).find(".potential-stat select").each(function() 
    {
      if ($(this).val() != "" || check) 
      {
        return;
      }

      check = true;
      $(this).val(stat);
      addCloneButton(this);
      functionList.push(getEquipPotentials(this));
    });
  });

  level = (level == "") ? 0 : level;

  $.when.apply(null, functionList).done(function() 
  {
    $("#equip-stats .potential-stat select option[value='" + stat + "']:selected").closest(".potential").find(".potential-val select option[data-level='" + level + "']").attr("selected", "selected");
    applyAllBgRarity();
    calculatePls();
    loader.stop();
  });
}

function setPlatinum(status, elements)
{
  elements = (elements == undefined || elements == "") ? "#equip-stats .platinum input" : elements;
  var functionList = [];
  loader.init();

  status = (status != 1) ? "" : 1;

  $(elements).val(status).each(function()
  {
    updateItemSelects(this);
    displayPlatinum(this);

    functionList.push(getEquipStats(this));
  });

  // When all ajax are complete...
  $.when.apply(null, functionList).done(function() 
  {
    calculatePls();
    loader.stop();
  });
}

function setEquipRarity(rarity)
{
  $("#equip-stats .item-rarity select").each(function()
  {
    var equip = $(this).closest(".equip");
    var set   = $(equip).find(".set select").val();
    var plat  = $(equip).find(".platinum input").val();

    if (rarity == "transcended" && (set == "" && plat == 0))
    {
      return;
    }

    $(this).val(rarity);
    updateItemSelects(this);
  });
  applyAllBgRarity();
}

function setEnhancement(level)
{
  var functionList = [];
  loader.init();

  $("#equip-stats .enhancement select").each(function()
  {
    var $el     = $(this);
    var $equip  = $el.closest(".equip");
    var plat    = $equip.find(".platinum input").val();
    var $rarity = $equip.find(".rarity option:selected");
    var min     = $rarity.data("minenhancement") || 0;
    var max     = $rarity.data("maxenhancement") || 0;

    if (level < min || level > max || (plat && level == 13)) {return;}

    $el.val(level);
    functionList.push(getEquipStats(this));
  });

  // When all ajax are complete...
  $.when.apply(null, functionList).done(function() 
  {
    calculatePls();
    loader.stop();
  });
}

function setEquipStuff(type, stuff)
{
  var functionList = [];
  loader.init();
  
  $("#equip-stats .equip[data-type='" + type + "'] .set select").val("");
  $("#equip-stats .equip[data-type='" + type + "'] .stuff select").val(stuff).removeAttr("disabled").each(function()
  {
    updateItemSelects(this);
    functionList.push(getEquipStats(this));
  });

  // When all ajax are complete...
  $.when.apply(null, functionList).done(function() 
  {
    calculatePls();
    loader.stop();
  });
}

function setRunesGrade(grade)
{
  var functionList = [];
  loader.init();

  $("#runes .rune-grade select").each(function()
  {
    var equip  = $(this).closest(".equip");

    $(this).val(grade);
    functionList.push(getRuneStats(this));
  });

  // When all ajax are complete...
  $.when.apply(null, functionList).done(function() 
  {
    calculatePls();
    loader.stop();
  });
}

function setRunesType(type)
{
  var functionList = [];
  loader.init();

  var balanced = {
    "left"  : "hp",
    "mid"   : "attack",
    "right" : "defense",
  };

  $("#runes .rune-type select").each(function()
  {
    var $equip = $(this).closest(".rune");
    var slot   = $equip.data("rune-slot");

    var real_type = ( type == "balanced" ) ? balanced[slot] : type;

    $(this).val(real_type);
    functionList.push( getRuneStats(this) );
  });

  // When all ajax are complete...
  $.when.apply(null, functionList).done(function() 
  {
    calculatePls();
    loader.stop();
  });
}

function toggleStatsTable(element)
{
  var $el = $(element);

  if ( $el.hasClass("active") )
  {
    return;
  }

  $el.parent().find("button").removeClass("active");
  $el.addClass("active");

  checkStatsTableDisplay();
}

function togglePlatinum(element)
{
  target      = $(element).data("target");
  $platinum   = $("#" + target);
  platinumVal = $platinum.val();

  if (platinumVal == 1)
  {
    $platinum.val("");
  }
  else
  {
    $platinum.val(1);
  }

  displayPlatinum($platinum);
}

function displayPlatinum(input)
{
  var $el       = $(input);
  var $platinum = $el.closest(".platinum");
  var $img      = $platinum.find("img");
  
  var $equip    = $platinum.closest(".equip");
  var $set      = $equip.find(".set select");
  var $stuff    = $equip.find(".stuff select");

  if ( $el.val() == 1 )
  {
    $img.addClass("active");
    $set.attr("disabled", true).val("");
    $stuff.removeAttr("disabled");
  }
  else
  {
    $img.removeClass("active");
    $set.removeAttr("disabled");
  }
}

function openImportDialog()
{
  emptyModal();

  var modal = $("#modal-calc");
  
  // add title
  $(modal).find(".modal-title").text("Import JSON data");

  // add textarea
  var textarea = $("<textarea class='form-control' id='calc-json-textarea' rows='10'></textarea>");
  $(modal).find(".modal-body").append(textarea);

  // add import button
  $button = $("<button id='btn-calc-import' class='btn btn-primary'>Import</button>");
  $(modal).find(".modal-footer").append($button); 

  $(modal).modal('show');
}

function openExportDialog()
{
  emptyModal();

  json = toJSONpls();

  var $modal = $("#modal-calc");

  // add title
  $modal.find(".modal-title").text("Export JSON data");

  // add textarea
  var $textarea = $("<textarea class='form-control' id='calc-json-textarea' rows='10'></textarea>").val(json);
  $modal.find(".modal-body").append($textarea);

  $modal.modal('show');
}

function emptyModal()
{
  var $modal = $("#modal-calc");

  $modal.find(".modal-title").empty();
  $modal.find(".modal-body").empty();
  $modal.find(".modal-footer button:not(.btn-close)").remove();
}


function displayCostumes()
{
  var hero = $("#select-hero").val();

  $("#hero-costumes > div").hide();

  if (hero != "")
  {
    $("#hero-costumes ." + hero).show();
  }
}

function displaySoulGears()
{
  var hero = $("#select-hero").val();

  $("#hero-soul-gears > div").hide();

  if (hero != "")
  {
    $("#hero-soul-gears ." + hero).show();
  }
}

function displayHeroPotential(num)
{
  if (num === undefined)
  {
    $("#hero-potentials .potential").each(function() 
    {
      var potential = $(this);
      var num       = $(potential).data("num");
      var stat      = $(potential).find(".potential-stat select").val();

      if (stat != "-1")
      {
        $(potential).find(".potential-val select").hide();

        if ( $(potential).find(".potential-val select[data-stat='" + stat + "']").length )
        {
          $(potential).find(".potential-val select[data-stat='" + stat + "']").show();
        }
      }

      $(potential).find(".potential-val select:hidden").val("");
    });
  }
  else
  {
    var potential = $("#hero-potentials .potential[data-num='" + num + "']");
    var stat      = $(potential).find(".potential-stat select").val();

    if (stat != "-1")
    {
      $(potential).find(".potential-val select").hide();
      
      if ( $(potential).find(".potential-val select[data-stat='" + stat + "']").length )
      {
        $(potential).find(".potential-val select[data-stat='" + stat + "']").show();
      }
    }

    $(potential).find(".potential-val select:hidden").val("");
  }
}

function updateItemSelects(element)
{
  var equip          = $(element).closest(".equip");
  var rarity         = $(equip).find(".item-rarity select");
  var minenhancement = $(rarity).find("option:selected").data("minenhancement") || 0;
  var maxenhancement = $(rarity).find("option:selected").data("maxenhancement") || 0;
  var maxpotential   = $(rarity).find("option:selected").data("maxpotential") || 0;
  var platinum       = $(equip).find(".platinum input").val();
  var set            = $(equip).find(".set select").val();

  maxenhancement = (platinum && $(rarity).val() == "transcended") ? 12 : maxenhancement;

  if (set == "" && platinum == 0)
  {
    minenhancement = 0;
    maxenhancement = 10;
    $(rarity).find("option[value='transcended']").hide();

    if ($(rarity).val() == "transcended")
    {
      $(rarity).val("");
      applyBgRarity(rarity);
    }
  }
  else
  {
    $(rarity).find("option[value='transcended']").show();
  }

  if ( $(equip).find(".enhancement select").val() > maxenhancement )
  {
    $(equip).find(".enhancement select").val(maxenhancement);
    getEquipStats(element);
  }

  if ( $(equip).find(".enhancement select").val() < minenhancement )
  {
    $(equip).find(".enhancement select").val(minenhancement);
    getEquipStats(element);
  }

  for (i = 0 ; i <= maxenhancement ; i++)
  {
    $(equip).find(".enhancement option[value='" + i + "']").show();
  }

  for (i = 0 ; i < minenhancement ; i++)
  {
    $(equip).find(".enhancement option[value='" + i + "']").hide();
  }

  for (i = maxenhancement + 1 ; i <= maxenhancement ; i++)
  {
    $(equip).find(".enhancement option[value='" + i + "']").hide();
  }

  for (i = 0 ; i <= maxpotential ; i++)
  {
    $(equip).find(".potential[data-num='" + i + "']").find("select").removeAttr("disabled");
  }

  for (i = maxpotential + 1 ; i <= 10 ; i++)
  {
    var $potential = $(equip).find(".potential[data-num='" + i + "']");
    $potential.find("select").attr("disabled", true).val("");
    $potential.find(".btn-action-container").remove();
  }
}

function updateRuneSelects(element)
{
  // var $rune = $(element).closest(".rune");
  // var slot  = $rune.data("rune-slot");
  // var grade = $rune.find(".rune-grade select").val();
  // var type  = $rune.find(".rune-type select").val();

  // handle colors ?

  getRuneStats(element);
}

function updateEquipPotential(element)
{
  var $potentials    = $(element).closest(".potentials");
  var potential_used = {};

  // gather used values
  $potentials.find(".potential").each(function()
  {
    var stat = $(this).find(".potential-stat select").val();

    // display hidden options
    $(this).find("option").show();

    if (stat != "")
    {
      potential_used[stat] = true;
    }
  });

  // hide stats already selected
  for (var stat_used in potential_used)
  {
    $potentials.find("option[value='" + stat_used + "']").hide();
  };
}

function getSetStuff(element)
{
  var $el           = $(element);
  var $equip        = $el.closest(".equip");
  var equip_type    = $equip.data("type");
  var set_id        = $el.val();
  var set_stuff     = $("#sets-list .set-info[data-id='" + set_id + "']").data(equip_type);
  var $stuff_select = $equip.find(".stuff select");

  $stuff_select.removeAttr("disabled");

  if ( !$el.val() == "" )
  {
    $stuff_select.val(set_stuff).attr("disabled", "true");
  }
}

function updateActivatedSets()
{
  $("#sets-list .set-info").removeClass("active");

  var list_set = [];
  $(".equip .set select").each(function() 
  {
    var set_id = parseInt( $(this).val() );
    if (isNaN(set_id))
    {
      return;
    }

    list_set.push(set_id);
    $("#sets-list .set-info[data-id='" + set_id + "']").addClass("active");
  });

  var list_set_unique = list_set.filter(function(item, pos) {
      return list_set.indexOf(item) == pos;
  })

  var $sets_activated = $("#equip-stats .sets-activated");

  // Existing "activated sets" which no more exists should be removed
  // Update existing "activated sets" and create new ones 
  $(".sets-activated .set-info").each(function()
  {
    var set_id = parseInt($(this).data("id"));
    var key    = list_set_unique.indexOf(set_id);
    if (key > -1)
    {
      list_set_unique.splice(key, 1); // update
    }
    else
    {
      $(this).remove(); // remove
    }
  });

  // Add new sets blocks
  for (var key in list_set_unique)
  {
    // clone every non-existing remaining sets
    var $set_element     = $("#sets-list .set-info[data-id='" + list_set_unique[key] + "']");
    var $set_new_element = $set_element.clone();

    $set_new_element.removeClass("active").attr("style", "");
    // if ($("#button-combat-condition").hasClass("bg-rare"))
    // {
    //   $set_new_element.addClass("activated");
    // }
    $sets_activated.append($set_new_element);
  }

  // Order blocks by grade
  $sets_activated.find('.set-info').sort(function (a, b) {
      var aa = parseInt( $(a).data('grade') );
      var bb = parseInt( $(b).data('grade') );
      if (aa == bb)
      {
        var strA = $(a).data('title').toLowerCase();
        var strB = $(b).data('title').toLowerCase();
        return strA.localeCompare(strB);
      }
      return (aa < bb) ? -1 : (aa > bb) ? 1 : 0;
   }).appendTo($sets_activated);
}

function getEquipStats(element)
{
  var $equip      = $(element).closest(".equip");
  var stuff       = $equip.find(".stuff select").val();
  var enhancement = $equip.find(".enhancement select").val();
  var rarity      = $equip.find(".item-rarity select").val();
  var platinum    = $equip.find(".platinum input").val();
  var set         = $equip.find(".set select").val();
  var rank        = $("#sets-list .set-info[data-id='" + set + "']").data("rank");
  var destination = $equip.find(".stats");

  if (stuff == "" || enhancement == "" || rarity == "")
  {
    $(destination).html("<i>Use dropdowns above</i>");
    return;
  }

  var deferred =
  $.ajax({
    method: "POST",
    url: "data.equip-stats.php",
    data: { 
      "action"      : "getStats",
      "grade"       : $equip.data("grade"),
      "type"        : $equip.data("type"),
      "stuff"       : stuff,
      "enhancement" : enhancement,
      "rarity"      : rarity,
      "rank"        : rank,
      "platinum"    : platinum
    }
  })
  .done(function(data) 
  {
    var json = JSON.parse(data);

    if (json.success)
    {
      var message = "";
      
      if (json.stat.length == 0){return;}
      for (i = 0 ; i < json.stat.length ; i++)
      {
        for (var stat in json.stat[i])
        {
          message += "<div class='stat' data-stat='" + tdf(stat) + "' data-val='" + json.stat[i][stat] + "'>" + stat + " : " + json.stat[i][stat] + "</div>";
        }
      }

      $(destination).html(message);
    }
    else
    {
      $(destination).html("<i>No stat for this combo</i>");
    }
  });

  return deferred;
}

function getEquipPotentials(element)
{
  var $el         = $(element);
  var stat        = $el.val();
  var $equip      = $el.closest(".equip");
  var $potential  = $el.closest(".potential");
  var grade       = $equip.data("grade");
  var type        = $equip.data("type");
  var num         = $potential.data("num");
  var destination = $potential.find(".potential-val");

  if (grade == "" || stat == "")
  {
    $(destination).html("<i>&lt;&lt; Use dropdowns</i>");
    return;
  }

  var deferred =
  $.ajax({
    method: "POST",
    url: "data.equip-stats.php",
    data: { 
      "action"      : "getEquipPotentials",
      "grade"       : grade,
      "stat"        : stat
    }
  })
  .done(function(data) 
  {
    var json = JSON.parse(data);

    if (json.success)
    {
      if (json.list.length == 0){return;}

      var message = "<select class='form-control rarity' name='equip[" + grade + "][" + type + "][potentials][" + num + "][val]'>";
      message    += "<option class='bg-normal'></option>";

      for (i = 0 ; i < json.list.length ; i++)
      {
        message += "<option class='bg-" + json.list[i]["rarity"] + "' value='" + json.list[i]["value"] + "' data-level='" + json.list[i]["level"] + "'>" + json.list[i]["value"] + "</option>";
      }

      message += "</select>";

      $(destination).html(message);
    }
    else
    {
      $(destination).html("<i>??</i>");
    }
  });

  return deferred;
}

function getRuneStats(element)
{
  var $rune       = $(element).closest(".rune");
  var slot        = $rune.data("rune-slot");
  var grade       = $rune.find(".rune-grade select").val();
  var type        = $rune.find(".rune-type select").val();
  var destination = $rune.find(".stats");

  if (grade == "" || type == "")
  {
    $(destination).html("<i>Use dropdowns above</i>");
    return;
  }

  var deferred =
  $.ajax({
    method: "POST",
    url: "data.runes-stats.php",
    data: { 
      "action" : "getStats",
      "slot"   : slot,
      "grade"  : grade,
      "type"   : type
    }
  })
  .done(function(data) 
  {
    var json = JSON.parse(data);

    if (json.success)
    {
      var message = "";
      
      if (json.stat.length == 0){return;}
      for (i = 0 ; i < json.stat.length ; i++)
      {
        for (var stat in json.stat[i])
        {
          message += "<div class='stat' data-stat='" + tdf(stat) + "' data-val='" + json.stat[i][stat] + "'>" + stat + " : " + json.stat[i][stat] + "</div>";
        }
      }

      $(destination).html(message);
    }
    else
    {
      $(destination).html("<i>No stat for this combo</i>");
    }
  });

  return deferred;
}

function populateItemPotentials(json)
{
  data = JSON.parse(json);

  for (var field in data)
  {
    if (field.indexOf("][potentials]") != -1)
    {
      $("select[name='" + field + "']").val(data[field]);
    }
  }
}

function applyAllBgRarity()
{
  $("select.rarity:visible").each( function()
  {
    applyBgRarity(this);
  });
}

function applyBgRarity(select)
{
  var value  = $(select).val();
  var classe = $(select).find("option[value='" + value + "']").attr("class");
    
  if (classe != undefined && classe != "")
  {  
    $(select).attr("class", "form-control rarity " + classe);
  }

  if ($(select).parent().hasClass("potential-val"))
  {
    $(select).closest(".potential").find(".potential-stat select").attr("class", "form-control rarity " + classe);
  }
}

function loadCalcFromJSON(json)
{
  loader.init();

  populateFields(json);
  
  checkAwakening();
  displayIllustration();
  displayCostumes();
  displaySoulGears();
  displayHeroPotential();

  $(".equip .set select").each(function()
  {
    getSetStuff(this);
  });

  $(".equip .item-rarity select").each(function()
  {
    updateItemSelects(this);
    getEquipStats(this);
  });

  $(".equip .platinum input").each(function()
  {
    displayPlatinum(this);
  });

  // use function list to gather all ajax calls in one "spot" and wait for the deferred
  // before populate and apply rarity colors
  var functionList = [];
  $(".equip .potential-stat select").each(function()
  {
    var $el = $(this);
    updateEquipPotential( $el );
    
    if ( $el.val() != "" )
    {
      functionList.push( getEquipPotentials( $el ) );
    }
  });

  $(".rune-grade select").each(function()
  {
    var $el = $(this);
    
    if ( $el.val() != "" )
    {
      functionList.push( getRuneStats( $el ) );
    }
  });

  // When all ajax are complete...
  $.when.apply(null, functionList).done(function() 
  {
    populateItemPotentials(json);
    applyAllBgRarity();
    updateActivatedSets();
    calculatePls();

    loader.stop();
  });
}

function buildNotification(type, message, title, is_temp)
{
  var timeout = 5000;

  // remove old notif
  if ($(".notif").length){$(".notif").remove();}

  if (is_temp === undefined)
  {
    is_temp = false;
  }

  var html = $("<div></div>");
  $(html).addClass("notif alert alert-dismissible fade in alert-" + type);
  $(html).append("<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>");

  if (title !== undefined)
  {
    $(html).append( "<h4>" + title + "</h4>" );
  }

  $(html).append( "<p>" + message + "</p>" );
  
  $("body > div.container:first, body > div.container-fluid:first").prepend(html);

  if (is_temp)
  {
    setTimeout(function() { 
      $(".alert").fadeOut(1000); 
    }, timeout);
  }
}

function calculatePls()
{
  // sum of stats (flat & %)
  var sum = {};

  // sum of sets (needed because not included in conv)
  var sumset = {};

  // total before conversions (needed for conversions)
  var pretotal = {};

  // final total
  var total    = {};

  // sets (id)
  var sets = [];

  // for a set : weapon / armor / accessory
  var sets_pieces = [];

  var hero      = $("#select-hero").val();
  var optHero   = $("#select-hero option:selected");
  var stat      = "";
  var equip_qty = 0;
  var critdmg   = 30;

  if (hero == ""){return;}

  // Get Hero Base stats
  $("#hero-stats #hero-stats-details #stats-table tr.stat:not(.stat-add)").each(function() 
  {
    var stat = $(this).data("stat");
    var val  = $("#hero-" + tdf(stat)).val();

    sum[stat] = parseFloat(val) || 0;
  });

  // Get Hero Soul Gears
  $("#hero-soul-gears ." + hero + " .costume.active .stats").each(function()
  {
    var stats = $(this).data();

    for(stat in stats)
    {
      sum[stat] = sum[stat] + parseFloat(stats[stat]) || parseFloat(stats[stat]);
    }
  });
  
  // Get Hero costumes
  $("#hero-costumes ." + hero + " .costume.active .stats").each(function()
  {
    var stats = $(this).data();

    for(stat in stats)
    {
      sum[stat] = sum[stat] + parseFloat(stats[stat]) || parseFloat(stats[stat]);
    }
  });

  // Get Hero potentials
  $("#hero-potentials .potential").each(function()
  {
    var $potential = $(this);
    var stat       = $potential.find(".potential-stat select").val();

    if (stat != null && stat != -1 && stat != undefined)
    {
      val       = $potential.find(".potential-val select:visible").val();
      sum[stat] = sum[stat] + parseFloat(val) || sum[stat] + 0;
    }
  });

  // Get Lord costumes
  $("#lord-costumes .costume.active .stats").each(function()
  {
    var laData = $(this).data();

    for(stat in laData)
    {
      sum[stat] = sum[stat] + parseFloat(laData[stat]) || parseFloat(laData[stat]);
    }
  });

  // Get Lord battle mastery
  $("#lord-battle-mastery .battle-mastery select").each(function()
  {
    var $el         = $(this);
    var stat        = $el.data("stat");
    var valperlevel = $el.data("valperlevel");
    var level       = $el.val();
    var condition   = $el.data("condition");
    var val         = level * valperlevel;

    if ( !checkCondition(condition, optHero) )
    {
      return;
    }

    if (level != 0)
    {
      sum[stat] = sum[stat] + parseFloat(val) || parseFloat(val);
    }
  });

  // Get Equip stats
  $("#equip-stats .equip").each(function()
  {
    var $equip      = $(this);
    var set         = $equip.find(".set select").val();
    var stuff       = $equip.find(".stuff select").val();
    var enhancement = $equip.find(".enhancement select").val();
    var rarity      = $equip.find(".item-rarity select").val();

    // Get Nb of set pieces
    if (set != "")
    {
      sets[set]        = sets[set] + 1 || 1;
      sets_pieces[set] = (sets_pieces[set] != undefined) ? sets_pieces[set] + ";" + stuff : stuff;
    }

    // Calculate Bonus Equip Qty
    if (stuff != "" && enhancement != "" && rarity != "")
    {
      equip_qty += 1;
    }

    $equip.find(".stat").each(function()
    {
      var $el  = $(this);
      var stat = $el.data("stat");
      var val  = parseFloat($el.data("val")) || 0;

      sum[stat] = sum[stat] + val || val;
    });    
  });

  // Add Equip Bonus
  var equip_bonus = calculateEquipBonus(equip_qty);
  sum["attack"]  += equip_bonus;
  $("#equip-attack-bonus").text(equip_bonus); 

  // Get Equip potentials
  $("#equip-stats .potential").each(function()
  {
    var $el  = $(this);
    var stat = $el.find(".potential-stat select").val();
    var val  = $el.find(".potential-val select").val() || 0;

    if (stat != "" && val != 0)
    {
      sum[stat] = sum[stat] + parseFloat(val) || parseFloat(val);
    }
  });
  
  // Add Set Effects
  for (var set in sets)
  {
    var $set_element      = $("#sets-list .set-info[data-id='" + set + "']");
    var $set_new_element = $(".sets-activated .set-info[data-id='" + set + "']");
    
    // table
    var set_name        = $set_element.data("title");
    var set_grade       = $set_element.data("grade");
    var set_rank        = $set_element.data("rank");
    var set_cond_label  = $set_element.data("condition_label");
    var set_condition   = $set_element.data("condition") || "";

    $set_new_element.find(".equips .icon, .effect").addClass("lighten");
    var battle_condition_activated = $set_new_element.hasClass("activated");

    var tab_piece = sets_pieces[set].split(";");

    // Remove opacity for pieces we own
    for (var piece in tab_piece)
    {
      $set_new_element.find(".icon[data-stuff='" + tab_piece[piece] + "']").removeClass("lighten");
    }

    var is_set_condition_ok = checkCondition(set_condition, optHero, battle_condition_activated);
    
    $set_new_element.find(".effect-stat.effect2").each(function()
    {
      var $el          = $(this);
      var $set_element = $el.closest(".set-info");
      var label        = $el.data("label");
      var stat         = $el.data("stat");
      var val          = $el.data("value");
      var max          = $el.data("max") || "";
      var condition    = $el.data("condition") || "";
      
      if ( sets[set] >= 2 && condition != "" && checkCondition(condition, optHero, battle_condition_activated) && is_set_condition_ok )
      {
        if (max != "")
        {
          val = max;
        }

        if (sum[stat] == undefined) {sum[stat] = 0; }

        sumset[stat] = sumset[stat] + parseFloat(val) || parseFloat(val);
        $set_element.find(".effect.effect2").removeClass("lighten");
        $set_element.find(".condition").removeClass("text-red");
      }
      else if ( sets[set] >= 2 && condition == "" && is_set_condition_ok )
      {
        if (sum[stat] == undefined) {sum[stat] = 0; }

        sumset[stat] = sumset[stat] + parseFloat(val) || parseFloat(val);
        $set_element.find(".effect.effect2").removeClass("lighten");
        $set_element.find(".condition").removeClass("text-red");
      }
      else if (!is_set_condition_ok)
      {
        $set_element.find(".condition").addClass("text-red");
      }
    });

    $set_new_element.find(".effect-stat.effect3").each(function() 
    {
      var $el          = $(this);
      var $set_element = $el.closest(".set-info");
      var label        = $el.data("label");
      var stat         = $el.data("stat");
      var val          = $el.data("value");
      var max          = $el.data("max") || "";
      var condition    = $el.data("condition") || "";

      if ( sets[set] == 3 && condition != "" && checkCondition(condition, optHero, battle_condition_activated) && is_set_condition_ok)
      {
        if (max != "")
        {
          val = max;
        }

        if (sum[stat] == undefined) 
        {
          sum[stat] = 0; 
        }

        sumset[stat] = sumset[stat] + parseFloat(val) || parseFloat(val);
        $set_element.find(".effect.effect3").removeClass("lighten");
      }
      else if ( sets[set] == 3 && condition == "" && is_set_condition_ok )
      {
        if (sum[stat] == undefined) 
        {
          sum[stat] = 0; 
        }

        sumset[stat] = sumset[stat] + parseFloat(val) || parseFloat(val);
        $set_element.find(".effect.effect3").removeClass("lighten");
      }
    });
  }

  // Get Runes stats
  $("#runes:not(.disabled) .rune").each(function()
  {
    var $rune = $(this);

    $rune.find(".stat").each(function()
    {
      var $el = $(this);
      var stat = $el.data("stat");
      var val  = parseFloat( $el.data("val") ) || 0;

      sum[stat] = sum[stat] + val || val;
    });    
  });


  // @todo : Skills passive
  // @todo : TILES

  // duplicate sum table to add non-displayed stats
  var sum_not_displayed = sortByKey(arrayClone(sum));

  // Remove old additional stats
  removeAdditionalStatDisplayed();
  
  // Hide or display some columns
  checkStatsTableDisplay();

  // Display without Conversions
  var defpct = 0;
  $("#hero-stats #hero-stats-details #stats-table .stat:not(.stat-add)").each(function ()
  {
    var $el = $(this);
    var stat           = $el.data("stat");
    var multiplicative = $el.data("multiplicative") || 0;
    var base_stat      = $("#hero-" + tdf(stat)).val() || 0;
    var set_flat       = sumset[stat] || 0;
    var set_pct        = sumset[stat + "%"] || 0;
    var pre_flat       = sum[stat] || 0;
    var pre_pct        = sum[stat + "%"] || 0;
    var flat           = pre_flat + set_flat;
    var pct            = pre_pct + set_pct;

    if (stat == "criticaldamage")
    {
      base_stat = critdmg;
    }
    
    $el.find(".base").text( base_stat );
    $el.find(".flat").text( roundIfFloat(flat) );
    $el.find(".pct").text( roundIfFloat(pct) );
    $el.find(".conversion").text( "-" );

    if (multiplicative)
    {
      pretotal[stat] = parseInt(flat * (1 + (pct / 100)));
      total[stat]    = pretotal[stat];

      if (stat == "defense")
      {
        defpct  = defFormulaToPct(pretotal[stat]) + sum[stat + "!"] || defFormulaToPct(pretotal[stat]);
        defflat = defFormulaToFlat(defpct);

        // add the pure in the total table
        total[stat] = Math.round(defflat);

        $el.find(".total").text( Math.round(defflat) + " (" + roundIfFloat(defpct) + " %)" );
      }
      else if (stat == "dodge")
      {
        $el.find(".total").text( pretotal[stat] + " (" + roundIfFloat(dodgeFormulaToPct(pretotal[stat]) ) + " %)" );
      }
      else if (stat == "critrate" || stat == "hitrate" || stat == "mastery" || stat == "counterrate" || stat == "counterdamage")
      {
        $el.find(".total").text( pretotal[stat] + " (" + roundIfFloat(pretotal[stat] / 100) + " %)" );
      }
      else
      {
        $el.find(".total").text( pretotal[stat] );
      }
    }
    else
    {
      if ( stat == "criticaldamage" )  
      {
        total[stat] = base_stat + pct;
        $el.find(".total").text( roundIfFloat( total[stat] ) + " %" );
      }
      else if (stat == "movement" || stat == "mprecovery") 
      {
        $el.find(".total").text( flat );
      }
      else if (stat == "dot")
      {
        $el.find(".total").text( roundIfFloat( flat ) + " %");
      }
    } 

    if (sum_not_displayed[stat] != undefined)
    {
      delete sum_not_displayed[stat];
    }
    if (sum_not_displayed[stat + "%"] != undefined)
    {
      delete sum_not_displayed[stat + "%"];
    }
    if (sum_not_displayed[stat + "!"] != undefined)
    {
      delete sum_not_displayed[stat + "!"];
    } 
  });

  // Create rows and display for non-base stats
  for (var stat in sum_not_displayed)
  {
    // create DOM
    var $row = createAdditionalStatDisplay(stat);

    var set_total   = sumset[stat] || 0;
    var pre_total   = sum[stat] || 0;
    total[stat] = pre_total + set_total;

    $row.find(".base").text( "-" );
    $row.find(".flat").text( "-" );
    $row.find(".pct").text( "-" );
    $row.find(".conversion").text( "-" );
    $row.find(".total").text(roundIfFloat(total[stat]));
  }

  // identify available conversions
  var conv_list = new Array();

  for (var stat in sum)
  {
    if (stat.indexOf("by") != -1)
    {
      conv_list.push(stat);
    }
  }

  // this array will contain stat => value
  var conv = new Array();

  // Total def% is required for Def conversions as it is based on %val and not flat val
  // NEW : sets effect should not be included in conversions...
  // 2nd turn for conversions
  for (var index in conv_list)
  {
    var label    = conv_list[index];
    var tab      = label.split("by");
    var stat     = tab[0];
    var statfrom = tab[1];
    var val      = 0;

    var flat    = sum[statfrom];
    var pct     = sum[statfrom + "%"] || 0;
    var pure    = sum[statfrom + "!"] || 0;

    var preconv = parseInt(flat * (1 + ( (pct + pure) / 100)));

    if (statfrom == "defense")
    {
      defpct = defFormulaToPct( preconv );

      val = Math.round( (defpct * 100) * (sum[stat + "by" + statfrom] / 100) );
    }
    else
    {
      val = Math.round( preconv * (sum[stat + "by" + statfrom] / 100) );
    }

    conv[stat]  = conv[stat] + val || val; 

    total[stat] = pretotal[stat] + conv[stat];

    var el = $("#hero-stats #hero-stats-details #stats-table .stat[data-stat='" + tdf(stat) + "']");
    
    if (stat == "dodge")
    {
      $(el).find(".total").text( total[stat] + " (" + roundIfFloat(dodgeFormulaToPct(total[stat])) + " %)" );
    }
    else if (stat == "critrate" || stat == "hitrate" || stat == "mastery" || stat == "counterrate" || stat == "counterdamage")
    {
      $(el).find(".total").text( total[stat] + " (" + roundIfFloat(total[stat] / 100) + " %)" );
    }
    else
    {
      $(el).find(".total").text( total[stat] );
    }

    $(el).find(".conversion").text( conv[stat] );
  }

  // Formula

  // Atk * Atk%
  // + (Conversions calculated on the previous bases)
  // x Skill Damage%
  // x (Base Critical damage + Critical Damage)%
  // x (Enemy Def - ignoring dmg)%
  // + flat damage (+ pure damage a.k.a %HPdmg / ignoring def) 
  // x damage%
  // x (-dmg taken%)
  // - dmg taken
  // MASTERY

  // /*
  var damage = {
    attack                : total["attack"] || 0,
    mastery               : (total["mastery"] > 10000) ? 10000 : total["mastery"] || 0,
    skillAttack           : parseInt($("#group-atk #skill-attack").val()) / 100 || 0,
    criticalDamage        : total["criticaldamage"],
    ignoreDef             : parseInt($("#group-atk #ignore-def").val()) || 0,
    targetPctDefense      : parseFloat($("#group-atk #enemy-def").val()) || 0,
    targetPctDamageTaken  : $("#group-atk #enemy-damage-taken-pct").val() || 0,
    targetFlatDamageTaken : $("#group-atk #enemy-damage-taken-flat").val() || 0,
    flatDamage            : sum["damage"] || 0,
    pctDamage             : sum["damage%"] || 0,
    direction             : 0,
    advantage             : 0,
    terrain               : 0,
    minion                : 0,
    boss                  : 0,
    nephtys               : 0,
    getUnmitigatedDamage : function()
    {
      dmg  = this.attack * this.skillAttack;
      dmg *= (1 + (this.direction / 100));
      dmg *= (1 + (this.advantage / 100));
      dmg *= (1 + (this.terrain / 100));
      dmg *= (1 + (this.pctDamage / 100));
      dmg *= (1 + (this.minion / 100));
      dmg *= (1 + (this.boss / 100));
      dmg *= (1 + (this.nephtys / 100));

      return dmg;
    },
    getUnmitigatedCritDamage : function()
    {
      return this.getUnmitigatedDamage() * (1 + this.criticalDamage / 100);
    },
    getMitigatedDamage : function()
    {
      def = this.targetPctDefense;
      // @todo
      def = defFormulaToFlat(def) - this.ignoreDef;
      def = defFormulaToPct(def);
      // console.log(def);

      return this.getUnmitigatedDamage() * (1 - (def / 100) );
    },
    getMitigatedCritDamage : function()
    {
      def = this.targetPctDefense;
      def = defFormulaToFlat(def) - this.ignoreDef;
      def = defFormulaToPct(def);

      return this.getUnmitigatedCritDamage() * (1 - (def / 100) );
    },
    getPureDamage : function()
    {
      dmg = this.flatDamage;
      // @todo
      // dmg += skillDamageIgnoringDef;

      return dmg * (1 + (this.pctDamage / 100) );
    },
    getFinalDamage : function()
    {
      return ( this.getMitigatedDamage() + this.getPureDamage() ) * ( 1 - (this.targetPctDamageTaken / 100) ) - this.targetFlatDamageTaken;
    },
    getFinalCritDamage : function()
    {
      return ( this.getMitigatedCritDamage() + this.getPureDamage() ) * ( 1 - (this.targetPctDamageTaken / 100) ) - this.targetFlatDamageTaken;
    },
    getFinalDamageWithMastery : function()
    {
      return this.getFinalDamage() * ( 1 - (1 - this.mastery / 10000) );
    },
    getFinalCritDamageWithMastery : function()
    {
      return this.getFinalCritDamage() * ( 1 - (1 - this.mastery / 10000) );
    }

  };
  
      

  // var damage_pct  = sum["damage%"] / 100 || 0;
  // var enemydef  = $("#group-atk #enemy-def").val() || 0;
  // critdmg += sum["criticaldamage%"] || 0;
  // var ignoredef = $("#group-atk #ignore-def").val() || 0;
  // var afterdef  = unmitiged * (1 - (enemydef / 100)) + sum['damage'];
  // damage  += (minion + boss + nephtys + direction + advantage + terrain) / 100 || 0;
  // var unmitiged = total["attack"] * skilldamage * (1 + damage) || 0;
  
  if ( $("#group-atk #advantages .minion:checked").length )
  {
    damage.minion += sum["minion%"] || 0;
  }
  if ( $("#group-atk #advantages .boss:checked").length )
  {
    damage.boss += sum["boss%"] || 0;
  }
  if ( $("#group-atk #advantages .nephtys:checked").length )
  {
    damage.nephtys += sum["nephtys%"] || 0;
  }
  if ( $("#group-atk #advantages .direction:checked").length )
  {
    damage.direction += 15;
  }
  if ( $("#group-atk #advantages .advantage:checked").length )
  {
    damage.advantage += 30;
  }
  if ( $("#group-atk #advantages .opposite-advantage:checked").length )
  {
    damage.advantage -= 25;
  }
  if ( $("#group-atk #advantages .terrain20:checked").length )
  {
    damage.terrain += 20;
  }

  // Display Stats Groups
  $("#group-atk .damagepct").text( roundIfFloat(damage.pctDamage) );
  $("#group-atk .damageflat").text( damage.flatDamage );

  $("#group-atk .unmitiged").text( Math.round( damage.getUnmitigatedDamage() ) );
  $("#group-atk .unmitigedcrit").text( Math.round( damage.getUnmitigatedCritDamage() ) );

  $("#group-atk .puredamage").text( roundIfFloat( damage.getPureDamage() ) );

  $("#group-atk .total").text( Math.round( damage.getFinalDamage() ) );
  $("#group-atk .totalcrit").text( Math.round( damage.getFinalCritDamage() ) );

  if (damage.mastery < 10000)
  {
    $("#group-atk .totalmastery").text( Math.round( damage.getFinalDamageWithMastery() ) + " ~ " );
    $("#group-atk .totalmasterycrit").text( Math.round( damage.getFinalCritDamageWithMastery() ) + " ~ " );
  }
  else
  {  
    $("#group-atk .totalmastery").text("");
    $("#group-atk .totalmasterycrit").text("");  
  }
  // */

  if (data_compare.checkBuild())
  {
    data_compare.compareStats(total);
  }

  return total;
}

function toJSONpls()
{
  var exportObj = new Object;

  data = $("form#calc-form").serializeArray();
  
  $.each(data, function(key, field)
  {
    if (field.value != "")
    {
      exportObj[field.name] = field.value;
    }
  });

  exportObj.lordcostumes = new Array;
  $("#lord-costumes .costume.active").each(function()
  {
    var lid = $(this).data("id");
    exportObj.lordcostumes.push(lid);
  });

  exportObj.herocostumes = {};
  $("#hero-costumes .costume.active").each(function()
  {
    var hero = $(this).data("hero");
    var lid  = $(this).data("id");
    if (exportObj.herocostumes[hero] === undefined)
    {
      exportObj.herocostumes[hero] = new Array;
    }

    exportObj.herocostumes[hero].push(lid);
  });

  exportObj.herosoulgears = {};
  $("#hero-soul-gears .costume.active").each(function()
  {
    var hero = $(this).data("hero");
    var type = $(this).data("type");
    var num  = $(this).data("num");
    if (exportObj.herosoulgears[hero] === undefined)
    {
      exportObj.herosoulgears[hero] = new Array;
    }

    if (exportObj.herosoulgears[hero][num] === undefined)
    {
      exportObj.herosoulgears[hero][num] = new Array;
    }

    exportObj.herosoulgears[hero][num].push(type);
  });
  
  json = JSON.stringify(exportObj);

  return json;
}

function saveJSONpls()
{
  datajson = toJSONpls();

  $.ajax({
    method: "POST",
    url: "ajax.save-stats.php",
    data: { 
      "dataform" : datajson
    }
  })
  .done(function(data) 
  {
    var json = JSON.parse(data);

    if (json.success)
    {
      buildNotification("success", json.message, undefined , 1);
    }
    else
    {
      buildNotification("danger", json.message, undefined , 1);
    }
  });
}

function saveInSession()
{
  datajson = toJSONpls();

  $.ajax({
    method: "POST",
    url: "ajax.save-calc-session.php",
    data: { 
      "dataform" : datajson
    }
  });
}

function loadJSONpls(notif)
{
  notif = (notif == undefined || notif == "") ? 1 : notif;

  loader.init();

  $.ajax({
    method: "POST",
    url: "ajax.load-stats.php"
  })
  .done(function(data) 
  {
    var json = JSON.parse(data);

    if (json.success)
    {      
      loadCalcFromJSON(json.data);

      if (notif)
      {
        buildNotification("success", json.message, undefined , 1);
      }
    }
    else
    {
      if (notif)
      {
        buildNotification("danger", json.message, undefined , 1);
      }
    }
  });

  loader.stop();
}

function populateFields(json)
{
  $("#calc-form")[0].reset();
  $(".costume.active").removeClass("active");

  data = JSON.parse(json);
 
  for (var field in data)
  {
    if (field == "lordcostumes")
    {
      if (data[field].length != 0)
      {
        for (var costumeid in data[field])
        {
          $("#lord-costumes div[data-id='" + data[field][costumeid] + "']").addClass("active");
        }
      }
    }
    else if (field == "herocostumes")
    {
      if (data[field].length != 0)
      {
        for (var hero in data[field])
        {
          for (var costumeid in data[field][hero])
          {  
            $("#hero-costumes ." + hero + " div[data-id='" + data[field][hero][costumeid] + "']").addClass("active");
          }
        }
      }
    }
    else if (field == "herosoulgears")
    {
      if (data[field].length != 0)
      {
        for (var hero in data[field])
        {
          for (var soulgearnum in data[field][hero])
          {  
            for (var soulgeartype in data[field][hero][soulgearnum])
            {  
              $("#hero-soul-gears ." + hero + " div[data-type='" + data[field][hero][soulgearnum][soulgeartype] + "']").addClass("active");
            }
          }
        }
      }
    }
    else
    {
      $("input[name='" + field + "'], select[name='" + field + "']").val(data[field]);
    }
  }
}

function calculateEquipBonus(qty)
{
  var atk = new Array(0, 10, 20, 30, 45, 60, 75, 95, 115, 135, 160, 185, 210, 240, 270, 300);

  return atk[qty]; 
}

function checkCondition(condition, el_hero, condition_active)
{
  var val = true;

  if (condition != "")
  {
    // if condition contains ! you have to check opposite
    if (condition.indexOf("=!") != -1)
    {
      condition_part = condition.split("=!");

      val = ( $(el_hero).data(condition_part[0]) != condition_part[1] ) ? true : false;
    }
    else if (condition.indexOf("=") != -1)
    {
      condition_part = condition.split("=");

      val = ( $(el_hero).data(condition_part[0]) == condition_part[1] ) ? true : false;
    }
    else // if condition is IG and button is activated
    {
      val = ( condition == "IG" && condition_active ) ? true : false;
    } 
  }

  return val;
}

function removeAdditionalStatDisplayed()
{
  $("#hero-stats #hero-stats-details .stat-add").remove();
}

function createAdditionalStatDisplay(stat_name, multiplicative)
{
  multiplicative = (multiplicative == undefined || multiplicative == "") ? 0 : multiplicative;

  var $row = $("<tr>").addClass("stat stat-add");
  $row.data("stat", tdf(stat_name));
  $row.data("multiplicative", multiplicative);
  $row.append("<td>" + t(stat_name) + "</td>");
  $row.append("<td class='base' style='display:none;'></td>");
  $row.append("<td class='flat' style='display:none;'></td>");
  $row.append("<td class='pct' style='display:none;'></td>");
  $row.append("<td class='conversion' style='display:none;'></td>");
  $row.append("<td class='total'></td>");

  $("#hero-stats #stats-table tbody").append($row);

  return $row;
}

function checkStatsTableDisplay()
{
  var $table_stats      = $("#hero-stats-details #stats-table");
  var $table_comparison = $("#hero-stats-details #comparison");

  switch ($("#hero-stats-details .display-mode button.active").prop("id"))
  {
    case "stats-simple-overview":
      $table_stats.find(".base, .flat, .pct, .conversion").hide();
      $table_comparison.hide();
      $table_stats.show();
    break;
    case "stats-detailed-overview":
      $table_stats.find(".base, .flat, .pct, .conversion").show();
      $table_comparison.hide();
      $table_stats.show();
    break;
    case "stats-comparison":
      $table_comparison.show();
      $table_stats.hide();
    break;
  }
}

function roundIfFloat(num)
{
  if (!Number.isInteger(num))
  {
    num = num.toFixed(2);
  }

  return num;
}

function defFormulaToPct(x)
{
  // val = (x/(x+(6700-((3300-x)/19))))*100;

  // u/Raijinili
  //Def% = def/(def + 6200) * 95%

  val = 19 / 20 * x / (x + 6200) * 100;
  return val;
}

function dodgeFormulaToPct(x)
{
  val = 19 / 20 * x / (x + 7000) * 100;
  return val;
}

function defFormulaToFlat(x)
{
  val = -6200 * x / (x - 95);
  // u/ydobonrehtonatsuj
  // -6200 * def% / (def%-95)
  return val;
}

function tdf(val)
{
  val = val.toLowerCase();
  val = val.replace(/ /g, "");
  return val;
}

function arrayClone(o)
{
  var output, v, key;
  output = Array.isArray(o) ? [] : {};
  for (key in o) 
  {
     v = o[key];
     output[key] = (typeof v === "object") ? copy(v) : v;
  }
  return output;
}

function ucfirst(string) 
{
  return string.charAt(0).toUpperCase() + string.slice(1);
}

var timeout_list = [];
var loader = 
{
  init : function()
  {
    timeout = setTimeout(function() {$(".loader").fadeIn(300); }, 1000);
    timeout_list.push(timeout);
  },
  stop : function()
  {
    for (var i in timeout_list)
    {
      clearTimeout(timeout_list[i]);
    } 
    $(".loader").fadeOut(300);
  }
};

// sort obj by keys
function sortByKey(obj)
{
  var keys = Object.keys(obj);
  var len = keys.length;

  keys.sort();

  var newObj = {}

  for (i = 0; i < len; i++) 
  {
    k = keys[i];
    newObj[k] = obj[k];
  }

  return newObj;
}

function theater($element)
{
  var $overlay = $(".overlay");

  // Adapt overlay to full document height
  $overlay.css("height", $(document).outerHeight() + "px");

  // Event to close
  $overlay.on("click", function() {
    cancelTheater($element)
  } );

  // $element.on("click", function() {
  //   cancelTheater($element)
  // } );

  // Add class to put element on top
  $element.addClass("overlay-expose");

  // Micro wait and fade
  $overlay.fadeIn(300);
}

function cancelTheater($element)
{
  var $overlay = $(".overlay");

  $overlay.fadeOut(300);

  $overlay.off("click", function() {
    cancelTheater($element)
  });

  // $element.off("click", function() {
  //   cancelTheater($element)
  // });

  $element.removeClass("overlay-expose");
}