// fix for Jquery contains case sensivity
jQuery.expr[":"].contains = function(obj,index,meta) {
    return jQuery(obj).text().toUpperCase().indexOf(meta[3].toUpperCase()) >= 0;
};

$(document).ready(function() 
{
    // on load
    $('[data-toggle="tooltip"]').tooltip({container: 'body', trigger : 'hover'});

    $("body").on("keyup", "#sets-filter input", function()
    {
        filterSets();
    })
    .on("click", "#sets-filter button[data-role='filter']", function()
    {
        var $btn = $(this);

        // Filter buttons must act like radios
        if ($btn.hasClass("active"))
        {
            $btn.removeClass("active");
        }
        else
        {
            $(this).closest(".btn-group").find("button").removeClass("active");
            $btn.addClass("active");
        }

        filterSets();
    })
    .on("click", "#sets-filter button[data-role='reset']", function()
    {
        $("#sets-filter button[data-role='filter']").removeClass("active");
        $("#sets-filter input").val("");
        filterSets();
    })
    .on("click", ".set-info", function()
    {
        console.log( $(this).data("id") );
    });

    if ( $("#upcoming-sets").length )
    {
        setInterval( tickSecond, 1000 );
        tickSecond();

        $("body").on("click", "#upcoming-sets .set", function() {
            var set_name = $(this).find("img").attr("alt");
            $("#sets-filter input").val( set_name );
            filterSets();
        });
    }
});

function filterSets()
{
    var search          = $("#sets-filter input").val();
    var $setsList       = $("#sets-list");
    var $gradeContainer = $setsList.find(".sets-grade");
    var $sets           = $gradeContainer.find(".set-info");

    if ($setsList.find("#no-sets").length)
    {
        $setsList.find("#no-sets").remove();
    }

    $gradeContainer.show();
    $sets.show();

    if (search == "" && $("#sets-filter button.active").length == 0)
    {
        return;
    }

    var filterSum = "";
    $("#sets-filter button.active").each(function()
    {
        var type  = $(this).data("type");
        var stuff = $(this).data("stuff");
        filterSum += "[data-" + type + "='" + stuff + "']";
    });

    if (filterSum != "")
    {
        $setsList.find(".set-info:not(" + filterSum + ")").hide();
    }

    if (search != "")
    {
        $setsList.find(".set-info:visible").each(function()
        {
            if ( $(this).find('span.effect:contains("' + search + '")').length == 0 
                && $(this).find('span.title:contains("' + search + '")').length == 0
                && $(this).find('span.condition:contains("' + search + '")').length == 0 )
            {
                $(this).hide();
            }
        });
    }

    oneElement = false;
    // If no elements in grade : hide Grade
    $gradeContainer.each(function() 
    {
        if ($(this).find(".set-info:visible").length == 0)
        {
            $(this).hide();
        }
        else
        {
            oneElement = true;
        }
    });

    if (!oneElement)
    {
        var el = $("<div>").addClass("text-center alert alert-warning").attr("id", "no-sets").text("No sets found :(");
        $setsList.find(".sets-container").append(el);
    }
}

function tickSecond()
{
    var now = new Date().getTime();
    $(".next-set:visible").each(function() {
        var date     = new Date( $(this).find(".datetime").text() );
        var datetime = date.getTime();
        var diff     = datetime - now;
        var hours    = Math.floor((diff / (1000 * 60 * 60)));
        var minutes  = Math.floor((diff % (1000 * 60 * 60)) / (1000 * 60));
        var seconds  = Math.floor((diff % (1000 * 60)) / 1000);

        if (diff < 0) {
            $(this).fadeOut(3000);
            $(this).find(".eta").text( "Bye bye" );
        }
        else
        {
            $(this).find(".eta").text( dateFormatDisplay(date, hours, minutes, seconds) );
        }
    });
}

function dateFormatDisplay(d,h,m,s) 
{
    var monthNames = [
        "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
    ];

    if ( h > 23 )
    {
        return d.getDate() + " " + monthNames[ d.getMonth() ];
    }

    var h = h < 10 ? "0" + h : h;
    var m = m < 10 ? "0" + m : m;
    var s = s < 10 ? "0" + s : s;

    return h + ":" + m + ":" + s;
}