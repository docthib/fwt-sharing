// fix for Jquery contains case sensivity
jQuery.expr[":"].contains = function(obj,index,meta) {
    return jQuery(obj).text().toUpperCase().indexOf(meta[3].toUpperCase()) >= 0;
};

$(document).ready(function()
{
  // on load
  $('[data-toggle="tooltip"]').tooltip({container: 'body', trigger : 'hover'});

  $("body").on("keyup", "#heroes-filter input", function()
  {
    filterHeroes();
  })
  .on("click", "#heroes-filter button[data-role='filter']", function(e)
  {
      var $btn = $(this);

      // Filter buttons must act like radios
      if ($btn.hasClass("active"))
      {
          $btn.removeClass("active");
      }
      else
      {
          $(this).closest(".btn-group").find("button").removeClass("active");
          $btn.addClass("active");
      }

      filterHeroes();
  })
  .on("click", "#heroes-table tbody tr", function()
  {
    $(this).toggleClass("pinned");
  })
  .on("click", "#heroes-table thead th", function()
  {
    var $el      = $(this);
    var sortStat = $el.data("stat");
    var order    = "";

    if (!$el.hasClass("order-asc") && !$el.hasClass("order-desc"))
    {
      $el.addClass("order-asc");
      order = "asc";
    }
    else if ($el.hasClass("order-asc"))
    {
      $el.toggleClass("order-asc").toggleClass("order-desc");
      order = "desc";
    }
    else
    {
      $el.toggleClass("order-asc").toggleClass("order-desc");
      order = "asc";
    }

    var arrayRows    = [];
    $("#heroes-table tbody tr").each(function()
    {
      arrayRows.push($(this));
    });

    arrayRows.sort(function(a, b)
    {
      var elA = a.find("td[data-stat='" + sortStat + "']");
      var elB = b.find("td[data-stat='" + sortStat + "']");

      var valA = elA.text() ? elA.text() : elA.find("*:first").attr('title');
      var valB = elB.text() ? elB.text() : elB.find("*:first").attr('title');

      if (!isNaN(parseInt(valA)))
      {
        valA = parseInt(valA);
        valB = parseInt(valB);
      }

      if (order == "asc")
      {
        if (valA < valB) return -1;
        if (valA > valB) return 1;
      }
      else
      {
        if (valA < valB) return 1;
        if (valA > valB) return -1;
      }

      return 0;
    });

    $("#heroes-table tbody").empty();
    for (var index in arrayRows)
    {
      $("#heroes-table tbody").append(arrayRows[index]);
    }

    arrayRows = null;
  });

  function filterHeroes()
  {
    var $filters    = $("#heroes-filter");
    var text        = $filters.find("input").val();
    var $heroesList = $("#heroes-table tbody tr");

    $heroesList.show();

    if (text == "" && $filters.find("button.active").length == 0)
    {
      return;
    }

    var filterSum = "";
    $filters.find("button.active").each(function()
    {
        var type  = $(this).data("type");
        var stuff = $(this).data("stuff");
        filterSum += "td[data-stat='" + type + "']:not([data-" + type + "*='" + stuff + "']),";
    });

    if (filterSum != "")
    {
        filterSum = filterSum.substr(0, filterSum.length - 1);
        $heroesList.find(filterSum).closest("tr:not(.pinned)").hide();
    }

    if (text != "")
    {
      $heroesList.find('td[data-stat="name"]:not(:contains("' + text + '"))').closest("tr:not(.pinned)").hide();
    }
  }
});