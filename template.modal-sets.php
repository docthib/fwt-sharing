<div id="modal-sets" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Sets</h4>
			</div>
			<div class="modal-body">
				<div id='sets-list'>
					<div class="sets-container">
						<div id="sets-filter" class="btn-toolbar">
							<input type="text" class="form-control" placeholder="Filter here" />
							<?php 
							/* filter part */

							foreach ($equip_details as $type => $stuff_list) 
							{
								echo "<div class='btn-group' role='group'>";

								foreach ($stuff_list as $stuff) 
								{
									echo "<button class='btn btn-default' data-type='" . tdf($type) . "' data-stuff='" . tdf($stuff) . "' title='" . $stuff . "' data-role='filter' data-toggle='tooltip' data-placement='top'><img src='" . ICONS_DIRECTORY . $stuff . ".png' class='icon' /></button>";
								}

								echo "</div>";
							}
							?>

							<div class='btn-group btn-radio' role='group'>
								<button class='btn btn-default' data-type='rank' data-stuff='b' data-role='filter'><img src="<?php echo ICONS_DIRECTORY; ?>rank-b.png" style="width:20px;" alt="B"/></button>
								<button class='btn btn-default' data-type='rank' data-stuff='a' data-role='filter'><img src="<?php echo ICONS_DIRECTORY; ?>rank-a.png" style="width:20px;" alt="A"/></button>
								<button class='btn btn-default' data-type='rank' data-stuff='s' data-role='filter'><img src="<?php echo ICONS_DIRECTORY; ?>rank-s.png" style="width:20px;" alt="S"/></button>
								<button class='btn btn-default' data-type='rank' data-stuff='ss' data-role='filter'><img src="<?php echo ICONS_DIRECTORY; ?>rank-ss.png" style="width:20px;" alt="SS"/></button>
							</div>

							<div class='btn-group btn-radio' role='group'>
							<?php
								foreach ($equip_slots as $num => $slot)
								{
									echo "<button class='slot btn btn-default' data-type='grade' data-stuff='" . $num . "' data-role='filter'>" . $slot . "</button>";
								}
							?>
							</div>

							<div class='btn-group btn-radio' role='group'>
								<button class='btn btn-default' data-role='filter' data-type='tags' data-stuff='new' title="New sets" data-toggle='tooltip' data-placement='top'>New</button>
							</div>

							<button class='btn btn-default' data-role='reset' title="Reset filters" data-toggle='tooltip' data-placement='top'><span class="glyphicon glyphicon-remove"></span></button>
						</div>

						<?php renderSets(); ?>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default btn-close" data-dismiss="modal">Close</button>
			</div>
		</div>

	</div>
</div>