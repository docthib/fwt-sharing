<div id="hero-costumes">
	<legend>Hero Costumes</legend>

<?php
	foreach ($hero_costumes as $hero => $costume_list)
	{
		echo "<div class='" . tdf($hero) . "' style='display:none;'>";
		foreach ($costume_list as $key => $costume)
		{
			// Préparation des données de stats
			$data_stats = "";
			$aff_stats  = "";
			foreach ($costume["stats"] as $stat => $value) 
			{
				$data_stats.= "data-" . tdf($stat) . "='" . $value . "' ";
				$aff_stats .= $stat . " : " . $value . "<br />";
			}

			echo "<div class='costume' data-hero='" . tdf($hero) . "' data-id='" . $key . "' title=\"" . $costume["name"] . "\" data-toggle='tooltip' data-placement='top'>";
				echo "<img class='pull-left' src='" . HERO_COSTUMES_DIRECTORY . $costume["img"] . "' alt=\"" . $costume["name"] . "\">";
				echo "<span class='stats' " . $data_stats . ">" . $aff_stats . "</span>";
			echo "</div>";
		}
		echo "</div>";
	}
?>

</div>