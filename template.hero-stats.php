<div id="hero-stats" class="form-horizontal">

	<!-- <ul class="nav nav-tabs">
	  <li class="active"><a data-toggle="tab" href="#hero-stats-details">Hero stats</a></li>
	  <li><a data-toggle="tab" href="#hero-stats-damage">Damage</a></li>
	</ul> -->

	<div class="tab-content">
		<div id="hero-stats-details" class="tab-pane fade in active">
			<div class="btn-group display-mode">
				<button id="stats-simple-overview" class="btn btn-default active" title="Simple overview" data-toggle='tooltip' data-placement='bottom'><span class="glyphicon glyphicon-th-large"></span></button>
				<button id="stats-detailed-overview" class="btn btn-default" title="Detailed overview" data-toggle='tooltip' data-placement='bottom'><span class="glyphicon glyphicon-th-list"></span></button>
				<button id="stats-comparison" class="btn btn-default" title="Stats comparison" data-toggle='tooltip' data-placement='bottom'><span class="glyphicon glyphicon-transfer"></span></button>
			</div>

			<div class="table-responsive">
				<table id="stats-table" class="table table-striped">
					<thead>
						<tr>
							<th>Stats</th>
							<th class="base" style="display: none;">Hero</th>
							<th class="flat" style="display: none;">Flat</th>
							<th class="pct" style="display: none;">%</th>
							<th class="conversion" style="display: none;">Conv.</th>
							<th class="total">Total</th>
						</tr>
					</thead>
					<tbody>
						<?php
							foreach ($stats_list as $stat => $setting)
							{
								echo "<tr class='stat' data-stat='" . tdf($stat) . "' data-multiplicative='" . $setting . "'>";
									echo "<td><input type='hidden' id='hero-" . tdf($stat) . "' name='" . tdf($stat) . "' />" . $stat . "</td>";
									echo "<td class='base' style='display: none;'></td>";
									echo "<td class='flat' style='display: none;'></td>";
									echo "<td class='pct' style='display: none;'></td>";
									echo "<td class='conversion' style='display: none;'></td>";
									echo "<td class='total'></td>";
								echo "</tr>";
							}
						?>
					</tbody>
				</table>

				<div id="comparison" style="display:none;padding-top:15px;">
					<div class="btn-group">
						<button id="build-save" class="btn btn-default" title="Store build" data-toggle="tooltip" data-placement="bottom"><span class="glyphicon glyphicon-floppy-disk"></span></button>
						<button id="build-restore" class="btn btn-default disabled" title="Restore build" data-toggle="tooltip" data-placement="bottom"><span class="glyphicon glyphicon-floppy-save"></span></button>
						<button id="build-delete" class="btn btn-default disabled" title="Delete save" data-toggle="tooltip" data-placement="bottom"><span class="glyphicon glyphicon-floppy-remove"></span></button>
					</div>

					<table id="comparison-table" class="table table-striped">
						<thead>
							<tr>
								<th>Stats</th>
								<th class="total">Before</th>
								<th class="total">After</th>
							</tr>
						</thead>
						<tbody>
								<td colspan="3">
									1. Use save build button => Column before<br />
									2. Any further change => Column after<br />
									easy ^^
								</td>
						</tbody>
					</table>
				</div>
			</div>
		</div>

		<div id="hero-stats-damage" class="">
			<div id="group-atk" class="stat-group">
				<div id="advantages">
					<label for="minion"><input class="minion" type="checkbox"  id="minion"> isMinion</label>
					<label for="boss"><input class="boss" type="checkbox"  id="boss"> isBoss</label>
					<label for="nephtys"><input class="nephtys" type="checkbox"  id="nephtys"> inNephtys</label>
					<label for="direction"><input class="direction" type="checkbox"  id="direction"> Direction 15%</label>
					<label for="advantage"><input class="advantage" type="checkbox"  id="advantage"> Advantage 30%</label>
					<label for="opposite-advantage"><input class="opposite-advantage" type="checkbox" id="opposite-advantage"> Opposite Advantage -25%</label>
					<label for="terrain20"><input class="terrain20" type="checkbox"  id="terrain20"> Terrain 20%</label>
				</div>

				<div class="table-responsive">
					<table id="damage-block" class="table table-striped">
						<thead>
							<tr>
								<th colspan="2">Damage stats</th>
							</tr>
						</thead>
						<tbody>
							<!-- <tr>
								<td>Damage modifiers</td>
								<td>
									<span class="damagepct">0</span>%  (+<span class="damageflat">0</span>)
								</td>
							</tr> -->
							<tr>
								<td>Skill attack %</td>
								<td>
									<input type="number" step="1" id="skill-attack" class="form-control" max="2000" value="100">
								</td>
							</tr>
							<!-- <tr>
								<td>Unmitiged (crit)</td>
								<td>
									<span class="unmitiged">0</span><br />(<span class="unmitigedcrit">0</span>)
								</td>
							</tr>
							<tr>
								<td>Skill ignore Def</td>
								<td>
									<input type="number" step="1" id="ignore-def" name="ignore-def" class="form-control" max="1000000" value="0">
								</td>
							</tr> -->
							<tr>
								<td>Enemy Def %</td>
								<td>
									<input type="number" step="0.1" id="enemy-def" class="form-control" max="100" value="70">
								</td>
							</tr>
							<!-- <tr>
								<td>Pure Damage</td>
								<td>
									<span class="puredamage">0</span>
								</td>
							</tr>
							<tr>
								<td>Enemy Damage Taken %</td>
								<td>
									<input type="number" step="0.1" id="enemy-damage-taken-pct" class="form-control" max="100" value="0">
								</td>
							</tr>
							<tr>
								<td>Enemy Flat Damage Taken</td>
								<td>
									<input type="number" step="1" id="enemy-damage-taken-flat" class="form-control" max="10000" value="0">
								</td>
							</tr> -->
							<tr>
								<td>Total</td>
								<td>
									<span class="totalmastery">0 ~ </span><span class="total">0</span><br />(<span class="totalmasterycrit">0 ~ </span><span class="totalcrit">0</span>)
								</td>
							</tr>
						</tbody>
					</table>
				</div>

				<!-- <div class="bs-callout bs-callout-warning">
					<h4>Tool Damage formula</h4>
					<p>
						<code>Unmitigated damage</code> = <var>Total Attack</var> * <var>Skill Attack%</var> * <var>Advantage (if)</var> * <var>Direction (if)</var> * <var>Terrain (if)</var> * <var>Damage%</var><br />
						<code>Critical damage</code> =  <var>Unmitigated damage</var> * <var>Critical Damage% (if crit)</var><br />
						<code>Mitigated damage</code> = <var>Critical damage</var> * <var>Defense%</var><br />
						<code>Pure damage</code> = [<var>Damage ignoring def (through skill)</var> + <var>Flat Damage</var> (+= <var>Final Skill Power</var>)] * <var>Damage%</var><br />
						<code>Final damage</code> = [<var>Mitigated damage</var> + <var>Pure damage</var>] * <var>Enemy -Damage Taken%</var> - <var>Enemy Flat -Damage Taken</var><br />
						<code>Damage Range (Mastery)</code> : Min = <var>Final damage</var> * <var>Mastery%</var> ~ Max = <var>Final Damage</var>
					</p>

					<p>There are many guides on these formulas, thanks to players who tested it (<a href="https://www.reddit.com/r/FantasyWarTactics/comments/3zh1pw/damage_mechanics_explained/" target="_blank">#1 Damage Mechanics</a>, <a href="https://www.reddit.com/r/FantasyWarTactics/comments/4amugw/regarding_damage_taken/" target="_blank">#2 Regarding -%Damage Taken</a>, <a href="https://www.reddit.com/r/FantasyWarTactics/comments/44q1ed/technical_strategic_advantages_pure_damage/" target="_blank">#3 Strategic Advantages &amp; Pure Damage</a>).</p>
					<p><b>NOTE :</b> However shadow remains on </p>
					
					<ul>
						<li>how Nephtys/Boss/Minion% is applied (multiplicative or additive to Damage%?) - that's why it is currently <i>DISABLED</i> in the tool</li>
						<li>if -Flat Damage Taken is applied after -Damage Taken% as many suppose</li>
						<li>when Slime King effect should be applied ? (Decrease the attack of all Attack-type enemies by 25%)</li>
						<li>when Difference between You and Me (Critical damage from enemy attacks -20%)</li>
					</ul>

					<p>If anyone has answers, I would be glad to update the tool with formula and link their guide ;)</p>
				</div> -->
				
			</div>
		</div>
	</div> 
</div>