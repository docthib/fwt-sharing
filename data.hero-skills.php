<?php
session_start();

define("PATH_INFO", "");

include_once "include.functions.php";
include_once "include.maintenance-check.php";

$hero_skills = array(
	"Alex" => array(
		"num"           => "1",
		"mp cost"       => "15",
		"cd"            => null,
		"range pattern" => null,
		"aoe pattern"   => null,
		"max range"     => "2",
		"max aoe"       => "2",
		"description"   => "A spear thrust that inflicts damage at 139.8% of Attack. Instantly increases Attack by 8% for every space traveled for 1 turn.",
		"tags"          => array("Increases Attack"),
		"attack power"  => 139.8,
		"effects"       => array(
			"description" => "Instantly increases Attack by 8% for every space traveled for 1 turn",
			"target"      => "self",
			"type"        => "buff", // debuff, damage
			"stat"        => null,
			"duration"    => null,
			"delay"       => null,
			"chance"      => null, 
		),
	),
);

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<!-- common head -->
	<?php include "template.head.php"; ?>

	<!-- custom css -->
	<link rel="stylesheet" href="/assets/css/calculator.css?<?php echo filemtime('assets/css/calculator.css'); ?>">
	<link rel="stylesheet" href="/assets/css/fluff.css?<?php echo filemtime('assets/css/fluff.css'); ?>"> 

	<title>FWT Sets</title>
</head>
<body>
	<div class="loader" style="display:none;"><img src="<?php echo ICONS_DIRECTORY; ?>black-caps.png" class="ncaps roll"></div>
	<header>
		<?php include "template.header-belt.php"; ?>
	</header>
	<div class="container-fluid" style="position:relative;overflow:hidden;">


<style>
	.pattern {background-color: #656872; display: inline-block;padding:2px;}
	.row-skill { clear:both; line-height: 0; }
	.col-skill { background-color: #abadb1; content:""; float:left; margin:1px; cursor: pointer; }
	.self { background-color: #6be9a5; border-radius: 50%; }
	.target { background-color: #f9636a; border-radius: 50%; }
	.pattern-range .selected { background-color: #ffff63; }
	.pattern-aoe .selected { background-color: #85b3ff; }

	.col-skill, .self, .target { width:15px; height: 15px; }
</style>

	<div class="skill">
		<!-- <div class="pattern pattern-range">
		</div>

		<div class="pattern pattern-aoe">
		</div> -->
	</div>

	<input type="text" id="pattern_name">
	<button id="save">Save</button>
</div>
</div>
<footer>
	<!-- common postload -->
	<?php include "template.postload.php"; ?>
	<script src="/assets/js/sets.min.js?<?php echo filemtime('assets/js/sets.min.js'); ?>"></script>  
	<script>
		$(document).ready(function() {
			skillFactory.init( $(".skill") );
			skillFactory.loadPattern();
		});

		$("body").on("click", ".col-skill", function() {
			$(this).toggleClass("selected");
		})
		.on("click", "#save", function() 
		{
			var i = 0;
			var r_range = [];
			$(".pattern-range .col-skill").each(function() 
			{
				i++;
				if ( $(this).hasClass("selected") )
				{
					r_range.push(i);
				}
			});

			var i = 0;
			var r_aoe = [];
			$(".pattern-aoe .col-skill").each(function() 
			{
				i++;
				if ( $(this).hasClass("selected") )
				{
					r_aoe.push(i);
				}
			});

			console.log(r_range);
			console.log(r_aoe);
		});

		var skillFactory = {
			init                : false,
			nb_rows              : 11,
			nb_cols              : 11,
			container           : false,
			container_class      : { 
				"range" : "pattern pattern-range",
				"aoe"   : "pattern pattern-aoe"
			},
			el_range       : false,
			el_aoe         : false,
			row_class      : "row-skill",
			col_class      : "col-skill",
			self_class     : "self",
			target_class   : "target",
			selected_class : "selected",
			
			loadPattern : function( range, aoe ) 
			{
				this.buildPattern('range', range);
				this.buildPattern('aoe', aoe);
			},
			resetPattern : function ( name ) 
			{
				if ( name != "range" && name != "aoe" )
				{
					alert("Invalid reset name");
					return false;
				}

				this['el_' + name].find( this.col_class ).removeClass( this.selected_class );
				return true;
			}
			buildPattern : function ( options, data )
			{
				var optionsList = [
					"name"
				]

				if ( options == undefined )
				{
					alert("options undefined");
					return false;
				}

				if ( options.name != "range" && options.name != "aoe" )
				{
					alert("option name");
					return false;
				}

				if ( data == undefined )
				{
					data = {};
					data.selection = [];
					data.self      = true;
					data.target    = false;
				}

				var $container = $("<div>").addClass( this.container_class[name] );

				var midRow = Math.ceil( this.nb_rows / 2 );
				var midCol = Math.ceil( this.nb_cols / 2 );

				for ( var row = 1 ; row <= this.nb_rows ; row++ )
				{
					var $row = $("<div>").addClass( this.row_class );

					for ( var col = 1 ; col <= this.nb_cols ; col++ ) 
					{
						var $col = $("<div>").addClass( this.col_class );

						if ( row == midRow && col == midCol )
						{
							if ( data.target )
							{
								$col.append( $("<div>").addClass( this.target_class ) );
							}
							else
							{
								$col.append( $("<div>").addClass( this.self_class ) );
							}
						}

						if ( data.selection.indexOf( col * row ) != -1 )
						{
							$col.addClass( this.selected_class );
						}

						$row.append( $col );
					}

					$container.append( $row );
				}

				this.container.append( $container );
			},
			init : function( container ) 
			{
				if ( container == undefined )
				{
					alert("error");
					return false;
				}

				this.container = container;
				this.init      = true;
			},
		};
	</script>
</footer>
</body>
</html>