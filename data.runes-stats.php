<?php

$rune_slot = array(
	"Left",
	"Mid",
	"Right" 
);

$rune_type = array(
	"Attack",
	"Defense",
	"HP",
);

$rune_grade = array(
	1,
	2,
	3,
	4,
);

$rune_potentials = array(
	"Atk" => array(
		"Attack"           => 0,
		"Attack%"          => 0,
		"Critical Damage%" => 0,
		"Crit Rate"        => 0,
		"Crit Rate%"       => 0,
		"Counter Damage"   => 0,
		"Counter Damage%"  => 0,
		"Damage"           => 0,
		"Damage%"          => 0,
		"Hit Rate"         => 0,
		"Hit Rate%"        => 0,
	),
	"Def" => array(
		"Counter Rate"  => 0,
		"Counter Rate%" => 0,
		"Damage Taken"  => 0,
		"Damage Taken%" => 0,
		"Defense"       => 0,
		"Defense%"      => 0,
		"Dodge"         => 0,
		"Dodge%"        => 0,
		"HP"            => 0,
		"HP%"           => 0,
		"HP Recovery"   => 0,
		"HP Recovery%"  => 0,
	),
	"HP" => array(
		"Attack%"                        => 5,
		"Attack by MP"                   => 0,
		"Attack by HP"                   => 0,
		"Boss%"                          => 0,
		"Counter Damage by Counter Rate" => 0,
		"Counter Damage%"                => 3,
		"Counter Rate by Hit Rate"       => 0,
		"Counter Rate%"                  => 3,
		"Critical Damage%"               => 2,
		"Crit Rate by Defense"           => 0,
		"Crit Rate%"                     => 2,
		"Damage Taken%"                  => 4,
		"Defense%"                       => 5,
		"Dodge by Defense"               => 0,
		"Dodge%"                         => 4,
		"DoT"                            => 0,
		"HP%"                            => 1,
		"HP Recovery%"                   => 5,
		"Hit Rate by Crit Rate"          => 0,
		"Hit Rate%"                      => 1,
		"Minion%"                        => 0,
		"MP"                             => 0,
		"MP Recovery"                    => 0,
		"Nephtys%"                       => 0,
	)
);


$rune_potential_val = array(
	"attack"          => (125/3),
	"attack%"         => 2,
	"boss%"           => (10/3),
	"counterdamage"   => (800/3),
	"counterdamage%"  => (10/3),
	"counterrate"     => (200/3),
	"counterrate%"    => (10/3),
	"criticaldamage%" => 4,
	"critrate"        => (200/3),
	"critrate%"       => (10/3),
	"damage"          => (100/3),
	"damage%"         => (8/3),
	"damagetaken"     => (100/3),
	"damagetaken%"    => 1,
	"defense"         => (200/3),
	"defense%"        => (10/3),
	"dodge"           => 50,
	"dodge%"          => (10/3),
	"dot"             => (70/3),
	"hitrate"         => (200/3),
	"hitrate%"        => (10/3),
	"hp"              => (400/3),
	"hp%"             => 2,
	"hprecovery"      => 100,
	"hprecovery%"     => 0.5,
	"minion%"         => (10/3),
	"mp"              => (10/3),
	"mprecovery"      => 2,
	"nephtys%"        => 4
);

$rune_potential_rarity_color = array(
	9 => "normal",
	8 => "rare",
	7 => "rare",
	6 => "epic",
	5 => "epic",
	4 => "unique",
	3 => "unique",
	2 => "unique",
	1 => "unique",
	0 => "legendary"
);


$rune[1]["attack"]["left"]["Attack"]    = 1990;
$rune[1]["attack"]["mid"]["Attack"]     = 2189;
$rune[1]["attack"]["right"]["Attack"]   = 1990;
$rune[1]["defense"]["left"]["Defense"]  = 567;
$rune[1]["defense"]["mid"]["Defense"]   = 567;
$rune[1]["defense"]["right"]["Defense"] = 624;
$rune[1]["hp"]["left"]["HP"]            = 2819;
$rune[1]["hp"]["mid"]["HP"]             = 2562;
$rune[1]["hp"]["right"]["HP"]           = 2562;
$rune[2]["attack"]["left"]["Attack"]    = 2487;
$rune[2]["attack"]["mid"]["Attack"]     = 2735;
$rune[2]["attack"]["right"]["Attack"]   = 2487;
$rune[2]["defense"]["left"]["Defense"]  = 707;
$rune[2]["defense"]["mid"]["Defense"]   = 707;
$rune[2]["defense"]["right"]["Defense"] = 778;
$rune[2]["hp"]["left"]["HP"]            = 4761;
$rune[2]["hp"]["mid"]["HP"]             = 4328;
$rune[2]["hp"]["right"]["HP"]           = 4328;
$rune[3]["attack"]["left"]["Attack"]    = 3109;
$rune[3]["attack"]["mid"]["Attack"]     = 3420;
$rune[3]["attack"]["right"]["Attack"]   = 3109;
$rune[3]["defense"]["left"]["Defense"]  = 861;
$rune[3]["defense"]["mid"]["Defense"]   = 861;
$rune[3]["defense"]["right"]["Defense"] = 948;
$rune[3]["hp"]["left"]["HP"]            = 7213;
$rune[3]["hp"]["mid"]["HP"]             = 6556;
$rune[3]["hp"]["right"]["HP"]           = 6556;
$rune[4]["attack"]["left"]["Attack"]    = 3886;
$rune[4]["attack"]["mid"]["Attack"]     = 4274;
$rune[4]["attack"]["right"]["Attack"]   = 3886;
$rune[4]["defense"]["left"]["Defense"]  = 1032;
$rune[4]["defense"]["mid"]["Defense"]   = 1032;
$rune[4]["defense"]["right"]["Defense"] = 1136;
$rune[4]["hp"]["left"]["HP"]            = 10368;
$rune[4]["hp"]["mid"]["HP"]             = 9425;
$rune[4]["hp"]["right"]["HP"]           = 9425;

if (isset($_POST["action"]) && $_POST["action"] == "getStats")
{
	global $rune;

	$grade = $_POST["grade"];
	$type  = $_POST["type"];
	$slot  = $_POST["slot"];

	if ( !isset( $rune[$grade][$type][$slot] ) )
	{
		$json['success'] = FALSE; 
		echo json_encode($json);exit;
	}

	foreach ($rune[$grade][$type][$slot] as $stat => $value) 
	{
		$kuku[] = [$stat => $value];
	}

	$json['stat']    = $kuku;
	$json['success'] = TRUE; 

	echo json_encode($json);exit;
}