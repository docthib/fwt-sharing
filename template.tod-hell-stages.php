<ul class="stage-select-group">
<?php
    for ($i = STAGE_MINI ; $i <= STAGE_MAXI ; $i++)
    {
        $active = ($stage == $i) ? "active" : "";
        echo "<li><a href='/tod/" . $i . "' class='btn btn-default " . $active . "'>" . $i . "</a></li>";
    }
?>
</ul>