<div id="upcoming-sets" class="container-fluid">
    <div class="title">
        Upcoming sets
    </div>

    <div class="flex">
        <!-- <div class="no-set">
            <div class="date"><span class="eta">Add set</span></div>
                <div class="sets">
                    <div class="set">
                        <div class="image">
                            <img src="<?php echo SETS_IMAGE_DIRECTORY; ?>add-new-set.png" alt="pick set" title="pick set" data-toggle="tooltip" data-placement="bottom">
                        </div>
                    </div>
                    <div class="set">
                        <div class="image">
                            <img src="<?php echo SETS_IMAGE_DIRECTORY; ?>add-new-set.png" alt="pick set" title="pick set" data-toggle="tooltip" data-placement="bottom">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div> -->
    <?php 
    if ( !$upcoming_sets )
    { 
    ?>
        <div class="no-set">Nothing atm :(</div>
    <?php
    }
    else
    {
        foreach ($upcoming_sets as $next_date) 
        { 
            $diff = $next_date['end_date']->diff( new DateTime() );

            ?>
            <div class="next-set">
                <span class="datetime hidden"><?php echo $next_date['end_date']->format('Y-m-d H:i:s'); ?></span>
                <div class="date"><span class="eta"></span></div>
                <div class="sets">
                <?php foreach ( $next_date['sets'] as $set_id ) { 
                    $set     = $sets[ searchForId( $set_id, $sets ) ];
                    $picture = $set['weapon_image'] != "" ? $set['rank'] . "/" . $set['weapon_image'] : DEFAULT_SET_IMAGE;
                ?>
                    <div class="set set-<?php echo $set['rank']; ?>" target="<?php echo $set['id']; ?>">
                        <div class="image">
                            <img src="<?php echo SETS_IMAGE_DIRECTORY . $picture; ?>" alt="<?php echo $set['title']; ?>" title="<?php echo $set['title']; ?>" data-toggle="tooltip" data-placement="bottom">
                        </div>
                    </div>
                <?php } ?>
                    <div class="clearfix"></div>
                </div>
            </div>
        <?php } 
    } ?>

    </div>
</div>